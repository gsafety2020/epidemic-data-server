create table LOCATION_TOKEN
(
   AREA_CODE            varchar(32) not null comment '地区编码',
   TOKEN                varchar(32) not null comment '访问口令',
   TOKEN_STATUS         char(1) not null default '0' comment '凭证是否可用，1-是 0-否',
   UPDATE_TIME          datetime not null comment '更新时间',
   primary key (AREA_CODE)
);

alter table LOCATION_TOKEN comment '地区登录凭证';
