-- --------------------------------------------------------
-- 主机:                           rm-2ze6a0cvu6skgg8f11o.mysql.rds.aliyuncs.com
-- 服务器版本:                        8.0.16 - Source distribution
-- 服务器操作系统:                      Linux
-- HeidiSQL 版本:                  9.3.0.4984
-- -------------------------------------------------------- 
use virus;

-- 导出  表 virus.city_image_carousel 结构
DROP TABLE IF EXISTS `city_image_carousel`;
CREATE TABLE IF NOT EXISTS `city_image_carousel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num` int(11) NOT NULL DEFAULT '0' COMMENT '序号，从小到大排序',
  `area_code` varchar(50) DEFAULT NULL COMMENT '城市编码',
  `description` varchar(100) DEFAULT NULL COMMENT '描述',
  `url` varchar(250) DEFAULT NULL COMMENT '图片链接',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='图片轮播';

-- 正在导出表  virus.city_image_carousel 的数据：~0 rows (大约)
DELETE FROM `city_image_carousel`;
/*!40000 ALTER TABLE `city_image_carousel` DISABLE KEYS */;
INSERT INTO `city_image_carousel` (`id`, `num`, `area_code`, `description`, `url`) VALUES
	(1, 1, '440600', '', 'http://fs-omc.com:18080/shenzhen/1.jpg'),
	(2, 2, '440600', '', 'http://fs-omc.com:18080/shenzhen/2.png'),
	(3, 3, '440600', '', 'http://fs-omc.com:18080/shenzhen/3.png'),
	(5, 3, '440600', '', 'http://fs-omc.com:18080/shenzhen/3.jpg'),
	(4, 2, '440600', '', 'http://fs-omc.com:18080/shenzhen/2.jpg');
/*!40000 ALTER TABLE `city_image_carousel` ENABLE KEYS */;


-- 导出  表 virus.city_material 结构
DROP TABLE IF EXISTS `city_material`;
CREATE TABLE IF NOT EXISTS `city_material` (
  `material_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `area_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '地区ID',
  `area_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '地区名称',
  `material_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '物资名称',
  `unit_name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '单位',
  `storage_num` decimal(9,2) DEFAULT NULL COMMENT '存储数量',
  `can_num` decimal(9,2) DEFAULT NULL COMMENT '可用数量',
  `lock_num` decimal(9,2) DEFAULT NULL COMMENT '锁定数量',
  `total` decimal(9,2) DEFAULT NULL COMMENT '物资累计数',
  `org_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '所属部门',
  `org_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '机构code',
  `warehouse` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '仓库名称',
  `warehouse_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '仓库code',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '地址',
  `link_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '联系人',
  `mobile` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '联系电话',
  `status_cd` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '状态(1新建;5确定)',
  `longitude` decimal(9,6) DEFAULT NULL COMMENT '维度',
  `latitude` decimal(9,6) DEFAULT NULL COMMENT '精度',
  `city_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '所属市级区域编码',
  `city_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '所属市级区域名称',
  `province_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '所属省级区域编码',
  `province_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '所属省级区域名称',
  `parent_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '上级区域',
  `parent_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '上级区域',
  `in_time` datetime DEFAULT NULL COMMENT '入口时间',
  `create_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '修改人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `seq_num` int(11) DEFAULT NULL,
  PRIMARY KEY (`material_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='地区物资与资源表';

-- 正在导出表  virus.city_material 的数据：~0 rows (大约)
DELETE FROM `city_material`;
/*!40000 ALTER TABLE `city_material` DISABLE KEYS */;
INSERT INTO `city_material` (`material_id`, `area_no`, `area_name`, `material_name`, `unit_name`, `storage_num`, `can_num`, `lock_num`, `total`, `org_name`, `org_code`, `warehouse`, `warehouse_code`, `address`, `link_name`, `mobile`, `status_cd`, `longitude`, `latitude`, `city_no`, `city_name`, `province_no`, `province_name`, `parent_no`, `parent_name`, `in_time`, `create_by`, `update_by`, `create_time`, `update_time`, `seq_num`) VALUES
	('1', NULL, NULL, '医用防护口罩（N95）', '（个）', 0.00, 0.00, 0.00, 18252.00, '市物资保障组', ' ', ' ', ' ', ' ', NULL, NULL, '5', NULL, NULL, '', '佛山市', '440000', '广东省', '440600', '佛山', '2020-02-05 15:36:05', '1', NULL, '2020-02-05 15:36:48', '2020-02-05 15:36:49', NULL),
	('10', NULL, NULL, '洗手消毒液（瓶）', '瓶', 80.00, 0.00, 0.00, 29942.00, '市物资保障组', ' ', ' ', ' ', ' ', NULL, NULL, '5', NULL, NULL, '', '佛山市', '440000', '广东省', '440600', '佛山', '2020-02-05 15:36:05', '1', NULL, '2020-02-05 15:36:48', '2020-02-05 15:36:49', NULL),
	('11', NULL, NULL, '环境消毒液（瓶）', '瓶', 1120.00, 0.00, 0.00, 18317.00, '市物资保障组', ' ', ' ', ' ', ' ', NULL, NULL, '5', NULL, NULL, '', '佛山市', '440000', '广东省', '440600', '佛山', '2020-02-05 15:36:05', '1', NULL, '2020-02-05 15:36:48', '2020-02-05 15:36:49', NULL),
	('12', NULL, NULL, '环境消毒粉（公斤）', '公斤', 79.20, 0.00, 0.00, 5370.71, '市物资保障组', ' ', ' ', ' ', ' ', NULL, NULL, '5', NULL, NULL, '', '佛山市', '440000', '广东省', '440600', '佛山', '2020-02-05 15:36:05', '1', NULL, '2020-02-05 15:36:48', '2020-02-05 15:36:49', NULL),
	('13', NULL, NULL, '75%酒精（瓶）', '瓶', 10000.00, 0.00, 0.00, 51195.00, '市物资保障组', ' ', ' ', ' ', ' ', NULL, NULL, '5', NULL, NULL, '', '佛山市', '440000', '广东省', '440600', '佛山', '2020-02-05 15:36:05', '1', NULL, '2020-02-05 15:36:48', '2020-02-05 15:36:49', NULL),
	('3', NULL, NULL, '其他口罩（万个）', '万', 27.66, 0.00, 0.00, 136.14, '市物资保障组', ' ', ' ', ' ', ' ', NULL, NULL, '5', NULL, NULL, '', '佛山市', '440000', '广东省', '440600', '佛山', '2020-02-05 15:36:05', '1', NULL, '2020-02-05 15:36:48', '2020-02-05 15:36:49', NULL),
	('4', NULL, NULL, '医用防护服/一般防护服（件）', '件', 2210.00, 0.00, 0.00, 14238.00, '市物资保障组', ' ', ' ', ' ', ' ', NULL, NULL, '5', NULL, NULL, '', '佛山市', '440000', '广东省', '440600', '佛山', '2020-02-05 15:36:05', '1', NULL, '2020-02-05 15:36:48', '2020-02-05 15:36:49', NULL),
	('5', NULL, NULL, '防护眼罩/护目镜（副）', '副', 5167.00, 0.00, 0.00, 10562.00, '市物资保障组', ' ', ' ', ' ', ' ', NULL, NULL, '5', NULL, NULL, '', '佛山市', '440000', '广东省', '440600', '佛山', '2020-02-05 15:36:05', '1', NULL, '2020-02-05 15:36:48', '2020-02-05 15:36:49', NULL),
	('6', NULL, NULL, '一次性手套（万副）', '副', 3.17, 0.00, 0.00, 180.36, '市物资保障组', ' ', ' ', ' ', ' ', NULL, NULL, '5', NULL, NULL, '', '佛山市', '440000', '广东省', '440600', '佛山', '2020-02-05 15:36:05', '1', NULL, '2020-02-05 15:36:48', '2020-02-05 15:36:49', NULL),
	('7', NULL, NULL, '自动测温仪（套）', '套', 6.00, 0.00, 0.00, 30.00, '市物资保障组', ' ', ' ', ' ', ' ', NULL, NULL, '5', NULL, NULL, '', '佛山市', '440000', '广东省', '440600', '佛山', '2020-02-05 15:36:05', '1', NULL, '2020-02-05 15:36:48', '2020-02-05 15:36:49', NULL),
	('8', NULL, NULL, '手持测温仪（支）', '支', 17.00, 0.00, 0.00, 1898.00, '市物资保障组', ' ', ' ', ' ', ' ', NULL, NULL, '5', NULL, NULL, '', '佛山市', '440000', '广东省', '440600', '佛山', '2020-02-05 15:36:05', '1', NULL, '2020-02-05 15:36:48', '2020-02-05 15:36:49', NULL),
	('9', NULL, NULL, '隔离衣/手术衣（件）', '件', 0.00, 0.00, 0.00, 43001.00, '市物资保障组', ' ', ' ', ' ', ' ', NULL, NULL, '5', NULL, NULL, '', '佛山市', '440000', '广东省', '440600', '佛山', '2020-02-05 15:36:05', '1', NULL, '2020-02-05 15:36:48', '2020-02-05 15:36:49', NULL);

