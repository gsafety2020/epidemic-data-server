/*==============================================================*/
/* Table: china_area_medical_daily                             */
/*==============================================================*/
create table china_area_medical_daily
(
   id                   varchar(32) not null comment 'ID',
   stas_date            char(8) not null comment '报表日',
   area_code            varchar(32) not null comment '地区编码',
   area_name            varchar(255) not null comment '地区名称',
   parent_no            varchar(32) not null comment '上级编码',
   province_no          varchar(32) not null comment '省编码',
   city_no              varchar(32) not null comment '市编码',
   number_of_fcr        int not null default 0 comment '发热门诊接诊数fever clinic reception',
   number_of_fc         int not null default 0 comment '发热门诊数fever clinic',
   number_of_pa         int not null default 0 comment '收治病人数Number of patients admitted',
   update_time          datetime not null default CURRENT_TIMESTAMP comment '最后更新时间',
   primary key (id)
);

alter table china_area_medical_daily comment '各地区医院情况日报';
