CREATE TABLE report_community_stat_daily
(
    id varchar(32) PRIMARY KEY NOT NULL COMMENT '主键（日期+行政编码）',
    area_code varchar(32) COMMENT '行政编码',
    parent_area_code varchar(32) COMMENT '上级编码',
    stat_date char(8) COMMENT '统计日期（yyyymmdd）',
    total_fever_number int(11) COMMENT '累计发热',
    new_fever_number int(11) COMMENT '新增发热',
    total_isolated_number int(11) COMMENT '累计隔离',
    new_isolated_number int(11) COMMENT '新增隔离',
    total_suspected_number int(11) COMMENT '累计疑似',
    new_suspected_number int(11) COMMENT '新增疑似',
    total_diagnosed_number int(11) COMMENT '累计确诊',
    new_diagnosed_number int(11) COMMENT '新增确诊',
	update_time datetime COMMENT '更新时间'
);
ALTER TABLE report_community_stat_daily COMMENT = '上报信息社区统计表';