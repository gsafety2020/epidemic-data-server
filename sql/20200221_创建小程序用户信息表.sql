/*==============================================================*/
/* Table: pest_wechat_user                                      */
/*==============================================================*/
create table pest_wechat_user
(
   open_id              varchar(255) not null comment '用户',
   nick_name            varchar(255) default 'UNKNOWN' comment '昵称',
   gender               char(255) not null default '0' comment '性别，0-未知，1-男性，2-女性',
   city                 varchar(255) comment '城市',
   province             varchar(255) comment '省',
   country              varchar(255) comment '国家',
   avatar_url           varchar(1024) comment '头像',
   unionId              varchar(255) comment '唯一标识',
   with_credentials     char(1) not null default '0' comment '是否授权，0-未授权，1-已授权',
   first_access_time    datetime not null comment '首次访问时间',
   credentials_time     datetime comment '授权时间',
   update_time          datetime not null comment '最后更新时间',
   appid                varchar(255) not null comment 'appid',
   primary key (open_id)
);

alter table pest_wechat_user comment '微信用户信息';