create table china_hash_data
(
   id                   varchar(32) not null comment '业务主键',
   area_code            varchar(32) not null comment '地区编码',
   data_code            varchar(32) not null comment '数据编码',
   data_label           varchar(255) not null comment '数据标签',
   data_type            varchar(255) not null comment '数据类型,字典STRING, NUMBER',
   data_value           varchar(255) not null comment '数据描述',
   update_time          datetime not null comment '最后更新时间',
   primary key (id)
);

alter table china_hash_data comment '各地信息散列表';
