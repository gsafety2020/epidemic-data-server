
CREATE TABLE `patient_area_statistic_ext`  (
  `id` varchar(32)  NOT NULL COMMENT '主键',
  `area` varchar(32)  NOT NULL COMMENT '区域名称',
  `area_code` varchar(32)  NOT NULL COMMENT '区域编码',
  `stas_time` datetime(0) NOT NULL COMMENT '统计时间',
  `stas_date` char(8)  NOT NULL COMMENT '统计日期;YYYYMMDD',
  `stas_hour` char(6)  NULL DEFAULT NULL COMMENT '统计具体时间;HHmmSS',
  `data_src` varchar(32)  NULL DEFAULT NULL COMMENT '数据来源渠道',
  `new_case_rate` decimal(15, 4) NULL DEFAULT NULL COMMENT '当日确诊比例=当前城市当日确诊数量/上级当日确诊数量',
  `total_case_rate` decimal(15, 4) NULL DEFAULT NULL COMMENT '累计确诊比例=当前城市累计确诊数量/上级累计确诊数量',
  `case_growth_rate` decimal(15, 4) NULL DEFAULT NULL COMMENT '当日增长率=当日新增确诊/前一天累计确诊-1',
  `new_total_rate` decimal(15, 4) NULL DEFAULT NULL COMMENT '当前城市当日新增/当日累计',
  `death_rate` decimal(15, 4) NULL DEFAULT NULL COMMENT '死亡率=截止到当天累计死亡人数/截止到当天累计确诊',
  `create_by` varchar(32)  NULL DEFAULT NULL COMMENT '创建人',
  `upate_by` varchar(32)  NULL DEFAULT NULL COMMENT '更新人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ;
alter table patient_area_statistic_ext comment '病例信息表';
