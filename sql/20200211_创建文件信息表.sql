drop table if exists basic_file_info;

/*==============================================================*/
/* Table: basic_file_info                                       */
/*==============================================================*/
create table basic_file_info
(
   file_id              varchar(32) not null,
   file_name            varchar(255) not null comment '文件名',
   file_date            datetime not null default CURRENT_TIMESTAMP,
   status               varchar(10) comment '-1是删除 5 正常',
   url                  varchar(2000) not null comment '地址',
   oth_url              varchar(2000) comment '其他链接地址',
   file_type            varchar(10) default '0' comment '文件类型 默认:0 分析报告',
   update_date          datetime not null default CURRENT_TIMESTAMP comment '修改时间',
   create_date          datetime not null default CURRENT_TIMESTAMP comment '更新时间',
   primary key (file_id)
);

alter table basic_file_info comment '文件信息列表';
