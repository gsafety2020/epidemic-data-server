/*==============================================================*/
/* Table: basic_report_info                                     */
/*==============================================================*/
create table basic_report_info
(
   id                   varchar(32) not null,
   report_time          datetime not null default CURRENT_TIMESTAMP,
   report_title         varchar(255) not null,
   report_info          varchar(4000),
   remark               varchar(1000) default '5' comment '状态：默认5',
   channel              varchar(20) not null comment '1:12345热线 2:北京市应急局 3:延庆区',
   area_no              varchar(32) comment '地区ID',
   area_name            varchar(32) comment '地区名称',
   parent_no            varchar(32) comment '上级区域',
   parent_name          varchar(32) comment '上级区域',
   community            varchar(32) comment '小区/街道',
   status               varchar(5) comment '默认是 5 0是删除',
   seq_num              int default 0 comment '排序',
   create_by            varchar(32) comment '创建人',
   update_by            varchar(32) comment '修改人',
   create_time          datetime not null default CURRENT_TIMESTAMP comment '创建时间',
   update_time          datetime not null default CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table basic_report_info comment '区域信息上报表';