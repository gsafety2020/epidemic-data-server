/*==============================================================*/
/* Table: china_area_case_info                                  */
/*==============================================================*/
create table china_area_case_info
(
   case_id              varchar(32) not null,
   area_no              varchar(32) comment '地区ID',
   area_name            varchar(32) comment '地区名称',
   patient_name         varchar(32) comment '人员姓名',
   age                  DECIMAL(3,1) comment '年龄',
   gender               varchar(5) comment '男 女',
   case_detail          varchar(2000) comment '详细病例',
   hospital             varchar(32) comment '医院',
   community            varchar(255) comment '社区名称',
   illness_date         date comment '生病日期',
   diagnosis_date       date comment '就诊时间',
   illness_type         varchar(32) comment '生病类型：确诊confirmed  疑似 debt 发烧 hot  密切接触：clsconts  治愈：cured',
   remark               varchar(255) comment '说明',
   seq_num              int comment '排序',
   city_no              varchar(32) comment '所属市级区域编码',
   city_name            varchar(32) comment '所属市级区域名称',
   province_no          varchar(32) comment '所属省级区域编码',
   province_name        varchar(32) comment '所属省级区域名称',
   parent_no            varchar(32) comment '上级区域',
   parent_name          varchar(32) comment '上级区域',
   status               varchar(5) comment '默认是 5 0是删除',
   create_by            varchar(32) comment '创建人',
   update_by            varchar(32) comment '修改人',
   create_time          datetime comment '创建时间',
   update_time          datetime comment '修改时间',
   primary key (case_id)
);

alter table china_area_case_info comment '病例信息表';
