create table report_area_stat_daily
(
   id                   varchar(32) not null comment '主键（日期 + 行政编码）',
   stas_date            char(8) comment '统计日期',
   area_code            varchar(32) comment '行政编码',
   parent_code          varchar(32) comment '上级编码',
   people_number        numeric(11) comment '辖区总人数',
   register_number      numeric(11) comment '今日登记人数',
   hot_number           numeric(11) comment '发热人数',
   patient_number       numeric(11) comment '确诊患者',
   debt_number          numeric(11) comment '疑似患者',
   ct_patient_number    numeric(11) comment 'CT诊断肺炎患者',
   general_hot_number   numeric(11) comment '一般发热患者',
   close_contacts_number numeric(11) comment '密切接触者',
   update_time          datetime comment '更新时间',
   primary key (id)
);

alter table report_area_stat_daily comment '每日情况汇总表,汇总江夏区每日上报情况';
