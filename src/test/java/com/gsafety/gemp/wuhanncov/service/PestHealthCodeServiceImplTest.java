package com.gsafety.gemp.wuhanncov.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.gsafety.gemp.wuhanncov.contract.service.PestHealthCodeService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class PestHealthCodeServiceImplTest {

	@Autowired
	private PestHealthCodeService pestHealthCodeService;
	
	@Test
	public void testGetEffectiveHealthCodeByOpenIdAndAreaCode() {
		pestHealthCodeService.getEffectiveHealthCodeByOpenIdAndAreaCode("13123", "123123");
	}

}
