/**
 * 
 */
package com.gsafety.gemp.d420115.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.annotation.Resource;

import com.gsafety.gemp.d420115.contract.dto.D420115Datav02;
import com.gsafety.gemp.wuhanncov.contract.dto.*;
import com.gsafety.gemp.wuhanncov.contract.enums.NumberTypeEnum;
import com.gsafety.gemp.wuhanncov.contract.service.*;
import com.gsafety.gemp.wuhanncov.contract.service.BasicReportService;
import com.gsafety.gemp.wuhanncov.contract.service.BasicReportStatisticDayService;
import com.gsafety.gemp.wuhanncov.contract.service.ChinaAreaMedicalDailyService;
import com.gsafety.gemp.wuhanncov.contract.service.ChinaHashDataService;

import org.apache.commons.lang.time.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gsafety.gemp.wuhanncov.contract.dto2.PatientAreaInfoDailyDTO2;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 疫情数据江夏420115接口
 *
 * @author yangbo
 *
 * @since 2020年2月1日 下午10:45:03
 *
 */
@Api(value = "疫情数据江夏420115接口", tags = { "疫情数据江夏420115接口" })
@RestController
@RequestMapping("/ncov/d420115/")
public class D420115DataController {

	/**
	 * 
	 */
	private static final DateTimeFormatter DTF_YMD = DateTimeFormat.forPattern("yyyyMMdd");

	/**
	 * 
	 */
	private static final DateTimeFormatter DTF_YMD2 = DateTimeFormat.forPattern("yyyy/MM/dd");

	/**
	 *
	 */
	private static final String KEY_REPORT_DATE = "ReportDate";

	@Resource
	private NcovCommonDataService ncovCommonDataService;

	@Resource
	private PatientAreaRuntimeService patientAreaRuntimeService;

	/**
	 *
	 */
	@Resource
	private ChinaHashDataService chinaHashDataService;

	/**
	 * 
	 */
	@Resource
	private BasicReportStatisticDayService basicReportStatisticDayService;

	/**
	 * 
	 */
	@Resource
	private BasicReportService basicReportService;

	/**
	 * 
	 */
	@Resource
	private ReportAreaStatDailyService reportAreaStatDailyService;

	/**
	 * 
	 */
	@Resource
	private ReportCommunityStatDailyService reportCommunityStatDailyService;

	/**
	 * 
	 */
	@Resource
	private ChinaAreaMedicalDailyService chinaAreaMedicalDailyService;

	/**
	 * 集中医学观察每日情况汇总
	 * 
	 * @author Luoganggang
	 * @since 2020/2/9 16:40
	 * @param areaCode
	 *            行政编码
	 * @return ReportAreaStatDailyDTO
	 */
	@ApiOperation(value = "集中医学观察每日情况汇总")
	@GetMapping("/report_area_stat_daily")
	public ReportAreaStatDailyDTO dailySummary(
			@ApiParam(value = "单个区域行政区划编码", example = "420115", required = true) @RequestParam(name = "areaCode") String areaCode) {
		return reportAreaStatDailyService.dailySummary(areaCode);
	}

	/**
	 * 社区发热人员发展趋势
	 * 
	 * @author Luoganggang
	 * @since 2020/2/9 20:36
	 * @param areaCode
	 *            行政编码
	 * @return List<PatientAreaCountDailyDTO>
	 */
	@ApiOperation(value = "社区发热人员发展趋势,用于折线图")
	@GetMapping("/report_community_stat_daily")
	public List<PatientAreaCountDailyDTO> communityHotPeopleStat(
			@ApiParam(value = "单个区域行政区划编码", example = "420115", required = true) @RequestParam(name = "areaCode") String areaCode) {
		return reportCommunityStatDailyService.communityHotPeopleStat(areaCode);
	}

	/**
	 * 
	 * 查询单个区域患者情况按天统计历史数据
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月1日 下午10:59:39
	 *
	 * @param areaId
	 * @param days
	 * @return
	 */
	@ApiOperation(value = "查询单个区域患者情况按天统计历史数据")
	@GetMapping("/patient_area_history_info")
	public List<PatientAreaD420115Datav> getPatientAreaDailyInfo(
			@ApiParam(value = "单个区域行政区划编码", example = "420115") @RequestParam("area_id") String areaId,
			@ApiParam(value = "需要展示最近多少天的历史数据（不包含当日）", example = "7") @RequestParam("days") Integer days) {
		List<PatientAreaInfoDailyDTO2> records = ncovCommonDataService.getPatientAreaInfoHistoryV2(areaId, days);
		// 病患数据
		List<PatientAreaD420115Datav> list = new ArrayList<>();
		records.forEach(temp -> list.addAll(toPatientAreaD420115Datav(temp)));
		// 医疗数据，发热数据
		// 查询医疗数量
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date now = new Date();
		String from = sdf.format(DateUtils.addDays(now, -2 - days));
		String to = sdf.format(now);
		List<ChinaAreaMedicalDailyDTO> chinaAreaMedicalDailys = this.chinaAreaMedicalDailyService
				.findByStasDateBetween(areaId, from, to);
		chinaAreaMedicalDailys.stream().forEach(chinaAreaMedicalDaily -> {
			list.add(toPatientAreaD420115Datav(chinaAreaMedicalDaily));
		});
		return list;
	}

	/**
	 * 
	 * 查询用户希望可以看到历史每日病患，新增确诊、新增疑似、现存重症、现存危重、新增死亡、新增出院、当日发热接诊数API
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月10日 上午10:33:37
	 *
	 * @param areaId
	 * @param days
	 * @return
	 */
	@ApiOperation(value = "查询单个区域患者情况按天统计历史数据")
	@GetMapping("/d420115Datav02")
	public List<D420115Datav02> getD420115Datav02(
			@ApiParam(value = "单个区域行政区划编码", example = "420115") @RequestParam("area_id") String areaId,
			@ApiParam(value = "需要展示最近多少天的历史数据（不包含当日）", example = "7") @RequestParam("days") Integer days) throws ParseException {

		List<D420115Datav02> list = new ArrayList<>();

		//数据类型。1-当日新增确诊，2-新增死亡，3-当日新增出院，4-当日发热接诊数 , 5-当日收治
		List<String> typesList = new ArrayList<>();
		typesList.add(NumberTypeEnum.INC.getType());
		//typesList.add(NumberTypeEnum.NEW_DEBT.getType());
		//typesList.add(NumberTypeEnum.NEW_SEVERE.getType());
		//typesList.add(NumberTypeEnum.NEW_DYING.getType());
		typesList.add(NumberTypeEnum.NEW_DEATH.getType());
		typesList.add(NumberTypeEnum.NEW_CURED.getType());
		//typesList.add(NumberTypeEnum.HOT.getType());

		List<PatientAreaInfoDailyDTO> records = ncovCommonDataService.getPatientAreaInfoHistory(areaId, days,
				typesList);

		D420115Datav02 datav02 = null;
		for(PatientAreaInfoDailyDTO dto : records){
			datav02 = new D420115Datav02();
			BeanUtils.copyProperties(dto, datav02);
			list.add(datav02);
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		Date now = new Date();
		String from = sdf.format(DateUtils.addDays(now, -1 - days));
		String to = sdf.format(DateUtils.addDays(now, -1));
		List<ChinaAreaMedicalDailyDTO> chinaAreaMedicalDailys = this.chinaAreaMedicalDailyService
				.findByStasDateBetween(areaId, from, to);

		chinaAreaMedicalDailys.sort(Comparator.comparing(ChinaAreaMedicalDailyDTO::getStasDate));

		ChinaAreaMedicalDailyDTO dto = null;
		for(int i=1; i< chinaAreaMedicalDailys.size(); i++){

			//发热门诊接诊数
			dto = chinaAreaMedicalDailys.get(i);
			datav02 = new D420115Datav02();
			datav02.setX(format.format(sdf.parse(dto.getStasDate())));
			datav02.setY(dto.getNumberOfFcr());
			datav02.setS("4");
			list.add(datav02);

			//当日收治
			datav02 = new D420115Datav02();
			datav02.setX(format.format(sdf.parse(dto.getStasDate())));
			datav02.setY(dto.getNumberOfPa() - chinaAreaMedicalDailys.get(i - 1).getNumberOfPa());
			datav02.setS("5");
			list.add(datav02);
		}

		list.sort(Comparator.comparing(D420115Datav02::getX).thenComparing(D420115Datav02::getS));

		return list;
	}
	
	/**
	 * 
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月9日 下午10:07:39
	 *
	 * @param chinaAreaMedicalDaily
	 * @return
	 */
	private PatientAreaD420115Datav toPatientAreaD420115Datav(ChinaAreaMedicalDailyDTO chinaAreaMedicalDaily) {
		DateTime d = DateTime.parse(chinaAreaMedicalDaily.getStasDate(), DTF_YMD);
		String date = d.toString(DTF_YMD2);
		return new PatientAreaD420115Datav(date, chinaAreaMedicalDaily.getNumberOfFcr(),
				PatientAreaD420115DatavEnum.FEVER_CLINIC_RECEPTION.scode);
	}

	/**
	 * 
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月2日 上午12:04:42
	 *
	 * @return
	 */
	private Collection<PatientAreaD420115Datav> toPatientAreaD420115Datav(PatientAreaInfoDailyDTO2 record) {
		List<PatientAreaD420115Datav> list = new ArrayList<>();

		DateTime d = DateTime.parse(record.getStasDate(), DTF_YMD);
		String date = d.toString(DTF_YMD2);
		list.add(new PatientAreaD420115Datav(date, record.getPatientNumber(),
				PatientAreaD420115DatavEnum.CONFIRM.scode));
		list.add(new PatientAreaD420115Datav(date, record.getDebtNumber(), PatientAreaD420115DatavEnum.DEBT.scode));
		list.add(new PatientAreaD420115Datav(date, record.getSevereNumber(), PatientAreaD420115DatavEnum.SEVERE.scode));
		list.add(new PatientAreaD420115Datav(date, 0, PatientAreaD420115DatavEnum.DANGER.scode));
		list.add(new PatientAreaD420115Datav(date, record.getDeathNumber(), PatientAreaD420115DatavEnum.DEATH.scode));
		list.add(new PatientAreaD420115Datav(date, record.getCuredNumber(), PatientAreaD420115DatavEnum.CURE.scode));
		return list;
	}

	/**
	 * 
	 * 查询增量情况
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月1日 下午11:03:02
	 *
	 * @param areaId
	 * @return
	 */
	@ApiOperation(value = "查询指定行政区划及下地区当日增量情况")
	@GetMapping("/findNewStatics")
	public List<PatientAreaD420115Add> findNewStatics(
			@ApiParam(value = "单个区域行政区划编码", example = "420115") @RequestParam("area_id") String areaId) {
		List<PatientAreaD420115Add> list = new ArrayList<>();

		ChinaCountStatisticsJSON one = patientAreaRuntimeService.findByAreaCode(areaId);
		list.add(new PatientAreaD420115Add(one.getAreaName(), one.getNewCase(), one.getNew_probable(),
				one.getNewCloseContacts()));

		List<ChinaCountStatisticsJSON> records = patientAreaRuntimeService.findByParentNo(areaId);
		records.forEach(record -> list.add(new PatientAreaD420115Add(record.getAreaName(), record.getNewCase(),
				record.getNew_probable(), record.getNewCloseContacts())));
		Collections.sort(list);
		Collections.reverse(list);

		return list;
	}

	/**
	 *
	 * 查询江夏当天的社区，发热和上报信息历史曲线图
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年2月7日 下午11:32:02
	 *
	 * @return
	 */
	@ApiOperation(value = "查询江夏当天的社区，发热和上报信息历史曲线图")
	@GetMapping("/findReportHistroySum")
	public List<BasicReportStaticsDTO> findReportHistroySum(
			@ApiParam(value = "返回多少条数据", example = "5", required = true) @RequestParam("topSize") int topSize) {
		return basicReportStatisticDayService.findReportHistroySum(topSize);
	}

	/**
	 *
	 * 查询江夏当天的社区，发热和上报信息汇总
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年2月7日 下午11:32:02
	 *
	 * @return
	 */
	@ApiOperation(value = "查询江夏当天的社区，发热和上报信息汇总")
	@GetMapping("/findReportStatics")
	public BasicReportDTO findReportStatics(
			@ApiParam(value = "开始时间", example = "20200203") @RequestParam("date") String date) {
		return basicReportService.findByDate(date);
	}

	/**
	 * 
	 * 查询增量情况
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月1日 下午11:04:43
	 *
	 * @param areaId
	 * @return
	 */
	@ApiOperation(value = "查询指定行政区划及下地区当日全量量情况")
	@GetMapping("/findAllStatics")
	public List<PatientAreaD420115All> findAllStatics(
			@ApiParam(value = "单个区域行政区划编码", example = "420115") @RequestParam("area_id") String areaId) {
		List<PatientAreaD420115All> list = new ArrayList<>();

		ChinaCountStatisticsJSON one = patientAreaRuntimeService.findByAreaCode(areaId);
		list.add(new PatientAreaD420115All(one.getAreaName(), one.getConfirmCase(), one.getProbableCase(),
				one.getCloseContactsCase()));

		List<ChinaCountStatisticsJSON> records = patientAreaRuntimeService.findByParentNo(areaId);
		records.forEach(record -> list.add(new PatientAreaD420115All(record.getAreaName(), record.getConfirmCase(),
				record.getProbableCase(), record.getCloseContactsCase())));
		Collections.sort(list);
		Collections.reverse(list);

		return list;
	}

	/**
	 * 查询指定区域实时病患情况
	 *
	 * @param areaCode
	 * @return
	 * @author yangbo
	 * @since 2020年1月30日 下午9:30:49
	 */
	@ApiOperation(value = "查询指定区域实时病患情况")
	@GetMapping("/runtime_patient_area")
	public PatientRuntimeD420115Datav runtimePatientArea(
			@RequestParam(name = "areaCode", required = true) String areaCode) {
		ChinaCountStatisticsJSON one = patientAreaRuntimeService.findByAreaCode(areaCode);
		PatientRuntimeD420115Datav v = toPatientRuntimeD420115Datav(one); 

		// 根据库表设置发布日期时间
		Map<String, Object> map = this.chinaHashDataService.findMapByAreaCode(areaCode);
		v.setStasDate((String) map.get(KEY_REPORT_DATE));

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date now = new Date();
		String from = sdf.format(DateUtils.addDays(now, -5));
		String to = sdf.format(now);
		List<ChinaAreaMedicalDailyDTO> chinaAreaMedicalDailys = this.chinaAreaMedicalDailyService
				.findByStasDateBetween(areaCode, from, to);

		chinaAreaMedicalDailys.sort(Comparator.comparing(ChinaAreaMedicalDailyDTO::getStasDate).reversed());
		if(chinaAreaMedicalDailys != null && chinaAreaMedicalDailys.size() > 0){
			v.setAdmitted(chinaAreaMedicalDailys.get(0).getNumberOfPa());
		}

		return v;
	}

	/**
	 * 
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月10日 下午7:03:28
	 *
	 * @param one
	 * @return
	 */
	private PatientRuntimeD420115Datav toPatientRuntimeD420115Datav(ChinaCountStatisticsJSON one) {
		PatientRuntimeD420115Datav v = new PatientRuntimeD420115Datav();
		// 拷贝属性名称一样的部分
		BeanUtils.copyProperties(one, v);
		// 拷贝属性名不一样的部分
		v.setDangerCase(one.getDyingCase());
		v.setNewProbable(one.getNew_probable());
		v.setNewCure(one.getNew_cure());
		v.setNewDanger(one.getNewDying());
		v.setNewDead(one.getNew_dead());
		v.setContact(one.getCloseContactsCase());
		v.setObserve(one.getObservedCase());
		v.setRelieve(one.getReleaseObservedCase());
		// 返回数据
		return v;
	}

	/**
	 * 用于地图上的患者数量热力图展示，查询父区域下各子区域的患者确诊数量接口 数据来源人工维护，较准确，表：patient_area_runtime
	 * 
	 * @author lyh
	 * @since 2020/1/30 14:21 父区域行政区划编码
	 * @return List<AreaHotDTO>
	 */
	@ApiOperation(value = "查询指定父区域下子区域患者热力值列表, 数据来源手工维护")
	@GetMapping("/locations_patient_count_runtime")
	public List<AreaHotDTO> getAreaPatientCoutFromRuntime() {
		List<AreaHotDTO> records = new ArrayList<>();

		// 江夏要求将420115401000经开区拆分为4个区
		// "name": "经开区(庙山)",
		// "adcode": "420115401000",
		//
		// "name": "经开区(藏龙岛)",
		// "adcode": "420115402000",
		//
		// "name": "经开区(大桥)",
		// "adcode": "420115403000",
		//
		// "name": "经开区(金港)",
		// "adcode": "420115405000"
		List<AreaHotDTO> list = ncovCommonDataService.getAreaPatientCountFromRuntimeByParent("420115");
		for (AreaHotDTO temp : list) {
			if (temp.getArea_id().equals("420115401000")) {
				records.add(mock("420115401000", "经开区(庙山)", temp));
				records.add(mock("420115402000", "经开区(藏龙岛)", temp));
				records.add(mock("420115403000", "经开区(大桥)", temp));
				records.add(mock("420115405000", "经开区(金港)", temp));

			} else {
				records.add(temp);
			}
		}
		return records;
	}

	/**
	 *
	 * 获取社区发热人员每日汇总
	 *
	 * @author dusiwei
	 *
	 * @since 2020年2月9日 下午11:04:43
	 *
	 * @param areaId
	 * @return
	 */
	@ApiOperation(value = "获取社区发热人员每日汇总")
	@GetMapping("/community_stat_daily")
	public Map<String, Object> getCommunityStatDaily(
			@ApiParam(value = "单个区域行政区划编码", example = "420115") @RequestParam("area_id") String areaId) {
		return reportCommunityStatDailyService.getCommunityStatDaily(areaId);
	}

	/**
	 * 
	 * 江夏按领导要求造一条假数据。
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月4日 上午12:52:52
	 *
	 * @param code
	 * @param name
	 * @param temp
	 * @return
	 */
	private AreaHotDTO mock(String code, String name, AreaHotDTO temp) {
		AreaHotDTO mock = new AreaHotDTO();
		BeanUtils.copyProperties(temp, mock);
		mock.setArea_id(code);
		mock.setAreaName(name);
		return mock;
	}

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	class PatientAreaD420115Datav {

		@ApiModelProperty(value = "日期。 格式：yyyy/MM/dd")
		private String x;

		@ApiModelProperty(value = "数据值")
		private Integer y;

		@ApiModelProperty(value = "数据类型。1- 累计确诊，2-累计疑似，3-现存重症，4-现存危重，5-累计死亡，6-累计出院, 7-当日发热门诊接诊")
		private String s;

	}

	/**
	 * 
	 * 数据类型。1- 确诊，2-疑似，3-重症，4-危重，5-死亡，6-出院
	 *
	 * @author yangbo
	 *
	 * @since 2020年2月2日 上午12:16:40
	 *
	 */
	enum PatientAreaD420115DatavEnum {
		CONFIRM("1"), DEBT("2"), SEVERE("3"), DANGER("4"), DEATH("5"), CURE("6"), FEVER_CLINIC_RECEPTION("7");

		private final String scode;

		private PatientAreaD420115DatavEnum(String scode) {
			this.scode = scode;
		}

		public String getScode() {
			return scode;
		}

	}

	/**
	 * 
	 * @author yangbo
	 *
	 * @since 2020年2月1日 下午11:33:59
	 *
	 */
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	class PatientAreaD420115Add implements Comparable<PatientAreaD420115Add> {

		@JsonProperty("name")
		@ApiModelProperty(value = "地区")
		private String areaName;

		@JsonProperty("new")
		@ApiModelProperty(value = "新增人数")
		private Integer newCase;

		@JsonProperty("new_probable")
		@ApiModelProperty(value = "新增疑似")
		private Integer newProbable = 0;

		@JsonProperty("new_contact")
		@ApiModelProperty(value = "新增密接")
		private Integer newContact = 0;

		@Override
		public int compareTo(PatientAreaD420115Add o) {
			if (o == null) {
				return -1;
			}
			if (this.newCase.compareTo(o.newCase) == 0) {
				return this.newProbable.compareTo(o.newProbable);
			}
			return this.newCase.compareTo(o.newCase);
		}

	}

	/**
	 * 
	 * @author yangbo
	 *
	 * @since 2020年2月1日 下午11:33:53
	 *
	 */
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	class PatientAreaD420115All implements Comparable<PatientAreaD420115All> {

		@JsonProperty("name")
		@ApiModelProperty(value = "地区")
		private String areaName;

		@JsonProperty("confirm")
		@ApiModelProperty(value = "确诊人数")
		private Integer confirmCase;

		@JsonProperty("probable")
		@ApiModelProperty(value = "疑似人数")
		private Integer probableCase;

		@JsonProperty("contact")
		@ApiModelProperty(value = "累计密接")
		private Integer contact;

		@Override
		public int compareTo(PatientAreaD420115All o) {
			if (o == null) {
				return -1;
			}
			if (this.confirmCase.compareTo(o.confirmCase) == 0) {
				return this.probableCase.compareTo(o.probableCase);
			}
			return this.confirmCase.compareTo(o.confirmCase);
		}

	}

	/**
	 * 
	 * 该类用于...，用于实现...等能力。
	 *
	 * @author yangbo
	 *
	 * @since 2020年2月2日 上午12:43:35
	 *
	 */
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	class PatientRuntimeD420115Datav {

		@JsonProperty("stasDate")
		@ApiModelProperty(value = "截止日期")
		private String stasDate;

		@JsonProperty("name")
		@ApiModelProperty(value = "地区")
		private String areaName;

		@JsonProperty("confirm")
		@ApiModelProperty(value = "确诊人数")
		private Integer confirmCase = 0;

		@JsonProperty("probable")
		@ApiModelProperty(value = "疑似人数")
		private Integer probableCase = 0;

		@JsonProperty("cure")
		@ApiModelProperty(value = "治愈人数")
		private Integer cureCase = 0;

		@JsonProperty("dead")
		@ApiModelProperty(value = "死亡人数")
		private Integer deadCase = 0;

		@JsonProperty("severe")
		@ApiModelProperty(value = "重症人数")
		private Integer severeCase = 0;

		@JsonProperty("danger")
		@ApiModelProperty(value = "危重症人数")
		private Integer dangerCase = 0;

		@JsonProperty("new")
		@ApiModelProperty(value = "新增人数")
		private Integer newCase = 0;

		@JsonProperty("new_cure")
		@ApiModelProperty(value = "新增治愈")
		private Integer newCure = 0;

		@JsonProperty("new_dead")
		@ApiModelProperty(value = "新增死亡")
		private Integer newDead = 0;

		@JsonProperty("new_probable")
		@ApiModelProperty(value = "新增疑似")
		private Integer newProbable = 0;

		@JsonProperty("new_severe")
		@ApiModelProperty(value = "新增疑似")
		private Integer newSevere = 0;

		@JsonProperty("contact")
		@ApiModelProperty(value = "密切接触者")
		private Integer contact = 0;

		@JsonProperty("observe")
		@ApiModelProperty(value = "观察中")
		private Integer observe = 0;

		@JsonProperty("relieve")
		@ApiModelProperty(value = "已解除")
		private Integer relieve = 0;

		@JsonProperty("new_danger")
		@ApiModelProperty(value = "当日危重症人数")
		private Integer newDanger = 0;

		@JsonProperty("admitted")
		@ApiModelProperty(value = "累计收治人数")
		private Integer admitted = 0;
	}
}
