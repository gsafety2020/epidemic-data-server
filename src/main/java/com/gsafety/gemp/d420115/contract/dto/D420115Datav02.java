/**
 * 
 */
package com.gsafety.gemp.d420115.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * D420115Datav02模型，新增确诊、新增疑似、现存重症、现存危重、新增死亡、新增出院、当日发热接诊数
 *
 * @author yangbo
 *
 * @since 2020年2月10日 上午10:38:01
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class D420115Datav02 {

	@ApiModelProperty(value = "日期。 格式：yyyy/MM/dd")
	private String x;

	@ApiModelProperty(value = "数据值")
	private Integer y;

	@ApiModelProperty(value = "数据类型。1-当日新增确诊，2-当日新增疑似，3-现存重症，4-现存危重，5-新增死亡，6-当日新增出院，7-当日发热接诊数")
	private String s;
}
