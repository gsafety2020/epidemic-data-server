/**
 * 
 */
package com.gsafety.gemp.d420110.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.time.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsafety.gemp.wuhanncov.contract.dto.ChinaAreaMedicalDailyDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaInfoDailyDTO;
import com.gsafety.gemp.wuhanncov.contract.dto2.PatientAreaInfoDailyDTO2;
import com.gsafety.gemp.wuhanncov.contract.service.ChinaAreaMedicalDailyService;
import com.gsafety.gemp.wuhanncov.contract.service.NcovCommonDataService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 疫情数据武汉420100接口
 *
 * @author yangbo
 *
 * @since 2020年2月9日 上午2:54:55
 *
 */
@Api(value = "疫情数据武汉420100接口", tags = { "疫情数据武汉420100接口" })
@RestController
@RequestMapping("/ncov/d420100/")
public class D420100DataController {

	/**
	 * 
	 */
	private static final DateTimeFormatter DTF_YMD = DateTimeFormat.forPattern("yyyyMMdd");

	/**
	 * 
	 */
	private static final DateTimeFormatter DTF_YMD2 = DateTimeFormat.forPattern("yyyy/MM/dd");

	/**
	 * 
	 */
	@Resource
	private NcovCommonDataService ncovCommonDataService;

	/**
	 * 
	 */
	@Resource
	private ChinaAreaMedicalDailyService chinaAreaMedicalDailyService;

	/**
	 * 
	 */
	private static final String AREA_CODE = "420100";

	/**
	 * 
	 * 武汉疑似病例接口
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月9日 上午3:02:07
	 *
	 * @return
	 */
	@ApiOperation(value = "武汉疑似病例接口")
	@GetMapping("/d420110Datav01")
	public List<D420100Datav01> d420110Datav01(
			@ApiParam(value = "向前推多少天", example = "7", required = true) @RequestParam("days") int days) {
		List<D420100Datav01> list = new ArrayList<>();

		// 构建疑似数量
		List<PatientAreaInfoDailyDTO2> records = ncovCommonDataService.getPatientAreaInfoHistoryV2(AREA_CODE, days-1);
		records.forEach(record -> list.addAll(convertD420100Datav01(record)));

		// 查询医疗数量
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date now = new Date();
		String from = sdf.format(DateUtils.addDays(now, - days));
		String to = sdf.format(DateUtils.addDays(now, - 1));
		List<ChinaAreaMedicalDailyDTO> dailys = this.chinaAreaMedicalDailyService.findByStasDateBetween(AREA_CODE, from,
				to);
		dailys.forEach(daily -> list.addAll(convertD420100Datav01(daily)));

		return list;
	}

	/**
	 * 
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月9日 上午3:28:57
	 *
	 * @param daily
	 * @return
	 */
	private Collection<? extends D420100Datav01> convertD420100Datav01(ChinaAreaMedicalDailyDTO daily) {
		List<D420100Datav01> list = new ArrayList<>();
		DateTime d = DateTime.parse(daily.getStasDate(), DTF_YMD);
		String date = d.toString(DTF_YMD2);
		list.add(new D420100Datav01(date, daily.getNumberOfFcr(), "3"));
		return list;
	}

	/**
	 * 
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月9日 上午3:18:16
	 *
	 * @param record
	 * @return
	 */
	private Collection<D420100Datav01> convertD420100Datav01(PatientAreaInfoDailyDTO2 record) {
		List<D420100Datav01> list = new ArrayList<>();
		DateTime d = DateTime.parse(record.getStasDate(), DTF_YMD);
		String date = d.toString(DTF_YMD2);
		list.add(new D420100Datav01(date, record.getDebtNumber(), "1"));
		list.add(new D420100Datav01(date, record.getNewDebt(), "2"));
		return list;
	}

	/**
	 * 
	 * d420110Datav02
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月9日 上午3:02:16
	 *
	 * @return
	 */
	@ApiOperation(value = "武汉确诊病例接口")
	@GetMapping("/d420110Datav02")
	public List<D420100Datav02> d420110Datav02(
			@ApiParam(value = "向前推多少天", example = "7", required = true) @RequestParam("days") int days) {
		List<D420100Datav02> list = new ArrayList<>();

		// 调用datav接口,查询确诊confirmed、新增new确诊2项；
		List<String> typesList = new ArrayList<>();
		String[] typeArray = "confirmed_new".split("_");
		for (String type : typeArray) {
			typesList.add(type.trim());
		}
		List<PatientAreaInfoDailyDTO> records = ncovCommonDataService.getPatientAreaInfoHistory(AREA_CODE, days,
				typesList);
		// 构建确诊数量
		records.forEach(record -> list.addAll(convertD420100Datav02(record)));

		// 查询医疗数量
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date now = new Date();
		String from = sdf.format(DateUtils.addDays(now, - days));
		String to = sdf.format(DateUtils.addDays(now, - 1));
		List<ChinaAreaMedicalDailyDTO> dailys = this.chinaAreaMedicalDailyService.findByStasDateBetween(AREA_CODE, from,
				to);
		dailys.forEach(daily -> list.addAll(convertD420100Datav02(daily)));
		return list;
	}

	/**
	 * 
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月9日 上午3:46:16
	 *
	 * @param daily
	 * @return
	 */
	private Collection<D420100Datav02> convertD420100Datav02(ChinaAreaMedicalDailyDTO daily) {
		List<D420100Datav02> list = new ArrayList<>();
		DateTime d = DateTime.parse(daily.getStasDate(), DTF_YMD);
		String date = d.toString(DTF_YMD2);
		list.add(new D420100Datav02(date, daily.getNumberOfPa(), "3"));
		return list;
	}

	/**
	 * 
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月9日 上午3:47:11
	 *
	 * @param record
	 * @return
	 */
	private Collection<D420100Datav02> convertD420100Datav02(PatientAreaInfoDailyDTO record) {
		List<D420100Datav02> list = new ArrayList<>();
		list.add(new D420100Datav02(record.getX(), record.getY(), record.getS()));
		return list;
	}

	/**
	 * 
	 * 武汉疑似病例行记录
	 *
	 * @author yangbo
	 *
	 * @since 2020年2月9日 上午3:00:29
	 *
	 */
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	class D420100Datav01 {

		@ApiModelProperty(value = "日期。 格式：yyyy/MM/dd")
		private String x;

		@ApiModelProperty(value = "数据值")
		private Integer y;

		@ApiModelProperty(value = "疑似总数S=1，疑似较上日增加s=2，发热门诊接诊人数s=3")
		private String s;

	}

	/**
	 * 
	 * 武汉确诊病例行记录
	 *
	 * @author yangbo
	 *
	 * @since 2020年2月9日 上午2:59:40
	 *
	 */
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	class D420100Datav02 {

		@ApiModelProperty(value = "日期。 格式：yyyy/MM/dd")
		private String x;

		@ApiModelProperty(value = "数据值")
		private Integer y;

		@ApiModelProperty(value = "确诊总数s=1，当日新增s=2，收治病人s=3")
		private String s;

	}
}
