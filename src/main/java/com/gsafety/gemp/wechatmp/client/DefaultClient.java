/**
 * 
 */
package com.gsafety.gemp.wechatmp.client;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSON;
import com.gsafety.gemp.wechatmp.client.exception.DefaultClientException;

/**
 * @author lenovo
 * 
 */
public abstract class DefaultClient implements LooseClient {

	/**
	 * 客户端，类似于HTTP
	 */
	private final RestTemplate restTemplate;

	/**
	 * 
	 */
	public DefaultClient() {
		super();
		this.restTemplate = ClientFactory.defaultClient().getRestTemplate();
	}

	/**
	 * @return the restTemplate
	 */
	protected RestTemplate getRestTemplate() {
		return restTemplate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sqsoft.mars.qyweixin.client.QyweixinClient#execute(com.sqsoft.mars
	 * .qyweixin.client.QyweixinRequest)
	 */
	@Override
	public <T extends Response> T execute(Request<T> request) throws DefaultClientException {
		HttpMethod httpMethod = request.getHttpMethod();
		switch (httpMethod) {
		case GET:
			return doGet(request);
		case POST:
			return doPost(request);
		case DELETE:
		case PUT:
		default:
			throw new UnsupportedOperationException(httpMethod.name());
		}
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws DefaultClientException
	 */
	public <T extends Response> T doGet(Request<T> request) throws DefaultClientException {
		Class<String> responseType = String.class;
		Map<String, Object> urlVariables = new HashMap<>();
		urlVariables.put("method", request.getMethodScope());
		ResponseEntity<String> entity = getRestTemplate().getForEntity(getUrl(), responseType, urlVariables);
		if (entity.getStatusCode().is2xxSuccessful()) {
			return fromJson(entity.getBody(), request.getResponseClass());
		} else {
			throw new DefaultClientException();
		}
	}

	/**
	 * 请求的资源路径
	 * 
	 * @return
	 */
	protected abstract String getUrl();

	/**
	 * 
	 * @param request
	 * @return
	 * @throws DefaultClientException
	 * @throws QyweixinErrCodeException
	 */
	public <T extends Response> T doPost(Request<T> request) throws DefaultClientException {
		Class<String> responseType = String.class;
		Map<String, Object> urlVariables = new HashMap<>();
		urlVariables.put("method", request.getMethodScope());

		Object str = request.getRequestMessage();
		ResponseEntity<String> entity = getRestTemplate().postForEntity(getUrl(), str, responseType, urlVariables);

		if (entity.getStatusCode().is2xxSuccessful()) {
			return fromJson(entity.getBody(), request.getResponseClass());
		} else {
			throw new DefaultClientException();
		}
	}

	/**
	 * 
	 * @param json
	 * @param classOfT
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T> T fromJson(String json, Class<?> classOfT) {
		return (T) JSON.parseObject(json, classOfT);
	}

}