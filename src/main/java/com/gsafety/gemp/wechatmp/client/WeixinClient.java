/**
 * 
 */
package com.gsafety.gemp.wechatmp.client;

import com.gsafety.gemp.wechatmp.client.exception.AbstractWeixinClientException;

/**
 * @author lenovo
 *
 */
public interface WeixinClient {

	/**
	 * 执行发送微信客户端请求
	 * 
	 * @param request
	 * @return
	 * @throws AbstractWeixinClientException
	 */
	public <T extends WeixinResponse> T execute(WeixinRequest<T> request) throws AbstractWeixinClientException;
}