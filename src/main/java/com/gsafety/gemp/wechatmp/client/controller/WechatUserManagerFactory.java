/**
 * 
 */
package com.gsafety.gemp.wechatmp.client.controller;

/**
 * 该类用于...，用于实现...等能力。
 *
 * @author yangbo
 *
 * @since 2020年2月14日 下午8:55:59
 *
 */
public class WechatUserManagerFactory {

	/**
	 * 
	 */
	private static WechatUserManager UNIQUE; 
	/**
	 * 
	 */
	private WechatUserManagerFactory() {
		super();
	}

	/**
	 * 
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月14日 下午8:56:18
	 *
	 * @return
	 */
	public static WechatUserManager getInstance() {
		if(null == UNIQUE) {
			UNIQUE = new LocalCacheWechatUserManager();
		}
		return UNIQUE;
	}
}
