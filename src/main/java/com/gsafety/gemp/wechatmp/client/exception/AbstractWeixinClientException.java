/**
 * 
 */
package com.gsafety.gemp.wechatmp.client.exception;

/**
 * @author yangbo
 * 
 * @since 2018-06-11 重构该部分代码更新集合
 *
 */
public abstract class AbstractWeixinClientException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 获取错误码
	 * 
	 * @return
	 */
	public abstract int getErrcode();

	/**
	 * 获取错误消息
	 * 
	 * @return
	 */
	public abstract String getErrmsg();
}