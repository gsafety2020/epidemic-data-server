/**
 * 
 */
package com.gsafety.gemp.wechatmp.client.exception;

/**
 * @author lenovo
 *
 */
public class WeixinErrCodeException extends AbstractWeixinClientException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	private final int errcode;

	/**
	 * 
	 */
	private final String errmsg;

	/**
	 * 
	 * @param errcode
	 * @param errmsg
	 */
	public WeixinErrCodeException(int errcode, String errmsg) {
		super();
		this.errcode = errcode;
		this.errmsg = errmsg;
	}

	@Override
	public int getErrcode() {
		return errcode;
	}

	@Override
	public String getErrmsg() {
		return errmsg;
	}

	@Override
	public String getMessage() {
		return "[" + this.errcode + "," + this.errmsg + "]";
	}

}