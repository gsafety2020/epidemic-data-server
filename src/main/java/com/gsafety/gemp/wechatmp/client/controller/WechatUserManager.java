/**
 * 
 */
package com.gsafety.gemp.wechatmp.client.controller;

import com.gsafety.gemp.wechatmp.client.controller.WechatAuthController.WechatUserinfo;

/**
 * 该类用于...，用于实现...等能力。
 *
 * @author yangbo
 *
 * @since 2020年2月14日 下午6:41:16
 *
 */
public interface WechatUserManager {

	/**
	 * 
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月14日 下午6:43:13
	 *
	 * @param sessionKey
	 * @param userInfo
	 */
	public void setUser(String sessionKey, WechatUserinfo userInfo);
	
	/**
	 * 
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月14日 下午6:43:31
	 *
	 * @param sessionKey
	 * @return
	 */
	public WechatUserinfo getUser(String sessionKey);
	
	/**
	 * 
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月22日 下午3:47:28
	 *
	 * @param nickname
	 * @return
	 */
	public String touchSessionKey(String nickname);
}
