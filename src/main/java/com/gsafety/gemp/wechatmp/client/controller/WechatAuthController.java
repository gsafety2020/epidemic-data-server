package com.gsafety.gemp.wechatmp.client.controller;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSON;
import com.gsafety.gemp.wechatmp.client.exception.CustomException;
import com.gsafety.gemp.wechatmp.client.exception.DefaultClientException;
import com.gsafety.gemp.wechatmp.client.sns.Jscode2sessionRequest;
import com.gsafety.gemp.wechatmp.client.sns.Jscode2sessionResponse;
import com.gsafety.gemp.wechatmp.client.sns.SnsClient;
import com.gsafety.gemp.wechatmp.utils.Pkcs7Encoder;
import com.gsafety.gemp.wuhanncov.constant.PestWechatUserStatus;
import com.gsafety.gemp.wuhanncov.contract.dto.CodeBasDistrictDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.PestHealthCodeDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.PestHealthCodeSaveDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.Result;
import com.gsafety.gemp.wuhanncov.contract.dto.in.PestHealthCodeReportDTO;
import com.gsafety.gemp.wuhanncov.contract.service.PestHealthCodeService;
import com.gsafety.gemp.wuhanncov.contract.service.PestWechatUserService;
import com.gsafety.gemp.wuhanncov.dao.po.PestWechatUserPO;
import com.gsafety.gemp.wuhanncov.wrapper.PestHealthCodeWrapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 该类用于...，用于实现...等能力。
 *
 * @author yangbo
 *
 * @since 2020年2月13日 下午9:58:49
 *
 */
@Api(value = "微信小程序用户信息接口", tags = { "微信小程序" })
@RestController
@RequestMapping("/wechatmp/oauthor2/")
@Slf4j
public class WechatAuthController {

	/**
	 * 
	 */
	private static final String APPID = "wx350fdeae24c17b03";

	/**
	 * 
	 */
	private static final String SECRET = "440c653d554cb0500f145e665b266c2a";
	
	@Autowired
	private PestHealthCodeService pestHealthCodeService;
	
	@Autowired
	private PestWechatUserService pestWechatUserService;

	/**
	 * 
	 * 授权获取用户信息
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月13日 下午10:08:07
	 *
	 * @param loginInf
	 * @return
	 * @throws DefaultClientException
	 * @throws CustomException
	 * @throws IOException
	 */
	@ApiOperation(value = "小程序用户登录")
	@PostMapping("/login")
	public Result<WechatUserinfo> login(@RequestBody LoginInf loginInf)
			throws DefaultClientException, CustomException, IOException {
		Result<WechatUserinfo> result = new Result<>();
		try {
			String jscode = loginInf.jscode;
			SnsClient client = new SnsClient();
			Jscode2sessionRequest request = new Jscode2sessionRequest(APPID, SECRET, jscode);
			Jscode2sessionResponse response = client.execute(request);
			if (response.getErrcode() != 0) {
				throw new CustomException(response.getErrcode(), response.getErrmsg());
			}
			// 通过code获取用户敏感信息
			String sessionKey = response.getSessionKey();
			WechatUserinfo userInfo = new WechatUserinfo();
			userInfo.setOpenid(response.getOpenid());
			userInfo.setSessionKey(sessionKey);
			log.info("小程序用户登录[{}]成功", userInfo);
			// 将openid,sessionKey写入session
			WechatUserManagerFactory.getInstance().setUser(sessionKey, userInfo);
			
			//记录微信登陆信息
			recordLoginWechatUserMessage(userInfo);
			return result.success(userInfo);
		} catch (Exception e) {
			log.error("小程序用户登录[{}]失败", loginInf, e);
			return result.fail("小程序用户登录失败");
		}
	}
	
	/**
	 * 
	 * 获取小程序用户信息
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月15日 下午2:31:35
	 *
	 * @param authorInf
	 * @return
	 * @throws DefaultClientException
	 * @throws CustomException
	 * @throws IOException
	 */
	@ApiOperation(value = "获取小程序用户信息")
	@PostMapping("/getUserInfo")
	public Result<WechatUserinfo> getUserInfo(@RequestBody SecretInf authorInf)
			throws DefaultClientException, CustomException, IOException {
		log.info("获取小程序用户信息[{}]开始", authorInf);
		Result<WechatUserinfo> result = new Result<>();
		try {
			String sessionKey = authorInf.getSessionKey();
			log.info("获取小程序用户信息[{}][{}]解密", sessionKey, authorInf);
			String userinfo = Pkcs7Encoder.toStr(sessionKey, authorInf.encryptedData, authorInf.iv);
			WechatUserinfo userInfo = JSON.parseObject(userinfo, WechatUserinfo.class);
			log.info("获取小程序用户信息[{}]成功", userInfo);
			// 将详细信息写入session
			WechatUserManagerFactory.getInstance().setUser(sessionKey, userInfo);
			userInfo.setSessionKey(sessionKey);
			//更新授权状态
			try{
				PestWechatUserPO po = pestWechatUserService.getPestWechatUserById(userInfo.getOpenid());
				if(po != null){
					pestWechatUserService.authorizeUser(po,userInfo);
				}
			}catch(Exception e){
				log.error("用户信息保存[{}]失败", e);
			}
			
			return result.success(userInfo);
		} catch (Exception e) {
			log.error("获取小程序用户信息[{}]失败", authorInf, e);
			return result.fail("获取小程序用户信息失败");
		}
	}

	/**
	 * 
	 * 用户授权后，获取微信用户手机号
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月14日 上午9:54:17
	 *
	 * @param authorInf
	 * @param session
	 * @return
	 * @throws DefaultClientException
	 * @throws CustomException
	 * @throws IOException
	 */
	@ApiOperation(value = "用户授权后，获取微信用户手机号")
	@PostMapping("/getMobile")
	public Result<MobileInf> getMobile(@RequestBody SecretInf authorInf)
			throws DefaultClientException, CustomException, IOException {
		log.info("获取微信用户手机号[{}]", authorInf);
		Result<MobileInf> result = new Result<>();
		try {
			String sessionKey = authorInf.getSessionKey();
			String mobile = Pkcs7Encoder.toStr(sessionKey, authorInf.encryptedData, authorInf.iv);
			log.info("获取微信用户手机号[{}]成功", mobile);
			MobileInf m = JSON.parseObject(mobile, MobileInf.class);
			return result.success(m);
		} catch (Exception e) {
			log.error("获取微信用户手机号[{}]失败", authorInf, e);
			return result.fail("获取微信用户手机号失败");
		}
	}
		
	@ApiOperation(value = "获取市名")
	@PostMapping("/getCity")
	public Result<?> getCity(){
		Result<List<CodeBasDistrictDTO>> result = new Result<List<CodeBasDistrictDTO>>();
		try{
			List<CodeBasDistrictDTO> dto = pestHealthCodeService.getObjectByParentCode();
			return result.success("查询成功!", dto);
		}catch(Exception e){
			log.error("获取市名失败", e);
			return result.fail(e.getMessage());
		}
	}
	
	@ApiOperation(value = "保存更新健康码信息")
	@PostMapping("/saveOrUpateHealthCode")
	public Result<?> saveOrUpateHealthCode(@RequestBody PestHealthCodeReportDTO dto){
		Result<PestHealthCodeSaveDTO> result = new Result<>();
		try{
			PestHealthCodeSaveDTO saveDto = pestHealthCodeService.saveUpdate(dto);
			return result.success("保存更新成功!",saveDto);
		}catch(Exception e){
			log.error("保存更新健康码信息[{}]失败",dto,e);
			return result.fail(e.getMessage());
		}
	}
	
	@ApiOperation(value = "查询用户在某地区健康码信息是否存在")
	@PostMapping("/existsHealthCode")
	public Result<?> existsHealthCode(String sessionKey,String areaCode){
		Result<PestHealthCodeSaveDTO> result = new Result<>();
		try{
			if(StringUtils.isEmpty(areaCode)){
				throw new Exception("地区编码不能为空!");
			}
			//获取微信用户信息
			WechatUserManager wechatUserManager = WechatUserManagerFactory.getInstance();
			WechatUserinfo userInfo =wechatUserManager.getUser(sessionKey);
			if(userInfo == null){
				throw new Exception("登陆失效,请重新登陆!");
			}
			String openId = userInfo.getOpenid();  //小程序openId
			PestHealthCodeDTO dto = pestHealthCodeService.getEffectiveHealthCodeByOpenIdAndAreaCode(openId, areaCode);
			if(dto == null){
				return result.success("查询成功", null);
			}else{
				return result.success("查询成功", PestHealthCodeWrapper.dtoToSaveDto(dto));
			}	
		}catch(Exception e){
			log.error("查询用户在某地区健康码信息是否存在[sessionKey ,{}][areaCode,{}]失败",sessionKey,areaCode,e);
			return result.fail(e.getMessage());
		}
	}

	@ApiOperation(value = "扫码查询用户健康码信息")
	@PostMapping("/scanHealthCode")
	public Result<?> existsHealthCode(String healthCodeId){
		Result<PestHealthCodeDTO> result = new Result<>();
		try{
			PestHealthCodeDTO dto = pestHealthCodeService.getHealthCodeById(healthCodeId);
			return result.success("查询成功!", dto);
		}catch(Exception e){
			log.error("扫码查询用户健康码信息查询[{}]失败","healthCodeId="+healthCodeId,e);
			return result.fail(e.getMessage());
		}
	}
	
	@ApiOperation(value = "获取指定用户的sessionKey")
	@PostMapping("/touchSessioinKey")
	public Result<String> touchSessioinKey(String nickname){
		Result<String> result = new Result<>();
		return result.success(WechatUserManagerFactory.getInstance().touchSessionKey(nickname));
	}
	
	@ApiOperation(value = "删除健康码信息")
	@PostMapping("/removeHealthCode")
	public Result<String> removeHealthCode(String healthCodeId){
		Result<String> result = new Result<>();
		try{
			pestHealthCodeService.removeHealthCode(healthCodeId);
			return result.success("删除成功!");
		}catch(Exception e){
			log.error("删除健康码[healthCodeId ,{}]失败",healthCodeId,e);
			return result.fail("删除失败!");
		}
	}
	
	
	
	
	private void recordLoginWechatUserMessage(WechatUserinfo userInfo){
		PestWechatUserPO po = new PestWechatUserPO();
		po.setOpenId(userInfo.getOpenid());
		po.setGender(PestWechatUserStatus.GenderEnum.UNKONW.getCode());
		po.setWithCredentials(PestWechatUserStatus.AuthorizeEnum.NOT_AUTHORIZE.getCode());
		po.setFirstAccessTime(Calendar.getInstance().getTime());
		po.setUpdateTime(Calendar.getInstance().getTime());
		po.setAppid(APPID);
		pestWechatUserService.saveUpdate(po);
	}
	
	/**
	 * 
	 * 该类用于...，用于实现...等能力。
	 *
	 * @author yangbo
	 *
	 * @since 2020年2月14日 下午9:17:19
	 *
	 */
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	static class SecretInf {
		/**
		 * 
		 */
		@ApiModelProperty(value = "sessionKey", required = true, example = "sessionKey", dataType = "String")
		private String sessionKey;
		/**
		 * 
		 */
		@ApiModelProperty(value = "encryptedData", required = true, example = "encryptedData", dataType = "String")
		private String encryptedData;
		/**
		 * 
		 */
		@ApiModelProperty(value = "iv", required = true, example = "iv", dataType = "String")
		private String iv;
	}

	/**
	 * 
	 * 该类用于...，用于实现...等能力。
	 *
	 * @author yangbo
	 *
	 * @since 2020年2月14日 下午9:24:10
	 *
	 */
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	static class MobileInf {
		/**
		 * 
		 */
		@ApiModelProperty(value = "电话号码")
		private String phoneNumber;
		/**
		 * 
		 */
		@ApiModelProperty(value = "纯数字电话号码")
		private String purePhoneNumber;
		/**
		 * 
		 */
		@ApiModelProperty(value = "区号")
		private String countryCode;
		/**
		 * 
		 */
		@ApiModelProperty(value = "水印")
		private Watermark watermark;
	}

	/**
	 * 
	 * 登录用户请求信息
	 *
	 * @author yangbo
	 *
	 * @since 2020年2月15日 下午2:28:53
	 *
	 */
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	static class LoginInf {
		/**
		 * 
		 */
		@ApiModelProperty(value = "jscode", required = true, example = "jscode", dataType = "String")
		private String jscode;
		
	}
	
	/**
	 * 
	 * 该类用于...，用于实现...等能力。
	 *
	 * @author yangbo
	 *
	 * @since 2020年2月13日 下午10:07:02
	 *
	 */
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	static class AuthorInf {
		/**
		 * 
		 */
		@ApiModelProperty(value = "jscode", required = true, example = "jscode", dataType = "String")
		private String jscode;
		/**
		 * 
		 */
		@ApiModelProperty(value = "encryptedData", required = true, example = "encryptedData", dataType = "String")
		private String encryptedData;
		/**
		 * 
		 */
		@ApiModelProperty(value = "iv", required = true, example = "iv", dataType = "String")
		private String iv;
	}

	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class WechatUserinfo {
		/**
		 * 
		 */
		private String nickName;
		/**
		 * 
		 */
		private String avatarUrl;
		/**
		 * 
		 */
		private String gender;
		/**
		 * 
		 */
		private String province;
		/**
		 * 
		 */
		private String city;
		/**
		 * 
		 */
		private String country;
		/**
		 * 
		 */
		private String unionId;
		/**
		 * 
		 */
		private Watermark watermark;

		/**
		 * 
		 */
		private String openid;

		/**
		 * 
		 */
		private String sessionKey;

	}

	/**
	 * 小程序水印
	 * 
	 * @author yangbo
	 *
	 */
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	static class Watermark {
		/**
		 * 
		 */
		private long timestamp;

		/**
		 * 
		 */
		private String appid;
	}

}