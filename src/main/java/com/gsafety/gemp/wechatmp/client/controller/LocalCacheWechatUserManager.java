/**
 * 
 */
package com.gsafety.gemp.wechatmp.client.controller;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.gsafety.gemp.wechatmp.client.controller.WechatAuthController.WechatUserinfo;

/**
 * 该类用于...，用于实现...等能力。
 *
 * @author yangbo
 *
 * @since 2020年2月14日 下午6:44:26
 *
 */
public class LocalCacheWechatUserManager implements WechatUserManager {

	/**
	 * 
	 */
	private Cache<String, WechatUserinfo> cache = CacheBuilder.newBuilder().maximumSize(2048)
			.expireAfterWrite(360, TimeUnit.SECONDS).build();

	/**
	 * 
	 */
	private static final String[] INGORE_FIELD = new String[] { "openid", "sessionKey" };

	@Override
	public void setUser(String sessionKey, WechatUserinfo userInfo) {
		WechatUserinfo u = cache.getIfPresent(sessionKey);
		if (null != u) {
			// 属性对拷时忽略openid，sessionKey
			BeanUtils.copyProperties(userInfo, u, INGORE_FIELD);
			cache.put(sessionKey, u);
		} else {
			// 更新一个用户
			cache.put(sessionKey, userInfo);
		}
	}

	@Override
	public WechatUserinfo getUser(String sessionKey) {
		WechatUserinfo u = cache.getIfPresent(sessionKey);
		cache.put(sessionKey, u);
		return u;
	}

	@Override
	public String touchSessionKey(String nickname) {
		ConcurrentMap<String, WechatUserinfo> map = cache.asMap();
		Set<Entry<String, WechatUserinfo>> set = map.entrySet();
		Iterator<Entry<String, WechatUserinfo>> it = set.iterator();
		while(it.hasNext()) {
			Entry<String, WechatUserinfo> entry = it.next();
			WechatUserinfo info = entry.getValue();
			if(StringUtils.equals(nickname, info.getNickName())) {
				return entry.getKey();
			}
		}
		return null;
	}

}
