/**
 * 
 */
package com.gsafety.gemp.wechatmp.client;

import com.gsafety.gemp.wechatmp.client.exception.DefaultClientException;

/**
 * @author lenovo
 *
 */
public interface LooseClient {

	/**
	 * 
	 * @param request
	 * @return
	 */
	public <T extends Response> T execute(Request<T> request) throws DefaultClientException;
}