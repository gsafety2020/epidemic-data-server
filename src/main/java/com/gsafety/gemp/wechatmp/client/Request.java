/**
 * 
 */
package com.gsafety.gemp.wechatmp.client;

import org.springframework.http.HttpMethod;

/**
 * @author yangbo
 *
 */
public interface Request<T extends Response> {

	/**
	 * 请求报文头(get,post,delete,put...)
	 * 
	 * @return
	 */
	public HttpMethod getHttpMethod();

	/**
	 * 方法的动作（gettoken,send...）
	 * 
	 * @return
	 */
	public String getMethodScope();

	/**
	 * 请求响应类型
	 * 
	 * @return
	 */
	public Class<T> getResponseClass();

	/**
	 * 请求报文 json类型
	 * 
	 * @return
	 */
	public Object getRequestMessage();
}