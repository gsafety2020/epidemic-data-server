package com.gsafety.gemp.wechatmp.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.Arrays;

import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author ngh AES128 算法
 *
 *         CBC 模式
 *
 *         PKCS7Padding 填充模式
 *
 *         CBC模式需要添加一个参数iv--对称解密算法初始向量 iv
 *
 *         介于java 不支持PKCS7Padding，只支持PKCS5Padding 但是PKCS7Padding 和 PKCS5Padding
 *         没有什么区别 要实现在java端用PKCS7Padding填充，需要用到bouncycastle组件来实现
 * 
 * 
 *         该类会报错因为引入 org.bouncycastle.jce.provider.BouncyCastleProvider;
 * 
 *         该类通常会放到jre目录下使用，因此该包不做上传处理
 * 
 * 
 */
@Slf4j
public class Pkcs7Encoder {
	/**
	 *  算法名称
	 */
	static final String KEY_ALGORITHM = "AES";
	/**
	 *  加解密算法/模式/填充方式
	 */
	static final String ALGORITHMSTR = "AES/CBC/PKCS7Padding";

	/**
	 * 
	 */
	static final String DEFAULT_CHARSET = "utf-8";

	/**
	 * 
	 */
	private static Key key;
	
	/**
	 * 
	 */
	private static Cipher cipher;
	
	/**
	 * 
	 */
	boolean isInited = false;

	/**
	 *  默认对称解密算法初始向量 iv
	 */
	static byte[] iv = { 0x30, 0x31, 0x30, 0x32, 0x30, 0x33, 0x30, 0x34, 0x30, 0x35, 0x30, 0x36, 0x30, 0x37, 0x30,
			0x38 };

	/**
	 * 
	 * @param keyBytes
	 */
	public static void init(byte[] keyBytes) {
		// 如果密钥不足16位，那么就补足. 这个if 中的内容很重要
		int base = 16;
		if (keyBytes.length % base != 0) {
			int groups = keyBytes.length / base + (keyBytes.length % base != 0 ? 1 : 0);
			byte[] temp = new byte[groups * base];
			Arrays.fill(temp, (byte) 0);
			System.arraycopy(keyBytes, 0, temp, 0, keyBytes.length);
			keyBytes = temp;
		}
		// 初始化
		Security.addProvider(new BouncyCastleProvider());
		// 转化成JAVA的密钥格式
		key = new SecretKeySpec(keyBytes, KEY_ALGORITHM);
		try {
			// 初始化cipher
			cipher = Cipher.getInstance(ALGORITHMSTR, "BC");
		} catch (NoSuchAlgorithmException e) {
			log.error("Pkcs7Encoder init with NoSuchAlgorithmException", e);
		} catch (NoSuchPaddingException e) {
			log.error("Pkcs7Encoder init with NoSuchPaddingException", e);
		} catch (NoSuchProviderException e) {
			log.error("Pkcs7Encoder init with NoSuchProviderException", e);
		}
	}

	/**
	 * 加密方法 --使用默认iv时
	 * 
	 * @param content
	 *            要加密的字符串
	 * @param keyBytes
	 *            加密密钥
	 * @return
	 */
	public static byte[] encrypt(byte[] content, byte[] keyBytes) {
		return encryptOfDiyIV(content, keyBytes, iv);
	}

	/**
	 * 解密方法 --使用默认iv时
	 * 
	 * @param encryptedData
	 *            要解密的字符串
	 * @param keyBytes
	 *            解密密钥
	 * @return
	 */
	public static byte[] decrypt(byte[] encryptedData, byte[] keyBytes) {
		return decryptOfDiyIV(encryptedData, keyBytes, iv);
	}

	/**
	 * 加密方法 ---自定义对称解密算法初始向量 iv
	 * 
	 * @param content
	 *            要加密的字符串
	 * @param keyBytes
	 *            加密密钥
	 * @param ivs
	 *            自定义对称解密算法初始向量 iv
	 * @return 加密的结果
	 */
	public static byte[] encryptOfDiyIV(byte[] content, byte[] keyBytes, byte[] ivs) {
		byte[] encryptedText = null;
		init(keyBytes);
		try {
			cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(ivs));
			encryptedText = cipher.doFinal(content);
		} catch (Exception e) {
			log.error("Pkcs7Encoder encryptOfDiyIV with Exception", e);
		}
		return encryptedText;
	}

	/**
	 * 解密方法
	 *
	 * @param encryptedData
	 *            要解密的字符串
	 * @param keyBytes
	 *            解密密钥
	 * @param ivs
	 *            自定义对称解密算法初始向量 iv
	 * @return
	 */
	public static byte[] decryptOfDiyIV(byte[] encryptedData, byte[] keyBytes, byte[] ivs) {
		byte[] encryptedText = null;
		init(keyBytes);
		try {
			cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(ivs));
			encryptedText = cipher.doFinal(encryptedData);
		} catch (Exception e) {
			log.error("Pkcs7Encoder decryptOfDiyIV with Exception", e);
		}
		return encryptedText;
	}

	public static void main(String[] args) throws IOException {
		String encryptedData = "IGdb7G0WIkKrCs0Sova479gR6kFD3uZB/E5uqwoUfa+bmykd+WFhm7SpZAuc/I8udNxRTW///T/dMjbiv03lIGCU3jbI0FWmJDrpyoSP0O4RiGORoZ6OkE+CSP+uUtbCoVyBGEohpdytPQd6fWI6v/3zyp2x4AA7BYGYgonUvz+pLAFSJ2p9kuvJXqRIkIoeYvn7CqGuEcm6zcT8fFH3zk+ZfyYRtPXLW+ebp11ZnbOIKBxW48XYopKjpVLF2FJfymufmZDgGV4yDqW/dNQ6hb6aqlOQUZ6QKUE+8lmH3KA+aXKfSWgyZzkB0ttbDZrJ16mAH+4eLMNtkrExeFYPUQGGGCXPzPvMtAPH8G3Lt8r3CBkV1ttO/l/PE5jR0GM5xUuL7HyGKiNEHTKS1SyQAp0LYnqYMBVeA4hhb9qP2KxajS2Xs7pFJcnwhdwGx+aVPRwaEbweexgaTs2zEEwiJ2O0iJtiDtxBSSV31TbWEed7KSt2iHkseBC33p7wZsYX3tDVoJ3k+dPRppsaGlYBf5te2C36SyxAiZaIW/WG/w8=";
		String iv = "6xzpBN+gHheKNoZ5Ld+sUw==";
		// String appid = "wx5bfdc9872309b425";
		String sessionKey = "A+IQIpmExZpbsMepVEKYmQ==";
		System.out.println(toStr(sessionKey, encryptedData, iv));
	}

	/**
	 * 
	 * @param sessionKey
	 * @param encryptedData
	 * @param iv
	 * @return
	 * @throws IOException
	 */
	public static String toStr(String sessionKey, String encryptedData, String iv) throws IOException {
		byte[] sessionKeyBy = Base64.decodeBase64(sessionKey.getBytes());
		byte[] encryptedDataBy = Base64.decodeBase64(encryptedData.getBytes());
		byte[] ivBy = Base64.decodeBase64(iv.getBytes());
		byte[] dec = Pkcs7Encoder.decryptOfDiyIV(encryptedDataBy, sessionKeyBy, ivBy);
		return IOUtils.toString(new ByteArrayInputStream(dec), DEFAULT_CHARSET);
	}

//	public static void main(String[] args) throws IOException {
//		String sessionKey = "Rf9hwKv17Ah93moUXuKRUg==";
//		String encryptedData = "MkCjcKxaVjok1NOhMg+RohmhzgJIGDcDJbl8iGGBNJ6WBGMpaSW1YleXHw4su8K8uscYvfvXXSG63lS1dst/7twu89D3WTuYh/yzdWlebfbA1TvvyaDMqQl8/A1jXeCK+hBErjleMCm8zp//NCFs+2yA6urNQt5GdzNIoxc0fHv1rtwMUvrKG4rWk7uL6HhcPOnPgj0GIW2btYbdexERS+Yy9yhov25Il7yVBskhX3QEnZMt4Dyf9oP7Ga8oYmyHMN4bY6z5+JKRzJ8i50Z0UW9R6skrLwnKHYbYB/Fz+8aFsp0Af459U0RQj5y3Az0EYk5tDzTyIqtjkjLVRcqVm/b9UwtMQLmVaNvjMklp7QExUwxaL8yF9nNO8hL8sgdVKyeOlRSUndZMIbr05PzJ6ziTwJuWzgrAMXYN+EYAwRY+DPg3e9AMcF4eHmHFHgiNZfYWACEElfuHHdqBOzmq5dWFYY0QJdepLhtOfDD0UWY=";
//		String iv = "p5M24d6qwIz5LZ95XQ5h4A==";
//		System.out.println(toStr(sessionKey, encryptedData, iv));
//	}
}