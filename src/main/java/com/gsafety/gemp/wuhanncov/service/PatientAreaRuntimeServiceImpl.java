package com.gsafety.gemp.wuhanncov.service;

import static com.gsafety.gemp.wuhanncov.service.PatientAreaRuntimeSpecification.FIELD_AREACODE;
import static com.gsafety.gemp.wuhanncov.service.PatientAreaRuntimeSpecification.FIELD_CONFIRMCASE;
import static com.gsafety.gemp.wuhanncov.service.PatientAreaRuntimeSpecification.areaCodeEqual;
import static com.gsafety.gemp.wuhanncov.service.PatientAreaRuntimeSpecification.parentNo;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.constraints.NotNull;

import com.gsafety.gemp.wuhanncov.contract.dto.*;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.gsafety.gemp.wuhanncov.contract.params.DimQueryParam;
import com.gsafety.gemp.wuhanncov.contract.service.DimService;
import com.gsafety.gemp.wuhanncov.contract.service.PatientAreaRuntimeService;
import com.gsafety.gemp.wuhanncov.dao.LocationDimDao;
import com.gsafety.gemp.wuhanncov.dao.PatientAreaRuntimeDao;
import com.gsafety.gemp.wuhanncov.dao.po.LocationDimPO;
import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaRuntimePO;
import com.gsafety.gemp.wuhanncov.utils.DateUtils;
import com.gsafety.gemp.wuhanncov.utils.RegExUtil;
import com.gsafety.gemp.wuhanncov.wrapper.PatientAreaRuntimeWrapper;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;

/**
 * 区域病患实时统计报表
 *
 * @author yangbo
 *
 * @since 2020年1月30日 下午9:02:47
 *
 */
@Service("patientAreaRuntimeService")
public class PatientAreaRuntimeServiceImpl implements PatientAreaRuntimeService {

	@Resource
	private PatientAreaRuntimeDao patientAreaRuntimeDao;

	@Resource
	private LocationDimDao locationDimDao;

	@Resource
	private DimClassifier dimClassifier;

	@Override
	public ChinaCountStatisticsJSON findByAreaCode(String areaCode) {
		List<PatientAreaRuntimePO> list = this.patientAreaRuntimeDao.findAll(areaCodeEqual(areaCode));
		if (CollectionUtils.isEmpty(list)) {
			return new ChinaCountStatisticsJSON();
		}
		List<ChinaCountStatisticsJSON> listResult = new ArrayList<>();
		list.stream().forEach(temp -> {
			ChinaCountStatisticsJSON count=PatientAreaRuntimeWrapper.toChinaCountStatisticsJSON(temp);
			String areaId=temp.getAreaCode();
			LocationDimPO dimPO=locationDimDao.findByAreaNo(areaId);
			if(dimPO!=null){
				count.setAreaShortName(dimPO.getShortName());
			}
			listResult.add(count);
		});
		return listResult.iterator().next();
	}

	@Override
	public List<ChinaCountStatisticsJSON> findByParentNo(String areaCode) {
		List<PatientAreaRuntimePO> list = this.patientAreaRuntimeDao.findAll(parentNo(areaCode),
				Sort.by(Order.desc(FIELD_CONFIRMCASE), Order.asc(FIELD_AREACODE)));
		List<ChinaCountStatisticsJSON> listResult = new ArrayList<>();
		list.stream().forEach(temp -> {
			ChinaCountStatisticsJSON count=PatientAreaRuntimeWrapper.toChinaCountStatisticsJSON(temp);
			String areaId=temp.getAreaCode();
			LocationDimPO dimPO=locationDimDao.findByAreaNo(areaId);
			if(dimPO!=null){
				count.setAreaShortName(dimPO.getShortName());
			}
			listResult.add(count);
		});
		return listResult;
	}

	@Override
	public Page<PatientAreaRuntimeDTO> findPatientRuntimePage(DimQueryParam dimQuery, Pageable pageable) {
		Page<PatientAreaRuntimePO> page = patientAreaRuntimeDao.findAll((Specification<PatientAreaRuntimePO>) (root, criteriaQuery, cb) -> {
			if (null != dimQuery) {
				List<Predicate> list = new ArrayList<>();
				if (StringUtils.isNotBlank(dimQuery.getArea())) {
					list.add(cb.like(root.get("areaName"), "%" + dimQuery.getArea() + "%"));
				}

				if (StringUtils.isNotBlank(dimQuery.getAreaNo())) {
					list.add(cb.or(cb.equal(root.get("parentNo"), dimQuery.getAreaNo()),
							cb.equal(root.get("areaCode"), dimQuery.getAreaNo())));
				}

				Predicate[] p = new Predicate[list.size()];
				return cb.and(list.toArray(p));
			}

			return null;
		}, pageable);

		return po2DTO(page);
	}

	@Override
	public Result patientRuntimeSave(PatientAreaRuntimeDTO dto) {
		Result result = patientRuntimeValid(dto.getAreaCode());
		if (StringUtils.equals(Result.DEFAULT_SUCCESS_MSG, result.getMsg())) {
			DimService dimService = dimClassifier.Sort(dto.getAreaCode());
			if ( null != dimService) {
				PatientAreaRuntimePO po = dto2Po(dto, dimService);
				return new Result().success("保存成功", patientAreaRuntimeDao.save(po).getId());
			}
		} else {
			//返回校验错误信息
			return result;
		}

		return new Result().fail("保存失败");
	}

	@Override
	public Result patientRuntimeUpdate(PatientAreaRuntimeDTO dto) {
		PatientAreaRuntimePO po = patientAreaRuntimeDao.getOne(dto.getId());
		po.setUpdateTime(DateUtils.localDateTime2Date(LocalDateTime.now()));
		BeanUtil.copyProperties(dto, po, true, CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));

		return new Result().success(Result.DEFAULT_SUCCESS_MSG, patientAreaRuntimeDao.save(po).getId());
	}

	@Override
	public Result patientRuntimeLock(PatientAreaRuntimeDTO dto) {
		PatientAreaRuntimePO po = patientAreaRuntimeDao.getOne(dto.getId());
		po.setUpdateTime(DateUtils.localDateTime2Date(LocalDateTime.now()));
		po.setLockFlag(PatientAreaRuntimePO.LockFlag.LOCK);

		return new Result().success(Result.DEFAULT_SUCCESS_MSG, patientAreaRuntimeDao.save(po).getId());
	}

	@Override
	public Result patientRuntimeUnlock(PatientAreaRuntimeDTO dto) {
		PatientAreaRuntimePO po = patientAreaRuntimeDao.getOne(dto.getId());
		po.setUpdateTime(DateUtils.localDateTime2Date(LocalDateTime.now()));
		po.setLockFlag(PatientAreaRuntimePO.LockFlag.UNLOCK);

		return new Result().success(Result.DEFAULT_SUCCESS_MSG, patientAreaRuntimeDao.save(po).getId());
	}

	@Override
	public List<AreaHotDTO> findMapByParentAreaCode(List<String> areaIdList) {
		List<AreaHotDTO> list=new ArrayList<AreaHotDTO>();
		Sort sort = Sort.by(Sort.Direction.DESC, "confirmCase");
		List<PatientAreaRuntimePO> poList =  patientAreaRuntimeDao.findAll(new Specification<PatientAreaRuntimePO>() {
				@Override
				public Predicate toPredicate(Root<PatientAreaRuntimePO> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder builder) {
					return builder.and(root.get("parentNo").in(areaIdList));
				}
			}, sort);
		for(PatientAreaRuntimePO po:poList){
			AreaHotDTO areaHotDTO=new AreaHotDTO();
			BeanUtils.copyProperties(po,areaHotDTO);
			areaHotDTO.setArea_id(po.getAreaCode());
			areaHotDTO.setValue(po.getConfirmCase());
			String areaNo=po.getAreaCode();
			LocationDimPO locationDim=locationDimDao.findByAreaNo(areaNo);
			if(locationDim!=null){
				areaHotDTO.setLat(locationDim.getLatitude());
				areaHotDTO.setLng(locationDim.getLongitude());
				list.add(areaHotDTO);
			}
		}
		return list;
	}

	@Override
	public Result patientRuntimeValid(@NotNull String areaCode) {
		if (!RegExUtil.isChinaDim(areaCode) || !locationDimDao.existsById(areaCode.substring(0, 6))) {
			return new Result().fail("错误的行政区划编码");
		}

		if(patientAreaRuntimeDao.existsById(areaCode)) {
			return new Result().fail("已存在该地区数据，请进行修改操作来维护数据");
		}

		return new Result().success();
	}


	/**
	 * 分页po转dto
	 * @param page
	 * @return
	 */
	private Page<PatientAreaRuntimeDTO> po2DTO(Page<PatientAreaRuntimePO> page){
		List<PatientAreaRuntimeDTO> content = po2DTO(page.getContent());
		return new PageImpl<>(content, page.getPageable(), page.getTotalElements());
	}

	/**
	 * 集合po转dto
	 * @param list
	 * @return
	 */
	private List<PatientAreaRuntimeDTO> po2DTO(List<PatientAreaRuntimePO> list){
		List<PatientAreaRuntimeDTO> content = new ArrayList<>();
		Iterator<PatientAreaRuntimePO> it = list.iterator();
		while (it.hasNext()) {
			PatientAreaRuntimePO po = it.next();
			content.add(po2DTO(po));
		}

		return content;
	}

	/**
	 * 单个po转dto
	 * @param po
	 * @return
	 */
	private PatientAreaRuntimeDTO po2DTO(PatientAreaRuntimePO po){
		PatientAreaRuntimeDTO dto = new PatientAreaRuntimeDTO();
		BeanUtils.copyProperties(po, dto);
		if(StringUtils.isNotBlank(po.getParentNo())) {
			Optional<LocationDimPO> parent = locationDimDao.findById(po.getParentNo());
			if (parent.isPresent() && StringUtils.isNotBlank(parent.get().getAreaName())) {
				dto.setParent(parent.get().getAreaName());
			}
		}

		if(StringUtils.isNotBlank(po.getProvinceNo())) {
			Optional<LocationDimPO> province = locationDimDao.findById(po.getProvinceNo());
			if (province.isPresent() && StringUtils.isNotBlank(province.get().getAreaName())) {
				dto.setProvince(province.get().getAreaName());
			}
		}

		if(StringUtils.isNotBlank(po.getCityNo())) {
			Optional<LocationDimPO> city = locationDimDao.findById(po.getCityNo());
			if (city.isPresent() && StringUtils.isNotBlank(city.get().getAreaName())) {
				dto.setCity(city.get().getAreaName());
			}
		}

		if(PatientAreaRuntimePO.LockFlag.LOCK.equals(po.getLockFlag())){
            dto.setLockFlag("lock");
        }
        if(PatientAreaRuntimePO.LockFlag.UNLOCK.equals(po.getLockFlag())){
            dto.setLockFlag("unlock");
        }
		return dto;
	}

	/**
	 * dto转po对象
	 * @param dto
	 * @return
	 */
	private PatientAreaRuntimePO dto2Po(PatientAreaRuntimeDTO dto, DimService dimService){
		PatientAreaRuntimePO po = new PatientAreaRuntimePO();
		if(StringUtils.isNotBlank(dto.getId())) {
			Optional<PatientAreaRuntimePO> opo = patientAreaRuntimeDao.findById(dto.getId());
			if (opo.isPresent()) {
				po = opo.get();
			}
		} else {
			dto.setProvinceNo(dimService.getProvince());
			dto.setParentNo(dimService.getParent());
			dto.setCityNo(dimService.getCity());
			dto.setAreaType(dimService.getType());
			dto.setId(dto.getAreaCode());
		}

		BeanUtil.copyProperties(dto, po, true, CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
		return po;
	}


}
