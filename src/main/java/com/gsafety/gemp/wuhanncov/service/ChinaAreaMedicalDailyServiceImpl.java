/**
 * 
 */
package com.gsafety.gemp.wuhanncov.service;

import static com.gsafety.gemp.wuhanncov.specification.ChinaAreaMedicalDailySpecifications.areaCodeIs;
import static com.gsafety.gemp.wuhanncov.specification.ChinaAreaMedicalDailySpecifications.stasDateBetween;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.gsafety.gemp.wuhanncov.contract.params.DimQueryParam;
import com.gsafety.gemp.wuhanncov.contract.service.DimService;
import com.gsafety.gemp.wuhanncov.dao.LocationDimDao;
import com.gsafety.gemp.wuhanncov.utils.DateUtils;
import com.gsafety.gemp.wuhanncov.utils.RegExUtil;
import com.gsafety.gemp.wuhanncov.wrapper.ChinaAreaMedicalDailyWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.gsafety.gemp.wuhanncov.contract.dto.ChinaAreaMedicalDailyDTO;
import com.gsafety.gemp.wuhanncov.contract.service.ChinaAreaMedicalDailyService;
import com.gsafety.gemp.wuhanncov.dao.ChinaAreaMedicalDailyDao;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaAreaMedicalDailyPO;
import static com.gsafety.gemp.wuhanncov.wrapper.ChinaAreaMedicalDailyWrapper.toPageDTO;
import static com.gsafety.gemp.wuhanncov.specification.ChinaAreaMedicalDailySpecifications.areaNameLike;

/**
 * 各地区医院情况日报Service。实现类
 *
 * @author yangbo
 *
 * @since 2020年2月9日 上午2:50:37
 *
 */
@Slf4j
@Service
public class ChinaAreaMedicalDailyServiceImpl implements ChinaAreaMedicalDailyService {

	/**
	 * 
	 */
	@Resource
	private ChinaAreaMedicalDailyDao chinaAreaMedicalDailyDao;

	@Resource
	private LocationDimDao locationDimDao;

	@Resource
	private DimClassifier dimClassifier;

	@Override
	public List<ChinaAreaMedicalDailyDTO> findByStasDateBetween(String areaCode, String from, String to) {
		List<ChinaAreaMedicalDailyPO> records = this.chinaAreaMedicalDailyDao
				.findAll(areaCodeIs(areaCode).and(stasDateBetween(from, to)));
		return ChinaAreaMedicalDailyWrapper.toDTOList(records);
	}

	@Override
	public Page<ChinaAreaMedicalDailyDTO> findAreaMedicalDailyPage(DimQueryParam queryParam, Pageable pageable) {
		Page<ChinaAreaMedicalDailyPO> page = chinaAreaMedicalDailyDao.findAll(areaNameLike(queryParam.getArea()),pageable);
		return toPageDTO(page);
	}

	@Override
	public ChinaAreaMedicalDailyPO areaMedicalDailySave(ChinaAreaMedicalDailyDTO dto) {
		if (!RegExUtil.isChinaDim(dto.getAreaCode()) || !locationDimDao.existsById(dto.getAreaCode().substring(0, 6))) {
			log.error("错误的地区编码");
		}
		ChinaAreaMedicalDailyPO po = new ChinaAreaMedicalDailyPO();
		BeanUtil.copyProperties(dto, po, true, CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
		DimService dimService = dimClassifier.Sort(dto.getAreaCode());
		po.setParentNo(dimService.getParent());
		po.setProvinceNo(dimService.getProvince());
		po.setCityNo(dimService.getCity());
		po.setUpdateTime(DateUtils.localDateTime2Date(LocalDateTime.now()));

		String dateStr = DateUtils.format(new Date(),"yyyyMMdd");
		po.setId(dateStr + dto.getAreaCode());

		return chinaAreaMedicalDailyDao.save(po);
	}

	@Override
	public ChinaAreaMedicalDailyPO areaMedicalDailytUpdate(ChinaAreaMedicalDailyDTO dto) {
		ChinaAreaMedicalDailyPO po = chinaAreaMedicalDailyDao.getOne(dto.getId());
		BeanUtil.copyProperties(dto, po, true, CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
		po.setUpdateTime(DateUtils.localDateTime2Date(LocalDateTime.now()));

		return chinaAreaMedicalDailyDao.save(po);
	}

}
