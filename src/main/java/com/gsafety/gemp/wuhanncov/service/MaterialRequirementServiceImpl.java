package com.gsafety.gemp.wuhanncov.service;

import com.gsafety.gemp.wuhanncov.contract.dto.MaterialRequirementDTO;
import com.gsafety.gemp.wuhanncov.contract.service.MaterialRequirementService;
import com.gsafety.gemp.wuhanncov.dao.MaterialRequirementDao;
import com.gsafety.gemp.wuhanncov.dao.po.MaterialRequirementPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 物资需求 service
 *
 * @author zhangay
 * @date 2020-01-31 20:24:38
 */
@Service("materialRequirementService")
public class MaterialRequirementServiceImpl implements MaterialRequirementService {

    @Autowired
    MaterialRequirementDao materialRequirementDao;

    /**
     * 查询 指定区域及其下级区域的物资需求情况列表
     *
     * @param areaNo 区域代码
     * @return
     */
    @Override
    public List<MaterialRequirementDTO> selectIncludeSubByAreaNo(String areaNo) {
        List<MaterialRequirementPO> list = materialRequirementDao.findIncludeSubByAreaNo(areaNo);
        List<MaterialRequirementDTO> resultList = new ArrayList<>();
        for (MaterialRequirementPO po : list) {
            resultList.add(po2DTO(po));
        }
        return resultList;
    }

    private MaterialRequirementDTO po2DTO(MaterialRequirementPO po) {
        MaterialRequirementDTO dto = new MaterialRequirementDTO();
        BeanUtils.copyProperties(po, dto);
//        dto.setCreateTime(DateUtils.formatDateTime(po.getCreateTime()));
//        dto.setUpdateTime(DateUtils.formatDateTime(po.getUpdateTime()));
//        dto.setPublishDate(DateUtils.format(po.getPublishDate(),DateUtils.DATE_PATTERN));
        return dto;
    }
}
