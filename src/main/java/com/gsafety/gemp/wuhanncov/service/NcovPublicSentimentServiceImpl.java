package com.gsafety.gemp.wuhanncov.service;

import static com.gsafety.gemp.wuhanncov.service.MigrateSpecification.*;
import static com.gsafety.gemp.wuhanncov.service.pubSentimentNewsSpecification.newsContentLike;
import static com.gsafety.gemp.wuhanncov.service.pubSentimentNewsSpecification.newsTimeBetween;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.alibaba.fastjson.JSON;
import com.gsafety.gemp.wuhanncov.constant.AppConstant;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang.time.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.gsafety.gemp.wuhanncov.contract.dto.AreaHotDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.AreaInfo;
import com.gsafety.gemp.wuhanncov.contract.dto.ChinaCountStatisticsBubbling;
import com.gsafety.gemp.wuhanncov.contract.dto.ChinaCountStatisticsDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.EmotionAnalyzingContent;
import com.gsafety.gemp.wuhanncov.contract.dto.HotwordContent;
import com.gsafety.gemp.wuhanncov.contract.dto.HotwordDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.HotwordLevel;
import com.gsafety.gemp.wuhanncov.contract.dto.LocationHotDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.LocationValue;
import com.gsafety.gemp.wuhanncov.contract.dto.MigrateMapContent;
import com.gsafety.gemp.wuhanncov.contract.dto.MigratePeopleContent;
import com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaCountDailyDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.PubSensHeatDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.RealVideoPublicDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.RefreshPatientDailyCond;
import com.gsafety.gemp.wuhanncov.contract.dto.TextValue;
import com.gsafety.gemp.wuhanncov.contract.dto.WorldHotMap;
import com.gsafety.gemp.wuhanncov.contract.dto.WorldInfoStatisticsDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.WorldRealBroadcastDTO;
import com.gsafety.gemp.wuhanncov.contract.service.NcovPublicSentimentService;
import com.gsafety.gemp.wuhanncov.dao.ChinaCountStatisticsDao;
import com.gsafety.gemp.wuhanncov.dao.ChinaCountStatisticsHistoryDAO;
import com.gsafety.gemp.wuhanncov.dao.ChinaMigrationInfoDao;
import com.gsafety.gemp.wuhanncov.dao.ChinaRealBroadcastDao;
import com.gsafety.gemp.wuhanncov.dao.ChinaRealVideoPublicDao;
import com.gsafety.gemp.wuhanncov.dao.CountDao;
import com.gsafety.gemp.wuhanncov.dao.EmotionAnalyzingDao;
import com.gsafety.gemp.wuhanncov.dao.HotWordDao;
import com.gsafety.gemp.wuhanncov.dao.LocationDimDao;
import com.gsafety.gemp.wuhanncov.dao.LocationSentimentDailyDao;
import com.gsafety.gemp.wuhanncov.dao.PatientAreaRuntimeDao;
import com.gsafety.gemp.wuhanncov.dao.PatientAreaStatisticDao;
import com.gsafety.gemp.wuhanncov.dao.PubSentimentNewsDao;
import com.gsafety.gemp.wuhanncov.dao.WorldCountryInfoDao;
import com.gsafety.gemp.wuhanncov.dao.WorldInfoStatisticsDao;
import com.gsafety.gemp.wuhanncov.dao.WorldRealBroadcastDao;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaCountStatisticsHistoryPO;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaCountStatisticsPO;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaMigrationInfoPO;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaRealBroadcastPO;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaRealVideoPublicPO;
import com.gsafety.gemp.wuhanncov.dao.po.EmotionAnalyzingPO;
import com.gsafety.gemp.wuhanncov.dao.po.HotWordPO;
import com.gsafety.gemp.wuhanncov.dao.po.LocationDimPO;
import com.gsafety.gemp.wuhanncov.dao.po.LocationSentimentDailyPO;
import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaRuntimePO;
import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaRuntimePO.LockFlag;
import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaStatisticPO;
import com.gsafety.gemp.wuhanncov.dao.po.WorldCountryInfoPO;
import com.gsafety.gemp.wuhanncov.dao.po.WorldInfoStatisticsPO;
import com.gsafety.gemp.wuhanncov.dao.po.WorldRealBroadcastPO;
import com.gsafety.gemp.wuhanncov.handler.PatientAreaStatDataHandler;
import com.gsafety.gemp.wuhanncov.handler.PatientAreaStatDataHandlerFactory;

import lombok.extern.slf4j.Slf4j;

/**
 * 该类用于...,用于实现...等能力
 *
 * @author 86186
 * @since 2020/1/25 15:07
 * <p>
 * 1.0.0
 */
@Service("ncovPublicSentimentService")
@Slf4j
@EnableAsync
public class NcovPublicSentimentServiceImpl implements NcovPublicSentimentService {

    @Resource
    private EmotionAnalyzingDao emotionAnalyzingDao;

    @Resource
    private HotWordDao hotWordDao;

    @Resource
    private LocationDimDao locationDimDao;

    @Resource
    private PubSentimentNewsDao pubSentimentNewsDao;

    @Resource
    private ChinaRealBroadcastDao chinaRealBroadcastDao;

    @Resource
    private LocationSentimentDailyDao locationSentimentDailyDao;

    @Resource
    private ChinaMigrationInfoDao chinaMigrationInfoDao;

    @Resource
    private CountDao countDao;

    @Autowired
    private EntityManager entityManager;

    @Resource
    private PatientAreaStatisticDao patientAreaStatisticDao;

    @Resource
	private ChinaCountStatisticsHistoryDAO chinaCountStatisticsHistoryDAO;

    @Resource
    private ChinaRealVideoPublicDao chinaRealVideoPublicDao;

    @Resource
    private WorldInfoStatisticsDao worldInfoStatisticsDao;
    @Resource
    private WorldCountryInfoDao worldCountryInfoDao;

    @Value("${video.url.prefix}")
    private String videoUrlPrefix;

    @Resource
    private ChinaCountStatisticsDao chinaCountStatisticsDao;

    @Resource
    private WorldRealBroadcastDao worldRealBroadcastDao;

    @Resource
    private PatientAreaRuntimeDao patientAreaRuntimeDao;

    @Autowired
    private TaskExecutor taskExecutor;
    
    @Override
    public HotwordDTO findLatestHotword() {
        Sort sort = Sort.by(Direction.DESC, "wordTime");
        Pageable p = PageRequest.of(0, 10, sort);
        HotWordPO one = hotWordDao.findAll(p).getContent().iterator().next();
        return HotwordDTO.builder().createTime(one.getCreateTime()).partWord(one.getPartWord())
                .wordTime(one.getWordTime()).build();
    }

    @Override
    public HotwordContent findLatestHotwordContent() {
        // 构造一个热词模型，数量模拟
        HotwordContent hotwordContent = HotwordContent.builder().build();
        HotwordDTO dto = findLatestHotword();
        String[] words = StringUtils.split(dto.getPartWord(), HotwordContent.DEFAULT_SEPARATORCHARS);
        for (int i = 0; i < words.length; i++) {
            int index = i % 10;
            HotwordLevel hotwordLevel = HotwordLevel.get(index);
            hotwordContent.add(words[i], hotwordLevel.randomValue(), hotwordLevel.getHotVal());
        }
        return hotwordContent;
    }

    @Override
    public String findRollNows() {
        Sort sort = Sort.by(Direction.DESC, "time");
        Pageable p = PageRequest.of(0, 20, sort);
        List<ChinaRealBroadcastPO> list = chinaRealBroadcastDao.findAll(p).getContent();
        String titles = "";
        for (ChinaRealBroadcastPO one : list) {
            String title = one.getTitle();
            titles = titles + "   " + title;
        }
        return titles;
    }

    @Override
    public EmotionAnalyzingContent findEmotionAnalyzing() {
        EmotionAnalyzingContent emotionAnalyzingContent = EmotionAnalyzingContent.builder().build();
        EmotionAnalyzingPO emotionAnalyzing = emotionAnalyzingDao.findNewOne();
        int positiveSum = 0;
        int negativeSum = 0;//负面
        int neutralSum = 0;
        if (emotionAnalyzing!=null) {
            String analysisTime = emotionAnalyzing.getAnalysisTime();
            Integer positive = emotionAnalyzing.getPositive();//正面
            Integer negative = emotionAnalyzing.getNegative();//负面
            Integer neutral = emotionAnalyzing.getNeutral();//中性
            positiveSum = positiveSum + positive.intValue();
            negativeSum = negativeSum + negative.intValue();
            neutralSum = neutralSum + neutral.intValue();
        }
        emotionAnalyzingContent.add(positiveSum, "正面");
        emotionAnalyzingContent.add(negativeSum, "负面");
        emotionAnalyzingContent.add(neutralSum, "中性");
        return emotionAnalyzingContent;
    }

    @Override
    public List<PubSensHeatDTO> getListHeatOfPubSens() {
        List<PubSensHeatDTO> resultList = new ArrayList<>();
        List list = pubSentimentNewsDao.getCountListGroupByDate();
        PubSensHeatDTO dto = null;
        for (Object row : list) {
            Object[] cells = (Object[]) row;
            dto = new PubSensHeatDTO();
            dto.setX((String) cells[0]);
            dto.setY(((java.math.BigInteger) cells[1]).intValue());
            resultList.add(dto);
        }
        return resultList;
    }

    @Override
    public List<LocationHotDTO> findLocationHot(String date) {
        List<LocationSentimentDailyPO> list = this.locationSentimentDailyDao.findByStatDate(date);
        List<LocationHotDTO> result = new ArrayList<>();
        list.stream().forEach(temp -> {
            LocationHotDTO dto = LocationHotDTO.builder().areaId(NumberUtils.toInt(temp.getAreaNo()))
                    .value(temp.getHotValue()).build();
            result.add(dto);
        });
        return result;
    }

	@Override
	//@Async
	public void mergeLocationHot(String date, String areaId) {
		try {
			DateTimeFormatter format = DateTimeFormat.forPattern("yyyyMMdd");
			// 查询当日的舆情"pub_sentiment_news"
			DateTime dateTime = DateTime.parse(date, format);
			Date startDate = dateTime.toDate();
			Date endDate = dateTime.plusHours(24).toDate();

			// 获取指定areaId的关键字
			Optional<LocationDimPO> optional = locationDimDao.findById(areaId);
			if (optional.isPresent()) {
				LocationDimPO locationDim = optional.get();
				String ereaName = locationDim.getAreaName();
				// 计算舆情数据
				long count = pubSentimentNewsDao
						.count(newsTimeBetween(startDate, endDate).and(newsContentLike(ereaName)));
				Date now = DateTime.now().toDate();
				String id = StringUtils.leftPad(date + areaId, 32, "0");
				// 判断是省还是市，如果parentNo是0，则代表省
				String parentNo = locationDim.getParentNo();
				String areaType = locationDim.getAreaType();

				LocationSentimentDailyPO po = LocationSentimentDailyPO.builder().statId(id).areaNo(areaId)
						.areaName(locationDim.getAreaName()).parentNo(parentNo).areaType(areaType).egative(0).neutral(0)
						.positive(0).hotValue((int) count).statDate(date).createTime(now).updateTime(now).build();
				// 写入舆情表
				locationSentimentDailyDao.save(po);
				if (log.isDebugEnabled()) {
					log.info("更新地区舆情热力完成[{}][{}]", date, areaId);
				}
			} else {
				log.error("更新地区舆情热力失败[{}][{}]，无效的area_code", date, areaId);
			}
		} catch (Exception e) {
			log.error("更新地区舆情热力失败[{}][{}]", date, areaId, e);
		}
	}

	@Override
	//@Async
	public void mergeLocationHot(String date) {
		log.info("=============开始更新舆情热力[{}]============", date);
		BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<>(1000);
		ThreadPoolExecutor executor = new ThreadPoolExecutor(2, 4, 10, TimeUnit.SECONDS, workQueue);
		List<LocationDimPO> list = locationDimDao.findAll();
		list.stream().forEach(temp -> executor.execute(() -> {
			String areaId = temp.getAreaNo();
			mergeLocationHot(date, areaId);
		}));
		log.info("=============结束更新舆情热力[{}]============", date);
	}

    @Override
    public List<ChinaCountStatisticsDTO> findAreaCount() {
        Sort sort = Sort.by(Direction.DESC, "updateTime");
        List<ChinaCountStatisticsPO> list = countDao.findAll(new Specification<ChinaCountStatisticsPO>() {
            @Override
            public Predicate toPredicate(Root<ChinaCountStatisticsPO> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
//                List<Predicate> list = new ArrayList<Predicate>();
//                List<String> names = new ArrayList<String>();
//                names.add("台湾");
//                names.add("香港");
//                names.add("澳门");
//                Predicate p1 =builder.equal(root.get("areaType"),"province");
//                Predicate p2=builder.not(root.get("province").in(names));
//                list.add(p1);
//                list.add(p2);
//                Predicate[] p = new Predicate[list.size()];
//                return builder.and(list.toArray(p));
                return builder.equal(root.get("areaType"),"province");
            }
        }, sort);

        List<ChinaCountStatisticsDTO> content = new ArrayList<ChinaCountStatisticsDTO>();
        Iterator<ChinaCountStatisticsPO> it = list.iterator();
        SimpleDateFormat f = new SimpleDateFormat("MM月dd日");
        while (it.hasNext()) {
            ChinaCountStatisticsPO po = it.next();
            ChinaCountStatisticsDTO dto = new ChinaCountStatisticsDTO();
            BeanUtils.copyProperties(po, dto);
            Date updateTime = po.getUpdateTime();
            String dateTime = f.format(updateTime);
            dto.setCountTime(dateTime);
            content.add(dto);
        }
        return content;
    }

    @Override
    public ChinaCountStatisticsDTO findChinaCount() {
        ChinaCountStatisticsDTO chinaCountStatistics=new ChinaCountStatisticsDTO();
        Sort sort = Sort.by(Direction.DESC, "updateTime");
        List<ChinaCountStatisticsPO> list = countDao.findAll(new Specification<ChinaCountStatisticsPO>() {
            @Override
            public Predicate toPredicate(Root<ChinaCountStatisticsPO> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                return builder.equal(root.get("areaType"), "country");
            }
        }, sort);
        if(list!=null&&list.size()>0){
            SimpleDateFormat f = new SimpleDateFormat("MM月dd日");
            ChinaCountStatisticsPO po=list.get(0);
            BeanUtils.copyProperties(po,chinaCountStatistics);
            Date updateTime = po.getUpdateTime();
            String dateTime = f.format(updateTime);
            chinaCountStatistics.setAreaName("中国");
            chinaCountStatistics.setCountTime(dateTime);
        }
        return chinaCountStatistics;

    }
    @Override
    public List<ChinaCountStatisticsDTO> findHBCityCount(){
        return getCityCountByProvince("湖北");
    }

    @Override
    public List<ChinaCountStatisticsDTO> findProvinceCityCount(String province){
        return getCityCountByProvince(province);
    }

    @Override
    public List<ChinaCountStatisticsDTO> findCityCountyCount(String city){
        return findCountyByCityCount(city);
    }

    @Override
    public List<ChinaCountStatisticsBubbling> getChinaCountStatisticsBubbling() {
        List<ChinaCountStatisticsBubbling> chinaCountStatisticsBubblings=new ArrayList<>();
        List<ChinaCountStatisticsDTO> chinaCountStatistics= findAreaCount();
        chinaCountStatistics.stream().forEach(count -> {
            ChinaCountStatisticsBubbling bubbling=new ChinaCountStatisticsBubbling();
            String areaName=count.getAreaName();
            List<LocationDimPO> list=locationDimDao.getLocationDimByProvinceName(areaName);
            if(!CollectionUtils.isEmpty(list)&&list.size()>0){
                LocationDimPO locationDim=list.get(0);

//                bubbling.setLat(locationDim.getLatitude());
//                bubbling.setLng(locationDim.getLongitude());
                //datav需要将经纬度反过来
                bubbling.setLat(locationDim.getLongitude());
                bubbling.setLng(locationDim.getLatitude());
                String info=areaName+":"+count.getConfirmCase();
                bubbling.setInfo(info);
                chinaCountStatisticsBubblings.add(bubbling);
            }

        });
        return chinaCountStatisticsBubblings;
    }

    @Override
    public List<ChinaCountStatisticsBubbling> getParentCountStatisticsBubbling(String province) {
        List<ChinaCountStatisticsBubbling> chinaCountStatisticsBubblings=new ArrayList<>();
        List<ChinaCountStatisticsDTO> chinaCountStatistics= findProvinceCityCount(province);
        chinaCountStatistics.stream().forEach(count -> {
            ChinaCountStatisticsBubbling bubbling=new ChinaCountStatisticsBubbling();
            String areaName=count.getAreaName();
            List<LocationDimPO> list=locationDimDao.getLocationDimByCityName(areaName);
            if(!CollectionUtils.isEmpty(list)&&list.size()>0){
                LocationDimPO locationDim=list.get(0);
                bubbling.setLat(locationDim.getLatitude());
                bubbling.setLng(locationDim.getLongitude());
                String info="确诊:"+count.getConfirmCase()+",治愈:"+count.getCureCase()+",死亡:"+count.getDeadCase();
                bubbling.setInfo(info);
                bubbling.setName(locationDim.getAreaName());
                bubbling.setConfirmCase(count.getConfirmCase());
                bubbling.setCureCase(count.getCureCase());
                bubbling.setDeadCase(count.getDeadCase());
                chinaCountStatisticsBubblings.add(bubbling);
            }

        });
        return chinaCountStatisticsBubblings;
    }

    /**
     * 获得迁徙图最新数据时间 yyyy-mm-dd
     * @return
     */
    @Override
    public String getMigrateMapDefaultDate() {
        String createDate=chinaMigrationInfoDao.getMigrateMapDefaultDate();
        try {
            return strToDateFormat(createDate);
        } catch (ParseException e) {
            return getdefaultDate(-1);
        }
    }

    @Override
    public List<LocationValue> getInternationalLocationValue() {
        Sort sort = Sort.by(Direction.DESC, "confirmCase");
        List<LocationValue> content = new ArrayList<LocationValue>();
        List<WorldInfoStatisticsPO> polist=worldInfoStatisticsDao.findAll(sort);
        Iterator<WorldInfoStatisticsPO> it = polist.iterator();
        while (it.hasNext()) {
            WorldInfoStatisticsPO po = it.next();
            String country=po.getCountry();
            LocationValue dto = new LocationValue();
            dto.setValue(po.getConfirmCase());
            List<WorldCountryInfoPO> list= worldCountryInfoDao.findByCountry(country);
            if(list!=null&&list.size()>0){
                WorldCountryInfoPO worldCountryInfoPO=list.get(0);
                BigDecimal lat=worldCountryInfoPO.getLatitude();
                BigDecimal lng=worldCountryInfoPO.getLongitude();
                if(lat!=null&&lng!=null){
                    dto.setLat(lat);
                    dto.setLng(lng);
                    content.add(dto);
                }
            }

        }
        return content;
    }


    @Override
    public List<LocationValue> findChinaProvinceLocationValue() {
        List<LocationValue> content = new ArrayList<LocationValue>();
        List<ChinaCountStatisticsPO> chinaCountStatistics = countDao.getProvinces();
        chinaCountStatistics.stream().forEach(count -> {
            String to = "";
            String areaName=count.getAreaName();
            List<LocationDimPO> list=locationDimDao.getLocationDimByProvinceName(areaName);
            if(!CollectionUtils.isEmpty(list)&&list.size()>0){
                LocationValue dto = new LocationValue();
                dto.setValue(count.getConfirmCase());
                LocationDimPO locationDim=list.get(0);
                BigDecimal lat=locationDim.getLatitude();
                BigDecimal lng=locationDim.getLongitude();
                if(lat!=null&&lng!=null){
                    dto.setLat(lat);
                    dto.setLng(lng);
                    content.add(dto);
                }
            }
        });
        return content;
    }

    @Override
    public MigrateMapContent getFlyLineByWutoInternationalLocation(String from) {
        MigrateMapContent migrateMap=MigrateMapContent.builder().build();
        List<WorldInfoStatisticsPO> polist=worldInfoStatisticsDao.findAll();
        Iterator<WorldInfoStatisticsPO> it = polist.iterator();
        while (it.hasNext()) {
            String to = "";
            WorldInfoStatisticsPO po = it.next();
            String country=po.getCountry();
            List<WorldCountryInfoPO> list= worldCountryInfoDao.findByCountry(country);
            if(list!=null&&list.size()>0){
                WorldCountryInfoPO worldCountryInfoPO=list.get(0);
                BigDecimal lat=worldCountryInfoPO.getLatitude();
                BigDecimal lng=worldCountryInfoPO.getLongitude();
                if(lat!=null&&lng!=null){
                    to=String.valueOf(lat)+","+String.valueOf(lng);
                    migrateMap.add(from,to);
                }
            }

        }
        if(migrateMap.getMigrateMaps()==null){
            migrateMap.init();
        }
        return migrateMap;
    }

    @Override
    public List<WorldRealBroadcastDTO> getWorldRealBroadcastTop() {
        Sort sort = Sort.by(Direction.DESC, "timestamp");
        Pageable p = PageRequest.of(0, 50, sort);
        List<WorldRealBroadcastPO> list = worldRealBroadcastDao.findAll(new Specification<WorldRealBroadcastPO>(){
            @Override
            public Predicate toPredicate(Root<WorldRealBroadcastPO> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                List<Predicate> list = new ArrayList<Predicate>();
                Predicate p1 =builder.like(root.get("dataSource"),"%微博%");
                Predicate p2 =builder.like(root.get("outline"),"%确诊%");
                list.add(builder.and(p1, p2));
                Predicate[] p = new Predicate[list.size()];
                return builder.and(list.toArray(p));
            }
        },p).getContent();
        List<WorldRealBroadcastDTO> dtos=new ArrayList<WorldRealBroadcastDTO>();
        String titles = "";
        for (WorldRealBroadcastPO one : list) {
            WorldRealBroadcastDTO dto=new WorldRealBroadcastDTO();
           BeanUtils.copyProperties(one,dto);
           String outline=dto.getOutline();
           if(StringUtils.isNotEmpty(outline)){
               outline=outline.replaceAll("展开全文c","").replaceAll("展开全文","");
           }
            dto.setOutline(outline);
           dtos.add(dto);
        }
        return dtos;
    }

    @Override
    public List<ChinaCountStatisticsDTO> findBJCityCount() {
        return getCityCountByProvince("北京");
    }

    @Override
    public ChinaCountStatisticsDTO findHBCount() {
        return getAreaNameCount("湖北");
    }

    @Override
    public ChinaCountStatisticsDTO findBJCount() {
        return getAreaNameCount("北京");
    }

    @Override
    public ChinaCountStatisticsDTO findWHCount() {
        return getAreaNameCount("武汉");
    }

    @Override
    public List<AreaHotDTO> getAreaHotList(String areaParent) {
        List<AreaHotDTO> resultList = new ArrayList<>();

        List list = locationSentimentDailyDao.getAreaHotList(areaParent);
        AreaHotDTO dto = null;
        for (Object row : list) {
            Object[] cells = (Object[]) row;
            dto = new AreaHotDTO();
            dto.setArea_id((String) cells[0]);
            dto.setValue(((java.math.BigDecimal) cells[1]).intValue());
            resultList.add(dto);
        }
        return resultList;
    }

    @Override
    public List<ChinaCountStatisticsDTO> findGATCount() {
        Sort sort = Sort.by(Direction.DESC, "confirmCase");
        List<ChinaCountStatisticsPO> list = countDao.findAll(new Specification<ChinaCountStatisticsPO>() {
            @Override
            public Predicate toPredicate(Root<ChinaCountStatisticsPO> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                List<Predicate> list = new ArrayList<Predicate>();
                CriteriaBuilder.In<String> in = builder.in(root.get("province"));
                in.value("台湾");
                in.value("香港");
                in.value("澳门");
                list.add(in);
                Predicate[] p = new Predicate[list.size()];
                return builder.and(list.toArray(p));
            }
        }, sort);
        List<ChinaCountStatisticsDTO> content = new ArrayList<ChinaCountStatisticsDTO>();
        Iterator<ChinaCountStatisticsPO> it = list.iterator();
        SimpleDateFormat f = new SimpleDateFormat("MM月dd日");
        while (it.hasNext()) {
            ChinaCountStatisticsPO po = it.next();
            ChinaCountStatisticsDTO dto = new ChinaCountStatisticsDTO();
            BeanUtils.copyProperties(po, dto);
            Date updateTime = po.getUpdateTime();
            String dateTime = f.format(updateTime);
            dto.setCountTime(dateTime);
            content.add(dto);
        }
        return content;
    }

    @Override
    public List<WorldInfoStatisticsDTO> findInternationalCount() {
        Sort sort = Sort.by(Direction.DESC, "confirmCase");
        List<WorldInfoStatisticsDTO> content = new ArrayList<WorldInfoStatisticsDTO>();
        ChinaCountStatisticsDTO chinaCount=findChinaCount();
        WorldInfoStatisticsDTO china=new WorldInfoStatisticsDTO();
        BeanUtils.copyProperties(chinaCount,china);
        china.setCountry(chinaCount.getAreaName());
        china.setCountTime(String.valueOf(System.currentTimeMillis()));
        china.setCountDate(chinaCount.getCountTime());
        china.setConfirmCase(chinaCount.getConfirmCase());
        content.add(china);
        List<WorldInfoStatisticsPO> list=worldInfoStatisticsDao.findAll(sort);
        Iterator<WorldInfoStatisticsPO> it = list.iterator();
        SimpleDateFormat f = new SimpleDateFormat("MM月dd日");
        while (it.hasNext()) {
            WorldInfoStatisticsPO po = it.next();
            WorldInfoStatisticsDTO dto = new WorldInfoStatisticsDTO();
            BeanUtils.copyProperties(po, dto);
            Date date = po.getCountDate();
            String countDate = f.format(date);
            dto.setCountDate(countDate);
            dto.setCountTime(po.getCountTime());
            content.add(dto);
        }
        return content;
    }



    @Override
    public TextValue getTimeAxis() {
        TextValue textValue = TextValue.builder().build();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        for (int i = 6; i >= 0; i--) {
            Calendar c = Calendar.getInstance();
            //过去七天
            c.setTime(new Date());
            c.add(Calendar.DATE, -i);
            Date d = c.getTime();
            String dataTime = format.format(d);
            int dayValue = Integer.valueOf(dataTime.replaceAll("-", ""));
            textValue.add(dayValue, dataTime);
        }

        return textValue;
    }

    @Override
    public TextValue getProvinceArea() {
        TextValue textValue = TextValue.builder().build();
        List<LocationDimPO> list = this.getProvinces();
        list.stream().forEach(temp -> {
            String areaNo = temp.getAreaNo();
            String areaName = temp.getAreaName();
            textValue.add(Integer.valueOf(areaNo), areaName);
        });
        return textValue;
    }

    @Override
    public TextValue getCityArea(String areaId) {
        TextValue textValue = TextValue.builder().build();
        List<LocationDimPO> list = this.getAreaByParentArea(areaId);
        list.stream().forEach(temp -> {
            String areaNo = temp.getAreaNo();
            String areaName = temp.getAreaName();
            textValue.add(Integer.valueOf(areaNo), areaName);
        });
        return textValue;
    }

    @Override
    public AreaInfo getAllArea() {
        AreaInfo areaInfo = AreaInfo.builder().build();
        List<LocationDimPO> provinces = getProvinces();
        provinces.stream().forEach(province -> {
            String abcode = province.getAreaNo();
            String name = province.getAreaName();
            String level = "province";
            String parent = "中华人民共和国";
            areaInfo.add(name, abcode, level, parent);
            List<LocationDimPO> citys = getAreaByParentArea(abcode);
            citys.stream().forEach(city -> {
                String cabcode = city.getAreaNo();
                String cname = city.getAreaName();
                String clevel = "city";
                String cparent = name;
                areaInfo.add(cname, cabcode, clevel, cparent);
            });
        });
        return areaInfo;
    }

    @Override
    public MigratePeopleContent getMigratePeople(String date, String migrate_type,String areaType, String area_id) {
        MigratePeopleContent migratePeople=MigratePeopleContent.builder().build();
        Sort sort = Sort.by(Direction.DESC, "value");
        List<ChinaMigrationInfoPO> chinaMigrationInfos=chinaMigrationInfoDao.findAll(areaIdEqual(area_id).and(migrateTypeEqual(migrate_type)).and(createDateEqual(date)).and(levelEqual(areaType)),sort);
        chinaMigrationInfos.stream().forEach(migrationInfo -> {
            DecimalFormat df = new DecimalFormat("###,###0.00");
            String id=migrationInfo.getAreaId();
            String area =migrationInfo.getAreaName();
            String cityName =migrationInfo.getCityName();
            String provinceName =migrationInfo.getProvinceName();
            if("province".equalsIgnoreCase(areaType)){
                cityName =migrationInfo.getProvinceName();
            }
            BigDecimal value = migrationInfo.getValue().setScale(2,BigDecimal.ROUND_HALF_UP);
            String percent="0.00";
            if(value!=null){
                percent=df.format(value)+"%";
            }
            String cityname=cityName;
            LocationDimPO locationDim1=locationDimDao.findByAreaNo(area_id);
            List<LocationDimPO> list=locationDimDao.findAll(new Specification<LocationDimPO>(){
                @Override
                public Predicate toPredicate(Root<LocationDimPO> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                    if("province".equalsIgnoreCase(areaType)){
                        return builder.equal(root.get("areaName"), provinceName);
                    }else{
                        return builder.equal(root.get("areaName"), cityname);
                    }

                }
            });
            String latitude="";
            String longitude="";
            if(!CollectionUtils.isEmpty(list)&&list.size()>0) {
                LocationDimPO dimPO = list.get(0);
                 latitude=String.valueOf(dimPO.getLatitude());
                 longitude=String.valueOf(dimPO.getLongitude());
            }
            migratePeople.add(cityName,percent,latitude,longitude);
        });
        if(migratePeople.getMigratePeoples()==null){
            migratePeople.init();
        }
        return migratePeople;
    }

    @Override
    public MigrateMapContent getMigrateMap(String date, String migrate_type,String areaType, String area_id) {
        MigrateMapContent migrateMap=MigrateMapContent.builder().build();
        Sort sort = Sort.by(Direction.DESC, "value");
//        List<ChinaMigrationInfoPO> chinaMigrationInfos=new ArrayList<ChinaMigrationInfoPO> ();
//        if("city".equalsIgnoreCase(areaType)){
//            chinaMigrationInfos= chinaMigrationInfoDao.findAll(areaIdEqual(area_id).and(migrateTypeEqual(migrate_type)).and(createDateEqual(date)),sort);
//        }else{
//            chinaMigrationInfos=chinaMigrationInfoDao.findMigrateByProvince(date,migrate_type,area_id);
//        }
        List<ChinaMigrationInfoPO> chinaMigrationInfos=chinaMigrationInfoDao.findAll(areaIdEqual(area_id).and(migrateTypeEqual(migrate_type)).and(createDateEqual(date)).and(levelEqual(areaType)),sort);
        chinaMigrationInfos.stream().forEach(migrationInfo -> {
            String from="";
            String to="";
            String fromName="";
            String toName="";
            BigDecimal value=new BigDecimal(0);
            String area =migrationInfo.getAreaName();
            String areaId=migrationInfo.getAreaId();

            String cityName =migrationInfo.getCityName();
            String provinceName =migrationInfo.getProvinceName();
            LocationDimPO locationDim1=locationDimDao.findByAreaNo(area_id);
            List<LocationDimPO> list=locationDimDao.findAll(new Specification<LocationDimPO>(){
                @Override
                public Predicate toPredicate(Root<LocationDimPO> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                    if("province".equalsIgnoreCase(areaType)){
                        return builder.equal(root.get("areaName"), provinceName);
                    }else{
                        return builder.equal(root.get("areaName"), cityName);
                    }

                }
            });
            if(!CollectionUtils.isEmpty(list)&&list.size()>0){
                LocationDimPO locationDim2=list.get(0);
                String dmi1=String.valueOf(locationDim1.getLatitude())+","+String.valueOf(locationDim1.getLongitude());
                String dmi2=String.valueOf(locationDim2.getLatitude())+","+String.valueOf(locationDim2.getLongitude());
                if("move_out".equalsIgnoreCase(migrate_type)){
                    from=dmi1;
                    to=dmi2;
                    fromName=locationDim1.getShortName();
                    toName=locationDim2.getShortName();
                }else{
                    from=dmi2;
                    to=dmi1;
                    fromName=locationDim2.getShortName();
                    toName=locationDim1.getShortName();
                }
            }
            if(!StringUtils.isEmpty(from)){
                value=migrationInfo.getValue();
                migrateMap.add(from,to,value,fromName,toName);
            }
        });
        if(migrateMap.getMigrateMaps()==null){
            migrateMap.init();
        }
        return migrateMap;
    }

    @Override
    public List<PatientAreaCountDailyDTO> getPatientAreaCountDaily() {

        List<PatientAreaCountDailyDTO> resultList = new ArrayList<>();
        //获取类型1，武汉的统计患者数据
        resultList.addAll(getPatientAreaCountDaily("420100", "1"));

        List<PatientAreaCountDailyDTO> huheiList = getPatientAreaCountDaily("420000", "2");
//        Map<String, Integer> hubeiMap = huheiList.stream().collect(Collectors.toMap(PatientAreaCountDailyDTO::getX, PatientAreaCountDailyDTO::getY));

        List<PatientAreaCountDailyDTO> chinaList = getPatientAreaCountDaily(AppConstant.DIM_ChINA_CODE, "4");
//        Map<String, Integer> chinaMap = chinaList.stream().collect(Collectors.toMap(PatientAreaCountDailyDTO::getX, PatientAreaCountDailyDTO::getY));

//        List<PatientAreaCountDailyDTO> otherList = new ArrayList<>();
//        PatientAreaCountDailyDTO dto = null;
//        for (Map.Entry<String, Integer> en : chinaMap.entrySet()) {
//            dto = new PatientAreaCountDailyDTO();
//            dto.setX(en.getKey());
//            dto.setY(en.getValue() - hubeiMap.getOrDefault(en.getKey(), 0));
//            dto.setS("3");
//            otherList.add(dto);
//        }
//        otherList.sort(Comparator.comparing(PatientAreaCountDailyDTO::getX).reversed());

        resultList.addAll(huheiList);
        resultList.addAll(getPatientAreaCountDaily("110000", "3"));
        resultList.addAll(chinaList);

        return resultList;
    }

    /***
     * 查询疫情相关实时视频地址列表
     * @author  lyh
     * @since   2020/1/26 21:10
     * @return  java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.RealVideoPublicDTO>
     * @throws
     */
    @Override
    public List<RealVideoPublicDTO> getRealVideoPublicList() {
        List<RealVideoPublicDTO> resultList = new ArrayList<>();
        List<ChinaRealVideoPublicPO> poList = chinaRealVideoPublicDao.findAll();

        if(poList != null && poList.size() > 0){
            poList.sort(Comparator.comparing(ChinaRealVideoPublicPO::getDate).reversed());
            RealVideoPublicDTO dto = null;
            for(ChinaRealVideoPublicPO po : poList){
                if(StringUtils.isNotBlank(po.getVideoBigUrl())){
                    dto = new RealVideoPublicDTO();
                    //dto.setUrl(videoUrlPrefix + po.getVideoBigUrl());
                    dto.setUrl(po.getVideoBigUrl());
                    resultList.add(dto);
                }
            }
        }
        return resultList;
    }



    /***
     * 按区域和类型获取患者统计按天统计数据
     * @author lyh
     * @since 2020/1/26 20:02
     * @param   areaCode 区域编码
     * @param   type 类型
     * @return java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaCountDailyDTO>
     * @throws
     */
    @Override
    public List<PatientAreaCountDailyDTO> getPatientAreaCountDaily(String areaCode, String type) {
        List<PatientAreaCountDailyDTO> resultList = new ArrayList<>();
        String todayDate=getdefaultDate(0);
        todayDate=todayDate.replaceAll("-","").replaceAll("/","");
        //获取全国患者统计数据列表
        List<PatientAreaStatisticPO> poList = patientAreaStatisticDao.findByAreaCodeAndStasDateLessThanOrderByStasTimeDesc(areaCode, todayDate);
        if (poList != null && poList.size() > 0) {
            PatientAreaCountDailyDTO dto = null;
            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
            for (PatientAreaStatisticPO po : poList) {
                dto = new PatientAreaCountDailyDTO();
                dto.setX(format.format(po.getStasTime()));
                dto.setY(po.getPatientNumber());
                dto.setS(type);
                resultList.add(dto);
            }
        }
        return resultList;
    }

    @Override
    public List<AreaHotDTO> getAreaPatientCountList(String areaParent) {
        List<AreaHotDTO> resultList = new ArrayList<>();

        List<ChinaCountStatisticsPO> poList = null;

        if("100000".equals(AppConstant.getDimCode(areaParent))){
            poList = chinaCountStatisticsDao.findByAreaType("province");
        }else if(areaParent.endsWith("0000")){
            Optional<LocationDimPO> byId = locationDimDao.findById(areaParent);
            poList = chinaCountStatisticsDao.findByAreaTypeAndProvinceLike("city", byId.get().getAreaName().substring(0,2));
        }else{
            Optional<LocationDimPO> byId = locationDimDao.findById(areaParent);
            poList = chinaCountStatisticsDao.findByAreaTypeAndCityLike("county", byId.get().getAreaName().substring(0,2));
        }

        Sort sort = Sort.by(Direction.ASC, "seqNum");
        List<LocationDimPO> locationList = locationDimDao.findByParentNo(areaParent, sort);

        AreaHotDTO dto = null;
        for(ChinaCountStatisticsPO po : poList){
            dto = new AreaHotDTO();

            for(LocationDimPO lpo : locationList){
                if(lpo.getAreaName().contains(po.getAreaName())
                        || lpo.getShortName().contains(po.getAreaName())){
                    dto.setArea_id(lpo.getAreaNo());
                    dto.setLat(lpo.getLatitude());
                    dto.setLng(lpo.getLongitude());
                    break;
                }
            }

            if(StringUtils.isEmpty(dto.getArea_id())){
                dto.setArea_id(po.getAreaName());
            }
            Integer confirmCase=po.getConfirmCase();
            dto.setValue((confirmCase == null || confirmCase == 0) ? null : confirmCase);

            resultList.add(dto);
        }

        return resultList;
    }

    @Override
    public List<PatientAreaCountDailyDTO> getPatientAreaInfoDaily(String areaId) {
        List<PatientAreaCountDailyDTO> resultList = new ArrayList<>();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date now = new Date();
        String startDate = sdf.format(DateUtils.addDays(now, -8));
        String endDate = sdf.format(now);
        //获取全国患者统计数据列表
        List<PatientAreaStatisticPO> poList = patientAreaStatisticDao
                .findByAreaCodeAndStasDateGreaterThanAndStasDateLessThanOrderByStasTimeAsc(areaId, startDate, endDate);
        if (poList != null && poList.size() > 0) {
            PatientAreaCountDailyDTO dto = null;
            SimpleDateFormat format = new SimpleDateFormat("MM/dd");
            String dateStr = null;
            for (PatientAreaStatisticPO po : poList) {

                dateStr = format.format(po.getStasTime());
                //s=1 确诊
                dto = new PatientAreaCountDailyDTO();
                dto.setX(dateStr);
                dto.setY(po.getPatientNumber());
                dto.setS("1");
                resultList.add(dto);

                //s=2 死亡
                dto = new PatientAreaCountDailyDTO();
                dto.setX(dateStr);
                dto.setY(po.getDeathNumber());
                dto.setS("2");
                resultList.add(dto);

                //s=3 治愈
                dto = new PatientAreaCountDailyDTO();
                dto.setX(dateStr);
                dto.setY(po.getCuredNumber());
                dto.setS("3");
                resultList.add(dto);
            }
        }
        return resultList;
    }

    @Override
    public List<WorldHotMap> getWorldHotMap() {
        List<WorldHotMap> worldHotMaps=new ArrayList<WorldHotMap>();
        List<WorldInfoStatisticsDTO> internations=findInternationalCount();
        internations.stream().forEach(internation -> {
            String country = internation.getCountry();
            Integer confirmCase = internation.getConfirmCase();

            List<WorldCountryInfoPO> list= worldCountryInfoDao.findByCountry(country);
            if(list!=null&&list.size()>0){
                WorldCountryInfoPO po=list.get(0);
                String id=po.getId();
                String name=po.getName();
                WorldHotMap worldHotMap=new WorldHotMap();
                worldHotMap.setId(id);
                worldHotMap.setName(name);
                if(confirmCase==0){
                    confirmCase=null;
                }
                worldHotMap.setValue(confirmCase);
                worldHotMaps.add(worldHotMap);
            }

        });
        return worldHotMaps;
    }

    /**
     * 武汉到全国飞线图
     * @return
     */
    @Override
    public MigrateMapContent getFlyLineByWutoProvince(String from) {
        MigrateMapContent migrateMap=MigrateMapContent.builder().build();
        List<ChinaCountStatisticsPO> chinaCountStatistics = countDao.getFlyLineByWutoProvince();
        chinaCountStatistics.stream().forEach(count -> {
            String to = "";
            String areaName=count.getAreaName();
            List<LocationDimPO> list=locationDimDao.getLocationDimByProvinceName(areaName);
            if(!CollectionUtils.isEmpty(list)&&list.size()>0){
                LocationDimPO locationDim=list.get(0);
                to=String.valueOf(locationDim.getLatitude())+","+String.valueOf(locationDim.getLongitude());
            }
            if(!StringUtils.isEmpty(to)){
                migrateMap.add(from,to);
            }

        });
        if(migrateMap.getMigrateMaps()==null){
            migrateMap.init();
        }
        return migrateMap;
    }


    private List<LocationDimPO> getProvinces() {
        String parentNo = "100000";
        List<LocationDimPO> list = getAreaByParentArea(parentNo);
        return list;
    }
    
    private List<LocationDimPO> getAreaByParentArea(String parentNo) {
        Sort sort = Sort.by(Direction.ASC, "seqNum");
        List<LocationDimPO> list = this.locationDimDao.findByParentNo(parentNo, sort);
        return list;
    }

    @Override
    public ChinaCountStatisticsDTO getAreaNameCount(String areaName) {
        ChinaCountStatisticsDTO chinaCountStatistics=new ChinaCountStatisticsDTO();
        List<ChinaCountStatisticsPO> list = countDao.findAll(new Specification<ChinaCountStatisticsPO>() {
            @Override
            public Predicate toPredicate(Root<ChinaCountStatisticsPO> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                return builder.equal(root.get("areaName"), areaName);
            }
        });
        if(list!=null&&list.size()>0){
            SimpleDateFormat f = new SimpleDateFormat("MM月dd日");
            ChinaCountStatisticsPO po=list.get(0);
            BeanUtils.copyProperties(po,chinaCountStatistics);
            Date updateTime = po.getUpdateTime();
            String dateTime = f.format(updateTime);
            chinaCountStatistics.setCountTime(dateTime);
        }
        return chinaCountStatistics;
    }

    @Override
    public List<ChinaCountStatisticsDTO> findAreaByAreaIdsCount(List<String> areaIdList) {
        List<LocationDimPO> dimList=locationDimDao.findAll(new Specification<LocationDimPO>(){
            @Override
            public Predicate toPredicate(Root<LocationDimPO> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                CriteriaBuilder.In<String> in = builder.in(root.get("areaNo"));
                for (String id : areaIdList) {
                    in.value(id);
                }
                return builder.and(in);
            }
        });
        List<String> areaNames=new ArrayList<String>();
        for(LocationDimPO dimPO:dimList){
            areaNames.add(dimPO.getAreaName().replaceAll("省","").replaceAll("市",""));
        }
        Sort sort = Sort.by(Direction.DESC, "confirmCase");
        List<ChinaCountStatisticsPO> list = countDao.findAll(new Specification<ChinaCountStatisticsPO>() {
            @Override
            public Predicate toPredicate(Root<ChinaCountStatisticsPO> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                CriteriaBuilder.In<String> in = builder.in(root.get("areaName"));
                for (String name : areaNames) {
                    in.value(name);
                }
                return builder.and(in);
            }
        }, sort);
        List<ChinaCountStatisticsDTO> content = new ArrayList<ChinaCountStatisticsDTO>();
        Iterator<ChinaCountStatisticsPO> it = list.iterator();
        SimpleDateFormat f = new SimpleDateFormat("MM月dd日");
        while (it.hasNext()) {
            ChinaCountStatisticsPO po = it.next();
            ChinaCountStatisticsDTO dto = new ChinaCountStatisticsDTO();
            BeanUtils.copyProperties(po, dto);
            Date updateTime = po.getUpdateTime();
            String dateTime = f.format(updateTime);
            dto.setCountTime(dateTime);
            content.add(dto);
        }
        return content;
    }

    private List<ChinaCountStatisticsDTO> getCityCountByProvince(String areaName) {
        Sort sort = Sort.by(Direction.DESC, "confirmCase");
        List<ChinaCountStatisticsPO> list = countDao.findAll(new Specification<ChinaCountStatisticsPO>() {
            @Override
            public Predicate toPredicate(Root<ChinaCountStatisticsPO> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                List<Predicate> list = new ArrayList<Predicate>();
                Predicate p1 =builder.equal(root.get("province"),areaName);
                Predicate p2 =builder.equal(root.get("areaType"),"city");
                list.add(builder.and(p1, p2));
                Predicate[] p = new Predicate[list.size()];
                return builder.and(list.toArray(p));
            }
        }, sort);
        List<ChinaCountStatisticsDTO> content = new ArrayList<ChinaCountStatisticsDTO>();
        Iterator<ChinaCountStatisticsPO> it = list.iterator();
        SimpleDateFormat f = new SimpleDateFormat("MM月dd日");
        while (it.hasNext()) {
            ChinaCountStatisticsPO po = it.next();
            ChinaCountStatisticsDTO dto = new ChinaCountStatisticsDTO();
            BeanUtils.copyProperties(po, dto);
            Date updateTime = po.getUpdateTime();
            String dateTime = f.format(updateTime);
            dto.setCountTime(dateTime);
            content.add(dto);
        }
        return content;
    }

    private List<ChinaCountStatisticsDTO> findCountyByCityCount(String areaName) {
        Sort sort = Sort.by(Direction.DESC, "confirmCase");
        List<ChinaCountStatisticsPO> list = countDao.findAll(new Specification<ChinaCountStatisticsPO>() {
            @Override
            public Predicate toPredicate(Root<ChinaCountStatisticsPO> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                List<Predicate> list = new ArrayList<Predicate>();
                Predicate p1 =builder.equal(root.get("city"),areaName);
                Predicate p2 =builder.equal(root.get("areaType"),"county");
                list.add(builder.and(p1, p2));
                Predicate[] p = new Predicate[list.size()];
                return builder.and(list.toArray(p));
            }
        }, sort);
        List<ChinaCountStatisticsDTO> content = new ArrayList<ChinaCountStatisticsDTO>();
        Iterator<ChinaCountStatisticsPO> it = list.iterator();
        SimpleDateFormat f = new SimpleDateFormat("MM月dd日");
        while (it.hasNext()) {
            ChinaCountStatisticsPO po = it.next();
            ChinaCountStatisticsDTO dto = new ChinaCountStatisticsDTO();
            BeanUtils.copyProperties(po, dto);
            Date updateTime = po.getUpdateTime();
            String dateTime = f.format(updateTime);
            dto.setCountTime(dateTime);
            content.add(dto);
        }
        return content;
    }




    @Override
	public void refreshPatientDaily(RefreshPatientDailyCond cond) {
    	log.info("更新病患日报{}", cond);
		// 查询时间段内的所有数据，并按名称映射到location_dim
		DateTimeFormatter format = DateTimeFormat.forPattern("yyyyMMddHH");
		// 查询当日的舆情"pub_sentiment_news"
		DateTime dateTime = DateTime.parse(cond.getSampleDate() + cond.getSampleHour(), format);
		Date startDate = dateTime.toDate();
		Date endDate = dateTime.plusHours(1).toDate();
		String[] areaTypes = new String[] { "country", "province", "city" };
		//从百度渠道来的数据进行同步
		List<ChinaCountStatisticsHistoryPO> list = this.chinaCountStatisticsHistoryDAO
				.findByUpdateTimeBetweenAndAreaTypeInAndFromSource(startDate, endDate, areaTypes,"baidu");
		// 定时逻辑
		for (ChinaCountStatisticsHistoryPO temp : list) {
            taskExecutor.execute(()-> excuteTaskForSaveHistoryData(cond, temp));
		}
		// 组装日报数据写入结果表
		log.info("更新病患日报{}完成", cond);
	}

	//@Async
	public void excuteTaskForSaveHistoryData(RefreshPatientDailyCond cond, ChinaCountStatisticsHistoryPO temp){
        String areaName=temp.getAreaName();
        try {
            //吉林市和吉林省有冲突
            String areaType=temp.getAreaType();
            if ("吉林".equals(areaName)&&"city".equals(areaType)){
                areaName="吉林市";
            }
            PatientAreaStatDataHandler handler = PatientAreaStatDataHandlerFactory.get(areaName);
            PatientAreaStatisticPO stat = handler.transData(temp, cond);

            // 如果转换器可用，则写入数据库
            if(handler.isEnabled()) {
                stat.setUpdateTime(DateTime.now().toDate());
                stat.setPostDate(cond.getPostDate());
                this.patientAreaStatisticDao.save(stat);
            }
            log.info("处理病患日报数据[{}]成功", cond, areaName);
        } catch (Exception e) {
            log.error("处理病患日报数据[{}][{}]失败", cond, areaName, e);
        }
    }

	private  String strToDateFormat(String date) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        formatter.setLenient(false);
        Date newDate = null;
        newDate = formatter.parse(date);
        formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(newDate);
    }
	
	/**
	 * 
	 * 获取默认的时间，格式yyyy-MM-dd
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月6日 下午10:49:17
	 *
     * @Parm day天数
	 * @return
	 */
    private String getdefaultDate(int day) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, day);
        Date d = c.getTime();
        return format.format(d);
    }

	@Override
	public void syncPatientRuntime() {
		log.info("同步病患实时数据开始");
		// 构造一个地区维度列表
		List<LocationDimPO> dimPOS = locationDimDao.getAutoJobOn();
		Map<String, LocationDimPO> dimMap = dimPOS.stream()
				.collect(Collectors.toMap(LocationDimPO::getShortName, dimPO -> dimPO, (dimOld, dimNew) -> dimNew));

		List<ChinaCountStatisticsPO> list = chinaCountStatisticsDao.findAll();
		list.stream().forEach(temp -> taskExecutor.execute(() -> {
			LocationDimPO dimPO = dimMap.get(temp.getAreaName());
			updateOneRuntime(dimPO, temp);
			}));
		log.info("同步病患实时数据结束");
	}

    @Override
    public void patientUncertainRuntime() {
        log.info("同步病患待确认实时数据开始");
        String currentDateString=getdefaultDate(0);//当天的时间
        Date beginDate=strToDate(currentDateString+" 00:00:00");
        Date endDate=strToDate(currentDateString+" 59:59:59");
        List<ChinaCountStatisticsPO> pos=chinaCountStatisticsDao.findUncertainAll(beginDate,endDate);
        List<PatientAreaRuntimePO> patientAreaRuntimeList=new ArrayList<PatientAreaRuntimePO>();
        for(ChinaCountStatisticsPO temp:pos){
            PatientAreaRuntimePO.PatientAreaRuntimePOBuilder builder = PatientAreaRuntimePO.builder();
            String areaName=temp.getAreaName();
            String areaType=temp.getAreaType();
            String province=temp.getProvince();
            //因为爬虫只能爬到省市，所以，待确认只能是市这一级
            List<LocationDimPO> dimList= locationDimDao.getLocationDimProvinceByName(province);
            if(dimList!=null&&dimList.size()>0){
                LocationDimPO dimPO=dimList.get(0);
                String parentAreaNo=dimPO.getAreaNo();
                String dimAreaNo=parentAreaNo+"X";
                Integer newCase=temp.getNewCase();
                Integer cureCase=temp.getCureCase();
                Integer deadCase=temp.getDeadCase();
                Integer confirmCase=temp.getConfirmCase();
                Integer probableCase=temp.getProbableCase();
                if(newCase!=null&&cureCase!=null&&deadCase!=null&&confirmCase!=null&&probableCase!=null){
                    PatientAreaRuntimePO runtimePO = builder.id(dimAreaNo).areaCode(dimAreaNo)
                            .areaType(areaType).areaName("待确认")
                            .newCase(newCase)
                            .cureCase(cureCase)
                            .deadCase(deadCase)
                            .probableCase(probableCase)
                            .confirmCase(confirmCase)
                            .lockFlag(LockFlag.UNLOCK)
                            .updateTime(DateTime.now().toDate()).parentNo(parentAreaNo).build();
                    patientAreaRuntimeList.add(runtimePO);
                }
            }
        }
        //批量删除旧的实时待确认信息
        List<PatientAreaRuntimePO> patientAreaRuntimeUncertainList=patientAreaRuntimeDao.findUncertainAll();
        patientAreaRuntimeDao.deleteAll(patientAreaRuntimeUncertainList);
        if(patientAreaRuntimeList.size()>0){
            patientAreaRuntimeDao.saveAll(patientAreaRuntimeList);
            log.info("同步病患待确认实时数据{}条",patientAreaRuntimeList.size());
        }
        log.info("同步病患待确认实时数据结束");
    }

    /**
	 * 
	 * 更新同步单条测试数据
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月6日 下午10:32:43
	 *
	 * @param dimPO
	 * @param temp
	 */
	public void updateOneRuntime(LocationDimPO dimPO, ChinaCountStatisticsPO temp) {
		if (dimPO == null) {
			log.warn("同步数据未配置行政区划维度[{}][{}]", dimPO, temp);
			return;
		}

		String areaNo = dimPO.getAreaNo();
		// 在runtime表中查找对应数据
		Optional<PatientAreaRuntimePO> optional = patientAreaRuntimeDao.findById(areaNo);
		if (optional.isPresent()) {
			// 判断是否需要更新实时数据
			PatientAreaRuntimePO one = optional.get();
			if (LockFlag.LOCK.equals(one.getLockFlag())) {
				return;
			}
			
			Integer confirmCase = temp.getConfirmCase() == null ? 0 : temp.getConfirmCase();
			one.setConfirmCase(confirmCase);
			Integer deadCase = temp.getDeadCase() == null ? 0 : temp.getDeadCase();
			one.setDeadCase(deadCase);
			Integer cureCase = temp.getCureCase() == null ? 0 : temp.getCureCase();
			one.setCureCase(cureCase);
			Integer newCase = temp.getNewCase() == null ? 0 : temp.getNewCase();
			one.setNewCase(newCase);
			Integer newDead = temp.getNewDead();
			one.setNewDead(newDead);
			Integer newCure = temp.getNewCure();
			one.setNewCure(newCure);
            Integer probableCase=temp.getProbableCase() == null ? 0 : temp.getProbableCase();
            one.setProbableCase(probableCase);
            one.setUpdateTime(DateTime.now().toDate());
			patientAreaRuntimeDao.save(one);
		} else {
			// 更新实时数据
			PatientAreaRuntimePO.PatientAreaRuntimePOBuilder builder = PatientAreaRuntimePO.builder();
			PatientAreaRuntimePO runtimePO = builder.id(dimPO.getAreaNo()).areaCode(dimPO.getAreaNo())
					.areaType(temp.getAreaType()).areaName(temp.getAreaName())
					.newCase(temp.getNewCase() == null ? 0 : temp.getNewCase())
					.cureCase(temp.getCureCase() == null ? 0 : temp.getCureCase())
					.deadCase(temp.getDeadCase() == null ? 0 : temp.getDeadCase())
					.probableCase(temp.getProbableCase() == null ? 0 : temp.getProbableCase())
					.confirmCase(temp.getConfirmCase() == null ? 0 : temp.getConfirmCase())
					.lockFlag(LockFlag.UNLOCK)
					.updateTime(DateTime.now().toDate()).parentNo(dimPO.getParentNo()).build();
			patientAreaRuntimeDao.save(runtimePO);
		}
		
		log.info("更新实时数据[{}]", areaNo);
	}

    public static Date strToDate(String dateString) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date currentTime = formatter.parse(dateString);
            return currentTime;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return DateTime.now().toDate();

	}
}
