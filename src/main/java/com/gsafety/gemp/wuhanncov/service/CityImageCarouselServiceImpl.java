package com.gsafety.gemp.wuhanncov.service;

import cn.hutool.core.collection.CollectionUtil;
import com.google.common.collect.Lists;
import com.gsafety.gemp.wuhanncov.contract.dto.ImageCarouselDTO;
import com.gsafety.gemp.wuhanncov.contract.service.CityImageCarouselService;
import com.gsafety.gemp.wuhanncov.dao.CityImageCarouselDao;
import com.gsafety.gemp.wuhanncov.dao.po.CityImageCarouselPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("cityImageCarouselService")
public class CityImageCarouselServiceImpl implements CityImageCarouselService {

    @Autowired
    private CityImageCarouselDao cityImageCarouselDao;

    @Override
    public List<ImageCarouselDTO> listCarouselByAreaCode(String areaCode) {
        List<CityImageCarouselPO> list = cityImageCarouselDao.findAllByAreaCodeOrderByNumAsc(areaCode);
        List<ImageCarouselDTO> ret = Lists.newArrayList();
        if (CollectionUtil.isEmpty(list)) {
            return ret;
        }
        for (CityImageCarouselPO po : list) {
            ret.add(ImageCarouselDTO.builder()
                    .description(po.getDescription())
                    .url(po.getUrl())
                    .build());
        }
        return ret;
    }
}
