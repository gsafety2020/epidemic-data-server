package com.gsafety.gemp.wuhanncov.service;

import com.gsafety.gemp.wuhanncov.contract.service.DimService;
import com.gsafety.gemp.wuhanncov.dao.LocationDimDao;
import com.gsafety.gemp.wuhanncov.dao.po.LocationDimPO;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author Administrator
 * @Title: DimClassfiy
 * @ProjectName wuhan-ncov
 * @Description: TODO
 * @date 2020/2/316:02
 */

@Service("dimClassifier")
public class DimClassifier {

    public static final int STREET_CODE_LENGTH = 12;

    public static final String DEFAULT_COUNTRY_PARENT = "-1";

    public static final String DEFAULT_PROVINCE_END = "0000";

    @Resource
    private LocationDimDao locationDao;

    public DimService Sort(String code) {
        if(code.length() == STREET_CODE_LENGTH) {
            return new StreetDimServiceImpl(code);
        }

        LocationDimPO ldpo = locationDao.findByAreaNo(code);
        if (null != ldpo) {
            if(StringUtils.equals(DEFAULT_COUNTRY_PARENT, ldpo.getParentNo())) {
                return new CountryDimServiceImpl(ldpo);
            }

            if(code.endsWith(DEFAULT_PROVINCE_END)) {
                return new ProvinceDimServiceImpl(ldpo);
            }

            if(ldpo.getParentNo().endsWith(DEFAULT_PROVINCE_END) && !ldpo.getAreaNo().startsWith("81")) {
                return new CityDimServiceImpl(ldpo);
            }

            return new DistrictDimServiceImpl(ldpo);
        }

        return null;
    }
}
