package com.gsafety.gemp.wuhanncov.service;

import com.gsafety.gemp.wuhanncov.contract.service.DimService;
import com.gsafety.gemp.wuhanncov.dao.LocationDimDao;
import com.gsafety.gemp.wuhanncov.dao.po.LocationDimPO;
import com.gsafety.gemp.wuhanncov.utils.RegExUtil;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Resource;

/**
 * @author Administrator
 * @Title: CountryDimServiceImpl
 * @ProjectName wuhan-ncov
 * @Description: parent返回省，city返回自己，省返回省份
 * @date 2020/2/314:29
 */
public class StreetDimServiceImpl implements DimService{

    private final String AREA_TYPE = "street";

    private String areaCode;

    @Resource
    private LocationDimDao locationDao;

    public StreetDimServiceImpl(String areaCode) {
        this.areaCode = areaCode;
    }

    @Override
    public String getParent() {
        return areaCode.substring(0, 6);
    }

    @Override
    public String getProvince() {
        return areaCode.substring(0, 2).concat("0000");
    }

    @Override
    public String getCity() {
        return areaCode.substring(0, 4).concat("00");
    }

    @Override
    public String getType() {
        return this.AREA_TYPE;
    }

}
