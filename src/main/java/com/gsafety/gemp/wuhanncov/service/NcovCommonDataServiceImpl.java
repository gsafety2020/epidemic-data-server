package com.gsafety.gemp.wuhanncov.service;

import com.gsafety.gemp.wuhanncov.constant.AppConstant;
import com.gsafety.gemp.wuhanncov.contract.dto.*;
import com.gsafety.gemp.wuhanncov.contract.dto2.PatientAreaInfoDailyDTO2;
import com.gsafety.gemp.wuhanncov.contract.enums.NumberTypeEnum;
import com.gsafety.gemp.wuhanncov.contract.service.NcovCommonDataService;
import com.gsafety.gemp.wuhanncov.dao.*;
import com.gsafety.gemp.wuhanncov.dao.po.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ReflectionUtils;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

/**
 * 该类用于疫情数据大屏,用于实现通用查询疫情数据服务等能力
 *
 * @author lyh
 * @since 2020/1/30  13:17
 */
@Service("ncovCommonDataService")
public class NcovCommonDataServiceImpl implements NcovCommonDataService {

    @Resource
    private PatientAreaStatisticDao patientAreaStatisticDao;

    @Resource
    private LocationDimDao locationDimDao;

    @Resource
    private ChinaCountStatisticsDao chinaCountStatisticsDao;

    @Resource
    private PatientAreaRuntimeDao patientAreaRuntimeDao;

    @Resource
    private PubSentimentNewsDao pubSentimentNewsDao;

    @Resource
    private ChinaAreaCaseInfoDao chinaAreaCaseInfoDao;

    /***
     * 行政区病患状态拟合接口，查询指定行政区划内时间窗口的累计确诊、新增确诊、重症、死亡、治愈数据。参数：时间窗口、行政区划编码
     * @author lyh
     * @since 2020/1/30 13:15
     * @param   areaId 区域行政编码
     * @param   days 要返回的数据天数
     * @return java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaCountDailyDTO>
     * @throws
     */
    @Override
    public List<PatientAreaInfoDailyDTO> getPatientAreaInfoHistory(String areaId, Integer days, List<String> types) {

        List<PatientAreaInfoDailyDTO> resultList = new ArrayList<>();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date now = new Date();
        String startDate = sdf.format(DateUtils.addDays(now, -2 - days));
        String endDate = sdf.format(now);
        //获取指定区域和时间段的患者统计数据列表
        List<PatientAreaStatisticPO> poList = patientAreaStatisticDao
                .findByAreaCodeAndStasDateGreaterThanAndStasDateLessThanOrderByStasTimeAsc(areaId, startDate, endDate);
        if (poList != null && poList.size() > 0) {
            PatientAreaInfoDailyDTO dto = null;
            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
            String dateStr = null;
            PatientAreaStatisticPO po = null;
            for (int i = 0; i < poList.size(); i++) {

                po = poList.get(i);

                //新增确诊人数计算
                Integer incNumber = 0;
                Integer incCured = 0;
                Integer incDeath = 0;
                Integer incDying = 0;
                Integer incSevere = 0;
                if (types.contains(NumberTypeEnum.INC.getType()) || types.contains(NumberTypeEnum.NEW_CURED.getType())
                        || types.contains(NumberTypeEnum.NEW_DEATH.getType())
                        || types.contains(NumberTypeEnum.NEW_DYING.getType())
                        || types.contains(NumberTypeEnum.NEW_SEVERE.getType())) {
                    if (i == 0) {
                        if (poList.size() > days) {
                            continue;
                        } else {
                            incNumber = 0;
                            incCured = 0;
                            incDeath = 0;
                            incDying = 0;
                            incSevere = 0;
                        }
                    } else {
                        incNumber = po.getPatientNumber() - poList.get(i - 1).getPatientNumber();
                        incCured = (po.getCuredNumber() == null ? 0 : po.getCuredNumber()) -
                                (poList.get(i - 1).getCuredNumber() == null ? 0 : poList.get(i - 1).getCuredNumber());
                        incDeath = (po.getDeathNumber() == null ? 0 : po.getDeathNumber()) -
                                (poList.get(i - 1).getDeathNumber() == null ? 0 : poList.get(i - 1).getDeathNumber());
                        incDying = (po.getDyingNumber() == null ? 0 : po.getDyingNumber()) -
                                (poList.get(i - 1).getDyingNumber() == null ? 0 : poList.get(i - 1).getDyingNumber());
                        incSevere = (po.getSevereNumber() == null ? 0 : po.getSevereNumber()) -
                                (poList.get(i - 1).getSevereNumber() == null ? 0 : poList.get(i - 1).getSevereNumber());
                    }
                }

                dateStr = format.format(po.getStasTime());

                int j = 1;
                for (String t : types) {
                    dto = new PatientAreaInfoDailyDTO();
                    dto.setX(dateStr);
                    if (NumberTypeEnum.INC.getType().equals(t)) {
                        dto.setY(incNumber);
                    } else if (NumberTypeEnum.NEW_CURED.getType().equals(t)) {
                        dto.setY(incCured);
                    } else if (NumberTypeEnum.NEW_DEATH.getType().equals(t)) {
                        dto.setY(incDeath);
                    } else if (NumberTypeEnum.NEW_DYING.getType().equals(t)) {
                        dto.setY(incDying);
                    } else if (NumberTypeEnum.NEW_SEVERE.getType().equals(t)) {
                        dto.setY(incSevere);
                    } else {
                        dto.setY(getValueByFiledNameFromPO(NumberTypeEnum.getFiledNameByType(t), po));
                    }
                    dto.setS(String.valueOf(j++));
                    resultList.add(dto);
                }
            }
        }
        return resultList;
    }

    @Override
    public List<PatientAreaCountDailyDTO> getAreaListPatientCountHistory(List<String> areaIds) {

        List<PatientAreaCountDailyDTO> resultList = new ArrayList<>();
        if (areaIds != null && areaIds.size() > 0) {
            Integer i = 1;
            for (String areaCode : areaIds) {
                resultList.addAll(getPatientAreaCountDaily(areaCode, String.valueOf(i++)));
            }
        }
        return resultList;
    }
    @Override
    public List<PatientAreaCountDailyDTO> getAreaListPatientNewCountHistory(List<String> areaIds) {

        List<PatientAreaCountDailyDTO> resultList = new ArrayList<>();
        if (areaIds != null && areaIds.size() > 0) {
            Integer i = 1;
            for (String ereaCode : areaIds) {
                resultList.addAll(getPatientAreaNewCountDaily(ereaCode, String.valueOf(i++)));
            }
        }
        return resultList;
    }

    @Override
    public TopArea getPatientAreaTop(List<String> areaIdList, int pageNum, int pageSize) {
        Sort sort = Sort.by(Sort.Direction.DESC, "confirmCase");
        List<PatientAreaRuntimePO> poList = new ArrayList<PatientAreaRuntimePO>();
        if (pageSize == -1) {
            poList = patientAreaRuntimeDao.findAll(new Specification<PatientAreaRuntimePO>() {

                @Override
                public Predicate toPredicate(Root<PatientAreaRuntimePO> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder builder) {
                    List<Predicate> list = new ArrayList<Predicate>();
                    Predicate p1 = builder.equal(root.get("areaType"), "province");
                    Predicate p2 = builder.not(root.get("areaCode").in(areaIdList));
                    list.add(p1);
                    if (areaIdList != null && areaIdList.size() > 0) {
                        list.add(p2);
                    }
                    Predicate[] p = new Predicate[list.size()];
                    return builder.and(list.toArray(p));
                }
            }, sort);
        } else {
            Pageable pageable = PageRequest.of(pageNum, pageSize, sort);
            poList = patientAreaRuntimeDao.findAll(new Specification<PatientAreaRuntimePO>() {
                @Override
                public Predicate toPredicate(Root<PatientAreaRuntimePO> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder builder) {
                    List<Predicate> list = new ArrayList<Predicate>();
                    Predicate p1 = builder.equal(root.get("areaType"), "province");
                    Predicate p2 = builder.not(root.get("areaCode").in(areaIdList));
                    list.add(p1);
                    if (areaIdList != null && areaIdList.size() > 0) {
                        list.add(p2);
                    }
                    Predicate[] p = new Predicate[list.size()];
                    return builder.and(list.toArray(p));
                }
            }, pageable).getContent();
        }
        TopArea topAreaList = TopArea.builder().build();
        for (PatientAreaRuntimePO po : poList) {
            topAreaList.add(po.getAreaCode(), po.getAreaName(), po.getConfirmCase());
        }
        return topAreaList;
    }


    @Override
    public List<AreaWeekValue> getPatientAreaListTop(List<String> areaIdList, int pageNum, int pageSize, int week) {
        List<AreaWeekValue> dtos = new ArrayList();
        Date beginDate = com.gsafety.gemp.wuhanncov.utils.DateUtils.getBeginDayOfLastWeek(week);
        Date endDate = com.gsafety.gemp.wuhanncov.utils.DateUtils.getEndDayOfLastWeek(week);
        String beginStr = com.gsafety.gemp.wuhanncov.utils.DateUtils.format(beginDate, com.gsafety.gemp.wuhanncov.utils.DateUtils.DATE_PATTERN_A);
        String endStr = com.gsafety.gemp.wuhanncov.utils.DateUtils.format(endDate, com.gsafety.gemp.wuhanncov.utils.DateUtils.DATE_PATTERN_A);
        List<LocationDimPO> dimPOList = new ArrayList<LocationDimPO>();
        if (pageSize == -1) {
            dimPOList = locationDimDao.findAllProvince();
        } else {
            Sort sort = Sort.by(Sort.Direction.ASC, "areaNo");
            Pageable pageable = PageRequest.of(pageNum, pageSize, sort);
            dimPOList = locationDimDao.findAll(new Specification<LocationDimPO>() {
                @Override
                public Predicate toPredicate(Root<LocationDimPO> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder builder) {
                    List<Predicate> list = new ArrayList<Predicate>();
                    Predicate p1 = builder.equal(root.get("parentNo"), AppConstant.DIM_ChINA_CODE);
                    Predicate p2 = builder.not(root.get("areaNo").in(areaIdList));
                    list.add(p1);
                    if (areaIdList != null && areaIdList.size() > 0) {
                        list.add(p2);
                    }
                    Predicate[] p = new Predicate[list.size()];
                    return builder.and(list.toArray(p));
                }
            }, pageable).getContent();
        }
        int n = 0;
        for (LocationDimPO dimPO : dimPOList) {
            n++;
            String areaNo = dimPO.getAreaNo();
            String areaName = dimPO.getShortName();
            int value = 0;
            int lastValue = 0;
            List<Object[]> obj = patientAreaStatisticDao.findWeekArea(areaNo, beginStr, endStr);
            if (obj != null && obj.size() > 0) {
                String patientNumber = String.valueOf(obj.get(0)[2]);
                value = Integer.valueOf(patientNumber);
                AreaWeekValue lastWeek = getAreaWeekValue(areaNo, week + 1);
                if (lastWeek != null) {
                    Integer _value = lastWeek.getValue();
                    lastValue = _value.intValue();
                }
            }
            AreaWeekValue dto = new AreaWeekValue();
            dto.setAreaName(areaName);
            dto.setValue(value - lastValue);
            dto.setAreaCode(areaNo);
            dto.setS(String.valueOf(n));
            dtos.add(dto);
        }
        Collections.sort(dtos, new Comparator<AreaWeekValue>() {
            @Override
            public int compare(AreaWeekValue o1, AreaWeekValue o2) {
                return o2.getValue() - o1.getValue();
            }
        });
        return dtos;
    }

    private AreaWeekValue getAreaWeekValue(String area_code, int week) {
        Date beginDate = com.gsafety.gemp.wuhanncov.utils.DateUtils.getBeginDayOfLastWeek(week);
        Date endDate = com.gsafety.gemp.wuhanncov.utils.DateUtils.getEndDayOfLastWeek(week);
        String beginStr = com.gsafety.gemp.wuhanncov.utils.DateUtils.format(beginDate, com.gsafety.gemp.wuhanncov.utils.DateUtils.DATE_PATTERN_A);
        String endStr = com.gsafety.gemp.wuhanncov.utils.DateUtils.format(endDate, com.gsafety.gemp.wuhanncov.utils.DateUtils.DATE_PATTERN_A);
        List<Object[]> poList = patientAreaStatisticDao.findWeekAreaCountByareaCode(area_code, beginStr, endStr);
        if (poList != null && poList.size() > 0) {
            Object[] obj = poList.get(0);
            if (obj != null && obj.length > 0) {
                String areaCode = String.valueOf(obj[0]);
                String area = String.valueOf(obj[1]);
                String patientNumber = String.valueOf(obj[2]);
                Integer value = Integer.parseInt(patientNumber);
                AreaWeekValue dto = new AreaWeekValue();
                dto.setAreaName(area);
                dto.setValue(value);
                dto.setAreaCode(areaCode);
                dto.setS("0");
                return dto;
            }
        }
        return null;
    }

    @Override
    public List<NewsInfo> findNewsInfo(List<String> keyworlds, int size) {
        List<NewsInfo> news = new ArrayList<NewsInfo>();
        Sort sort = Sort.by(Sort.Direction.DESC, "newsTime");
        Pageable pageable = PageRequest.of(0, size, sort);
        List<PubSentimentNewsPO> list = pubSentimentNewsDao.findAll(new Specification<PubSentimentNewsPO>() {

            @Override
            public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder builder) {

                if (keyworlds.size() > 0) {
                    List predicateList = new ArrayList();
                    Predicate[] p = new Predicate[keyworlds.size()];
                    for (String keyworld : keyworlds) {
                        predicateList.add(builder.like(root.get("newsTitle"), "%" + keyworld + "%"));
                    }
                    predicateList.toArray(p);
                    return builder.or(p);
                }
                return null;


            }
        }, pageable).getContent();
        for (PubSentimentNewsPO po : list) {
            NewsInfo info = new NewsInfo();
            info.setTitle(po.getNewsTitle());
            info.setDetail(po.getNewsContent());
            info.setLink(po.getNewsUrl());
            Instant instant = po.getNewsTime().toInstant();
            ZoneId zoneId = ZoneId.systemDefault();
            LocalDateTime localDateTime = instant.atZone(zoneId).toLocalDateTime();
            info.setTime(com.gsafety.gemp.wuhanncov.utils.DateUtils.formatDateTime(localDateTime, com.gsafety.gemp.wuhanncov.utils.DateUtils.DATE_TIME_PATTERN));
            news.add(info);
        }

        return news;
    }

    @Override
    public List<ChinaCountStatisticsBubbling> getParentCountStatisticsBubbling(String parentNo) {
        List<ChinaCountStatisticsBubbling> chinaCountStatisticsBubblings = new ArrayList<>();
        List<PatientAreaRuntimePO> patientAreaRuntimes = patientAreaRuntimeDao.findByParentNo(parentNo);
        patientAreaRuntimes.stream().forEach(count -> {
            ChinaCountStatisticsBubbling bubbling = new ChinaCountStatisticsBubbling();
            String areaName = count.getAreaName();
            String areaNo=count.getAreaCode();
            LocationDimPO locationDim = locationDimDao.findByAreaNo(areaNo);
            if (locationDim!=null) {
                bubbling.setLat(locationDim.getLatitude());
                bubbling.setLng(locationDim.getLongitude());
                String info = "确诊:" + count.getConfirmCase() + ",治愈:" + count.getCureCase() + ",死亡:" + count.getDeadCase();
                bubbling.setInfo(info);
                bubbling.setName(locationDim.getAreaName());
                bubbling.setConfirmCase(count.getConfirmCase());
                bubbling.setCureCase(count.getCureCase());
                bubbling.setDeadCase(count.getDeadCase());
                chinaCountStatisticsBubblings.add(bubbling);
            }

        });
        return chinaCountStatisticsBubblings;
    }


    @Override
    public List<AreaHotDTO> getAreaPatientCountListByParent(String areaParent) {
        List<AreaHotDTO> resultList = new ArrayList<>();

        List<ChinaCountStatisticsPO> poList = null;
        if ("0".equals(areaParent)) {//全国
            poList = chinaCountStatisticsDao.findByAreaType("province");
        } else if (areaParent.endsWith("0000")) {//省级和直辖市级
            Optional<LocationDimPO> byId = locationDimDao.findById(areaParent);
            poList = chinaCountStatisticsDao.findByAreaTypeAndProvinceLike("city", byId.get().getAreaName().substring(0, 2));
        } else {//市级
            Optional<LocationDimPO> byId = locationDimDao.findById(areaParent);
            poList = chinaCountStatisticsDao.findByAreaTypeAndCityLike("county", byId.get().getAreaName().substring(0, 2));
        }

        Sort sort = Sort.by(Sort.Direction.ASC, "seqNum");
        List<LocationDimPO> locationList = locationDimDao.findByParentNo(areaParent, sort);

        AreaHotDTO dto = null;
        for (ChinaCountStatisticsPO po : poList) {
            dto = new AreaHotDTO();

            //通过区域名称查询区域编码
            for (LocationDimPO lpo : locationList) {
                if (lpo.getAreaName().contains(po.getAreaName().substring(0, 2))) {
                    dto.setArea_id(lpo.getAreaNo());
                    break;
                }
            }

            if (StringUtils.isEmpty(dto.getArea_id())) {
                dto.setArea_id(po.getAreaName());
            }

            Integer confirmCase = po.getConfirmCase();

            dto.setValue((confirmCase == null || confirmCase == 0) ? null : confirmCase);
            resultList.add(dto);
        }

        return resultList;
    }

    /**
     * 用于地图上的患者数量热力图展示，查询父区域下各子区域的患者确诊数量接口
     * 数据来源人工维护，较准确，表：patient_area_runtime
     *
     * @param areaParent 父区域行政区划编码
     * @return List<AreaHotDTO>
     * @author lyh
     * @since 2020/1/30 14:21
     */
    @Override
    public List<AreaHotDTO> getAreaPatientCountFromRuntimeByParent(String areaParent) {
        List<AreaHotDTO> resultList = new ArrayList<>();
        List<PatientAreaRuntimePO> poList = patientAreaRuntimeDao.findByParentNo(areaParent);
        AreaHotDTO dto;
        for (PatientAreaRuntimePO po : poList) {
            dto = new AreaHotDTO();

//            dto.setAreaName(po.getAreaName());
//            dto.setProbableCase(po.getProbableCase());
//            dto.setCureCase(po.getCureCase());
//            dto.setDeadCase(po.getDeadCase());
//            dto.setConfirmCase(po.getConfirmCase());
//            dto.setNewCase(po.getNewCase());
            BeanUtils.copyProperties(po, dto);
            dto.setArea_id(po.getAreaCode());
            // value 使用 确诊人数
            dto.setValue(po.getConfirmCase());

            resultList.add(dto);
        }
        return resultList;
    }


    /***
     * 按区域和类型获取患者统计按天统计数据
     * @author lyh
     * @since 2020/1/26 20:02
     * @param   areaCode 区域编码
     * @param   type 类型
     * @return java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaCountDailyDTO>
     * @throws
     */
    private List<PatientAreaCountDailyDTO> getPatientAreaCountDaily(String areaCode, String type) {
        List<PatientAreaCountDailyDTO> resultList = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String todayDate = dateFormat.format(new Date());

        //获取全国患者统计数据列表
        List<PatientAreaStatisticPO> poList = patientAreaStatisticDao.findByAreaCodeAndStasDateLessThanOrderByStasTimeDesc(areaCode, todayDate);
        if (poList != null && poList.size() > 0) {
            PatientAreaCountDailyDTO dto = null;
            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
            for (PatientAreaStatisticPO po : poList) {
                dto = new PatientAreaCountDailyDTO();
                dto.setX(format.format(po.getStasTime()));
                dto.setY(po.getPatientNumber());
                dto.setS(type);
                resultList.add(dto);
            }
        }
        return resultList;
    }

    /**
     * 查询区域的新增确诊数
     * @param areaCode
     * @param type
     * @return
     */
    private List<PatientAreaCountDailyDTO> getPatientAreaNewCountDaily(String areaCode, String type) {
        List<PatientAreaCountDailyDTO> resultList = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String todayDate = dateFormat.format(new Date());

        //获取全国患者统计数据列表
        List<PatientAreaStatisticPO> poList = patientAreaStatisticDao.findByAreaCodeAndStasDateLessThanOrderByStasTimeAsc(areaCode, todayDate);
        if (poList != null && poList.size() > 0) {
            PatientAreaCountDailyDTO dto = null;
            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
            PatientAreaStatisticPO po = null;
            for (int i = 0; i < poList.size(); i++) {
                po = poList.get(i);

                //新增确诊人数计算
                Integer incNumber = 0;
                if (i == 0) {
                    incNumber = 0;
                } else {
                    incNumber = po.getPatientNumber() - poList.get(i - 1).getPatientNumber();
                }
                dto = new PatientAreaCountDailyDTO();
                dto.setX(format.format(po.getStasTime()));
                dto.setY(incNumber);
                dto.setS(type);
                resultList.add(dto);

            }
        }
        return resultList;
    }


    @Override
    public List<PatientAreaInfoDailyDTO2> getPatientAreaInfoHistoryV2(String areaId, Integer days) {
        List<PatientAreaInfoDailyDTO2> resultList = new ArrayList<>();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date now = new Date();
        String startDate = sdf.format(DateUtils.addDays(now, -2 - days));
        String endDate = sdf.format(now);
        //获取指定区域和时间段的患者统计数据列表
        List<PatientAreaStatisticPO> poList = patientAreaStatisticDao
                .findByAreaCodeAndStasDateGreaterThanAndStasDateLessThanOrderByStasTimeAsc(areaId, startDate, endDate);
        poList.forEach(temp -> {
            PatientAreaInfoDailyDTO2 record = PatientAreaInfoDailyDTO2.builder().build();
            BeanUtils.copyProperties(temp, record);
            resultList.add(record);
        });

        return resultList;
    }

    /***
     * 根据属性名获取属性值
     * @author lyh
     * @since 2020/2/2 15:15
     * @param   methodName  属性名
     * @return java.lang.Integer
     * @throws
     */
    private Integer getValueByFiledNameFromPO(String methodName, PatientAreaStatisticPO po) {
        return (Integer) ReflectionUtils.invokeMethod(ReflectionUtils.findMethod(PatientAreaStatisticPO.class, methodName), po);
    }

}
