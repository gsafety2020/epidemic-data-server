/**
 * 
 */
package com.gsafety.gemp.wuhanncov.service;

import com.gsafety.gemp.wuhanncov.dao.po.ChinaMigrationInfoPO;
import org.springframework.data.jpa.domain.Specification;

import java.util.Date;

/**
 * 该类用于...，用于实现...等能力。
 *
 * @author zhoushuyan
 *
 * @since 2020年1月25日 下午8:30:22
 *
 */
public class MigrateSpecification {

	/**
	 *
	 */
	private MigrateSpecification() {
		super();
	}

	public static Specification<ChinaMigrationInfoPO> areaIdEqual(String regex) {
		return (root, query, builer) -> builer.equal(root.get("areaId"), regex);
	}

	public static Specification<ChinaMigrationInfoPO> migrateTypeEqual(String regex) {
		return (root, query, builer) -> builer.equal(root.get("migrateType"), regex);
	}

	public static Specification<ChinaMigrationInfoPO> createDateEqual(String regex) {
		return (root, query, builer) -> builer.equal(root.get("createDate"), regex);
	}
	public static Specification<ChinaMigrationInfoPO> levelEqual(String level) {
		return (root, query, builer) -> builer.equal(root.get("level"), level);
	}

}
