package com.gsafety.gemp.wuhanncov.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gsafety.gemp.wuhanncov.contract.dto.UserEpidemicInfoDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.UserEpidemicInfoPartDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.in.UserEpidemicInfoQueryDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.in.UserEpidemicInfoQueryUserDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.in.UserEpidemicInfoReportedDTO;
import com.gsafety.gemp.wuhanncov.contract.enums.UserViralInfectionStatusEnum;
import com.gsafety.gemp.wuhanncov.contract.service.UserEpidemicDepartmentService;
import com.gsafety.gemp.wuhanncov.contract.service.UserEpidemicDeployService;
import com.gsafety.gemp.wuhanncov.contract.service.UserEpidemicInfoService;
import com.gsafety.gemp.wuhanncov.contract.service.UserEpidemicListService;
import com.gsafety.gemp.wuhanncov.dao.UserEpidemicInfoDao;
import com.gsafety.gemp.wuhanncov.dao.po.UserEpidemicDeployPO;
import com.gsafety.gemp.wuhanncov.dao.po.UserEpidemicInfoPO;
import com.gsafety.gemp.wuhanncov.dao.po.UserEpidemicListPO;
import com.gsafety.gemp.wuhanncov.utils.DateUtils;
import com.gsafety.gemp.wuhanncov.utils.ListPageUtils;
import com.gsafety.gemp.wuhanncov.utils.UUIDUtils;


/**
 * 
* @ClassName: UserEpidemicInfoServiceImpl 
* @Description: 新型冠状病毒-用户上报服务方法
* @author luoxiao
* @date 2020年2月3日 上午1:48:31 
*
 */
@Service("userEpidemicInfoService")
public class UserEpidemicInfoServiceImpl implements UserEpidemicInfoService{

	@Autowired
	private UserEpidemicInfoDao userEpidemicInfoDao;
	
	@Autowired
	private UserEpidemicListService userEpidemicListService;
	
	@Autowired
	private UserEpidemicDepartmentService userEpidemicDepartmentService;
	
	@Autowired
	private UserEpidemicDeployService userEpidemicDeployService;
	
	private final String baseDateStr = "2020-01-01";
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String,Object> findAllByPage(UserEpidemicInfoQueryDTO dto,Integer pageSize,
			Integer pageNumber) {
		if(dto.getReportDate() == null){
			throw new RuntimeException("填报日期为null!");
		}
		if(dto.getMobile() == null){
			throw new RuntimeException("手机号码为null!");
		}
		//通过手机号获取组织机构
		List<String> lowerDepartmentCodes = getLowerDepartmentCode(dto.getMobile());
		//按条件查询数据
		Specification<UserEpidemicInfoPO> spec = null;
		Sort sort = new Sort(Sort.Direction.ASC, "userCondition");
		if(StringUtils.isEmpty(dto.getDepartmentName())){  //查询部门及其子部门下的数据
			spec = concatSpecification(dto,lowerDepartmentCodes);
		}else{                                          //查询本部门下的数据
			spec = concatSpecification(dto,null);
		}
		List<UserEpidemicInfoPO> queryList =  userEpidemicInfoDao.findAll(spec,sort);
		
		//查询某一天填报数据
		List<UserEpidemicInfoPO> reportedList = userEpidemicInfoDao.findByReportDate(dto.getReportDate());
		//查询基础数据
		Date baseDate = DateUtils.toDateFormatDate(baseDateStr);
		List<UserEpidemicInfoPO> baseLists = userEpidemicInfoDao.findByReportDate(baseDate);
		//获取某一天未填报数据  处理未填报数据   并且按照查询条件过滤
		List<UserEpidemicInfoPO> notReportedList = calcucateNotReportDay(reportedList,baseLists);
		handleNotReportedData(notReportedList,dto);
		if(StringUtils.isEmpty(dto.getDepartmentName())){  //查询部门及其子部门下的数据
			notReportedList = notReportedList.stream().filter(a->{
				for(String childCode : lowerDepartmentCodes){
					if(childCode.equals(a.getDepartmentCode())){
						return true;
					}
				}
				return false;
			}).collect(Collectors.toList());
		}else{                                          //查询本部门下的数据
			notReportedList = notReportedList.stream().filter(a->a.getDepartmentName().equals(dto.getDepartmentName()))
					.collect(Collectors.toList());
		}
		//拼接结果
		List<UserEpidemicInfoPO> result = new ArrayList<>();
		if(UserViralInfectionStatusEnum.NOT_REPORTE.getCode().equals(dto.getUserCondition())){
			result.addAll(notReportedList);
		}else if(UserViralInfectionStatusEnum.DIAGONSE.getCode().equals(dto.getUserCondition())
				|| UserViralInfectionStatusEnum.CONTIGUITY.getCode().equals(dto.getUserCondition())
				|| UserViralInfectionStatusEnum.SUSPECTED.getCode().equals(dto.getUserCondition())
				|| UserViralInfectionStatusEnum.NORMAL.getCode().equals(dto.getUserCondition())){
			result.addAll(queryList);
		}else{
			queryList = queryList.stream().filter(a->!UserViralInfectionStatusEnum.NORMAL.getCode().equals(a.getUserCondition())).collect(Collectors.toList());
			queryList.addAll(notReportedList);
			result.addAll(queryList);
		}
		//类型转换
		List<UserEpidemicInfoDTO> dtoList = new ArrayList<>();
		if(!CollectionUtils.isEmpty(result)){
			List<UserEpidemicInfoPO> pageList =(List<UserEpidemicInfoPO>) ListPageUtils.startPage(result, pageNumber, pageSize);
			pageList.stream().forEach(a->{
				UserEpidemicInfoDTO userEpidemicInfoDTO = new UserEpidemicInfoDTO();
				BeanUtils.copyProperties(a, userEpidemicInfoDTO);
				dtoList.add(userEpidemicInfoDTO);
			});
		}	
		if(StringUtils.isEmpty(dto.getDepartmentName())){  //查询部门及其子部门下的数据
			reportedList = reportedList.stream().filter(a->{
				for(String childCode:lowerDepartmentCodes){
					if(childCode.equals(a.getDepartmentCode())){
						return true;
					}
				}
				return false;
			}).collect(Collectors.toList());
		}else{       //查询本部门下的数据
			reportedList = reportedList.stream().filter(a->a.getDepartmentName().equals(dto.getDepartmentName()))
					.collect(Collectors.toList());
		}
		//查询感染人数	
		List<UserEpidemicInfoPO> diagonse = new ArrayList<>();	//确诊
		List<UserEpidemicInfoPO> contiguity = new ArrayList<>();//密接
		List<UserEpidemicInfoPO> suspected = new ArrayList<>(); //疑似
		List<UserEpidemicInfoPO> normal = new ArrayList<>();  //正常
		
		if(!CollectionUtils.isEmpty(reportedList)){
			diagonse = reportedList.stream().filter(a->UserViralInfectionStatusEnum.DIAGONSE.getCode().equals(a.getUserCondition())).collect(Collectors.toList());
			contiguity = reportedList.stream().filter(a->UserViralInfectionStatusEnum.CONTIGUITY.getCode().equals(a.getUserCondition())).collect(Collectors.toList());
			suspected = reportedList.stream().filter(a->UserViralInfectionStatusEnum.SUSPECTED.getCode().equals(a.getUserCondition())).collect(Collectors.toList());
			normal = reportedList.stream().filter(a->UserViralInfectionStatusEnum.NORMAL.getCode().equals(a.getUserCondition())).collect(Collectors.toList());
		}
		
		Map<String,Object> map = new HashMap<>();
		map.put("totalUserNum", notReportedList.size()+reportedList.size());//总人数
		map.put("diagonseUserNum", diagonse.size());//确诊人数
		map.put("contiguityUserNum", contiguity.size());//密接人数
		map.put("suspectedUserNum", suspected.size());//疑似人数
		map.put("normalUserNum", normal.size());//正常人数
		map.put("notReportUserNum", notReportedList.size());//未填报人数
		map.put("reportedUserNum", reportedList.size());//填报人数
		map.put("data", dtoList);
		return map;
	}

	@Override
	@Transactional
	public void saveUserEpidemicInfo(UserEpidemicInfoPO userEpidemicInfoPO) {
		//1.设置插入时间和填报时间
		userEpidemicInfoPO.setCreateTime(Calendar.getInstance().getTime());
		userEpidemicInfoPO.setReportDate(Calendar.getInstance().getTime());
		vaildateUserEpidemicInfoPO(userEpidemicInfoPO);
		
		//2.判断当天是否已经填写
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String nowDateString = format.format(Calendar.getInstance().getTime());
		Date newDate = DateUtils.toDateFormatDate(nowDateString);
		UserEpidemicInfoPO po = userEpidemicInfoDao.findByUserIdAndReportDate(userEpidemicInfoPO.getUserId(), newDate);
		if(po != null){
			throw new RuntimeException("您今天已经上报信息了,不能再次上报!");
		}
		
		//3.判断本人详情和家人详情的必填情况
		if(!UserViralInfectionStatusEnum.NORMAL.getCode().equals(userEpidemicInfoPO.getUserCondition()) 
				&& StringUtils.isBlank(userEpidemicInfoPO.getUserDetail())){
			throw new RuntimeException("本人状况是确诊/疑似/密接，请输入本人详情!");
		}
		if(!UserViralInfectionStatusEnum.NORMAL.getCode().equals(userEpidemicInfoPO.getFamilyCondition()) 
				&& StringUtils.isBlank(userEpidemicInfoPO.getFamilyDetail())){
			throw new RuntimeException("家人状况是确诊/疑似/密接，则输入家人详情!");
		}
		//通过手机号查询departmentCode
		UserEpidemicListPO listPo =userEpidemicListService.getUserByMobile(userEpidemicInfoPO.getMobile());
		if(listPo == null){
			userEpidemicInfoPO.setDepartmentCode(null);
		}else{
			userEpidemicInfoPO.setDepartmentCode(listPo.getDepartmentCode());
		}
		userEpidemicInfoPO.setEpidemicInfoId(UUIDUtils.getUUID());
		userEpidemicInfoDao.save(userEpidemicInfoPO);
		
		//查询基础表是否有该数据
		Date baseDate = DateUtils.toDateFormatDate(baseDateStr);
		UserEpidemicInfoPO  basePO =userEpidemicInfoDao.findByUserIdAndReportDate(userEpidemicInfoPO.getUserId(), baseDate);
		if(basePO == null){
			basePO = new UserEpidemicInfoPO();
			BeanUtils.copyProperties(userEpidemicInfoPO, basePO);
			basePO.setEpidemicInfoId(UUIDUtils.getUUID());
			basePO.setUserCondition(UserViralInfectionStatusEnum.NORMAL.getCode());
			basePO.setReportDate(baseDate);
			basePO.setFamilyCondition(UserViralInfectionStatusEnum.NORMAL.getCode());
			basePO.setFamilyDetail(null);
			basePO.setUserDetail(null);
			basePO.setHomeQuarantineDate(null);
			basePO.setHomeQuarantineDays(null);
			userEpidemicInfoDao.save(basePO);
		}else{
			//更新基础数据的部门和姓名
			if(!userEpidemicInfoPO.getDepartmentName().equals(basePO.getDepartmentName())
					|| !userEpidemicInfoPO.getUserName().equals(basePO.getUserName())
					|| !basePO.getDepartmentCode().equals(listPo.getDepartmentCode())){
				basePO.setDepartmentName(userEpidemicInfoPO.getDepartmentName());
				if(listPo == null){
					basePO.setDepartmentCode(null);
				}else{
					basePO.setDepartmentCode(listPo.getDepartmentCode());
				}
				basePO.setUserName(userEpidemicInfoPO.getUserName());	
				userEpidemicInfoDao.updateInfoByMobile(basePO.getUserName(), basePO.getDepartmentName(), basePO.getDepartmentCode(), basePO.getMobile());
			}
		}
		
	}

	@Override
	public UserEpidemicInfoDTO findByEpidemicInfoId(String epidemicInfoId) {
		if(StringUtils.isBlank(epidemicInfoId)){
			throw new RuntimeException("上报信息epidemicInfoId不能为空!");
		}
		UserEpidemicInfoPO po = userEpidemicInfoDao.findByEpidemicInfoId(epidemicInfoId);
		UserEpidemicInfoDTO dto = UserEpidemicInfoDTO.builder().build();
		BeanUtils.copyProperties(po, dto);
		return dto;
	}

	
	private void vaildateUserEpidemicInfoPO(UserEpidemicInfoPO po){
		if(po == null){
			throw new RuntimeException("上报信息为null!");
		}
		if(po.getReportDate() == null){
			throw new RuntimeException("上报信息-填报日期为null!");
		}
		if(StringUtils.isBlank(po.getUserId())){
			throw new RuntimeException("上报信息-用户Id为null!");
		}
	}
	
	@SuppressWarnings("serial")
	private Specification<UserEpidemicInfoPO> concatSpecification(UserEpidemicInfoQueryDTO dto,List<String> lowerDepartmentCodes){
		
		Specification<UserEpidemicInfoPO> spec = new Specification<UserEpidemicInfoPO>(){
			@Override
			public Predicate toPredicate(Root<UserEpidemicInfoPO> root, CriteriaQuery<?> query,
					CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList<>(); //所有的断言
				if(dto != null){
					if(StringUtils.isNotBlank(dto.getUserName())){
						Predicate equalsUserName = cb.equal(root.get("userName").as(String.class),dto.getUserName());
	                    predicates.add(equalsUserName);
					}
					if(StringUtils.isNotBlank(dto.getDepartmentName())){
						Predicate equalDepartmentName = cb.equal(root.get("departmentName").as(String.class),dto.getDepartmentName());
	                    predicates.add(equalDepartmentName);
					}
					if(dto.getReportDate() != null){
						Predicate equalReportDate = cb.equal(root.get("reportDate").as(Date.class), dto.getReportDate());
						predicates.add(equalReportDate);
					}
					if(!CollectionUtils.isEmpty(lowerDepartmentCodes)){
						Predicate inDepartmentCode = cb.in(root.get("departmentCode")).value(lowerDepartmentCodes);
						predicates.add(inDepartmentCode);
					}
					if(UserViralInfectionStatusEnum.DIAGONSE.getCode().equals(dto.getUserCondition())||
							UserViralInfectionStatusEnum.CONTIGUITY.getCode().equals(dto.getUserCondition())
							|| UserViralInfectionStatusEnum.SUSPECTED.getCode().equals(dto.getUserCondition())
							|| UserViralInfectionStatusEnum.NORMAL.getCode().equals(dto.getUserCondition())){
						Predicate equalUserCondition = cb.equal(root.get("userCondition").as(String.class), dto.getUserCondition());
						predicates.add(equalUserCondition);
					}
				}
				return cb.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};	
		return spec;
	}

	@Override
	public UserEpidemicInfoPartDTO getReportedMessage(String userId) {
		if(StringUtils.isBlank(userId)){
			throw new RuntimeException("上报信息-用户Id为null!");
		}
		UserEpidemicInfoPartDTO dto = new UserEpidemicInfoPartDTO();
		List<UserEpidemicInfoPO> poList = userEpidemicInfoDao.findByUserId(userId);
		if(CollectionUtils.isEmpty(poList)){
			dto.setHomeQuarantineDate(Calendar.getInstance().getTime());
			dto.setHomeQuarantineDays(0);
			return dto;
		}
		List<UserEpidemicInfoPO> poReversedList = poList.stream().sorted(Comparator.comparing(UserEpidemicInfoPO::getReportDate).reversed()).collect(Collectors.toList());
		UserEpidemicInfoPO po = poReversedList.get(0);
		BeanUtils.copyProperties(po, dto);
		if(dto.getHomeQuarantineDate() != null){
			if(dto.getHomeQuarantineDays() == null){
				dto.setHomeQuarantineDays(0);
			}else{
				dto.setHomeQuarantineDays(dto.getHomeQuarantineDays()+1);
			}
		}else{
			dto.setHomeQuarantineDate(Calendar.getInstance().getTime());
			dto.setHomeQuarantineDays(0);
		}
		return dto;
	}

	@Override
	public List<String> getAllDepartmentName(String mobile) {
		List<String> cascadeDepartmentCodes = getLowerDepartmentCode(mobile);
		List<String> results = userEpidemicInfoDao.findDpeartmentNameByDepartmentCode(cascadeDepartmentCodes);
		return results;
	}

	@Override
	public Map<String, Object> getAllNotNormalUser(UserEpidemicInfoQueryUserDTO dto) {
		if(dto.getReportDate() == null){
			throw new RuntimeException("填报日期为null!");
		}
		String[] userConditions = {UserViralInfectionStatusEnum.CONTIGUITY.getCode(),UserViralInfectionStatusEnum.DIAGONSE.getCode(),UserViralInfectionStatusEnum.SUSPECTED.getCode()};
		List<String> userNames = userEpidemicInfoDao.findUserNameByUserCondition(dto.getReportDate(),Arrays.asList(userConditions));
		Map<String,Object> map = new HashMap<>();
		map.put("data", userNames);
		map.put("total", userNames.size());
		return map;
	}

	@Override
	public Boolean isReportToday(String userId) {
		if(StringUtils.isEmpty(userId)){
			throw new RuntimeException("用户Id为null!");
		}
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String nowDateString = format.format(Calendar.getInstance().getTime());
		Date newDate =DateUtils.toDateFormatDate(nowDateString);
		UserEpidemicInfoPO po = userEpidemicInfoDao.findByUserIdAndReportDate(userId, newDate);
		if(po != null){
			return true;
		}
		return false;
	}

	@Override
	public Boolean isAlreadyReported(UserEpidemicInfoReportedDTO dto) {
		if(StringUtils.isBlank(dto.getUserName())){
			throw new RuntimeException("用户名不能为空!");
		}
		if(StringUtils.isBlank(dto.getDepartmentName())){
			throw new RuntimeException("部门不能为空!");
		}
		if(StringUtils.isBlank(dto.getMobile())){
			throw new RuntimeException("手机号不能为空!");
		}
		//查询基础表是否有该数据
		Date baseDate = DateUtils.toDateFormatDate(baseDateStr);
		UserEpidemicInfoPO  basePO = userEpidemicInfoDao.findByMobileAndReportDate(dto.getMobile(), baseDate);
		UserEpidemicListPO  listPO = userEpidemicListService.getUserByMobile(dto.getMobile());
		if(basePO == null || !dto.getDepartmentName().equals(basePO.getDepartmentName())
				|| !dto.getUserName().equals(basePO.getUserName())){
			return false;
		}
		if(listPO == null){
			throw new RuntimeException("基础数据不存在!");
		}else{
			if(listPO.getDepartmentCode() == null && basePO.getDepartmentCode() == null){
				return true;
			}else if(listPO.getDepartmentCode().equals(basePO.getDepartmentCode())){
				return true;
			}else{
				return false;
			}
		}
	}
	
	private List<String> getLowerDepartmentCode(String mobile){
		UserEpidemicDeployPO deployPo = userEpidemicDeployService.getDeployPOByMobile(mobile);
		String departmentCode = null;
		if(deployPo == null || StringUtils.isEmpty(deployPo.getDepartmentCode())){
			UserEpidemicListPO listPo = userEpidemicListService.getUserByMobile(mobile);
			if(listPo == null){
				throw new RuntimeException("手机对应的人员不存在!");
			}
			departmentCode = listPo.getDepartmentCode();
		}else{
			departmentCode = deployPo.getDepartmentCode();
		}
		List<String> cascadeDepartmentCodes=userEpidemicDepartmentService.getLowerAllDepartmentCode(departmentCode);
		return cascadeDepartmentCodes;
	}
	
	private List<UserEpidemicInfoPO>  calcucateNotReportDay(List<UserEpidemicInfoPO> reportedList,List<UserEpidemicInfoPO> baseLists){
		List<UserEpidemicInfoPO> notReportedList = new ArrayList<>();
		for(UserEpidemicInfoPO base : baseLists){
			int flag = 0;
			for(UserEpidemicInfoPO reported : reportedList){
				if(base.getUserId().equals(reported.getUserId())){
					flag = 1;
					break;
				}
			}
			if(flag == 0){
				notReportedList.add(base);
			}
		};
		return notReportedList;
	}
	
	private void handleNotReportedData(List<UserEpidemicInfoPO> notReportedList,UserEpidemicInfoQueryDTO dto){
		notReportedList.forEach(notReported->{
			notReported.setUserCondition(null);
			notReported.setFamilyCondition(null);
			notReported.setFamilyNumbers(null);
			notReported.setReportDate(dto.getReportDate());
		});
	}

	@Override
	public JSONObject statisticHistoryBrokenLine(String mobile,int pastDays) {
		//拼接折线图数据格式
		JSONObject  jsonObject = brokenLineDatePattern(pastDays);
		if(StringUtils.isEmpty(mobile)){
			return jsonObject;
		}
		List<String> pastDaysList = DateUtils.getListPastDateString(pastDays,"yyyy-MM-dd");
		pastDaysList = pastDaysList.stream().sorted(Comparator.naturalOrder()).collect(Collectors.toList());
		//获取所以机构代码
		List<String> departmentCodes = getLowerDepartmentCode(mobile);
		if(CollectionUtils.isEmpty(departmentCodes)){
			return jsonObject;
		}
		JSONArray seriesArray =(JSONArray)jsonObject.get("series");
		JSONObject diagonseObject = (JSONObject)seriesArray.get(0);
		JSONObject contiguityObject = (JSONObject)seriesArray.get(1);
		JSONObject suspectedObject = (JSONObject)seriesArray.get(2);
		JSONArray diagonseArray =(JSONArray) diagonseObject.get("data");
		JSONArray contiguityArray =(JSONArray) contiguityObject.get("data");
		JSONArray suspectedArray =(JSONArray) suspectedObject.get("data");
		List<UserEpidemicInfoPO> diagonse = new ArrayList<>();
		List<UserEpidemicInfoPO> contiguity = new ArrayList<>();
		List<UserEpidemicInfoPO> suspected = new ArrayList<>();
		for(String pastDay : pastDaysList){
			Date reportDate = DateUtils.toDateFormatDate(pastDay);
			List<UserEpidemicInfoPO> reportedList = userEpidemicInfoDao.findUserInfoByReprotDateAndDepartmentCode(reportDate, departmentCodes);
			diagonse = reportedList.stream().filter(a->UserViralInfectionStatusEnum.DIAGONSE.getCode().equals(a.getUserCondition())).collect(Collectors.toList());//确诊
			contiguity = reportedList.stream().filter(a->UserViralInfectionStatusEnum.CONTIGUITY.getCode().equals(a.getUserCondition())).collect(Collectors.toList());//密接
			suspected = reportedList.stream().filter(a->UserViralInfectionStatusEnum.SUSPECTED.getCode().equals(a.getUserCondition())).collect(Collectors.toList());//疑似
			diagonseArray.add(diagonse.size());
			contiguityArray.add(contiguity.size());
			suspectedArray.add(suspected.size());
		}
		return jsonObject;
	}
	
	public JSONObject brokenLineDatePattern(Integer pastDays){
		JSONObject jsonObject = new JSONObject();
		JSONArray seriesArray = new JSONArray();
		//获取前7天的日期
		List<String> dateDisplay = DateUtils.getListPastDateString(pastDays,"M.d");
		dateDisplay = dateDisplay.stream().sorted(Comparator.naturalOrder()).collect(Collectors.toList());
		jsonObject.put("date" , dateDisplay);
		jsonObject.put("series", seriesArray);
		JSONArray diagonseArray = new JSONArray();
		JSONArray contiguityArray = new JSONArray();
		JSONArray suspectedArray = new JSONArray();
		JSONObject data = new JSONObject();
		data = new JSONObject();
		data.put("name", "疑似");
		data.put("data", suspectedArray);
		seriesArray.add(data);
		data = new JSONObject();
		data.put("name", "密接");
		data.put("data", contiguityArray);
		seriesArray.add(data);
		data = new JSONObject();
		data.put("name", "确诊");
		data.put("data", diagonseArray);
		seriesArray.add(data);
		return jsonObject;
	}
}
