package com.gsafety.gemp.wuhanncov.service;

import com.gsafety.gemp.wuhanncov.contract.params.FileParam;
import com.gsafety.gemp.wuhanncov.contract.service.XfileManageService;
import com.gsafety.gemp.wuhanncov.dao.FilesManageDao;
import com.gsafety.gemp.wuhanncov.dao.LocationDimDao;
import com.gsafety.gemp.wuhanncov.dao.po.FilesManagePO;
import com.gsafety.gemp.wuhanncov.dao.po.LocationDimPO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * 素材文件服务实现
 *
 * @author liyanhong
 * @since 2020/2/6  16:58
 */
@Service("xfileManageService")
public class XfileManageServiceImpl implements XfileManageService {

    @Resource
    private FilesManageDao filesManageDao;

    @Resource
    private LocationDimDao locationDimDao;

    @Override
    public Integer addFileInfo(FilesManagePO po) {

        Optional<LocationDimPO> locOpt = locationDimDao.findById(po.getAreaCode());
        if (locOpt.isPresent()) {
            po.setAreaName(locOpt.get().getAreaName());
        }
        po.setCreateTime(new Date());
        filesManageDao.saveAndFlush(po);
        return 1;
    }

    @Override
    public List<FilesManagePO> findFiles(FileParam fileParam) {

        Sort sort = Sort.by(Sort.Direction.ASC, "seqNum");
        List<FilesManagePO> poList = filesManageDao.findAll(new Specification<FilesManagePO>() {
            @Override
            public Predicate toPredicate(Root<FilesManagePO> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();

                if (StringUtils.isNotEmpty(fileParam.getId())) {
                    predicates.add(criteriaBuilder.equal(root.get("id"), fileParam.getId()));
                }

                if (StringUtils.isNotEmpty(fileParam.getAreaCode())) {
                    predicates.add(criteriaBuilder.equal(root.get("areaCode"), fileParam.getAreaCode()));
                }

                if (StringUtils.isNotEmpty(fileParam.getBnType())) {
                    predicates.add(criteriaBuilder.equal(root.get("bnType"), fileParam.getBnType()));
                }

                if (fileParam.getStatus() != null) {
                    predicates.add(criteriaBuilder.equal(root.get("status"), fileParam.getStatus()));
                }

                if (StringUtils.isNotEmpty(fileParam.getTags())) {
                    String[] tagArr = fileParam.getTags().split(",");
                    List<Predicate> subPredicates = new ArrayList<>();
                    for(String tag : tagArr){
                        subPredicates.add(criteriaBuilder.like(root.get("tags"), tag));
                    }
                    predicates.add(criteriaBuilder.or(subPredicates.toArray(new Predicate[subPredicates.size()])));
                }

                return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        }, sort);

        return poList;
    }

    @Override
    public Integer updateFileInfo(FileParam fileParam) {

        FilesManagePO po = filesManageDao.getOne(fileParam.getId());

        if (po == null) {
            return 0;
        }

        BeanUtils.copyProperties(fileParam, po);
        po.setUpdateTime(new Date());
        filesManageDao.saveAndFlush(po);
        return 1;
    }
}
