package com.gsafety.gemp.wuhanncov.service;

import com.gsafety.gemp.wuhanncov.contract.service.DimService;
import com.gsafety.gemp.wuhanncov.dao.po.LocationDimPO;

/**
 * @author Administrator
 * @Title: CountryDimServiceImpl
 * @ProjectName wuhan-ncov
 * @Description: 国家只有parent返回-1， 其他返回null
 * @date 2020/2/314:29
 */
public class CountryDimServiceImpl implements DimService{

    private final String AREA_TYPE = "country";

    private LocationDimPO ldpo;

    public CountryDimServiceImpl(LocationDimPO ldpo) {
        this.ldpo = ldpo;
    }

    @Override
    public String getParent() {
        return "-1";
    }

    @Override
    public String getProvince() {
        return null;
    }

    @Override
    public String getCity() {
        return null;
    }

    @Override
    public String getType() {
        return this.AREA_TYPE;
    }
}
