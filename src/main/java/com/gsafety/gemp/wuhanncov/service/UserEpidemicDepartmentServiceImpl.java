package com.gsafety.gemp.wuhanncov.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.druid.util.StringUtils;
import com.gsafety.gemp.wuhanncov.contract.service.UserEpidemicDepartmentService;
import com.gsafety.gemp.wuhanncov.dao.UserEpidemicDepartmentDao;
import com.gsafety.gemp.wuhanncov.dao.po.UserEpidemicDepartmentPO;

/**
 * 
* @ClassName: UserEpidemicDepartmentServiceImpl 
* @Description: 综合应用机构服务类
* @author luoxiao
* @date 2020年2月6日 下午8:10:01 
*
 */
@Service("userEpidemicDepartmentService")
public class UserEpidemicDepartmentServiceImpl implements UserEpidemicDepartmentService{

	@Autowired
	private UserEpidemicDepartmentDao userEpidemicDepartmentDao;
	
	@Override
	public List<String> getLowerAllDepartmentCode(String departmentCode) {
		if(StringUtils.isEmpty(departmentCode)){
			throw new RuntimeException("部门Code为空!");
		}
		List<UserEpidemicDepartmentPO> poAll = userEpidemicDepartmentDao.findAll();
		if(poAll == null){
			throw new RuntimeException("部门层级数据为空!");
		}
		List<UserEpidemicDepartmentPO> childList = new ArrayList<UserEpidemicDepartmentPO>();
		recursionQuery(childList,poAll,departmentCode);
		List<String> results = new ArrayList<>();;
		results.add(departmentCode);
		for(UserEpidemicDepartmentPO po : childList){
			results.add(po.getDepartmentCode());
		}
		return results;
	}
	
	public void recursionQuery(List<UserEpidemicDepartmentPO> clildList,List<UserEpidemicDepartmentPO> poLists,String departmentCode){
		for(UserEpidemicDepartmentPO po : poLists){
			if(departmentCode.equals(po.getParentCode())){
				recursionQuery(clildList,poLists,po.getDepartmentCode());
				clildList.add(po);
			}
		}
	}

}
