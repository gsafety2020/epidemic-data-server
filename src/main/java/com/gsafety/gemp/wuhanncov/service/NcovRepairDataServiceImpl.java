package com.gsafety.gemp.wuhanncov.service;

import com.gsafety.gemp.wuhanncov.contract.dto.ChinaCountStatistics;
import com.gsafety.gemp.wuhanncov.contract.dto.ChinaCountStatisticsDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.Result;
import com.gsafety.gemp.wuhanncov.contract.service.NcovRepairDataService;
import com.gsafety.gemp.wuhanncov.dao.ChinaCountStatisticsDao;
import com.gsafety.gemp.wuhanncov.dao.LocationDimDao;
import com.gsafety.gemp.wuhanncov.dao.PatientAreaRuntimeDao;
import com.gsafety.gemp.wuhanncov.dao.PatientAreaStatisticDao;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaCountStatisticsPO;
import com.gsafety.gemp.wuhanncov.dao.po.LocationDimPO;
import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaRuntimePO;
import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaStatisticPO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zhoushuyan
 * @date 2020-01-29 9:47
 * @Description
 */
@Service("ncovRepairDataService")
@Slf4j
@EnableAsync
public class NcovRepairDataServiceImpl implements NcovRepairDataService {
    @Resource
    private LocationDimDao locationDimDao;
    @Resource
    private ChinaCountStatisticsDao chinaCountStatisticsDao;

    @Resource
    private PatientAreaStatisticDao patientAreaStatisticDao;

    @Resource
    private PatientAreaRuntimeDao patientAreaRuntimeDao;

    @Override
    public List<ChinaCountStatisticsDTO> findAreaCountList(String areaType) {
        Sort sort = Sort.by(Sort.Direction.DESC, "confirmCase");
        List<ChinaCountStatisticsPO> list = chinaCountStatisticsDao.findAll(new Specification<ChinaCountStatisticsPO>() {
            @Override
            public Predicate toPredicate(Root<ChinaCountStatisticsPO> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                return builder.equal(root.get("areaType"),areaType);
            }
        }, sort);
        return  chinaCountStatisticsListPOtoDTO(list);
    }

    @Override
    public List<ChinaCountStatisticsDTO> findAreaCountByNameList(String areaName) {
        Sort sort = Sort.by(Sort.Direction.DESC, "confirmCase");
        List<ChinaCountStatisticsPO> list = chinaCountStatisticsDao.findAll(new Specification<ChinaCountStatisticsPO>() {
            @Override
            public Predicate toPredicate(Root<ChinaCountStatisticsPO> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                return builder.like(root.get("areaName"), "%" +areaName+"%");
            }
        }, sort);
        return chinaCountStatisticsListPOtoDTO(list);
    }

    @Override
    public List<ChinaCountStatisticsDTO> findAreaCountByProvince(String province) {
        return chinaCountStatisticsListPOtoDTO(chinaCountStatisticsDao.findByProvince(province));
    }

    @Override
    public ChinaCountStatisticsDTO findAreaCountById(String id) {
        Optional<ChinaCountStatisticsPO> optional=chinaCountStatisticsDao.findById(id);
        if (optional.isPresent()) {
            return chinaCountStatisticsPOtoDTO(optional.get());
        }
        return new ChinaCountStatisticsDTO();
    }

    @Override
    public Result updateAreaCount(ChinaCountStatistics chinaCountStatistics) {
        try {
            String id=chinaCountStatistics.getId();
            ChinaCountStatisticsPO po=chinaCountStatisticsDao.getOne(id);
            po.setConfirmCase(chinaCountStatistics.getConfirmCase());
            po.setCureCase(chinaCountStatistics.getCureCase());
            po.setNewCase(chinaCountStatistics.getNewCase());
            po.setProbableCase(chinaCountStatistics.getProbableCase());
            po.setDeadCase(chinaCountStatistics.getDeadCase());
            chinaCountStatisticsDao.save(po);
            return new Result().success();
        }catch (Exception e){
            log.info(e.getMessage());
        }

        return new Result().fail();
    }

    @Override
    public Result initPatientHistoryData(String areaCode, Integer days) {

        Optional<LocationDimPO> areaOpt = locationDimDao.findById(areaCode);

        if(!areaOpt.isPresent()){
            return Result.builder().msg("区域编码不存在！").build();
        }

        initHistoryData(areaOpt.get(), days);

        initRuntimeData(areaOpt.get());

        return Result.builder().msg("OK").build();
    }

    /**
     * 初始化历史数据
     * @param areainfo
     * @param days
     */
    private void initHistoryData(LocationDimPO areainfo, Integer days){
        String areaCode = areainfo.getAreaNo();
        List<PatientAreaStatisticPO> hisPoList = patientAreaStatisticDao.findByAreaCodeOrderByStasTimeDesc(areaCode);

        Map<String, PatientAreaStatisticPO> poMap = hisPoList.stream().collect(Collectors.toMap(PatientAreaStatisticPO::getStasDate, p -> p));

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date now = new Date();
        String dateStr = null;
        PatientAreaStatisticPO poNew = null;
        for(int i = 0 ; i <= days ; i++){
            dateStr = sdf.format(DateUtils.addDays(now, 0 - i));

            if(poMap.get(dateStr) == null){
                poNew = new PatientAreaStatisticPO();
                poNew.setId(dateStr + areaCode);
                poNew.setAreaCode(areaCode);
                poNew.setArea(areainfo.getAreaName());
                poNew.setStasDate(dateStr);
                poNew.setStasHour("235959");
                poNew.setStasTime(com.gsafety.gemp.wuhanncov.utils.DateUtils.parse(dateStr + "235959", "yyyyMMddHHmmss"));
                poNew.setPatientNumber(0);
                poNew.setPostDate(sdf.format(now));

                patientAreaStatisticDao.saveAndFlush(poNew);
            }
        }
    }

    /**
     * 初始化实时数据
     * @param areainfo
     */
    private void initRuntimeData(LocationDimPO areainfo){

        Optional<PatientAreaRuntimePO> runtimeOpt = patientAreaRuntimeDao.findById(areainfo.getAreaNo());
        PatientAreaRuntimePO poNew = null;
        if(!runtimeOpt.isPresent()){
            poNew = new PatientAreaRuntimePO();
            poNew.setId(areainfo.getAreaNo());
            poNew.setAreaCode(areainfo.getAreaNo());
            poNew.setAreaName(areainfo.getAreaName());
            poNew.setAreaType(areainfo.getAreaType());
            poNew.setConfirmCase(0);
            poNew.setNewCase(0);
            poNew.setUpdateTime(new Date());
            poNew.setParentNo(areainfo.getParentNo());
            poNew.setLockFlag(PatientAreaRuntimePO.LockFlag.UNLOCK);
            patientAreaRuntimeDao.saveAndFlush(poNew);
        }

        List<LocationDimPO> subLocPos = locationDimDao.findByParentNo(areainfo.getAreaNo(), Sort.by(Sort.Direction.ASC, "seqNum"));

        if(subLocPos != null && subLocPos.size() > 0){

            List<PatientAreaRuntimePO> runtimePOS = patientAreaRuntimeDao.findByParentNo(areainfo.getAreaNo());
            Map<String, PatientAreaRuntimePO> runtimePOMap = runtimePOS.stream().collect(Collectors.toMap(PatientAreaRuntimePO::getAreaCode, p -> p));

            for(LocationDimPO subLoc : subLocPos){
                if(runtimePOMap.get(subLoc.getAreaNo()) == null){
                    poNew = new PatientAreaRuntimePO();
                    poNew.setId(subLoc.getAreaNo());
                    poNew.setAreaCode(subLoc.getAreaNo());
                    poNew.setAreaName(subLoc.getAreaName());
                    poNew.setAreaType(subLoc.getAreaType());
                    poNew.setConfirmCase(0);
                    poNew.setNewCase(0);
                    poNew.setUpdateTime(new Date());
                    poNew.setParentNo(areainfo.getAreaNo());
                    poNew.setLockFlag(PatientAreaRuntimePO.LockFlag.UNLOCK);
                    patientAreaRuntimeDao.saveAndFlush(poNew);
                }
            }
        }
    }

    private List<ChinaCountStatisticsDTO> chinaCountStatisticsListPOtoDTO(List<ChinaCountStatisticsPO> list){
        List<ChinaCountStatisticsDTO> content = new ArrayList<ChinaCountStatisticsDTO>();
        Iterator<ChinaCountStatisticsPO> it = list.iterator();
        while (it.hasNext()) {
            ChinaCountStatisticsPO po = it.next();
            content.add(chinaCountStatisticsPOtoDTO(po));
        }
        return content;
    }
    private ChinaCountStatisticsDTO chinaCountStatisticsPOtoDTO(ChinaCountStatisticsPO po){
        SimpleDateFormat f = new SimpleDateFormat("MM月dd日");
        ChinaCountStatisticsDTO dto = new ChinaCountStatisticsDTO();
        BeanUtils.copyProperties(po, dto);
        Date updateTime = po.getUpdateTime();
        String dateTime = f.format(updateTime);
        dto.setCountTime(dateTime);
        return dto;
    }
}
