/**
 * 
 */
package com.gsafety.gemp.wuhanncov.service;

import com.gsafety.gemp.wuhanncov.contract.dto.BasicReportDTO;
import com.gsafety.gemp.wuhanncov.contract.service.BasicReportService;
import com.gsafety.gemp.wuhanncov.dao.BasicReportDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 该类用于...，用于实现...等能力。
 *
 * @author yangbo
 *
 * @since 2020年2月7日 下午11:34:18
 *
 */
@Service("basicReportService")
public class BasicReportServiceImpl implements BasicReportService {

	@Resource
	private BasicReportDao basicReportDao;

	@Override
	public BasicReportDTO findByDate(String date) {
		BasicReportDTO dto = new BasicReportDTO();
		dto.setTolalFever(basicReportDao.getTolalFeverByDate());
		dto.setTotalDistrict(basicReportDao.getTotalDistrictByDate());
		dto.setTotalReport(basicReportDao.getTotalReportByDate());
		return dto;
	}

}
