package com.gsafety.gemp.wuhanncov.service;

import com.gsafety.gemp.wuhanncov.contract.dto.ChinaCountStatisticsHistoryDTO;
import com.gsafety.gemp.wuhanncov.contract.params.DimQueryParam;
import com.gsafety.gemp.wuhanncov.contract.service.CounthistoryService;
import com.gsafety.gemp.wuhanncov.dao.ChinaCountStatisticsHistoryDAO;
import com.gsafety.gemp.wuhanncov.dao.LocationDimDao;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaAreaMedicalDailyPO;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaCountStatisticsHistoryPO;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.gsafety.gemp.wuhanncov.specification.ChinaAreaMedicalDailySpecifications.areaCodeIs;
import static com.gsafety.gemp.wuhanncov.specification.ChinaAreaMedicalDailySpecifications.stasDateBetween;
import static com.gsafety.gemp.wuhanncov.specification.CountHistorySpecifications.areaNameEqual;
import static com.gsafety.gemp.wuhanncov.specification.CountHistorySpecifications.updateTimeBetween;
import static com.gsafety.gemp.wuhanncov.wrapper.CounthistoryWrapper.toPageDTO;

/**
 * 爬虫历史数据
 *
 * @author wangwenhai
 *
 * @since 2020年2月12日 下午7:31:00
 *
 */
@Service("countHistoryService")
public class CountHistoryServiceImpl implements CounthistoryService {

    @Resource
    private ChinaCountStatisticsHistoryDAO chinaCountStatisticsHistoryDAO;

    @Resource
    private LocationDimDao locationDimDao;



    @Override
    public Page<ChinaCountStatisticsHistoryDTO> findRetileInfoPage(DimQueryParam dimQuery, Pageable pageable) {
        Page<ChinaCountStatisticsHistoryPO> page=chinaCountStatisticsHistoryDAO.findAll(areaNameEqual(dimQuery.getArea()).and(updateTimeBetween(dimQuery.getSearchDate())), pageable);
        return toPageDTO(page);
    }


}
