package com.gsafety.gemp.wuhanncov.service;

import com.gsafety.gemp.wuhanncov.contract.service.DimService;
import com.gsafety.gemp.wuhanncov.dao.LocationDimDao;
import com.gsafety.gemp.wuhanncov.dao.po.LocationDimPO;

import javax.annotation.Resource;

/**
 * @author Administrator
 * @Title: CountryDimServiceImpl
 * @ProjectName wuhan-ncov
 * @Description: parent返回省，city返回自己，省返回省份
 * @date 2020/2/314:29
 */
public class DistrictDimServiceImpl implements DimService{

    private final String AREA_TYPE = "district";

    private LocationDimPO ldpo;

    public DistrictDimServiceImpl(LocationDimPO ldpo) {
        this.ldpo = ldpo;
    }

    @Override
    public String getParent() {
        return ldpo.getParentNo();
    }

    @Override
    public String getProvince() {
        return ldpo.getAreaNo().substring(0, 2).concat("0000");
    }

    @Override
    public String getCity() {
        return getParent();
    }

    @Override
    public String getType() {
        return this.AREA_TYPE;
    }
}
