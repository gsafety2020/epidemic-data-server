package com.gsafety.gemp.wuhanncov.service;

import com.gsafety.gemp.wuhanncov.contract.dto.ChinaAreaMaterialDTO;
import com.gsafety.gemp.wuhanncov.contract.service.CityMaterialService;
import com.gsafety.gemp.wuhanncov.dao.ChinaAreaMaterialDao;
import com.gsafety.gemp.wuhanncov.dao.CityMaterialDao;
import com.gsafety.gemp.wuhanncov.dao.po.CityMaterialPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * 物资信息列表 service
 *
 * @author zhoushuyan
 * @date 2020-01-31 20:24:38
 */
@Service("cityMaterialService")
public class CityMaterialmpl implements CityMaterialService {

    @Autowired
    CityMaterialDao chinaAreaMaterialDao;


    @Override
    public Page<ChinaAreaMaterialDTO> findAreaMaterialsPage(int pageNum, int pageSize, List<String> areaNos) {
        Sort sort = Sort.by(Sort.Direction.DESC, "storageNum","materialName");
        Pageable pageable = PageRequest.of(pageNum, pageSize, sort);
        Page<CityMaterialPO> materialList=chinaAreaMaterialDao.findAll(new Specification<CityMaterialPO>(){
            @Override
            public Predicate toPredicate(Root<CityMaterialPO> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                List<Predicate> list = new ArrayList<Predicate>();
                CriteriaBuilder.In<String> in = builder.in(root.get("parentNo"));
                for (String id : areaNos) {
                    in.value(id);
                }
                Predicate p1 =builder.equal(root.get("statusCd"),"5");
                Predicate p2=builder.and(in);
                list.add(builder.and(p1, p2));
                Predicate[] p = new Predicate[list.size()];
                return builder.and(list.toArray(p));
            }
        },pageable);
        List<ChinaAreaMaterialDTO> dtos=new ArrayList<ChinaAreaMaterialDTO>();
        for(CityMaterialPO po:materialList){
            ChinaAreaMaterialDTO dto=new ChinaAreaMaterialDTO();
            BeanUtils.copyProperties(po,dto);
            int storageNum=0,total=0;
            if(po.getStorageNum()!=null){
                storageNum=po.getStorageNum().intValue();
            }
            if(po.getTotal()!=null){
                total=po.getTotal().intValue();
            }
            dto.setStorageNum(storageNum);
            dto.setTotal(total);
            dtos.add(dto);
        }
        return new PageImpl<ChinaAreaMaterialDTO>(dtos,pageable,materialList.getTotalElements());
    }

    @Override
    public List<ChinaAreaMaterialDTO> findAreaMaterials(List<String> areaNos) {
//        Sort sort = Sort.by(Sort.Direction.ASC, "materialName","storageNum");
        Sort sort = new Sort(Sort.Direction.ASC,"materialName").and(new Sort(Sort.Direction.DESC,"storageNum"));
        List<CityMaterialPO> materialList=chinaAreaMaterialDao.findAll(new Specification<CityMaterialPO>(){
            @Override
            public Predicate toPredicate(Root<CityMaterialPO> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                List<Predicate> list = new ArrayList<Predicate>();
                CriteriaBuilder.In<String> in = builder.in(root.get("parentNo"));
                for (String id : areaNos) {
                    in.value(id);
                }
                Predicate p1 =builder.equal(root.get("statusCd"),"5");
                Predicate p2=builder.and(in);
                list.add(builder.and(p1, p2));
                Predicate[] p = new Predicate[list.size()];
                return builder.and(list.toArray(p));
            }
        },sort);
        List<ChinaAreaMaterialDTO> dtos=new ArrayList<ChinaAreaMaterialDTO>();
        for(CityMaterialPO po:materialList){
            ChinaAreaMaterialDTO dto=new ChinaAreaMaterialDTO();
            BeanUtils.copyProperties(po,dto);
            int storageNum=0,total=0;
            if(po.getStorageNum()!=null){
                storageNum=po.getStorageNum().intValue();
            }
            if(po.getTotal()!=null){
                total=po.getTotal().intValue();
            }
            dto.setStorageNum(storageNum);
            dto.setTotal(total);
            dtos.add(dto);
        }
        return dtos;
    }
}
