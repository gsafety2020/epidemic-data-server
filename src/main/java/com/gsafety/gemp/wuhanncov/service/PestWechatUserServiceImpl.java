package com.gsafety.gemp.wuhanncov.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;


import com.gsafety.gemp.wechatmp.client.controller.WechatAuthController.WechatUserinfo;
import com.gsafety.gemp.wuhanncov.constant.PestWechatUserStatus;
import com.gsafety.gemp.wuhanncov.contract.service.PestWechatUserService;
import com.gsafety.gemp.wuhanncov.dao.PestWechatUserDao;
import com.gsafety.gemp.wuhanncov.dao.po.PestWechatUserPO;

/**
 * 
* @ClassName: PestWechatUserService 
* @Description: 微信用户服务层
* @author luoxiao
* @date 2020年2月26日 上午12:04:22 
*
 */
@Service("pestWechatUserService")
public class PestWechatUserServiceImpl implements PestWechatUserService{

	@Autowired
	private PestWechatUserDao pestWechatUserDao;
	
	@Override
	public void saveUpdate(PestWechatUserPO po) {
		vaildProperty(po);
		Optional<PestWechatUserPO> optional = pestWechatUserDao.findById(po.getOpenId());
		if(!optional.isPresent()){
			pestWechatUserDao.saveAndFlush(po);
		}
	}
	
	@Override
	public void authorizeUser(PestWechatUserPO po,WechatUserinfo userInfo) {
		if(po == null){
			throw new RuntimeException("用户登陆失败!");
		}
		if(StringUtils.isEmpty(po.getOpenId())){
			throw new RuntimeException("主键为空!");
		}
		po.setWithCredentials(PestWechatUserStatus.AuthorizeEnum.AUTORIZED.getCode());
		po.setNickName(userInfo.getNickName());
		po.setGender(userInfo.getGender());
		po.setCity(userInfo.getCity());
		po.setProvince(userInfo.getProvince());
		po.setCountry(userInfo.getCountry());
		po.setAvatarUrl(userInfo.getAvatarUrl());
		po.setUnionId(userInfo.getUnionId());
		pestWechatUserDao.saveAndFlush(po);
	}
	
	@Override
	public PestWechatUserPO getPestWechatUserById(String openId) {
		if(StringUtils.isEmpty(openId)){
			throw new RuntimeException("主键为空!");
		}
		return pestWechatUserDao.getOne(openId);
	}
	
	public void vaildProperty(PestWechatUserPO po){
		if(po == null){
			throw new RuntimeException("用户登陆失败!");
		}
		if(StringUtils.isEmpty(po.getOpenId())){
			throw new RuntimeException("主键为空!");
		}
		if(StringUtils.isEmpty(po.getGender())){
			throw new RuntimeException("性别为空!");
		}
		if(StringUtils.isEmpty(po.getWithCredentials())){
			throw new RuntimeException("授权为空!");
		}
		if(po.getFirstAccessTime() == null){
			throw new RuntimeException("首次访问时间为空!");
		}
		if(po.getUpdateTime() == null){
			throw new RuntimeException("最后更新时间为空!");
		}
		if(StringUtils.isEmpty(po.getAppid())){
			throw new RuntimeException("appid为空!");
		}
	}

	

}
