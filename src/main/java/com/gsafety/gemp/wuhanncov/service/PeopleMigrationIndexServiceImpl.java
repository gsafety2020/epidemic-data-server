package com.gsafety.gemp.wuhanncov.service;

import com.google.common.collect.Lists;
import com.gsafety.gemp.wuhanncov.contract.dto.MigrationIndex;
import com.gsafety.gemp.wuhanncov.contract.dto.MigrationIndexDTO;
import com.gsafety.gemp.wuhanncov.contract.service.PeopleMigrationIndexService;
import com.gsafety.gemp.wuhanncov.dao.PeopleMigrationIndexDao;
import com.gsafety.gemp.wuhanncov.utils.DateUtils;
import com.gsafety.gemp.wuhanncov.utils.LuncarCalendar;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 全国人口迁移指数接口
 *
 * @author: WangGang
 * @since 2020-02-07 07:40
 */
@Service("peopleMigrationIndexService")
@Slf4j
@EnableAsync
public class PeopleMigrationIndexServiceImpl implements PeopleMigrationIndexService {

    @Resource
    private PeopleMigrationIndexDao peopleMigrationIndexDao;

    @Override
    public List<MigrationIndex> findMigrationIndexList(MigrationIndexDTO migrationIndexDTO) {
        //根据入参时间，数据初始化
        List<MigrationIndex> migrationIndexList = Lists.newArrayList();
        List<String> timeList = DateUtils.findDates(migrationIndexDTO.getStartDate(), migrationIndexDTO.getEndDate());
        for (int i = 0; i < timeList.size(); i++) {
            LocalDate thisLocalDate = DateUtils.parseToLocalDate(timeList.get(i), DateUtils.DATE_PATTERN_A);
            MigrationIndex migrationIndex = new MigrationIndex();
            migrationIndex.setMigrateDate(timeList.get(i));
            migrationIndex.setLunarCalendar(LuncarCalendar.oneDay(thisLocalDate.getYear(), thisLocalDate.getMonthValue(), thisLocalDate.getDayOfMonth()));
            migrationIndex.setThisYear(BigDecimal.valueOf(0));
            migrationIndex.setLastYear(BigDecimal.valueOf(0));
            migrationIndexList.add(migrationIndex);
        }
        //获取数据
        List<Object[]> thisYearCMSList = peopleMigrationIndexDao.findMigrationIndexList(migrationIndexDTO.getAreaCode(), migrationIndexDTO.getMigrateType(), DateUtils.format(migrationIndexDTO.getStartDate()), DateUtils.format(migrationIndexDTO.getEndDate()));
        List<Object[]> lastYearCMSList = peopleMigrationIndexDao.findMigrationIndexList(migrationIndexDTO.getAreaCode(), migrationIndexDTO.getMigrateType(), DateUtils.format(migrationIndexDTO.getStartDate().minusYears(1)), DateUtils.format(migrationIndexDTO.getEndDate().minusYears(1)));
        //数据组装
        return migrationIndexList.stream().map(p -> migrationIndexPackage(thisYearCMSList, lastYearCMSList, p)).collect(Collectors.toList());
    }

    private MigrationIndex migrationIndexPackage(List<Object[]> thisYearCMSList, List<Object[]> lastYearCMSList, MigrationIndex migrationIndex) {
        for (int i = 0; i < thisYearCMSList.size(); i++) {
            if (matchMonthday(migrationIndex.getMigrateDate(), String.valueOf(thisYearCMSList.get(i)[2]))) {
                migrationIndex.setThisYear(getBigDecimal(thisYearCMSList.get(i)[6]));
            }

        }
        for (int i = 0; i < lastYearCMSList.size(); i++) {
            if (matchMonthday(migrationIndex.getMigrateDate(), String.valueOf(lastYearCMSList.get(i)[2]))) {
                migrationIndex.setLastYear(getBigDecimal(lastYearCMSList.get(i)[6]));
            }
        }
        return migrationIndex;
    }


    /**
     * 比较两个日期的月，日
     *
     * @param dateOneStr
     * @param dateTwoStr
     * @return
     */
    public Boolean matchMonthday(String dateOneStr, String dateTwoStr) {
        LocalDate dateOne = DateUtils.parseToLocalDate(dateOneStr, DateUtils.DATE_PATTERN_A);
        LocalDate dateTwo = DateUtils.parseToLocalDate(dateTwoStr, DateUtils.DATE_PATTERN_A);
        if (dateOne.getMonth() == dateTwo.getMonth() && dateOne.getDayOfMonth() == dateTwo.getDayOfMonth()) {
            return true;
        }
        return false;
    }


    /**
     * Object转BigDecimal类型
     *
     * @param value 要转的object类型
     * @return 转成的BigDecimal类型数据
     */
    public static BigDecimal getBigDecimal(Object value) {
        BigDecimal ret = null;
        if (value != null) {
            if (value instanceof BigDecimal) {
                ret = (BigDecimal) value;
            } else if (value instanceof String) {
                ret = new BigDecimal((String) value);
            } else if (value instanceof BigInteger) {
                ret = new BigDecimal((BigInteger) value);
            } else if (value instanceof Number) {
                ret = new BigDecimal(((Number) value).doubleValue());
            } else {
                throw new ClassCastException("Not possible to coerce [" + value + "] from class " + value.getClass() + " into a BigDecimal.");
            }
        }
        return ret;
    }

}
