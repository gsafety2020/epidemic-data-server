package com.gsafety.gemp.wuhanncov.service;

import com.gsafety.gemp.wuhanncov.contract.dto.ChinaAreaMaterialDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.MaterialRequirementDTO;
import com.gsafety.gemp.wuhanncov.contract.service.ChinaAreaMaterialService;
import com.gsafety.gemp.wuhanncov.contract.service.MaterialRequirementService;
import com.gsafety.gemp.wuhanncov.dao.ChinaAreaMaterialDao;
import com.gsafety.gemp.wuhanncov.dao.MaterialRequirementDao;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaAreaMaterialPO;
import com.gsafety.gemp.wuhanncov.dao.po.LocationDimPO;
import com.gsafety.gemp.wuhanncov.dao.po.MaterialRequirementPO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 物资信息列表 service
 *
 * @author zhoushuyan
 * @date 2020-01-31 20:24:38
 */
@Service("chinaAreaMaterialService")
public class ChinaAreaMaterialmpl implements ChinaAreaMaterialService {

    @Autowired
    ChinaAreaMaterialDao chinaAreaMaterialDao;


    @Override
    public Page<ChinaAreaMaterialDTO> findAreaMaterialsPage(int pageNum, int pageSize, List<String> areaNos) {
        Sort sort = Sort.by(Sort.Direction.DESC, "storageNum","materialName");
        Pageable pageable = PageRequest.of(pageNum, pageSize, sort);
        Page<ChinaAreaMaterialPO> materialList=chinaAreaMaterialDao.findAll(new Specification<ChinaAreaMaterialPO>(){
            @Override
            public Predicate toPredicate(Root<ChinaAreaMaterialPO> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                List<Predicate> list = new ArrayList<Predicate>();
                CriteriaBuilder.In<String> in = builder.in(root.get("parentNo"));
                for (String id : areaNos) {
                    in.value(id);
                }
                Predicate p1 =builder.equal(root.get("statusCd"),"5");
                Predicate p2=builder.and(in);
                list.add(builder.and(p1, p2));
                Predicate[] p = new Predicate[list.size()];
                return builder.and(list.toArray(p));
            }
        },pageable);
        List<ChinaAreaMaterialDTO> dtos=new ArrayList<ChinaAreaMaterialDTO>();
        for(ChinaAreaMaterialPO po:materialList){
            ChinaAreaMaterialDTO dto=new ChinaAreaMaterialDTO();
            BeanUtils.copyProperties(po,dto);
            int storageNum=0,total=0;
            if(po.getStorageNum()!=null){
                storageNum=po.getStorageNum().intValue();
            }
            if(po.getTotal()!=null){
                total=po.getTotal().intValue();
            }
            dto.setStorageNum(storageNum);
            dto.setTotal(total);
            dtos.add(dto);
        }
        return new PageImpl<ChinaAreaMaterialDTO>(dtos,pageable,materialList.getTotalElements());
    }

    @Override
    public List<ChinaAreaMaterialDTO> findAreaMaterials(List<String> areaNos,String statusCd) {
//        Sort sort = Sort.by(Sort.Direction.ASC, "materialName","storageNum");
        Sort sort = new Sort(Sort.Direction.ASC,"materialName").and(new Sort(Sort.Direction.DESC,"storageNum"));
        List<ChinaAreaMaterialPO> materialList=chinaAreaMaterialDao.findAll(new Specification<ChinaAreaMaterialPO>(){
            @Override
            public Predicate toPredicate(Root<ChinaAreaMaterialPO> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                List<Predicate> list = new ArrayList<Predicate>();
                CriteriaBuilder.In<String> in = builder.in(root.get("parentNo"));
                for (String id : areaNos) {
                    in.value(id);
                }
                Predicate p1 =builder.equal(root.get("statusCd"),statusCd);
                Predicate p2=builder.and(in);
                list.add(builder.and(p1, p2));
                Predicate[] p = new Predicate[list.size()];
                return builder.and(list.toArray(p));
            }
        },sort);
        List<ChinaAreaMaterialDTO> dtos=new ArrayList<ChinaAreaMaterialDTO>();
        for(ChinaAreaMaterialPO po:materialList){
            ChinaAreaMaterialDTO dto=new ChinaAreaMaterialDTO();
            BeanUtils.copyProperties(po,dto);
            int storageNum=0,total=0;
            if(po.getStorageNum()!=null){
                storageNum=po.getStorageNum().intValue();
            }
            if(po.getTotal()!=null){
                total=po.getTotal().intValue();
            }
            dto.setStorageNum(storageNum);
            dto.setTotal(total);
            dtos.add(dto);
        }
        return dtos;
    }
}
