package com.gsafety.gemp.wuhanncov.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.collection.CollectionUtil;
import com.gsafety.gemp.wuhanncov.contract.dto.AreaStatDailyAnnouncementDTO;
import com.gsafety.gemp.wuhanncov.contract.params.DimQueryParam;
import com.gsafety.gemp.wuhanncov.contract.service.AreaStatDailyAnnouncementService;
import com.gsafety.gemp.wuhanncov.dao.AreaStatDailyAnnouncementDao;
import com.gsafety.gemp.wuhanncov.dao.LocationDimDao;
import com.gsafety.gemp.wuhanncov.dao.po.AreaStatDailyAnnouncementPO;
import com.gsafety.gemp.wuhanncov.utils.DateUtils;
import com.gsafety.gemp.wuhanncov.utils.RegExUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

import static com.gsafety.gemp.wuhanncov.specification.AreaStatDailyAnnouncementSpecifications.areaCodeIs;
import static com.gsafety.gemp.wuhanncov.wrapper.AreaStatDailyAnnouncementWrapper.toPageDTO;

/**
 * @Author ganbo
 * @Since 2020-02-09
 */
@Slf4j
@Service
public class AreaStatDailyAnnouncementServiceImpl implements AreaStatDailyAnnouncementService {

    @Autowired
    private AreaStatDailyAnnouncementDao areaStatDailyAnnouncementDao;

    @Resource
    private LocationDimDao locationDimDao;

    /**
     * 根据类型和区域编码查询最新一条公告信息数据
     * @param type
     * @param areaCode
     * @return
     */
    @Override
    public Map<String,Object> getLatestAnnouncementData(String areaCode,String type){
        AreaStatDailyAnnouncementPO po = areaStatDailyAnnouncementDao.findFirstByAreaCodeAndAnnouncementTypeOrderByUpdateTimeDesc(areaCode,type);
        Map<String,Object> map = new HashMap<>();
        if(po != null){
            map.put("title",po.getTitle());
            map.put("content",po.getContent());
            map.put("department",po.getDepartment());
        }
        return map;
    }


    /**
     * 根据类型和区域编码查询公告信息数据列表
     * @param areaCode
     * @param type
     * @return
     */
    @Override
    public List<Map<String,Object>> showAnnouncementDataList(String areaCode,String type) {
        List<Map<String,Object>> mapList = new ArrayList<>();
        List<AreaStatDailyAnnouncementPO> poList = areaStatDailyAnnouncementDao.findByAreaCodeAndAnnouncementTypeOrderByUpdateTimeDesc(areaCode, type);
        if(CollectionUtil.isNotEmpty(poList)){
            int i = 1;
            for(AreaStatDailyAnnouncementPO po : poList){
                if(i< 6){
                    Map<String,Object> map = new HashMap<>();
                    map.put("title",po.getTitle());
                    map.put("content",po.getContent());
                    map.put("department",po.getDepartment());
                    mapList.add(map);
                    i ++;
                }
            }
        }
        return mapList;
    }

    @Override
    public Page<AreaStatDailyAnnouncementDTO> findAreaAnnouncementPage(DimQueryParam queryParam, Pageable pageable) {
        Page<AreaStatDailyAnnouncementPO> page = areaStatDailyAnnouncementDao.findAll(areaCodeIs(queryParam.getAreaNo()),pageable);
        return toPageDTO(page);
    }

    @Override
    public AreaStatDailyAnnouncementPO areaAnnouncementSave(AreaStatDailyAnnouncementDTO dto) {
        if (!RegExUtil.isChinaDim(dto.getAreaCode()) || !locationDimDao.existsById(dto.getAreaCode().substring(0, 6))) {
            log.error("错误的地区编码");
        }
        AreaStatDailyAnnouncementPO po = new AreaStatDailyAnnouncementPO();
        BeanUtil.copyProperties(dto, po, true, CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
        po.setCreateTime(DateUtils.localDateTime2Date(LocalDateTime.now()));
        po.setUpdateTime(DateUtils.localDateTime2Date(LocalDateTime.now()));

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String dateStr = sdf.format(new Date());
        po.setId(dateStr + dto.getAreaCode());

        return areaStatDailyAnnouncementDao.save(po);

    }

    @Override
    public AreaStatDailyAnnouncementPO areaAnnouncementUpdate(AreaStatDailyAnnouncementDTO dto) {
        AreaStatDailyAnnouncementPO po = areaStatDailyAnnouncementDao.getOne(dto.getId());
        BeanUtil.copyProperties(dto, po, true, CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
        po.setUpdateTime(DateUtils.localDateTime2Date(LocalDateTime.now()));

        return areaStatDailyAnnouncementDao.save(po);
    }

    @Override
    public void areaAnnouncementDelete(String id) {
        areaStatDailyAnnouncementDao.deleteById(id);
    }


}
