package com.gsafety.gemp.wuhanncov.service;

import com.alibaba.fastjson.JSON;
import com.gsafety.gemp.wuhanncov.contract.dto.ChinaAreaCaseInfoDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.ChinaAreaCaseInfoValue;
import com.gsafety.gemp.wuhanncov.contract.service.ChinaAreaCaseInfoService;
import com.gsafety.gemp.wuhanncov.dao.ChinaAreaCaseInfoDao;
import com.gsafety.gemp.wuhanncov.dao.po.BasicReportInfoPO;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaAreaCaseInfoPO;
import com.gsafety.gemp.wuhanncov.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author zhoushuyan
 * @date 2020-02-09 13:24
 * @Description
 */
@Service
@Slf4j
@EnableAsync
public class ChinaAreaCaseInfoServiceImpl implements ChinaAreaCaseInfoService {
    @Resource
    private ChinaAreaCaseInfoDao chinaAreaCaseInfoDao;

    @Override
    public List<ChinaAreaCaseInfoDTO> getChinaAreaCaseInfoTopList(List<String> areaIdList, int topSize) {
        Sort sort = Sort.by(Sort.Direction.DESC, "updateTime");
        List<ChinaAreaCaseInfoPO> poList=new ArrayList<ChinaAreaCaseInfoPO>();
        if(topSize==-1){
            poList=chinaAreaCaseInfoDao.findAll(new Specification<ChinaAreaCaseInfoPO>() {
                @Override
                public Predicate toPredicate(Root<ChinaAreaCaseInfoPO> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                    return null;
                }
            });
            poList=chinaAreaCaseInfoDao.findAll(new Specification<ChinaAreaCaseInfoPO>(){
                @Override
                public Predicate toPredicate(Root<ChinaAreaCaseInfoPO> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder builder) {
                    if(areaIdList!=null&&areaIdList.size()>0){
                        return builder.and(root.get("areaNo").in(areaIdList));
                    }else{
                        return  null;
                    }
                }
            },sort);
        }else{
            Pageable pageable = PageRequest.of(0, topSize, sort);
            poList=chinaAreaCaseInfoDao.findAll(new Specification<ChinaAreaCaseInfoPO>(){
                @Override
                public Predicate toPredicate(Root<ChinaAreaCaseInfoPO> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder builder) {
                    if(areaIdList!=null&&areaIdList.size()>0){
                        return builder.and(root.get("areaNo").in(areaIdList));
                    }else{
                        return  null;
                    }
                }
            },pageable).getContent();
        }
        List<ChinaAreaCaseInfoDTO> list=new ArrayList<ChinaAreaCaseInfoDTO>();
        int i=0;
        for(ChinaAreaCaseInfoPO po:poList){
            i++;
            ChinaAreaCaseInfoDTO dto=new ChinaAreaCaseInfoDTO();
            BeanUtils.copyProperties(po,dto);
            dto.setSeqNum(i);
            list.add(dto);
        }
        return list;
    }

    @Override
    public List<ChinaAreaCaseInfoValue> getChinaAreaCaseInfoListByAreaName(String areaName, String date) {
        List<ChinaAreaCaseInfoValue> values=new ArrayList<>();
        try{
            List<ChinaAreaCaseInfoPO> poList=new ArrayList<ChinaAreaCaseInfoPO>();
            if(date=="-1"||"-1".equals(date.trim())){
                poList=chinaAreaCaseInfoDao.getChinaAreaCaseInfoList(areaName);
            }else{
                SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date1=sf.parse(date+" 00:00:00");
                Date date2=sf.parse(date+" 23:59:59");
                poList=chinaAreaCaseInfoDao.getChinaAreaCaseInfoListByAreaName(areaName,date1,date2);
            }
            for(ChinaAreaCaseInfoPO po:poList){
                ChinaAreaCaseInfoValue value=new ChinaAreaCaseInfoValue();
                BeanUtils.copyProperties(po,value);
                values.add(value);
            }

        }catch (Exception e){
            e.printStackTrace();
            log.error(e.getMessage());
        }
        return values;

    }

    @Override
    public String getCaseInfoDefaultDate(String areaNo) {
        Date date=chinaAreaCaseInfoDao.getCaseInfoDefaultDate(areaNo);
        try {
            if(date==null){
                return DateUtils.getCurrentDay();
            }
            return DateUtils.format(date,"yyyy-MM-dd");
        } catch (Exception e) {
            e.printStackTrace();
            return DateUtils.getCurrentDay();
        }
    }
}

