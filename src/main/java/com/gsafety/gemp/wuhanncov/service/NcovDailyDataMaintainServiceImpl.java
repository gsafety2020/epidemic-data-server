package com.gsafety.gemp.wuhanncov.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaStatisticDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.Result;
import com.gsafety.gemp.wuhanncov.contract.params.DimQueryParam;
import com.gsafety.gemp.wuhanncov.contract.service.NcovDailyDataMaintainService;
import com.gsafety.gemp.wuhanncov.dao.LocationDimDao;
import com.gsafety.gemp.wuhanncov.dao.PatientAreaStatisticDao;
import com.gsafety.gemp.wuhanncov.dao.po.LocationDimPO;
import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaRuntimePO;
import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaStatisticPO;
import com.gsafety.gemp.wuhanncov.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.Predicate;
import java.util.*;

/**
 * @author xiaoz
 * @date 2020-01-29 9:47
 * @Description
 */
@Service("ncovDailyDataMaintainService")
@Slf4j
@EnableAsync
public class NcovDailyDataMaintainServiceImpl implements NcovDailyDataMaintainService {

    private static final int DEFAULT_KEY_LENGTH = 14;

    private static final char DEFAULT_PREFIX = '0';

    private static final char DEFAULT_SUFFIX = '0';

    @Resource
    private PatientAreaStatisticDao pasDao;

    @Resource
    private LocationDimDao locationDao;

    @Override
    public List<PatientAreaStatisticDTO> findDailyReportByArea(String area) {
        Sort sort = Sort.by(Sort.Direction.DESC, "stasDate");
        List<PatientAreaStatisticPO> list = pasDao.findAll((Specification<PatientAreaStatisticPO>) (root, query, builder) -> builder.like(root.get("area"), "%" + area + "%"), sort);

        return po2DTO(list);
    }

    @Override
    public Result dailyReportSave(PatientAreaStatisticDTO dto) {
        PatientAreaStatisticPO po = dto2Po(dto);
        if(null != po) {
            Date now = DateTime.now().toDate();
            if(StringUtils.isBlank(po.getId())) {
                String id = generateId(po);
                //新增的时候判断是否已经有当天的数据，提醒用户修改而不是新增
                if (pasDao.existsById(id)) {
                    return new Result().fail("保存失败, 数据已存在请进行修改操作!");
                }
                po.setId(id);
                po.setCreateTime(now);
            }
            po.setUpdateTime(now);

            return new Result().success("保存成功", pasDao.save(po).getId());
        }

        return new Result().fail("保存失败");
    }

    @Override
    public Page<PatientAreaStatisticDTO> findAllByDim(DimQueryParam dimQuery, Pageable pageable) {
        Page<PatientAreaStatisticPO> page = pasDao.findAll((Specification<PatientAreaStatisticPO>) (root, criteriaQuery, cb) -> {
            if (null != dimQuery) {
                List<Predicate> list = new ArrayList<>();
                if (StringUtils.isNotBlank(dimQuery.getArea())) {
                    list.add(cb.like(root.get("area"), "%" + dimQuery.getArea() + "%"));
                }

                if (StringUtils.isNotBlank(dimQuery.getAreaNo())) {
                    list.add(cb.equal(root.get("areaCode"), dimQuery.getAreaNo()));
                }

                Predicate[] p = new Predicate[list.size()];
                return cb.and(list.toArray(p));
            }
            return null;
        }, pageable);

        return po2DTO(page);
    }

    /**
     * 分页po转dto
     * @param page
     * @return
     */
    private Page<PatientAreaStatisticDTO> po2DTO(Page<PatientAreaStatisticPO> page){
        List<PatientAreaStatisticDTO> content = po2DTO(page.getContent());
        return new PageImpl<>(content, page.getPageable(), page.getTotalElements());
    }

    /**
     * 集合po转dto
     * @param list
     * @return
     */
    private List<PatientAreaStatisticDTO> po2DTO(List<PatientAreaStatisticPO> list){
        List<PatientAreaStatisticDTO> content = new ArrayList<>();
        Iterator<PatientAreaStatisticPO> it = list.iterator();
        while (it.hasNext()) {
            PatientAreaStatisticPO po = it.next();
            content.add(po2DTO(po));
        }

        return content;
    }

    /**
     * 单个po转dto
     * @param po
     * @return
     */
    private PatientAreaStatisticDTO po2DTO(PatientAreaStatisticPO po){
        PatientAreaStatisticDTO dto = new PatientAreaStatisticDTO();
        BeanUtils.copyProperties(po, dto);
        dto.setStasTime(DateUtils.format(po.getStasTime(), DateUtils.DATE_TIME_PATTERN));
        if (null != po.getUpdateTime()) {
            dto.setUpdateTime(DateUtils.format(po.getUpdateTime(), DateUtils.DATE_TIME_PATTERN));
        }

        return dto;
    }

    private PatientAreaStatisticPO dto2Po(PatientAreaStatisticDTO dto){
//        SimpleDateFormat f = new SimpleDateFormat("MM月dd日");
        PatientAreaStatisticPO po = new PatientAreaStatisticPO();
        if(StringUtils.isNotBlank(dto.getId())) {
            po = pasDao.getOne(dto.getId());
        }
//        BeanUtils.copyProperties(dto, po);
        BeanUtil.copyProperties(dto, po, true, CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
        if(StringUtils.isNotBlank(dto.getStasTime())) {
            po.setStasTime(DateUtils.parseEndOfDay(dto.getStasTime(), DateUtils.DATE_PATTERN));
            po.setStasDate(DateUtils.format(po.getStasTime(), DateUtils.DATE_PATTERN_A));
            po.setStasHour(DateUtils.format(po.getStasTime(), DateUtils.TIME_PATTERN));
            po.setPostDate(DateUtils.format(po.getStasTime(), DateUtils.DATE_PATTERN_A, -1));
        }

        //设置行政区划名称
        if(StringUtils.isNotBlank(dto.getAreaCode())) {
            LocationDimPO ldpo = locationDao.findByAreaNo(dto.getAreaCode());
            if (null != ldpo) {
                po.setArea(ldpo.getAreaName());
            }
        }

        return po;
    }

    /**
     * 主键生成策略，日期+时间+行政区划编码左补齐到32位
     * @param po
     * @return
     */
    private String generateId(PatientAreaStatisticPO po) {
        String id = null;
        if(null != po) {
            StringBuilder bid = new StringBuilder();
            bid.append(DateUtils.format(po.getStasTime(), DateUtils.DATE_PATTERN_A));
            bid.append(po.getAreaCode());
            id = padRight(bid.toString());
        }

        return id;
    }

    /**
     * 将字符串右补0到14位
     * @param src
     * @return
     */
    private String padRight(String src) {
        int diff = DEFAULT_KEY_LENGTH - src.length();
        if (diff <= 0) {
            return src;
        }

        char[] array = new char[DEFAULT_KEY_LENGTH];
        Arrays.fill(array, DEFAULT_SUFFIX);
        System.arraycopy(src.toCharArray(), 0, array, 0, src.length());

        return new String(array);
    }

}
