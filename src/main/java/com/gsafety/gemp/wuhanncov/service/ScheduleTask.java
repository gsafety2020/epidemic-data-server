/**
 * 
 */
package com.gsafety.gemp.wuhanncov.service;

import javax.annotation.Resource;

import com.gsafety.gemp.wuhanncov.contract.service.PatientAreaStatisticsExtService;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.gsafety.gemp.wuhanncov.contract.dto.RefreshPatientDailyCond;
import com.gsafety.gemp.wuhanncov.contract.service.NcovPublicSentimentService;

import lombok.extern.slf4j.Slf4j;

/**
 * 定时调度。
 *
 * @author yangbo
 *
 * @since 2020年1月25日 下午8:59:01
 *
 */
@Component("ScheduleTask")
@Slf4j
public class ScheduleTask {

	/**
	 * 
	 */
	private static final int CHECK_POINT = 1;
	
	/**
	 * 
	 */
	private static final String ENDPOINT_HOUR = "23";

	@Resource
	private NcovPublicSentimentService ncovPublicSentimentService;

	@Resource
	private PatientAreaStatisticsExtService patientAreaStatisticsExtService;

	/**
	 * 
	 * 定时生成舆情热点数据
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月25日 下午9:01:30
	 *
	 */
	//@Scheduled(cron = "0 0/5 * * * ?")
	public void generateStat() {
		log.info("run" + DateTime.now());
		DateTimeFormatter format = DateTimeFormat.forPattern("yyyyMMdd");
		// 查询当日的舆情"pub_sentiment_news"
		String date = DateTime.now().toString(format);
		ncovPublicSentimentService.mergeLocationHot(date);
	}

	/**
	 * 
	 * 定时生成患者日报
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月25日 下午9:01:30
	 *
	 */
	//@Scheduled(cron = "0 0/10 * * * ?")
	public void generatePatientStat() {
		try {
			DateTimeFormatter format = DateTimeFormat.forPattern("yyyyMMdd");
			DateTime now = DateTime.now();
			String sampleDate = now.toString(format);
			Integer hour = now.getHourOfDay();
			String sampleHour = hour.toString();
			
			// 默认取样本日期为报表日期
			RefreshPatientDailyCond cond = new RefreshPatientDailyCond(sampleDate, sampleHour, sampleDate);
			ncovPublicSentimentService.refreshPatientDaily(cond);
			
			// 如果小于指定采集时间，则生成一次上日23:59:59的数据
			if (hour <= CHECK_POINT) {
				String targetDate = now.plusDays(-1).toString(format);
				RefreshPatientDailyCond lastDateCode = new RefreshPatientDailyCond(targetDate, ENDPOINT_HOUR, targetDate);
				ncovPublicSentimentService.refreshPatientDaily(lastDateCode);
			}
		} catch (Exception e) {
			log.error("调用定时job generatePatientStat 失败", e);
		}
		
	}


	/**
	 * 定时生成病患实时数据
	 * @author fanlx
	 * @since
	 */
//	@Scheduled(cron = "0 0/10 * * * ?")
	public void generatePatientRuntimeStat(){
		try{
			ncovPublicSentimentService.syncPatientRuntime();

		}catch (Exception e) {
			log.error("调用定时job generatePatientRuntimeStat 失败", e);
		}
	}
//	@Scheduled(cron = "0 0/5 * * * ?")
	public void generatePatientStatisticExt(){
		patientAreaStatisticsExtService.syncData("20200125");
		patientAreaStatisticsExtService.syncData("20200126");
		patientAreaStatisticsExtService.syncData("20200127");
		patientAreaStatisticsExtService.syncData("20200128");
		patientAreaStatisticsExtService.syncData("20200129");
		patientAreaStatisticsExtService.syncData("20200130");
		patientAreaStatisticsExtService.syncData("20200131");
		patientAreaStatisticsExtService.syncData("20200201");
		patientAreaStatisticsExtService.syncData("20200202");
		patientAreaStatisticsExtService.syncData("20200203");
		patientAreaStatisticsExtService.syncData("20200204");
		patientAreaStatisticsExtService.syncData("20200205");
		patientAreaStatisticsExtService.syncData("20200206");
		patientAreaStatisticsExtService.syncData("20200207");
		patientAreaStatisticsExtService.syncData("20200208");
	}
	
}
