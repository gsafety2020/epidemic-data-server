package com.gsafety.gemp.wuhanncov.service;

import com.gsafety.gemp.wuhanncov.contract.dto.ChinaRealBroadcastDTO;
import com.gsafety.gemp.wuhanncov.contract.service.NcovDataService;
import com.gsafety.gemp.wuhanncov.dao.ChinaRealBroadcastDao;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaRealBroadcastPO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 该类用于武汉冠状病毒疫情数据服务实现,用于实现查询武汉冠状病毒疫情信息等能力
 *
 * @author lyh
 * @since 2020/1/25  11:31
 * <p>
 * 1.0.0
 */
@Service("ncovDataService")
public class NcovDataServiceImpl implements NcovDataService {

    @Resource
    private ChinaRealBroadcastDao chinaRealBroadcastDao;

    /**
     * 该方法用于武汉冠状病毒疫情大屏，用于实现查询实时播报等能力。
     * @author  lyh
     * @since   2020/1/25 12:28
     * @return  java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.ChinaRealBroadcastDTO>
     * @throws
     */
    @Override
    public List<ChinaRealBroadcastDTO> getAllRealBroadcast() {

        List<ChinaRealBroadcastDTO> dtoList = new ArrayList<>();

        List<ChinaRealBroadcastPO> poList = chinaRealBroadcastDao.findAll();

        ChinaRealBroadcastDTO dto = null;
        for(ChinaRealBroadcastPO po : poList){
            dto = new ChinaRealBroadcastDTO();
            BeanUtils.copyProperties(po, dto);
            dtoList.add(dto);
        }
        return dtoList;
    }
}
