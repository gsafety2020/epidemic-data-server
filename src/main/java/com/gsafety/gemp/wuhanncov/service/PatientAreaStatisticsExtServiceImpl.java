package com.gsafety.gemp.wuhanncov.service;

import com.gsafety.gemp.wuhanncov.constant.AppConstant;
import com.gsafety.gemp.wuhanncov.contract.dto.*;
import com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaCountDailyDTO;
import com.gsafety.gemp.wuhanncov.contract.service.PatientAreaStatisticsExtService;
import com.gsafety.gemp.wuhanncov.dao.LocationDimDao;
import com.gsafety.gemp.wuhanncov.dao.PatientAreaStatisticDao;
import com.gsafety.gemp.wuhanncov.dao.PatientAreaStatisticsExtDao;
import com.gsafety.gemp.wuhanncov.dao.po.LocationDimPO;
import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaStatisticPO;
import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaStatisticsExtPO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;


/**
 * 地区病患统计信息扩展服务
 *
 * @author fanlx
 * @since 2020/02/02  16:25
 *
 */
@Service("patientAreaStatisticsExtService")
@Slf4j
@EnableAsync
public class PatientAreaStatisticsExtServiceImpl implements PatientAreaStatisticsExtService {

    @Autowired
    private TaskExecutor taskExecutor;

    /**
     * 市
     */
    private static final String CITY = "city";
    /**
     * 省
     */
    private static final String PROVINCE = "province";

    @Autowired
    private PatientAreaStatisticDao patientAreaStatisticDao;

    @Autowired
    private LocationDimDao locationDimDao;
    @Autowired
    private PatientAreaStatisticsExtDao patientAreaStatisticsExtDao;




    @Override
    public List<PatientAreaCountDailyDTO> getNewAreaListPatientCountHistory(String areaId) {
        List<PatientAreaCountDailyDTO> resultList = new ArrayList<>();
        List<List<PatientAreaCountDailyDTO>> arrayList = new ArrayList<>();
        if (StringUtils.isNotBlank(areaId) && locationDimDao.existsById(areaId)) {
            List<String> areaIdList = new ArrayList<>();
            areaIdList.add(areaId);
            LocationDimPO areaNo = locationDimDao.findByAreaNo(areaId);
            if (!CITY.equals(areaNo.getAreaType2())) {
                throw new RuntimeException("请输入市的行政编码");
            }
            areaIdList.add(areaNo.getParentNo());
            LocationDimPO dimPO = locationDimDao.findByAreaNo(areaNo.getParentNo());
            areaIdList.add(dimPO.getParentNo());
            Integer i = 1;
            for (String ereaCode : areaIdList) {
                arrayList.add(getPatientAreaCountDaily(ereaCode, String.valueOf(i++)));
            }

            for (List<PatientAreaCountDailyDTO> dtoList : arrayList) {
                resultList.addAll(dtoList);
            }
            //全国其他地区的新增 = 全国新增数 - 湖北新增数
            for (int j = 0; j < arrayList.get(0).size(); j++) {
                PatientAreaCountDailyDTO dto = new PatientAreaCountDailyDTO();
                dto.setX(arrayList.get(2).get(j).getX());
                dto.setY(arrayList.get(2).get(j).getY() - arrayList.get(1).get(j).getY());
                dto.setS(String.valueOf(i + 1));
                resultList.add(dto);
            }

        }
        return resultList;
    }
    /***
     * 按区域和类型获取患者统计按天统计数据
     * @author lyh
     * @since 2020/1/26 20:02
     * @param   areaCode 区域编码
     * @param   type 类型
     * @return java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaCountDailyDTO>
     * @throws
     */
    private List<PatientAreaCountDailyDTO> getPatientAreaCountDaily(String areaCode, String type) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date now = new Date();
        String endDate = dateFormat.format(now);
        String startDate = dateFormat.format(DateUtils.addDays(now, -30));
        List<PatientAreaCountDailyDTO> dtoList = new ArrayList<>();
        //获取全国患者统计数据列表
        List<PatientAreaStatisticPO> poList = patientAreaStatisticDao.findByAreaCodeAndStasDateGreaterThanAndStasDateLessThanOrderByStasTimeAsc(areaCode,startDate,endDate);
        if (poList != null && poList.size() > 0) {
            int incNumber = 0;
            PatientAreaCountDailyDTO dto = null;
            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
            for (int i = 0; i < poList.size(); i++) {
                PatientAreaStatisticPO po = poList.get(i);
                dto = new PatientAreaCountDailyDTO();
                if (i == 0) {
                    if (poList.size() > 0) {
                        continue;
                    } else {
                        incNumber = 0;
                    }
                } else {
                    incNumber = po.getPatientNumber() - poList.get(i - 1).getPatientNumber();
                }

                dto.setX(format.format(po.getStasTime()));
                dto.setY(incNumber);
                dto.setS(type);
                dtoList.add(dto);
            }
        }
        return dtoList;
    }



    private String getAppointedDateFormat(String stasDate){
        return stasDate.substring(0,4) + "/" + stasDate.substring(4,6) + "/"+stasDate.substring(6,8);
    }


    @Override
    public List<PatientAreaCaseRateDailyDTO> getAreaListPatientCaseRate(String areaId) {
        LocationDimPO locationDimPO = locationDimDao.findByAreaNo(areaId);
        List<PatientAreaCaseRateDailyDTO> resultList = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date now = new Date();
        String todayDate = dateFormat.format(now);
        String endDate = dateFormat.format(DateUtils.addDays(now, -30));
        //市数据
        List<PatientAreaStatisticsExtPO> cityList = patientAreaStatisticsExtDao.findByAreaCodeAndStasDateLessThanAndStasDateGreaterThanEqualOrderByStasTimeAsc(areaId, todayDate, endDate);
        if (cityList != null && cityList.size() > 0) {
            PatientAreaCaseRateDailyDTO cityDTO = null;
            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
            for (PatientAreaStatisticsExtPO cityPO : cityList) {
                String stasTime = format.format(cityPO.getStasTime());
                cityDTO = new PatientAreaCaseRateDailyDTO();
                cityDTO.setX(stasTime);
                cityDTO.setY(cityPO.getNewCaseRate()==null?0d:cityPO.getNewCaseRate().doubleValue());
                cityDTO.setT("1");
                cityDTO.setS("1");
                resultList.add(cityDTO);
                cityDTO = new PatientAreaCaseRateDailyDTO();
                cityDTO.setX(stasTime);
                cityDTO.setY(cityPO.getNewCaseAllRate()==null?0d:cityPO.getNewCaseAllRate().doubleValue());
                cityDTO.setT("1");
                cityDTO.setS("2");
                resultList.add(cityDTO);
                cityDTO = new PatientAreaCaseRateDailyDTO();
                cityDTO.setX(stasTime);
                cityDTO.setY(cityPO.getTotalCaseRate()==null?0d:cityPO.getTotalCaseRate().doubleValue());
                cityDTO.setT("2");
                cityDTO.setS("1");
                resultList.add(cityDTO);
                cityDTO = new PatientAreaCaseRateDailyDTO();
                cityDTO.setX(stasTime);
                cityDTO.setY(cityPO.getTotalCaseAllRate()==null?0d:cityPO.getTotalCaseAllRate().doubleValue());
                cityDTO.setT("2");
                cityDTO.setS("2");
                resultList.add(cityDTO);
            }
        }
        //省数据
        List<PatientAreaStatisticsExtPO> provinceList = patientAreaStatisticsExtDao.findByAreaCodeAndStasDateLessThanAndStasDateGreaterThanEqualOrderByStasTimeAsc(locationDimPO.getParentNo(), todayDate, endDate);
        if (provinceList != null && provinceList.size() > 0) {
            PatientAreaCaseRateDailyDTO provinceDTO = null;
            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
            for (PatientAreaStatisticsExtPO provincePO : provinceList) {
                String stasTime = format.format(provincePO.getStasTime());
                provinceDTO = new PatientAreaCaseRateDailyDTO();
                provinceDTO.setX(stasTime);
                provinceDTO.setY(provincePO.getNewCaseRate()==null?0d:provincePO.getNewCaseRate().doubleValue());
                provinceDTO.setT("1");
                provinceDTO.setS("3");
                resultList.add(provinceDTO);
                provinceDTO = new PatientAreaCaseRateDailyDTO();
                provinceDTO.setX(stasTime);
                provinceDTO.setY(provincePO.getTotalCaseRate()==null?0d:provincePO.getTotalCaseRate().doubleValue());
                provinceDTO.setT("2");
                provinceDTO.setS("3");
                resultList.add(provinceDTO);
            }
        }
        return resultList;
    }

    @Override
    public List<PatientAreaNewTotalRateDailyDTO> getAreaListPatientNewTotalRate(String areaId) {
        LocationDimPO locationDimPO = locationDimDao.findByAreaNo(areaId);
        List<PatientAreaNewTotalRateDailyDTO> resultList = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        Date now = new Date();
        String todayDate = dateFormat.format(now);
        String endDate = dateFormat.format(DateUtils.addDays(now, -30));
        //市数据
        List<PatientAreaStatisticsExtPO> cityList = patientAreaStatisticsExtDao.findByAreaCodeAndStasDateLessThanAndStasDateGreaterThanEqualOrderByStasTimeAsc(areaId, todayDate, endDate);
        if (cityList != null && cityList.size() > 0) {
            PatientAreaNewTotalRateDailyDTO cityDTO = null;
            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
            for (PatientAreaStatisticsExtPO cityPO : cityList) {
                String stasTime = format.format(cityPO.getStasTime());
                cityDTO = new PatientAreaNewTotalRateDailyDTO();
                cityDTO.setX(stasTime);
                cityDTO.setY(cityPO.getNewTotalRate()==null?0d:cityPO.getNewTotalRate().doubleValue());
                cityDTO.setS("1");
                resultList.add(cityDTO);
                cityDTO = new PatientAreaNewTotalRateDailyDTO();
                cityDTO.setX(stasTime);
                cityDTO.setY(cityPO.getOtherNewTotalRate()==null?0d:cityPO.getOtherNewTotalRate().doubleValue());
                cityDTO.setS("2");
                resultList.add(cityDTO);
            }
        }
        //省数据
        List<PatientAreaStatisticsExtPO> provinceList = patientAreaStatisticsExtDao.findByAreaCodeAndStasDateLessThanAndStasDateGreaterThanEqualOrderByStasTimeAsc(locationDimPO.getParentNo(), todayDate, endDate);
        if (provinceList != null && provinceList.size() > 0) {
            PatientAreaNewTotalRateDailyDTO provinceDTO = null;
            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
            for (PatientAreaStatisticsExtPO provincePO : provinceList) {
                String stasTime = format.format(provincePO.getStasTime());
                provinceDTO = new PatientAreaNewTotalRateDailyDTO();
                provinceDTO.setX(stasTime);
                provinceDTO.setY(provincePO.getOtherNewTotalRate()==null?0d:provincePO.getOtherNewTotalRate().doubleValue());
                provinceDTO.setS("3");
                resultList.add(provinceDTO);
            }
        }
        return resultList;
    }

    /**
     * 查询市-省-全国累计病例数增长率
     * @return
     */
    @Override
    public Map<String,Object> showCitySickRiseRate(String areaCode){
        //暂时写死成武汉市
        List<PatientAreaStatisticsExtRiseDTO> patientAreaStatisticsExtRiseDTOList1 = new ArrayList<>();
        List<PatientAreaStatisticsExtRiseDTO> patientAreaStatisticsExtRiseDTOList2 = new ArrayList<>();
        List<PatientAreaStatisticsExtRiseDTO> patientAreaStatisticsExtRiseDTOList3 = new ArrayList<>();
        Map<String,Object> map = new HashMap<>();
        //先查询市一级
        List<PatientAreaStatisticsExtPO> patientAreaStatisticsExtPOListCity = patientAreaStatisticsExtDao.findByAreaCodeOrderByStasTimeDesc(areaCode);
        for(PatientAreaStatisticsExtPO po : patientAreaStatisticsExtPOListCity){
            PatientAreaStatisticsExtRiseDTO dto1 = new PatientAreaStatisticsExtRiseDTO();
            PatientAreaStatisticsExtRiseDTO dto2 = new PatientAreaStatisticsExtRiseDTO();
            String stasDate = po.getStasDate();
            if(StringUtils.isNotBlank(stasDate)){
                dto1.setX(getAppointedDateFormat(stasDate));
                dto2.setX(getAppointedDateFormat(stasDate));
            }
            dto1.setS("1");
            dto1.setY(po.getCaseGrowthRate() != null ? po.getCaseGrowthRate().toString() : "");
            dto2.setS("2");
            dto2.setY(po.getOtherGrowthRate() != null ? po.getOtherGrowthRate().toString() : "");
            patientAreaStatisticsExtRiseDTOList1.add(dto1);
            patientAreaStatisticsExtRiseDTOList2.add(dto2);

        }
        //再查询省一级
        LocationDimPO locationDimPO = locationDimDao.findByAreaNo(areaCode);
        String provAreaCode = locationDimPO.getAreaNo();
        List<PatientAreaStatisticsExtPO> patientAreaStatisticsExtPOListProv = patientAreaStatisticsExtDao.findByAreaCodeOrderByStasTimeDesc(provAreaCode);
        for(PatientAreaStatisticsExtPO po : patientAreaStatisticsExtPOListProv){
            PatientAreaStatisticsExtRiseDTO dto3 = new PatientAreaStatisticsExtRiseDTO();
            String stasDate = po.getStasDate();
            if(StringUtils.isNotBlank(stasDate)){
                dto3.setX(getAppointedDateFormat(stasDate));
            }
            dto3.setS("3");
            dto3.setY(po.getOtherGrowthRate() != null ? po.getOtherGrowthRate().toString() : "");
            patientAreaStatisticsExtRiseDTOList3.add(dto3);
        }
        map.put("1",patientAreaStatisticsExtRiseDTOList1);
        map.put("2",patientAreaStatisticsExtRiseDTOList2);
        map.put("3",patientAreaStatisticsExtRiseDTOList3);
        return map;
    }

    /**
     * 查询市-省-全国死亡比例对比图
     * @return
     */
    @Override
    public Map<String,Object> showCitySickDeathRate(String areaCode){
        Map<String,Object> map = new HashMap<>();
        List<PatientAreaStatisticsExtDeathDTO> patientAreaStatisticsExtDeathDTOList1 = new ArrayList<>();
        List<PatientAreaStatisticsExtDeathDTO> patientAreaStatisticsExtDeathDTOList2 = new ArrayList<>();
        List<PatientAreaStatisticsExtDeathDTO> patientAreaStatisticsExtDeathDTOList3 = new ArrayList<>();
        List<PatientAreaStatisticsExtDeathDTO> patientAreaStatisticsExtDeathDTOList4 = new ArrayList<>();
        List<PatientAreaStatisticsExtDeathDTO> patientAreaStatisticsExtDeathDTOList5 = new ArrayList<>();
        //先查询市一级
        List<PatientAreaStatisticsExtPO> patientAreaStatisticsExtPOListCity = patientAreaStatisticsExtDao.findByAreaCodeOrderByStasTimeDesc(areaCode);
        for(PatientAreaStatisticsExtPO po : patientAreaStatisticsExtPOListCity){
            PatientAreaStatisticsExtDeathDTO dto1 = new PatientAreaStatisticsExtDeathDTO();
            PatientAreaStatisticsExtDeathDTO dto2 = new PatientAreaStatisticsExtDeathDTO();
            String stasDate = po.getStasDate();
            if(StringUtils.isNotBlank(stasDate)){
                dto1.setX(getAppointedDateFormat(stasDate));
                dto2.setX(getAppointedDateFormat(stasDate));
            }
            dto1.setS("1");
            dto1.setY(po.getDeathRate() != null ? po.getDeathRate().toString() : "");
            dto2.setS("4");
            dto2.setY(po.getOtherDeathRate() != null ? po.getOtherDeathRate().toString() : "");
            patientAreaStatisticsExtDeathDTOList1.add(dto1);
            patientAreaStatisticsExtDeathDTOList2.add(dto2);
        }
        //再查询省一级
        LocationDimPO locationDimPO = locationDimDao.findByAreaNo(areaCode);
        String provAreaCode = locationDimPO.getAreaNo();
        List<PatientAreaStatisticsExtPO> patientAreaStatisticsExtPOListProv = patientAreaStatisticsExtDao.findByAreaCodeOrderByStasTimeDesc(provAreaCode);
        for(PatientAreaStatisticsExtPO po : patientAreaStatisticsExtPOListProv){
            PatientAreaStatisticsExtDeathDTO dto3 = new PatientAreaStatisticsExtDeathDTO();
            PatientAreaStatisticsExtDeathDTO dto4 = new PatientAreaStatisticsExtDeathDTO();
            String stasDate = po.getStasDate();
            if(StringUtils.isNotBlank(stasDate)){
                dto3.setX(getAppointedDateFormat(stasDate));
                dto4.setX(getAppointedDateFormat(stasDate));
            }
            dto3.setS("2");
            dto3.setY(po.getDeathRate() != null ? po.getDeathRate().toString() : "");
            dto4.setS("5");
            dto4.setY(po.getOtherDeathRate() != null ? po.getOtherDeathRate().toString() : "");
            patientAreaStatisticsExtDeathDTOList3.add(dto3);
            patientAreaStatisticsExtDeathDTOList4.add(dto4);
        }
        //查询全国
        String counAreaCode = AppConstant.DIM_ChINA_CODE;
        List<PatientAreaStatisticsExtPO> patientAreaStatisticsExtPOListCoun = patientAreaStatisticsExtDao.findByAreaCodeOrderByStasTimeDesc(counAreaCode);
        for(PatientAreaStatisticsExtPO po : patientAreaStatisticsExtPOListCoun){
            PatientAreaStatisticsExtDeathDTO dto5 = new PatientAreaStatisticsExtDeathDTO();
            String stasDate = po.getStasDate();
            if(StringUtils.isNotBlank(stasDate)){
                dto5.setX(getAppointedDateFormat(stasDate));
            }
            dto5.setS("3");
            dto5.setY(po.getDeathRate() != null ? po.getDeathRate().toString() : "");
            patientAreaStatisticsExtDeathDTOList5.add(dto5);
        }
        map.put("1",patientAreaStatisticsExtDeathDTOList1);
        map.put("4",patientAreaStatisticsExtDeathDTOList2);
        map.put("2",patientAreaStatisticsExtDeathDTOList3);
        map.put("5",patientAreaStatisticsExtDeathDTOList4);
        map.put("3",patientAreaStatisticsExtDeathDTOList5);
        return map;
    }






    @Override
    public void syncData(String date){

        log.info("同步病患统计扩展数据{}",date);

        List<LocationDimPO> dimPOS = locationDimDao.getAutoJobOn();

        dimPOS.stream().forEach(temp -> taskExecutor.execute(() -> {
            syncOneCity(temp.getAreaNo(),temp.getParentNo(),date );
        }));

        log.info("同步病患统计扩展数据结束{}",date);
    }

    public void syncOneCity(String areaCode,String parentNo,String date) {

        DateTimeFormatter format = DateTimeFormat.forPattern("yyyyMMdd");
        DateTime dateTime = DateTime.parse(date, format);
        String beforeDate = dateTime.minusDays(1).toString(format);

        Optional<PatientAreaStatisticPO> optional1 = patientAreaStatisticDao.findById(date + areaCode);
        if (optional1.isPresent()) {
            PatientAreaStatisticPO one = optional1.get();
            Optional<PatientAreaStatisticPO> optional2 = patientAreaStatisticDao.findById(beforeDate + areaCode);
            Optional<PatientAreaStatisticPO> optional3 = patientAreaStatisticDao.findById(date + parentNo);
            Optional<PatientAreaStatisticPO> optional4 = patientAreaStatisticDao.findById(beforeDate + parentNo);
            Optional<PatientAreaStatisticPO> optional5 = patientAreaStatisticDao.findById(date + "000000");
            Optional<PatientAreaStatisticPO> optional6 = patientAreaStatisticDao.findById(beforeDate + "000000");

            //初始化计算器
            PatientAnalysisCalculator calculator = new PatientAnalysisCalculator(
                    one,
                    optional2.isPresent() ? optional2.get() : null,
                    optional3.isPresent() ? optional3.get() : null,
                    optional4.isPresent() ? optional4.get() : null,
                    optional5.isPresent() ? optional5.get() : null,
                    optional6.isPresent() ? optional6.get() : null
            );

            PatientAreaStatisticsExtPO po = PatientAreaStatisticsExtPO.builder().id(one.getId()).area(one.getArea()).areaCode(one.getAreaCode())
                    .stasTime(one.getStasTime()).stasDate(one.getStasDate()).stasHour(one.getStasHour())
                    .dataSrc(one.getDataSrc()).newCaseRate(calculator.newCaseRate()).newCaseAllRate(calculator.newCaseAllRate())
                    .totalCaseRate(calculator.totalCaseRate()).totalCaseAllRate(calculator.totalCaseAllRate())
                    .caseGrowthRate(calculator.growthRate()).otherGrowthRate(calculator.otherGrowthRate())
                    .newTotalRate(calculator.newTotalRate()).otherNewTotalRate(calculator.otherNewTotalRate())
                    .deathRate(calculator.deathRate()).otherDeathRate(calculator.otherDeathRate()).updateTime(new Date()).build();

            patientAreaStatisticsExtDao.save(po);


        }
    }


}
