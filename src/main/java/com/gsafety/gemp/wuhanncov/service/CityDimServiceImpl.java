package com.gsafety.gemp.wuhanncov.service;

import com.gsafety.gemp.wuhanncov.contract.service.DimService;
import com.gsafety.gemp.wuhanncov.dao.LocationDimDao;
import com.gsafety.gemp.wuhanncov.dao.po.LocationDimPO;
import org.aspectj.weaver.patterns.PerFromSuper;

import javax.annotation.Resource;

/**
 * @author Administrator
 * @Title: CountryDimServiceImpl
 * @ProjectName wuhan-ncov
 * @Description: parent返回省，city返回自己，省返回省份
 * @date 2020/2/314:29
 */
public class CityDimServiceImpl implements DimService{

    private LocationDimPO ldpo;

    private final String AREA_TYPE = "city";

    @Resource
    private LocationDimDao locationDao;

    public CityDimServiceImpl(LocationDimPO ldpo) {
        this.ldpo = ldpo;
    }

    @Override
    public String getParent() {
        return ldpo.getParentNo();
    }

    @Override
    public String getProvince() {
        return getParent();
    }

    @Override
    public String getCity() {
        return ldpo.getAreaNo();
    }

    @Override
    public String getType() {
        return this.AREA_TYPE;
    }
}
