package com.gsafety.gemp.wuhanncov.service;

import com.gsafety.gemp.wuhanncov.contract.dto.ReportAreaStatDailyDTO;
import com.gsafety.gemp.wuhanncov.contract.service.ReportAreaStatDailyService;
import com.gsafety.gemp.wuhanncov.dao.ReportAreaStatDailyDao;
import com.gsafety.gemp.wuhanncov.dao.po.ReportAreaStatDailyPO;
import com.gsafety.gemp.wuhanncov.utils.DateUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

/**
 * 每日情况汇总表,汇总江夏区每日上报情况
 * @author Luoganggang
 * @since 2020/2/9 16:28
 */
@Service
public class ReportAreaStatDailyServiceImpl implements ReportAreaStatDailyService {

    @Autowired
    private ReportAreaStatDailyDao reportAreaStatDailyDao;

    /**
     * 集中医学观察每日情况汇总
     * @param areaCode 行政编码
     * @return ReportAreaStatDailyDTO
     */
    @Override
    public ReportAreaStatDailyDTO dailySummary(String areaCode) {
        String today = DateUtils.format(LocalDate.now());
        String id = today + areaCode;
        ReportAreaStatDailyDTO dto = new ReportAreaStatDailyDTO();
        if (StringUtils.isNotBlank(areaCode) && reportAreaStatDailyDao.existsById(id)){
            ReportAreaStatDailyPO po = reportAreaStatDailyDao.getOne(id);
            BeanUtils.copyProperties(po,dto);
        }
        return dto;
    }
}
