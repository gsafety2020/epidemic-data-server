package com.gsafety.gemp.wuhanncov.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.gsafety.gemp.wuhanncov.contract.enums.UserViralInfectionStatusEnum;
import com.gsafety.gemp.wuhanncov.contract.service.UserEpidemicListService;
import com.gsafety.gemp.wuhanncov.dao.UserEpidemicInfoDao;
import com.gsafety.gemp.wuhanncov.dao.UserEpidemicListDao;
import com.gsafety.gemp.wuhanncov.dao.po.UserEpidemicInfoPO;
import com.gsafety.gemp.wuhanncov.dao.po.UserEpidemicListPO;
import com.gsafety.gemp.wuhanncov.utils.DateUtils;
import com.gsafety.gemp.wuhanncov.utils.UUIDUtils;

/**
 * 
* @ClassName: UserEpidemicListServiceImpl 
* @Description: 综合平台用户服务层
* @author luoxiao
* @date 2020年2月6日 下午8:09:09 
*
 */
@Service("userEpidemicListService")
public class UserEpidemicListServiceImpl implements UserEpidemicListService{

	@Autowired
	private UserEpidemicListDao userEpidemicListDao;
	
	@Autowired
	private UserEpidemicInfoDao userEpidemicInfoDao;
	
	@Override
	public UserEpidemicListPO getUserByMobile(String mobile) {
		if(StringUtils.isEmpty(mobile)){
			throw new RuntimeException("手机号不能为空!");
		}
		UserEpidemicListPO po = userEpidemicListDao.findByMobile(mobile);
		return po;
	}

	@Override
	@Transactional
	public void synchroListToInfo() {
		//查询综合平台数据
		List<UserEpidemicListPO> listPos = userEpidemicListDao.findAll();
		//查询上报信息基础数据
		Date reportDate = DateUtils.toDateFormatDate("2020-01-01");
		List<UserEpidemicInfoPO> infoPos = userEpidemicInfoDao.findByReportDate(reportDate);
		List<UserEpidemicInfoPO> results = differenceData(infoPos,listPos);
		if(!CollectionUtils.isEmpty(results)){
			int listSize=results.size();
			int toIndex=1000;
			int batchNum = toIndex;
			for(int i = 0;i<results.size();i+=batchNum){
		        if(i+batchNum>listSize){//作用为toIndex最后没有1000条数据则剩余几条newList中就装几条
		            toIndex=listSize-i;
		        }
		        List<UserEpidemicInfoPO> subList = results.subList(i,i+toIndex);
		        //批量插入
		        userEpidemicInfoDao.saveAll(subList);
		        userEpidemicInfoDao.flush();
		    }
		}
	}
	
	private List<UserEpidemicInfoPO> differenceData(List<UserEpidemicInfoPO> infoPos,List<UserEpidemicListPO> listPos){
		List<UserEpidemicInfoPO> results = new ArrayList<UserEpidemicInfoPO>();
		UserEpidemicInfoPO po = null;
		for(UserEpidemicListPO list : listPos){
			int flag = 0;
			for(UserEpidemicInfoPO info : infoPos){
				if(list.getMobile().equals(info.getMobile())){
					flag = 1;
					break;
				}
			}
			if(flag == 0){
				Date reportDate = DateUtils.toDateFormatDate("2020-01-01");
				po = new UserEpidemicInfoPO();
				po.setEpidemicInfoId("20200101"+list.getMobile());
				po.setReportDate(reportDate);
				po.setCreateTime(Calendar.getInstance().getTime());
				po.setUserName(list.getUserName());
				po.setUserId(list.getMobile());
				po.setMobile(list.getMobile());
				po.setDepartmentName("初始部门值");
				po.setDepartmentCode(list.getDepartmentCode());
				po.setUserCondition(UserViralInfectionStatusEnum.NORMAL.getCode());
				po.setFamilyCondition(UserViralInfectionStatusEnum.NORMAL.getCode());
				po.setFamilyDetail(null);
				po.setFamilyNumbers(0);
				po.setUserDetail(null);
				po.setHomeQuarantineDate(null);
				po.setHomeQuarantineDays(null);
				po.setParmanentAddress("初始值");
				po.setPresentAddress("初始值");
				results.add(po);
			}
		}
		return results;
	}
	
}
