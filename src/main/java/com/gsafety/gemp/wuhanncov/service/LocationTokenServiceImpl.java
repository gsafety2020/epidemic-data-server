package com.gsafety.gemp.wuhanncov.service;

import com.gsafety.gemp.wuhanncov.contract.dto.LocationTokenDTO;
import com.gsafety.gemp.wuhanncov.contract.params.DimQueryParam;
import com.gsafety.gemp.wuhanncov.contract.service.LocationTokenService;
import com.gsafety.gemp.wuhanncov.dao.LocationDimDao;
import com.gsafety.gemp.wuhanncov.dao.LocationTokenDao;
import com.gsafety.gemp.wuhanncov.dao.po.LocationDimPO;
import com.gsafety.gemp.wuhanncov.dao.po.LocationTokenPO;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.gsafety.gemp.wuhanncov.specification.LocationDimSpecifications.areaNoEqual;
import static com.gsafety.gemp.wuhanncov.specification.LocationDimSpecifications.parentNoEqual;
/**
 * 区域病患实时统计报表
 *
 * @author yangbo
 *
 * @since 2020年1月30日 下午9:02:47
 *
 */
@Service("locationTokenService")
public class LocationTokenServiceImpl implements LocationTokenService {

	@Resource
	private LocationTokenDao tokenDao;
	@Resource
	private LocationDimDao locationDimDao;


	@Override
	public LocationTokenPO getOne(String id) {
		return tokenDao.getOne(id);
	}

	@Override
	public LocationTokenPO updateToken(LocationTokenPO ltp) {
		return tokenDao.save(ltp);
	}

	@Override
	public Page<LocationTokenDTO> findTokenPage(DimQueryParam queryParam, Pageable pageable) {

		List<LocationTokenPO> infolist = tokenDao.findByAreaCodeAndToken(queryParam.getAreaNo(), queryParam.getToken());
		if((null==infolist)||0==(infolist.size())){
			List<LocationTokenDTO> emptylist=new ArrayList<>();
			return new PageImpl<>(emptylist,pageable,0);
		}
		Page<LocationDimPO> page=locationDimDao.findAll(areaNoEqual(queryParam.getAreaNo()).or(parentNoEqual(queryParam.getAreaNo())), pageable);
		return toPageDTO(page);
	}

	/**
	 * 分页po转dto
	 * @param page
	 * @return
	 */
	public Page<LocationTokenDTO> toPageDTO(Page<LocationDimPO> page){
		List<LocationTokenDTO> content = toDTOList(page.getContent());
		return new PageImpl<>(content, page.getPageable(), page.getTotalElements());
	}

	/**
	 * 集合po转dto
	 * @param list
	 * @return
	 */
	public List<LocationTokenDTO> toDTOList(List<LocationDimPO> list){
		List<LocationTokenDTO> content = new ArrayList<>();
		Iterator<LocationDimPO> it = list.iterator();
		while (it.hasNext()) {
			LocationDimPO po = it.next();
			content.add(po2DTO(po));
		}

		return content;
	}

	/**
	 * 单个po转dto
	 * @param po
	 * @return
	 */
	public LocationTokenDTO po2DTO(LocationDimPO po){
		LocationTokenDTO dto = new LocationTokenDTO();
		BeanUtils.copyProperties(po, dto);
		//装载token
		LocationTokenPO tokeninfo = tokenDao.getOne(dto.getAreaNo());
		dto.setToken(tokeninfo.getToken());
		//装载父节点
		LocationDimPO parentinfo = locationDimDao.getOne(po.getParentNo());
		dto.setParentAreaName(parentinfo.getAreaName());
		return dto;
	}
}
