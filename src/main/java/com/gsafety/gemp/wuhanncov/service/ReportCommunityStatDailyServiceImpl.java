package com.gsafety.gemp.wuhanncov.service;

import com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaCountDailyDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.ReportCommunityStatDailyDTO;
import com.gsafety.gemp.wuhanncov.contract.service.ReportCommunityStatDailyService;
import com.gsafety.gemp.wuhanncov.dao.CodeBasDistrictDao;
import com.gsafety.gemp.wuhanncov.dao.ReportCommunityStatDailyDao;
import com.gsafety.gemp.wuhanncov.dao.po.CodeBasDistrictPO;
import com.gsafety.gemp.wuhanncov.dao.po.ReportCommunityStatDailyPO;
import com.gsafety.gemp.wuhanncov.utils.DateUtils;
import com.gsafety.gemp.wuhanncov.wrapper.ReportCommunityStatDailyWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

import static com.gsafety.gemp.wuhanncov.specification.ReportCommunityStatDailySpecifications.*;

/**
 * @author dusiwei
 */
@Service("reportCommunityStatDailyService")
@Slf4j
@EnableAsync
public class ReportCommunityStatDailyServiceImpl implements ReportCommunityStatDailyService {

    /**
     * 社区发热人员发展趋势折线图类型-新增发热
     */
    private static final String NEW_HOT = "1";

    /**
     * 社区发热人员发展趋势折线图类型-累积发热
     */
    private static final String HOT = "2";

    @Resource
    private ReportCommunityStatDailyDao reportCommunityStatDailyDao;

    @Resource
    private CodeBasDistrictDao codeBasDistrictDao;

    @Override
    public Map<String, Object> getCommunityStatDaily(String areaCode) {
        if (StringUtils.isEmpty(areaCode)) {
            //默认查询江夏区
            areaCode = "420115";
        }
        //最新数据
        String statDate = reportCommunityStatDailyDao.getLatestStatDate();
        if (StringUtils.isEmpty(statDate)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            statDate = dateFormat.format(new Date());
        }
        //当前行政区域
        Specification<ReportCommunityStatDailyPO> areaSp = Specification.where(areaCodeEqual(areaCode)).and(statDateEqual(statDate));
        Optional<ReportCommunityStatDailyPO> areaOptional = reportCommunityStatDailyDao.findOne(areaSp);
        ReportCommunityStatDailyDTO dto = null;
        if (areaOptional.isPresent()) {
            ReportCommunityStatDailyPO po = areaOptional.get();
            dto = ReportCommunityStatDailyWrapper.toDTO(po);
            setAreaName(dto);
        }
        //下级数据列表
        Specification<ReportCommunityStatDailyPO> childrenSp = Specification.where(parentAreaCodeEqual(areaCode)).and(statDateEqual(statDate));
        List<ReportCommunityStatDailyPO> poList = reportCommunityStatDailyDao.findAll(childrenSp, new Sort(Sort.Direction.ASC, FIELD_AREA_CODE));
        List<ReportCommunityStatDailyDTO> dtoList = ReportCommunityStatDailyWrapper.toDTOList(poList);
        for (ReportCommunityStatDailyDTO childrenDTO : dtoList) {
            setAreaName(childrenDTO);
        }
        //结果
        Map<String,Object> result = new HashMap<>(16);
        result.put("dto",dto);
        result.put("dtoList",dtoList);
        return result;

    }

    /**
     * 社区发热人员发展趋势
     * @param areaCode 行政编码
     * @return List<PatientAreaCountDailyDTO>
     */
    @Override
    public List<PatientAreaCountDailyDTO> communityHotPeopleStat(String areaCode) {
        List<PatientAreaCountDailyDTO> dtos = new ArrayList<>();
        if (StringUtils.isNotBlank(areaCode)){
            List<ReportCommunityStatDailyPO> list = reportCommunityStatDailyDao.findByAreaCode(areaCode);
            for (ReportCommunityStatDailyPO po : list) {
                String time =  po.getStatDate().substring(0,4)+"/"+po.getStatDate().substring(4,6)+"/"+po.getStatDate().substring(6);
                dtos.add(new PatientAreaCountDailyDTO(time,po.getNewFeverNumber(),NEW_HOT));
                dtos.add(new PatientAreaCountDailyDTO(time,po.getTotalFeverNumber(),HOT));
            }
        }
        return dtos;
    }

    /**
     * 行政名称
     * @param dto
     */
    private void setAreaName(ReportCommunityStatDailyDTO dto) {
        if (dto!=null && StringUtils.isNotEmpty(dto.getAreaCode())) {
            CodeBasDistrictPO codeBasDistrictPO = codeBasDistrictDao.getOne(dto.getAreaCode());
            if (codeBasDistrictPO!=null) {
                dto.setAreaName(codeBasDistrictPO.getDistrictName());
            }
        }
    }

}
