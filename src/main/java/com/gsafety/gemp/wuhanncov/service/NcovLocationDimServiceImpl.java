package com.gsafety.gemp.wuhanncov.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.gsafety.gemp.wuhanncov.contract.dto.LocationDimDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaStatisticDTO;
import com.gsafety.gemp.wuhanncov.contract.params.DimQueryParam;
import com.gsafety.gemp.wuhanncov.contract.service.NcovLocationDimService;
import com.gsafety.gemp.wuhanncov.dao.LocationDimDao;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaCountStatisticsPO;
import com.gsafety.gemp.wuhanncov.dao.po.LocationDimPO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import static com.gsafety.gemp.wuhanncov.specification.LocationDimSpecifications.areaNameEqual;
import static com.gsafety.gemp.wuhanncov.specification.LocationDimSpecifications.areaNoNotEqual;

/**
 * @author xiaoz
 * @date 2020-01-29 9:47
 * @Description
 */
@Service("ncovLocationDimService")
@Slf4j
@EnableAsync
public class NcovLocationDimServiceImpl implements NcovLocationDimService {

    @Resource
    private LocationDimDao locationDimDao;

    @Override
    public void save(LocationDimDTO dto) {
        locationDimDao.save(dto2Po(dto));
    }

    @Override
    public void delete(String areaNo) {
        locationDimDao.deleteById(areaNo);
    }

    @Override
    public boolean exist(String areaNo, String areaName) {
        LocationDimPO po = new LocationDimPO();
        ExampleMatcher exampleMatcher = ExampleMatcher.matching();
        if(StringUtils.isNotBlank(areaNo)) {
            po.setAreaNo(areaNo);
            exampleMatcher.withMatcher("areaNo", ExampleMatcher.GenericPropertyMatchers.exact());
        }

        if(StringUtils.isNotBlank(areaName)) {
            po.setAreaName(areaName);
            exampleMatcher.withMatcher("areaName", ExampleMatcher.GenericPropertyMatchers.exact());
        }

        return locationDimDao.exists(Example.of(po, exampleMatcher));
    }

    @Override
    public LocationDimDTO load(String areaNo) {
        return po2DTO(locationDimDao.getOne(areaNo));
    }

    @Override
    public List<LocationDimDTO> loadSelfAndChild(String areaNo) {
        Sort sort = Sort.by(Sort.Direction.ASC, "areaNo");
        return po2DTO(locationDimDao.findByParentNoOrAreaNo(areaNo, areaNo, sort));
    }

    @Override
    public Page<LocationDimDTO> findByAreaNamePage(String areaName, Pageable pageable) {
        Page<LocationDimPO> page;
        if (StringUtils.isNotBlank(areaName)) {
            page = locationDimDao.findByAreaNameLike("%" + areaName + "%", pageable);
        } else {
            page = locationDimDao.findAll(pageable);
        }

        return po2DTO(page);
    }

    @Override
    public List<LocationDimDTO> findByAutoJobOn() {
        return po2DTO(locationDimDao.getAutoJobOn());
    }

    @Override
    public List<LocationDimDTO> loadChild(String areaNo) {
        Sort sort = Sort.by(Sort.Direction.ASC, "seqNum");
        return po2DTO(locationDimDao.findByParentNo(areaNo, sort));
    }

    @Override
    public List<LocationDimDTO> findLocationDimParentSearch(DimQueryParam queryParam) {
        Sort sort = Sort.by(Sort.Direction.DESC, "seqNum");
        List<LocationDimPO> list = locationDimDao.findAll(areaNameEqual(queryParam.getSearchAreaName()).and(areaNoNotEqual(queryParam.getAreaNo())), sort);
        return po2DTO(list);
    }

    @Override
    public List<LocationDimDTO> findByParentNo(String parentNo) {
        return po2DTO(locationDimDao.findByParentNo(parentNo));
    }


    /**
     * 分页po转dto
     * @param page
     * @return
     */
    private Page<LocationDimDTO> po2DTO(Page<LocationDimPO> page){
        List<LocationDimDTO> content = po2DTO(page.getContent());
        return new PageImpl<>(content, page.getPageable(), page.getTotalElements());
    }

    /**
     * 集合po转dto
     * @param list
     * @return
     */
    private List<LocationDimDTO> po2DTO(List<LocationDimPO> list){
        List<LocationDimDTO> content = new ArrayList<>();
        Iterator<LocationDimPO> it = list.iterator();
        while (it.hasNext()) {
            LocationDimPO po = it.next();
            content.add(po2DTO(po));
        }

        return content;
    }

    /**
     * 单个po转dto
     * @param po
     * @return
     */
    private LocationDimDTO po2DTO(LocationDimPO po){
        LocationDimDTO dto = new LocationDimDTO();
        BeanUtils.copyProperties(po, dto);
        return dto;
    }

    private LocationDimPO dto2Po(LocationDimDTO dto){
        LocationDimPO po = new LocationDimPO();
        BeanUtil.copyProperties(dto, po, true, CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
        return po;
    }
}
