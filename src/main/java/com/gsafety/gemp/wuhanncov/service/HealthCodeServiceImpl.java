package com.gsafety.gemp.wuhanncov.service;

import com.gsafety.gemp.wuhanncov.contract.dto.PestHealthCodeDTO;
import com.gsafety.gemp.wuhanncov.contract.params.DimQueryParam;
import com.gsafety.gemp.wuhanncov.contract.service.HealthCodeService;
import com.gsafety.gemp.wuhanncov.dao.PestHealthCodeDao;
import com.gsafety.gemp.wuhanncov.dao.po.PestHealthCodePO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import static com.gsafety.gemp.wuhanncov.wrapper.HealthCodeWrapper.toPageDTO;
import static com.gsafety.gemp.wuhanncov.specification.PestHealthCodeSpecifications.userNameLike;
import static com.gsafety.gemp.wuhanncov.specification.PestHealthCodeSpecifications.nickNameLike;
import static com.gsafety.gemp.wuhanncov.specification.PestHealthCodeSpecifications.mobilelike;
import static com.gsafety.gemp.wuhanncov.specification.PestHealthCodeSpecifications.areaNameLike;
import static com.gsafety.gemp.wuhanncov.specification.PestHealthCodeSpecifications.healthCodeColorEqual;

/**
 * @author wangwenhai
 * @Title: HealthCodeServiceImpl
 * @ProjectName wuhan-ncov
 * @Description:
 * @date 2020/2/25
 */
@Service("healthCodeService")
public class HealthCodeServiceImpl implements HealthCodeService {

    @Resource
    private PestHealthCodeDao pestHealthCodeDao;

    @Override
    public Page<PestHealthCodeDTO> findHealthCodePage(DimQueryParam queryParam, Pageable pageable) {
        Page<PestHealthCodePO> page = pestHealthCodeDao.findAll(userNameLike(queryParam.getKeywords())
                .or(nickNameLike(queryParam.getKeywords()))
                .or(mobilelike(queryParam.getKeywords()))
                .or(areaNameLike(queryParam.getKeywords()))
                .and(healthCodeColorEqual(queryParam.getHealthCodeColor())), pageable);
        return toPageDTO(page);
    }
}
