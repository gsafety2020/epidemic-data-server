package com.gsafety.gemp.wuhanncov.service;

import com.gsafety.gemp.wuhanncov.contract.dto.*;
import com.gsafety.gemp.wuhanncov.contract.service.NcovPublicSentimentDxyService;
import com.gsafety.gemp.wuhanncov.dao.*;
import com.gsafety.gemp.wuhanncov.dao.po.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.gsafety.gemp.wuhanncov.service.MigrateSpecification.*;
import static com.gsafety.gemp.wuhanncov.service.pubSentimentNewsSpecification.newsContentLike;
import static com.gsafety.gemp.wuhanncov.service.pubSentimentNewsSpecification.newsTimeBetween;

/**
 * 该类用于...,用于实现...等能力
 *
 * @author 86186
 * @since 2020/1/25 15:07
 * <p>
 * 1.0.0
 */
@Service("ncovPublicSentimentDxyService")
@Slf4j
@EnableAsync
public class NcovPublicSentimentDxyServiceImpl implements NcovPublicSentimentDxyService {

    @Autowired
    private EntityManager entityManager;

    @Resource
    private ChinaCountStatisticsDxyDao chinaCountStatisticsDxyDao;

    @Resource
    private LocationDimDao locationDimDao;

    @Override
    public List<ChinaCountStatisticsDTO> findAreaByAreaIdsCount(List<String> areaIdList) {
        List<LocationDimPO> dimList=locationDimDao.findAll(new Specification<LocationDimPO>(){
            @Override
            public Predicate toPredicate(Root<LocationDimPO> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                CriteriaBuilder.In<String> in = builder.in(root.get("areaNo"));
                for (String id : areaIdList) {
                    in.value(id);
                }
                if(areaIdList!=null&&areaIdList.size()>0) {
                    return builder.and(in);
                }else{
                    return null;
                }
            }
        });
        List<String> areaNames=new ArrayList<String>();
        for(LocationDimPO dimPO:dimList){
            areaNames.add(dimPO.getAreaName().replaceAll("省","").replaceAll("市",""));
        }
        Sort sort = Sort.by(Direction.DESC, "confirmCase");
        List<ChinaCountStatisticsDxyPO> list = chinaCountStatisticsDxyDao.findAll(new Specification<ChinaCountStatisticsDxyPO>() {
            @Override
            public Predicate toPredicate(Root<ChinaCountStatisticsDxyPO> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                CriteriaBuilder.In<String> in = builder.in(root.get("areaName"));
                for (String name : areaNames) {
                    in.value(name);
                }
                return builder.and(in);
            }
        }, sort);
        List<ChinaCountStatisticsDTO> content = new ArrayList<ChinaCountStatisticsDTO>();
        Iterator<ChinaCountStatisticsDxyPO> it = list.iterator();
        SimpleDateFormat f = new SimpleDateFormat("MM月dd日");
        while (it.hasNext()) {
            ChinaCountStatisticsDxyPO po = it.next();
            ChinaCountStatisticsDTO dto = new ChinaCountStatisticsDTO();
            BeanUtils.copyProperties(po, dto);
            Date updateTime = po.getUpdateTime();
            String dateTime = f.format(updateTime);
            dto.setCountTime(dateTime);
            content.add(dto);
        }
        return content;
    }
    @Override
    public List<ChinaCountStatisticsDTO> findHBCityCount(){
        return getCityCountByProvince("湖北");
    }

    private List<ChinaCountStatisticsDTO> getCityCountByProvince(String areaName) {
        Sort sort = Sort.by(Direction.DESC, "confirmCase");
        List<ChinaCountStatisticsDxyPO> list = chinaCountStatisticsDxyDao.findAll(new Specification<ChinaCountStatisticsDxyPO>() {
            @Override
            public Predicate toPredicate(Root<ChinaCountStatisticsDxyPO> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                List<Predicate> list = new ArrayList<Predicate>();
                Predicate p1 =builder.equal(root.get("province"),areaName);
                Predicate p2 =builder.equal(root.get("areaType"),"city");
                list.add(builder.and(p1, p2));
                Predicate[] p = new Predicate[list.size()];
                return builder.and(list.toArray(p));
            }
        }, sort);
        List<ChinaCountStatisticsDTO> content = new ArrayList<ChinaCountStatisticsDTO>();
        Iterator<ChinaCountStatisticsDxyPO> it = list.iterator();
        SimpleDateFormat f = new SimpleDateFormat("MM月dd日");
        while (it.hasNext()) {
            ChinaCountStatisticsDxyPO po = it.next();
            ChinaCountStatisticsDTO dto = new ChinaCountStatisticsDTO();
            BeanUtils.copyProperties(po, dto);
            Date updateTime = po.getUpdateTime();
            String dateTime = f.format(updateTime);
            dto.setCountTime(dateTime);
            content.add(dto);
        }
        return content;
    }

    @Override
    public List<AreaHotDTO> getAreaPatientCountListByParent(String areaParent) {
        List<AreaHotDTO> resultList = new ArrayList<>();

        List<ChinaCountStatisticsDxyPO> poList = null;
        if ("0".equals(areaParent)) {//全国
            poList = chinaCountStatisticsDxyDao.findByAreaType("province");
        } else if (areaParent.endsWith("0000")) {//省级和直辖市级
            Optional<LocationDimPO> byId = locationDimDao.findById(areaParent);
            poList = chinaCountStatisticsDxyDao.findByAreaTypeAndProvinceLike("city", byId.get().getAreaName().substring(0, 2));
        } else {//市级
            Optional<LocationDimPO> byId = locationDimDao.findById(areaParent);
            poList = chinaCountStatisticsDxyDao.findByAreaTypeAndCityLike("county", byId.get().getAreaName().substring(0, 2));
        }

        Sort sort = Sort.by(Sort.Direction.ASC, "seqNum");
        List<LocationDimPO> locationList = locationDimDao.findByParentNo(areaParent, sort);

        AreaHotDTO dto = null;
        for (ChinaCountStatisticsDxyPO po : poList) {
            dto = new AreaHotDTO();

            //通过区域名称查询区域编码
            for (LocationDimPO lpo : locationList) {
                if (lpo.getAreaName().contains(po.getAreaName().substring(0, 2))) {
                    dto.setArea_id(lpo.getAreaNo());
                    break;
                }
            }

            if (StringUtils.isEmpty(dto.getArea_id())) {
                dto.setArea_id(po.getAreaName());
            }

            Integer confirmCase = po.getConfirmCase();

            dto.setValue((confirmCase == null || confirmCase == 0) ? null : confirmCase);
            resultList.add(dto);
        }

        return resultList;
    }
    @Override
    public List<ChinaCountStatisticsBubbling> getParentCountStatisticsBubbling(String province) {
        List<ChinaCountStatisticsBubbling> chinaCountStatisticsBubblings=new ArrayList<>();
        List<ChinaCountStatisticsDTO> chinaCountStatistics= findProvinceCityCount(province);
        chinaCountStatistics.stream().forEach(count -> {
            ChinaCountStatisticsBubbling bubbling=new ChinaCountStatisticsBubbling();
            String areaName=count.getAreaName();
            List<LocationDimPO> list=locationDimDao.getLocationDimByCityName(areaName);
            if(!CollectionUtils.isEmpty(list)&&list.size()>0){
                LocationDimPO locationDim=list.get(0);
                bubbling.setLat(locationDim.getLatitude());
                bubbling.setLng(locationDim.getLongitude());
                String info="确诊:"+count.getConfirmCase()+",治愈:"+count.getCureCase()+",死亡:"+count.getDeadCase();
                bubbling.setInfo(info);
                bubbling.setName(locationDim.getAreaName());
                bubbling.setConfirmCase(count.getConfirmCase());
                bubbling.setCureCase(count.getCureCase());
                bubbling.setDeadCase(count.getDeadCase());
                chinaCountStatisticsBubblings.add(bubbling);
            }

        });
        return chinaCountStatisticsBubblings;
    }
    @Override
    public List<ChinaCountStatisticsDTO> findProvinceCityCount(String province){
        return getCityCountByProvince(province);
    }


}
