/**
 * 
 */
package com.gsafety.gemp.wuhanncov.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.gsafety.gemp.wuhanncov.contract.dto.BasicReportStaticsDTO;
import com.gsafety.gemp.wuhanncov.dao.BasicReportStatisticDayDao;
import com.gsafety.gemp.wuhanncov.dao.po.BasicReportStatisticDayPO;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaAreaCaseInfoPO;
import com.gsafety.gemp.wuhanncov.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.gsafety.gemp.wuhanncov.contract.dto.BasicReportDTO;
import com.gsafety.gemp.wuhanncov.contract.service.BasicReportStatisticDayService;

/**
 * 该类用于...，用于实现...等能力。
 *
 * @author yangbo
 *
 * @since 2020年2月7日 下午11:34:44
 *
 */
@Service
public class BasicReportStatisticDayServiceImpl implements BasicReportStatisticDayService {
	@Autowired
	BasicReportStatisticDayDao basicReportStatisticDayDao;
	@Override
	public List<BasicReportStaticsDTO> findReportHistroySum(int topSize) {
		List<BasicReportStaticsDTO> dtos=new ArrayList<BasicReportStaticsDTO>();
		Sort sort = Sort.by(Sort.Direction.DESC, "updateTime");
		Pageable pageable = PageRequest.of(0, topSize, sort);
		List<BasicReportStatisticDayPO> poList=basicReportStatisticDayDao.findAll(pageable).getContent();
		for(BasicReportStatisticDayPO po:poList){
			String reportdate=po.getReportDate();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			try {
				Date date = sdf.parse(reportdate);
				SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd");
				reportdate=df.format(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			BasicReportStaticsDTO dto1=new BasicReportStaticsDTO();
			dto1.setDate(reportdate);
			dto1.setValue(po.getReportNum());
			dto1.setType("1");

			BasicReportStaticsDTO dto2=new BasicReportStaticsDTO();
			dto2.setDate(reportdate);
			dto2.setValue(po.getHotNum());
			dto2.setType("2");

			BasicReportStaticsDTO dto3=new BasicReportStaticsDTO();
			dto3.setDate(reportdate);
			dto3.setValue(po.getCommunityNum());
			dto3.setType("3");

			dtos.add(dto1);
			dtos.add(dto2);
			dtos.add(dto3);
		}
		return dtos;
	}

}
