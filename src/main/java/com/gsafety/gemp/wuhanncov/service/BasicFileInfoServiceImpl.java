package com.gsafety.gemp.wuhanncov.service;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.ObjectMetadata;
import com.gsafety.gemp.wuhanncov.constant.AppConstant;
import com.gsafety.gemp.wuhanncov.contract.dto.BasicFileInfoParam;
import com.gsafety.gemp.wuhanncov.contract.dto.OSSAllFileDto;
import com.gsafety.gemp.wuhanncov.contract.dto.OSSFileDto;
import com.gsafety.gemp.wuhanncov.contract.dto.Result;
import com.gsafety.gemp.wuhanncov.contract.enums.FileTypeEnum;
import com.gsafety.gemp.wuhanncov.contract.params.BasicFileQueryParam;
import com.gsafety.gemp.wuhanncov.contract.service.BasicFileInfoService;
import com.gsafety.gemp.wuhanncov.dao.BasicFileInfoDao;
import com.gsafety.gemp.wuhanncov.dao.LocationDimDao;
import com.gsafety.gemp.wuhanncov.dao.po.BasicFileInfoPO;
import com.gsafety.gemp.wuhanncov.dao.po.LocationDimPO;
import com.gsafety.gemp.wuhanncov.specification.BasicFileInfoSpecifications;
import com.gsafety.gemp.wuhanncov.wrapper.BasicFileInfoWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.gsafety.gemp.wuhanncov.specification.BasicFileInfoSpecifications.*;

/**
 * @author zhoushuyan
 * @date 2020-02-11 10:51
 * @Description
 */
@Service
@Slf4j
@EnableAsync
public class BasicFileInfoServiceImpl implements BasicFileInfoService {
    @Value("${alioos.endpoint}")
    private String endpoint;

    @Value("${alioos.AccessKeyId}")
    private String accessKeyId;

    @Value("${alioos.AccessKeySecret}")
    private String accessKeySecret;

    @Value("${alioos.BucketName}")
    private String bucketName;

    @Value("${alioos.ReportObjectName}")
    private String reportObjectName;

    @Resource
    BasicFileInfoDao basicFileInfoDao;
    @Resource
    LocationDimDao locationDimDao;
    @Override
    public List<OSSFileDto> getNcovOSSFile(String fileType) {
        List<OSSFileDto> dtos=new ArrayList<OSSFileDto>();
        List<BasicFileInfoPO> pos=basicFileInfoDao.findAllByStatusAndFileTypeOrderByFileDateDesc("5",fileType);
        for(BasicFileInfoPO po:pos){
            OSSFileDto dto=new OSSFileDto();
            BeanUtils.copyProperties(po,dto);
            dtos.add(dto);
        }
        return dtos;
    }

    @Override
    public Result updateNcovOSSFile(BasicFileInfoParam fileInfo) {
        try {
            String fileId = fileInfo.getFileId();
            if (StringUtils.isEmpty(fileId)) {
                BasicFileInfoPO po = new BasicFileInfoPO();
                BeanUtils.copyProperties(fileInfo,po);
                po.setFileId(String.valueOf(System.currentTimeMillis()));
                if (po.getFileDate()==null) {
                    po.setFileDate(DateTime.now().toDate());
                }
                if (po.getCreateDate()==null) {
                    po.setCreateDate(DateTime.now().toDate());
                }
                if (po.getUpdateDate()==null) {
                    po.setUpdateDate(DateTime.now().toDate());
                }
                if (StringUtils.isEmpty(po.getStatus())) {
                    po.setStatus("5");
                }
                if (StringUtils.isEmpty(po.getFileType())) {
                    po.setFileType("0");
                }
                if (StringUtils.isNotEmpty(po.getAreaCode())) {
                    LocationDimPO locationDimPO=locationDimDao.findByAreaNo(po.getAreaCode());
                    if(locationDimPO!=null){
                        po.setAreaCode(locationDimPO.getAreaNo());
                        po.setAreaName(locationDimPO.getAreaName());
                    }
                }
                basicFileInfoDao.save(po);
            } else {
                BasicFileInfoPO po = basicFileInfoDao.getOne(fileId);
                if (po != null) {
                    if (StringUtils.isNotEmpty(fileInfo.getFileName())) {
                        po.setFileName(fileInfo.getFileName());
                    }
                    if (StringUtils.isNotEmpty(fileInfo.getUrl())) {
                        po.setUrl(fileInfo.getUrl());
                    }
                    if (StringUtils.isNotEmpty(fileInfo.getFileType())) {
                        po.setFileType(fileInfo.getFileType());
                    }
                    if (StringUtils.isNotEmpty(fileInfo.getStatus())) {
                        po.setStatus(fileInfo.getStatus());
                    }
                    if (StringUtils.isNotEmpty(fileInfo.getAreaCode())) {
                        LocationDimPO locationDimPO=locationDimDao.findByAreaNo(fileInfo.getAreaCode());
                        if(locationDimPO!=null){
                            po.setAreaCode(locationDimPO.getAreaNo());
                            po.setAreaName(locationDimPO.getAreaName());
                        }
                    }
                    po.setUpdateDate(DateTime.now().toDate());
                    basicFileInfoDao.save(po);
                }else{
                    return new Result().success("文件信息不存在!");
                }
            }
            return new Result().success("信息变更成功!");
        } catch (Exception e) {
            log.error(e.getMessage());
            return new Result().fail("信息变更失败!");
        }
    }

    @Override
    public  String ossFileUpdate(MultipartFile upFile,String objectName,String fileName){
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
        try {
            String objName=objectName+"/"+fileName;
            InputStream inputStream = upFile.getInputStream();
            ObjectMetadata objectMetadata = new ObjectMetadata();
            ossClient.putObject(bucketName, objName, inputStream);
            //上传成功 end
            //下面是获得oos的文件地址
            // 设置URL过期时间为5年过期。
            Date expiration = new Date((System.currentTimeMillis() + 3600 * 1000)*24*365*5);
            // 生成以GET方法访问的签名URL，访客可以直接通过浏览器访问相关内容。
            URL url = ossClient.generatePresignedUrl(bucketName, objName, expiration);
            return url.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            ossClient.shutdown();
        }
        return null;
    }

    @Override
    public List<OSSFileDto> findAreaFile(List<String> areaIdList, String fileType) {
        List<OSSFileDto> dtos=new ArrayList<OSSFileDto>();
        List<BasicFileInfoPO> pos=new ArrayList<BasicFileInfoPO>();
        if(areaIdList!=null){
            Sort sort = Sort.by(Sort.Direction.DESC, "fileDate");
            pos = basicFileInfoDao.findAll(new Specification<BasicFileInfoPO>() {
                @Override
                public Predicate toPredicate(Root<BasicFileInfoPO> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
                    List<Predicate> list = new ArrayList<Predicate>();
                    Predicate p1 =builder.equal(root.get("status"),"5");
                    Predicate p2 =builder.equal(root.get("fileType"),fileType);
                    CriteriaBuilder.In<String> in = builder.in(root.get("areaCode"));
                    for (String areaCode : areaIdList) {
                        in.value(areaCode);
                    }
                    list.add(builder.and(p1, p2,in));
                    Predicate[] p = new Predicate[list.size()];
                    return builder.and(list.toArray(p));
                }
            }, sort);
        }else{
            pos=basicFileInfoDao.findAllByStatusAndFileTypeOrderByFileDateDesc("5",fileType);
        }
        for(BasicFileInfoPO po:pos){
            OSSFileDto dto=new OSSFileDto();
            BeanUtils.copyProperties(po,dto);
            dtos.add(dto);
        }
        return dtos;
    }

    @Override
    public Page<OSSAllFileDto> findAllFileInfo(BasicFileQueryParam basicFileQueryParam) {
        Sort sort = Sort.by(Sort.Direction.DESC, "fileDate");
        Pageable pageable = PageRequest.of( null == basicFileQueryParam.getCurrentPage() ? AppConstant.DEFAULT_CURRENT_PAGE : basicFileQueryParam.getCurrentPage() - 1,
                null == basicFileQueryParam.getPageSize() ? AppConstant.DEAULT_PAGE_SIZE : basicFileQueryParam.getPageSize(),
                sort);
        Page<BasicFileInfoPO> page = basicFileInfoDao.findAll( Specification.where(areaNameLike(basicFileQueryParam.getAreaName()))
                .or(fileNameLike(basicFileQueryParam.getAreaName())).and(statusEqual(AppConstant.NORMAL_FILE_STATUS)), pageable);
        return BasicFileInfoWrapper.po2DTOPage(page);
    }
}
