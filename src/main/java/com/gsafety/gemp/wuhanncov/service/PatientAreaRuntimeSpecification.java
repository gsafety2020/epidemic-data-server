/**
 * 
 */
package com.gsafety.gemp.wuhanncov.service;

import org.springframework.data.jpa.domain.Specification;

import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaRuntimePO;

/**
 * 该类用于...，用于实现...等能力。
 *
 * @author yangbo
 *
 * @since 2020年1月30日 下午9:17:24
 *
 */
public class PatientAreaRuntimeSpecification {

	/**
	 * 
	 */
	public static final String FIELD_PARENTNO = "parentNo";
	
	/**
	 * 
	 */
	public static final String FIELD_AREACODE = "areaCode";
	
	/**
	 * 
	 */
	public static final String FIELD_CONFIRMCASE = "confirmCase";
	
	
	/**
	 * 
	 */
	private PatientAreaRuntimeSpecification() {
		super();
	}

	/**
	 * 
	 * areaCodeEqual
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月30日 下午9:21:16
	 *
	 * @param areaCode
	 * @return
	 */
	public static Specification<PatientAreaRuntimePO> areaCodeEqual(String areaCode) {
		return (root, query, builer) -> builer.equal(root.get(FIELD_AREACODE), areaCode);
	}

	/**
	 * 
	 * parentNo
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月30日 下午9:21:19
	 *
	 * @param parentNo
	 * @return
	 */
	public static Specification<PatientAreaRuntimePO> parentNo(String parentNo) {
		return (root, query, builer) -> builer.equal(root.get(FIELD_PARENTNO), parentNo);
	}

}
