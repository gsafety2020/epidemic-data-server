package com.gsafety.gemp.wuhanncov.service;

import com.gsafety.gemp.wuhanncov.constant.DataSyncLogEnum;
import com.gsafety.gemp.wuhanncov.contract.dto.DataSyncLogDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaRuntimeDTO;
import com.gsafety.gemp.wuhanncov.contract.params.DataSyncLogParam;
import com.gsafety.gemp.wuhanncov.contract.service.DataSyncLogService;
import com.gsafety.gemp.wuhanncov.dao.DataSyncLogDao;
import com.gsafety.gemp.wuhanncov.dao.po.DataSyncLogPO;
import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaRuntimePO;
import com.gsafety.gemp.wuhanncov.specification.DataSyncLogSpecifications;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service("dataSyncLogService")
public class DataSyncLogServiceImpl implements DataSyncLogService {

    @Resource
    DataSyncLogDao dataSyncLogDao;


    @Override
    public Page<DataSyncLogDTO> findDataSyncLogPage(DataSyncLogParam dataSyncLogParam, Pageable pageable) {
        Page<DataSyncLogPO> page = dataSyncLogDao.findAll(DataSyncLogSpecifications.timeBetween(dataSyncLogParam.getStartTime(),
                dataSyncLogParam.getEndTime()), pageable);
        return po2DTOPage(page);
    }

    private Page<DataSyncLogDTO> po2DTOPage(Page<DataSyncLogPO> page){
        List<DataSyncLogDTO> content = po2DTOList(page.getContent());
        return new PageImpl<>(content, page.getPageable(), page.getTotalElements());
    }

    private List<DataSyncLogDTO> po2DTOList(List<DataSyncLogPO> list){
        List<DataSyncLogDTO> content = new ArrayList<>();
        Iterator<DataSyncLogPO> it = list.iterator();
        while (it.hasNext()) {
            DataSyncLogPO po = it.next();
            content.add(po2DTO(po));
        }

        return content;
    }

    private DataSyncLogDTO po2DTO(DataSyncLogPO po){
        DataSyncLogDTO dto = new DataSyncLogDTO();
        BeanUtils.copyProperties(po, dto);
        dto.setStatusName(DataSyncLogEnum.getNameByCode(po.getStatus()));
        return dto;
    }
}
