package com.gsafety.gemp.wuhanncov.service;

import cn.hutool.core.bean.BeanUtil;
import com.gsafety.gemp.wuhanncov.contract.dto.ChinaCountStatisticsDTO;
import com.gsafety.gemp.wuhanncov.contract.params.ChinaCountStatisticsParam;
import com.gsafety.gemp.wuhanncov.contract.service.ChinaCountStatisticsService;
import com.gsafety.gemp.wuhanncov.dao.ChinaCountStatisticsDao;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaCountStatisticsPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;

import static com.gsafety.gemp.wuhanncov.specification.ChinaCountStatisticsSpecifications.areaNameEqual;
import static com.gsafety.gemp.wuhanncov.wrapper.ChinaCountStatisticsWrapper.toPageDTO;

@Service("chinaCountStatisticsServiceImpl")
public class ChinaCountStatisticsServiceImpl implements ChinaCountStatisticsService {

    @Autowired
    private ChinaCountStatisticsDao chinaCountStatisticsDao;


    public boolean update(ChinaCountStatisticsDTO dto) {
        ChinaCountStatisticsPO entity = new ChinaCountStatisticsPO();
        BeanUtil.copyProperties(dto, entity);
        entity.setCity("佛山");
        entity.setUpdateTime(new Date());
        entity.setFromSource("manual");
        chinaCountStatisticsDao.saveAndFlush(entity);
        return true;
    }

    @Override
    public Page<ChinaCountStatisticsDTO> findChinaCountStatPage(ChinaCountStatisticsParam queryParam, Pageable pageable){
        Page<ChinaCountStatisticsPO> page = chinaCountStatisticsDao.findAll(areaNameEqual(queryParam.getAreaName()),pageable);
        return toPageDTO(page);
    }

}
