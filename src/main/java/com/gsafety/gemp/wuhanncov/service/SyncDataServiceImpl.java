package com.gsafety.gemp.wuhanncov.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gsafety.gemp.wuhanncov.contract.service.SyncDataService;
import com.gsafety.gemp.wuhanncov.dao.DataSyncLogDao;
import com.gsafety.gemp.wuhanncov.dao.FilesManageDao;
import com.gsafety.gemp.wuhanncov.dao.PatientAreaStatisticDao;
import com.gsafety.gemp.wuhanncov.dao.po.DataSyncLogPO;
import com.gsafety.gemp.wuhanncov.dao.po.FilesManagePO;
import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaStatisticPO;
import com.gsafety.gemp.wuhanncov.utils.CsvUtils;
import com.gsafety.gemp.wuhanncov.utils.DateUtils;
import com.gsafety.gemp.wuhanncov.utils.UUIDUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * 数据同步加工服务实现类
 *
 * @author liyanhong
 * @since 2020/2/12  14:28
 */
@Service("syncDataService")
public class SyncDataServiceImpl implements SyncDataService {

    private static final Logger logger = LoggerFactory.getLogger(SyncDataServiceImpl.class);

    @Resource
    private FilesManageDao filesManageDao;

    @Resource
    private PatientAreaStatisticDao patientAreaStatisticDao;

    @Resource
    private DataSyncLogDao dataSyncLogDao;

    @Override
    public Integer handleAreaDailyCsvfile(String fileId) {

        Optional<FilesManagePO> fileOpt = filesManageDao.findById(fileId);

        if (!fileOpt.isPresent()) {
            return 0;
        }

        FilesManagePO filePo = fileOpt.get();
        String areaCode = filePo.getAreaCode();

        //插入数据同步日志
        DataSyncLogPO logPO = new DataSyncLogPO();
        logPO.setId(UUIDUtils.getUUID());
        logPO.setSrcFileId(fileId);
        logPO.setTargetTable("patient_area_statistic");
        logPO.setStartTime(new Date());
        logPO.setCreateTime(new Date());
        dataSyncLogDao.saveAndFlush(logPO);

        Integer totalRow = 0;
        Integer successRow = 0;
        try {
            URL url = new URL(filePo.getPath());
            InputStream in = url.openStream();

            JSONArray jsonArray = CsvUtils.ReadCsv(in);

            SimpleDateFormat db_format = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat scv_format = new SimpleDateFormat("yyyy年MM月dd日");
            Date now = new Date();
            List<PatientAreaStatisticPO> poList = patientAreaStatisticDao.findByAreaCodeOrderByStasTimeDesc(filePo.getAreaCode());
            Map<String, PatientAreaStatisticPO> poMap = poList.stream().collect(Collectors.toMap(PatientAreaStatisticPO::getStasDate, p -> p));
            PatientAreaStatisticPO po = null;
            for (int i = 1; i < jsonArray.size(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);

                try {
                    Date rowDate = scv_format.parse("2020年" + jsonObject.getString("0"));

                    //日期超过当日的，直接跳过
                    if (now.before(rowDate)) {
                        continue;
                    }

                    totalRow++;

                    String dateStr = db_format.format(rowDate);
                    po = poMap.get(dateStr);

                    //如果日报记录不在数据库，先创建
                    if (po == null) {
                        po = new PatientAreaStatisticPO();
                        po.setId(dateStr + areaCode);
                        po.setAreaCode(areaCode);
                        po.setArea(filePo.getAreaName());
                        po.setStasDate(dateStr);
                        po.setStasHour("235959");
                        po.setStasTime(DateUtils.parse(dateStr + "235959", "yyyyMMddHHmmss"));
                        po.setPatientNumber(0);
                        po.setCreateTime(now);
                    }

                    //累计确证病例
                    if (jsonObject.getInteger("1") != null) {
                        po.setPatientNumber(jsonObject.getInteger("1"));
                    }

                    //现有疑似病例
                    if (jsonObject.getInteger("2") != null) {
                        po.setDebtNumber(jsonObject.getInteger("2"));
                    }

                    // 死亡病例（累计）
                    if (jsonObject.getInteger("3") != null) {
                        po.setDeathNumber(jsonObject.getInteger("3"));
                    }

                    // 治愈病例（累计）
                    if (jsonObject.getInteger("4") != null) {
                        po.setCuredNumber(jsonObject.getInteger("4"));
                    }

                    // 现有重症病例
                    if (jsonObject.getInteger("5") != null) {
                        po.setSevereNumber(jsonObject.getInteger("5"));
                    }

                    // 现有病危病例
                    if (jsonObject.getInteger("6") != null) {
                        po.setDyingNumber(jsonObject.getInteger("6"));
                    }

                    // 累计追踪密切接触者
                    if (jsonObject.getInteger("7") != null) {
                        po.setCloseContactsNumber(jsonObject.getInteger("7"));
                    }

                    // 目前接受医学观察者
                    if (jsonObject.getInteger("8") != null) {
                        po.setObservedNumber(jsonObject.getInteger("8"));
                    }

                    // 解除医学观察者
                    if (jsonObject.getInteger("9") != null) {
                        po.setReleaseObservedNumber(jsonObject.getInteger("9"));
                    }

                    po.setUpdateTime(now);
                    patientAreaStatisticDao.saveAndFlush(po);

                    successRow ++ ;
                } catch (ParseException er) {
                    logger.error("数据格式错误！", er);
                }
            }
        } catch (MalformedURLException e1) {
            logger.error("远程文件不存在！", e1);
        } catch (IOException e2) {
            logger.error("远程文件输入流获取失败！", e2);
        }

        //更新数据同步日志
        logPO.setTotalRecord(totalRow);
        logPO.setSuccessRecord(successRow);
        logPO.setStatus(successRow > 0 ? (successRow < totalRow ? 2 : 1) : 3);
        logPO.setEndTime(new Date());
        logPO.setUpdateTime(new Date());
        dataSyncLogDao.saveAndFlush(logPO);

        return 1;
    }
}
