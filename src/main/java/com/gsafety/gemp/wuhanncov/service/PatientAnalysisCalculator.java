package com.gsafety.gemp.wuhanncov.service;

import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaStatisticPO;

import java.math.BigDecimal;

public class PatientAnalysisCalculator {




    public PatientAnalysisCalculator(PatientAreaStatisticPO current,
                                      PatientAreaStatisticPO before,
                                      PatientAreaStatisticPO parent,
                                      PatientAreaStatisticPO parentBefore,
                                      PatientAreaStatisticPO all,
                                      PatientAreaStatisticPO beforeAll){
        this.current = current;
        this.before = before;
        this.parent = parent;
        this.parentBefore = parentBefore;
        this.all = all;
        this.beforeAll = beforeAll;
    }

    public boolean isEnabled(){
        return !(current == null || before == null || parent == null || parentBefore == null || all == null || beforeAll == null);
    }


    private PatientAreaStatisticPO current;

    private PatientAreaStatisticPO before;

    private PatientAreaStatisticPO parent;

    private PatientAreaStatisticPO parentBefore;

    private PatientAreaStatisticPO all;

    private PatientAreaStatisticPO beforeAll;

    private static int roundingMode = BigDecimal.ROUND_HALF_UP;


    /**
     * 当前城市累计
     */
    private BigDecimal total(){
        return new BigDecimal(current.getPatientNumber());
    }

    /**
     * 上级累计
     */
    private BigDecimal parentTotal(){
        return parent == null && parent.getPatientNumber() == null? BigDecimal.ZERO :new BigDecimal(parent.getPatientNumber());
    }

    private BigDecimal totalBefore(){
        return parent == null && before.getPatientNumber() == null ? BigDecimal.ZERO : new BigDecimal(before.getPatientNumber());
    }
    
    private BigDecimal parentTotalBefore(){
        return parentBefore == null && parentBefore.getPatientNumber() == null ? BigDecimal.ZERO : new BigDecimal(parentBefore.getPatientNumber());
    }

    private BigDecimal totalAllBefore(){
        return beforeAll == null && beforeAll.getPatientNumber() == null ? BigDecimal.ZERO : new BigDecimal(beforeAll.getPatientNumber());
    }

    private BigDecimal totalDeath(){
        return current == null && current.getDeathNumber() == null ? BigDecimal.ZERO : new BigDecimal(current.getDeathNumber());
    }

    private BigDecimal parentTotalDeath(){
        return parent == null && parent.getDeathNumber() == null ? BigDecimal.ZERO : new BigDecimal(parent.getDeathNumber());
    }


    /**
     * 全国累计
     */
    private BigDecimal allTotal(){
        return all == null ? BigDecimal.ZERO : new BigDecimal(all.getPatientNumber());
    }



    private BigDecimal newCase() {
        return total().subtract(totalBefore());
    }

    private BigDecimal parentNewCase() {
        return parentTotal().subtract(parentTotalBefore());
    }

    private BigDecimal allNewCase() {
        return allTotal().subtract(totalAllBefore());
    }

    private BigDecimal otherCase(){
        return parentTotal().subtract(total());
    }

    private BigDecimal otherNewCase(){
        return parentNewCase().subtract(newCase());
    }

    private BigDecimal otherBeforeCase(){
        return parentTotalBefore().subtract(totalBefore());
    }

    private BigDecimal otherDeath(){
        return parentTotalDeath().subtract(totalDeath());
    }



    public BigDecimal newCaseRate() {
        return isEmpty(parentNewCase())? null : newCase().divide(parentNewCase(), 4, roundingMode);
    }

    public BigDecimal newCaseAllRate() {
        return isEmpty(allNewCase())? BigDecimal.ZERO : newCase().divide(allNewCase(), 4, roundingMode);
    }

    public BigDecimal totalCaseRate() {
        return isEmpty(parentTotal())? BigDecimal.ZERO : total().divide(parentTotal(), 4, roundingMode);
    }

    public BigDecimal totalCaseAllRate(){
        return isEmpty(allTotal())? BigDecimal.ZERO : total().divide(allTotal(),4,roundingMode);
    }

    public BigDecimal growthRate(){
        return isEmpty(totalBefore())? BigDecimal.ZERO : total().divide(totalBefore(),4,roundingMode).subtract(BigDecimal.ONE);
    }

    public BigDecimal otherGrowthRate(){
        return isEmpty(otherBeforeCase())? BigDecimal.ZERO : otherCase().divide(otherBeforeCase(),4 ,roundingMode).subtract(BigDecimal.ONE);
    }

    public BigDecimal newTotalRate(){
        return isEmpty(total())? BigDecimal.ZERO : newCase().divide(total(),4,roundingMode);
    }

    public BigDecimal otherNewTotalRate(){
        return isEmpty(otherCase())? BigDecimal.ZERO : otherNewCase().divide(otherCase(),4,roundingMode);
    }

    public BigDecimal deathRate(){
        return isEmpty(total())? BigDecimal.ZERO : totalDeath().divide(total(),4,roundingMode);
    }

    public BigDecimal otherDeathRate(){
        return isEmpty(otherCase())? BigDecimal.ZERO : otherDeath().divide(otherCase(),4,roundingMode);
    }

    private boolean isEmpty(BigDecimal num){
        return  BigDecimal.ZERO.equals(num) || num == null;
    }

}
