package com.gsafety.gemp.wuhanncov.service;

import com.gsafety.gemp.wuhanncov.contract.dto.BasicReportInfoDTO;
import com.gsafety.gemp.wuhanncov.contract.service.BasicReportInfoService;
import com.gsafety.gemp.wuhanncov.dao.BasicReportDao;
import com.gsafety.gemp.wuhanncov.dao.BasicReportInfoDao;
import com.gsafety.gemp.wuhanncov.dao.po.BasicReportInfoPO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhoushuyan
 * @date 2020-02-09 11:14
 * @Description
 */
@Service
@Slf4j
@EnableAsync
public class BasicReportInfoServiceImpl implements BasicReportInfoService {
    @Resource
    private BasicReportInfoDao basicReportInfoDao;

    @Override
    public List<BasicReportInfoDTO> getReportInfos(String channel, int topSize) {
        List<BasicReportInfoDTO> dtos=new ArrayList<BasicReportInfoDTO>();
        Sort sort = Sort.by(Sort.Direction.DESC, "reportTime");
        List<BasicReportInfoPO> pos=new ArrayList<BasicReportInfoPO>();
        if(topSize==-1){
            pos=basicReportInfoDao.findAll(new Specification<BasicReportInfoPO>(){
                @Override
                public Predicate toPredicate(Root<BasicReportInfoPO> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder builder) {
                    return builder.equal(root.get("channel"),channel);
                }
            },sort);
        }else{
            Pageable p = PageRequest.of(0, topSize, sort);
            pos=basicReportInfoDao.findAll(new Specification<BasicReportInfoPO>(){
                @Override
                public Predicate toPredicate(Root<BasicReportInfoPO> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder builder) {
                    return builder.equal(root.get("channel"),channel);
                }
            },p).getContent();
        }
        for(BasicReportInfoPO po:pos){
            BasicReportInfoDTO dto=new BasicReportInfoDTO();
            BeanUtils.copyProperties(po,dto);
            dtos.add(dto);
        }
        return dtos;
    }
}
