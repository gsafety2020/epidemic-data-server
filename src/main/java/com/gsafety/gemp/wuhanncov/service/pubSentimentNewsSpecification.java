/**
 * 
 */
package com.gsafety.gemp.wuhanncov.service;

import java.util.Date;

import org.springframework.data.jpa.domain.Specification;

import com.gsafety.gemp.wuhanncov.dao.po.PubSentimentNewsPO;

/**
 * 该类用于...，用于实现...等能力。
 *
 * @author yangbo
 *
 * @since 2020年1月25日 下午8:30:22
 *
 */
public class pubSentimentNewsSpecification {

	/**
	 * 
	 */
	private pubSentimentNewsSpecification() {
		super();
	}

	/**
	 * 
	 * newsTimeBetween
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月25日 下午8:35:14
	 *
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static Specification<PubSentimentNewsPO> newsTimeBetween(Date startDate, Date endDate) {
		return (root, query, builer) -> builer.between(root.get("newsTime"), startDate, endDate);
	}
	
	/**
	 * 
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月25日 下午8:36:41
	 *
	 * @param regex
	 * @return
	 */
	public static Specification<PubSentimentNewsPO> newsContentLike(String regex) {
		return (root, query, builer) -> builer.like(root.get("newsContent"), "%" +regex+"%");
	}
}
