package com.gsafety.gemp.wuhanncov.service;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.ObjectUtil;
import com.gsafety.gemp.wuhanncov.contract.dto.ChinaHashDataDTO;
import com.gsafety.gemp.wuhanncov.contract.params.DimQueryParam;
import com.gsafety.gemp.wuhanncov.contract.service.ChinaHashDataService;
import com.gsafety.gemp.wuhanncov.dao.ChinaHashDataRepository;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaHashDataPO;
import com.gsafety.gemp.wuhanncov.wrapper.ChinaHashDataWrapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

import static com.gsafety.gemp.wuhanncov.specification.ChinaHashDataSpecifications.areaCodeIs;

/**
 * 业务服务器层实现类。
 *
 * @author yangbo
 *
 * @since 2020年2月8日 下午10:16:05
 *
 */
@Service
public class ChinaHashDataServiceImpl implements ChinaHashDataService {

	@Resource
	private ChinaHashDataRepository chinaHashDataRepository;

	@Override
	public List<ChinaHashDataDTO> findByAreaCode(String areaCode) {
		return ChinaHashDataWrapper.toDTOList(this.chinaHashDataRepository.findAll(areaCodeIs(areaCode)));
	}

	@Override
	public Map<String, Object> findMapByAreaCode(String areaCode) {
		return ChinaHashDataWrapper.toMap(findByAreaCode(areaCode));
	}

    @Override
    public Page<ChinaHashDataDTO> hashDataPage(DimQueryParam dimQuery, Pageable pageable) {
		Page<ChinaHashDataPO> hashDataPage = chinaHashDataRepository.findAll(areaCodeIs(dimQuery.getAreaNo()), pageable);
		return ChinaHashDataWrapper.toDTOPage(hashDataPage);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateHashData(ChinaHashDataDTO dto) {
		ChinaHashDataPO po = chinaHashDataRepository.getOne(dto.getId());
		if (ObjectUtil.isNotNull(po)) {
			po.setDataLabel(dto.getDataLabel());
			po.setDataValue(dto.getDataValue());
//			po.setUpdateTime(DateTime.now().toJdkDate());
			po.setUpdateTime(dto.getUpdateTime());
		}
	}

}
