package com.gsafety.gemp.wuhanncov.wrapper;

import com.gsafety.gemp.wuhanncov.constant.AreaAnnouncementEnum;
import com.gsafety.gemp.wuhanncov.contract.dto.AreaStatDailyAnnouncementDTO;
import com.gsafety.gemp.wuhanncov.dao.po.AreaStatDailyAnnouncementPO;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 该类用于将返回结果转成DTO。
 *
 * @author wuhongling
 *
 * @since 2020年2月14日 下午5:50:00
 *
 */
public class AreaStatDailyAnnouncementWrapper {
    /**
     *
     */
    private AreaStatDailyAnnouncementWrapper() {
        super();
    }

    /**
     * 分页po转dto
     * @param page
     * @return
     */
    public static Page<AreaStatDailyAnnouncementDTO> toPageDTO(Page<AreaStatDailyAnnouncementPO> page){
        List<AreaStatDailyAnnouncementDTO> content = toDTOList(page.getContent());
        return new PageImpl<>(content, page.getPageable(), page.getTotalElements());
    }

    /**
     * 集合po转dto
     * @param list
     * @return
     */
    public static List<AreaStatDailyAnnouncementDTO> toDTOList(List<AreaStatDailyAnnouncementPO> list){
        List<AreaStatDailyAnnouncementDTO> content = new ArrayList<>();
        Iterator<AreaStatDailyAnnouncementPO> it = list.iterator();
        while (it.hasNext()) {
            AreaStatDailyAnnouncementPO po = it.next();
            content.add(po2DTO(po));
        }

        return content;
    }

    /**
     * 单个po转dto
     * @param po
     * @return
     */
    public static AreaStatDailyAnnouncementDTO po2DTO(AreaStatDailyAnnouncementPO po){
        AreaStatDailyAnnouncementDTO dto = new AreaStatDailyAnnouncementDTO();
        BeanUtils.copyProperties(po, dto);
        dto.setAnnouncementTypeName(AreaAnnouncementEnum.getNameByCode(Integer.valueOf(po.getAnnouncementType())));
        dto.setAreaName(po.getDim().getAreaName());

        return dto;
    }

}
