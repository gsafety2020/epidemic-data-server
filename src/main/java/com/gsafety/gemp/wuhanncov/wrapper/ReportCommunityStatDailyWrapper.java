package com.gsafety.gemp.wuhanncov.wrapper;

import com.gsafety.gemp.wuhanncov.contract.dto.ReportCommunityStatDailyDTO;
import com.gsafety.gemp.wuhanncov.dao.po.ReportCommunityStatDailyPO;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dusiwei
 */
public class ReportCommunityStatDailyWrapper {

    private ReportCommunityStatDailyWrapper() {
        super();
    }

    /**
     *
     * @param po
     * @return
     */
    public static ReportCommunityStatDailyDTO toDTO(ReportCommunityStatDailyPO po) {
        ReportCommunityStatDailyDTO dto = new ReportCommunityStatDailyDTO();
        BeanUtils.copyProperties(po, dto);
        return dto;
    }

    /**
     *
     * @param records
     * @return
     */
    public static List<ReportCommunityStatDailyDTO> toDTOList(List<ReportCommunityStatDailyPO> records) {
        List<ReportCommunityStatDailyDTO> list = new ArrayList<>();
        records.stream().forEach(record -> list.add(toDTO(record)));
        return list;
    }

}
