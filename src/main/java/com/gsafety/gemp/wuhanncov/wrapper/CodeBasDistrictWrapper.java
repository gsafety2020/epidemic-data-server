package com.gsafety.gemp.wuhanncov.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.gsafety.gemp.wuhanncov.contract.dto.CodeBasDistrictDTO;
import com.gsafety.gemp.wuhanncov.dao.po.CodeBasDistrictPO;

/**
 * 
* @ClassName: CodeBasDistrictWrapper 
* @Description: 地区编码表 po 与 dto 互转
* @author luoxiao
* @date 2020年2月21日 下午5:12:59 
*
 */
public class CodeBasDistrictWrapper {

	public static CodeBasDistrictDTO poToDto(CodeBasDistrictPO po){
		CodeBasDistrictDTO dto = new CodeBasDistrictDTO();
		BeanUtils.copyProperties(po, dto);
		return dto;
	}
	
	public static List<CodeBasDistrictDTO> listPoToDto(List<CodeBasDistrictPO> pos){
		List<CodeBasDistrictDTO> dtos = new ArrayList<CodeBasDistrictDTO>();
		for(CodeBasDistrictPO po : pos){
			CodeBasDistrictDTO dto = poToDto(po);
			dtos.add(dto);
		}
		return dtos;
	}
}
