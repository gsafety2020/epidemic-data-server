/**
 * 
 */
package com.gsafety.gemp.wuhanncov.wrapper;

import com.gsafety.gemp.wuhanncov.contract.dto.ChinaCountStatisticsHistoryDTO;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaCountStatisticsHistoryPO;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 该类用于将返回结果转成DTO。
 *
 * @author wangwenhai
 *
 * @since 2020年2月13日 下午4:06:51
 *
 */
public class CounthistoryWrapper {

	/**
	 *
	 */
	private CounthistoryWrapper() {
		super();
	}

	/**
	 * 
	 * 该方法用于实现单个PO转DTO
	 *
	 * @author wangwenhai
	 * 
	 * @since 2020年2月13日 下午4:08:08
	 *
	 * @param po
	 * @return
	 */
	public static ChinaCountStatisticsHistoryDTO toDTO(ChinaCountStatisticsHistoryPO po) {
		ChinaCountStatisticsHistoryDTO dto = ChinaCountStatisticsHistoryDTO.builder().build();
		BeanUtils.copyProperties(po, dto);
		return dto;
	}

	/**
	 * 
	 * 该方法用于实现list po转DTO
	 *
	 * @author wangwenhai
	 * 
	 * @since 2020年2月13日 下午4:09:53
	 *
	 * @param records
	 * @return
	 */
	public static List<ChinaCountStatisticsHistoryDTO> toDTOList(List<ChinaCountStatisticsHistoryPO> records) {
		List<ChinaCountStatisticsHistoryDTO> list = new ArrayList<>();
		records.stream().forEach(record -> list.add(toDTO(record)));
		return list;
	}

	/**
	 *
	 * 该方法用于实现page po转DTO
	 *
	 * @author wangwenhai
	 *
	 * @since 2020年2月13日 下午4:09:53
	 *
	 * @param page
	 * @return
	 */
	public static Page<ChinaCountStatisticsHistoryDTO> toPageDTO(Page<ChinaCountStatisticsHistoryPO> page){
		List<ChinaCountStatisticsHistoryDTO> content = toDTOList(page.getContent());
		return new PageImpl<>(content, page.getPageable(), page.getTotalElements());
	}
}
