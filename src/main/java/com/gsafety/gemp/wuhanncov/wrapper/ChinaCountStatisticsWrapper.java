package com.gsafety.gemp.wuhanncov.wrapper;

import com.gsafety.gemp.wuhanncov.contract.dto.ChinaCountStatisticsDTO;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaCountStatisticsPO;
import com.gsafety.gemp.wuhanncov.utils.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 该类用于将返回结果转成DTO。
 *
 * @author wuhongling
 *
 * @since 2020年2月13日 下午6:06:51
 *
 */
public class ChinaCountStatisticsWrapper {
    /**
     *
     */
    private ChinaCountStatisticsWrapper() {
        super();
    }

    /**
     * 分页po转dto
     * @param page
     * @return
     */
    public static Page<ChinaCountStatisticsDTO> toPageDTO(Page<ChinaCountStatisticsPO> page){
        List<ChinaCountStatisticsDTO> content = toDTOList(page.getContent());
        return new PageImpl<>(content, page.getPageable(), page.getTotalElements());
    }

    /**
     * 集合po转dto
     * @param list
     * @return
     */
    public static List<ChinaCountStatisticsDTO> toDTOList(List<ChinaCountStatisticsPO> list){
        List<ChinaCountStatisticsDTO> content = new ArrayList<>();
        Iterator<ChinaCountStatisticsPO> it = list.iterator();
        while (it.hasNext()) {
            ChinaCountStatisticsPO po = it.next();
            content.add(po2DTO(po));
        }

        return content;
    }

    /**
     * 单个po转dto
     * @param po
     * @return
     */
    public static ChinaCountStatisticsDTO po2DTO(ChinaCountStatisticsPO po){
        ChinaCountStatisticsDTO dto = new ChinaCountStatisticsDTO();
        BeanUtils.copyProperties(po, dto);
        if (null != po.getUpdateTime()) {
            dto.setUpdateTime(DateUtils.format(po.getUpdateTime(), DateUtils.DATE_TIME_PATTERN));
        }

        return dto;
    }
}
