package com.gsafety.gemp.wuhanncov.wrapper;

import com.gsafety.gemp.wuhanncov.contract.dto.OSSAllFileDto;
import com.gsafety.gemp.wuhanncov.contract.enums.FileTypeEnum;
import com.gsafety.gemp.wuhanncov.dao.po.BasicFileInfoPO;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * 该类用于疫情文件列表查询包装类
 *
 * @author xiongjinhui
 *
 * @since 2020年2月18日 上午11:02:45
 *
 */
public class BasicFileInfoWrapper {

    private BasicFileInfoWrapper() {
        super();
    }

    public static OSSAllFileDto po2DTO(BasicFileInfoPO po){
        OSSAllFileDto dto = new OSSAllFileDto();
        BeanUtils.copyProperties(po, dto);
        dto.setFileTypeName(FileTypeEnum.getNameByCode(dto.getFileType()));
        return dto;
    }

    public static List<OSSAllFileDto> po2DTOList(List<BasicFileInfoPO> list){
        List<OSSAllFileDto> content = new ArrayList<>();
        Iterator<BasicFileInfoPO> it = list.iterator();
        while (it.hasNext()) {
            BasicFileInfoPO po = it.next();
            content.add(po2DTO(po));
        }

        return content;
    }

    public static Page<OSSAllFileDto> po2DTOPage(Page<BasicFileInfoPO> page){
        List<OSSAllFileDto> content = po2DTOList(page.getContent());
        return new PageImpl<>(content, page.getPageable(), page.getTotalElements());
    }
}
