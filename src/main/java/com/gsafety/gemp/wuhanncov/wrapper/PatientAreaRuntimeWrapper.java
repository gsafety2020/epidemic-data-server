/**
 * 
 */
package com.gsafety.gemp.wuhanncov.wrapper;

import org.springframework.beans.BeanUtils;

import com.gsafety.gemp.wuhanncov.contract.dto.ChinaCountStatisticsJSON;
import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaRuntimePO;

/**
 * 该类用于...，用于实现...等能力。
 *
 * @author yangbo
 *
 * @since 2020年2月10日 下午6:53:09
 *
 */
public class PatientAreaRuntimeWrapper {

	/**
	 * 
	 */
	private PatientAreaRuntimeWrapper() {
		super();
	}

	/**
	 * 
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月10日 下午6:54:28
	 *
	 * @param PatientAreaRuntime
	 * @return
	 */
	public static ChinaCountStatisticsJSON toChinaCountStatisticsJSON(PatientAreaRuntimePO PatientAreaRuntime) {
		ChinaCountStatisticsJSON json = new ChinaCountStatisticsJSON();
		BeanUtils.copyProperties(PatientAreaRuntime, json);
		json.setNew_probable(PatientAreaRuntime.getNewProbable());
		json.setNew_cure(PatientAreaRuntime.getNewCure());
		json.setNew_dead(PatientAreaRuntime.getNewDead());
		return json;
	}
}
