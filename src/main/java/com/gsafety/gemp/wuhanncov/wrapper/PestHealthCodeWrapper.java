package com.gsafety.gemp.wuhanncov.wrapper;

import org.springframework.beans.BeanUtils;

import com.gsafety.gemp.wuhanncov.contract.dto.PestHealthCodeDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.PestHealthCodeSaveDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.in.PestHealthCodeReportDTO;
import com.gsafety.gemp.wuhanncov.dao.po.PestHealthCodePO;

/**
 * 
* @ClassName: PestHealthCodeWrapper 
* @Description: 健康码 po 与 dto互转
* @author luoxiao
* @date 2020年2月21日 下午5:10:58 
*
 */
public class PestHealthCodeWrapper {
	
	public static PestHealthCodePO dtoToPo(PestHealthCodeReportDTO dto){
		PestHealthCodePO po = new PestHealthCodePO();
		BeanUtils.copyProperties(dto, po);
		return po;
	}
	
	public static PestHealthCodeDTO poToDto(PestHealthCodePO po){
		PestHealthCodeDTO dto = new PestHealthCodeDTO();
		BeanUtils.copyProperties(po, dto);
		return dto;
	}
	
	public static PestHealthCodeSaveDTO poToSaveDto(PestHealthCodePO po){
		PestHealthCodeSaveDTO dto = new PestHealthCodeSaveDTO();
		BeanUtils.copyProperties(po, dto);
		return dto;
	}
	
	public static PestHealthCodeSaveDTO dtoToSaveDto(PestHealthCodeDTO dto){
		PestHealthCodeSaveDTO saveDto = new PestHealthCodeSaveDTO();
		BeanUtils.copyProperties(dto, saveDto);
		return saveDto;
	}
}
