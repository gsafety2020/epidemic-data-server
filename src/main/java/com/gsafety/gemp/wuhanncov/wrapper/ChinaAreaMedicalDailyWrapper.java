/**
 * 
 */
package com.gsafety.gemp.wuhanncov.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.gsafety.gemp.wuhanncov.contract.dto.ChinaAreaMedicalDailyDTO;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaAreaMedicalDailyPO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

/**
 * 各地区医院情况日报Wrapper
 *
 * @author yangbo
 *
 * @since 2020年2月9日 上午2:51:56
 *
 */
public class ChinaAreaMedicalDailyWrapper {

	/**
	 * 
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月9日 上午3:38:28
	 *
	 * @param po
	 * @return
	 */
	public static ChinaAreaMedicalDailyDTO toDTO(ChinaAreaMedicalDailyPO po) {
		ChinaAreaMedicalDailyDTO dto = new ChinaAreaMedicalDailyDTO();
		BeanUtils.copyProperties(po, dto);
		dto.setParentName(po.getParentDim().getAreaName());
		dto.setProvinceName(po.getProvinceDim().getAreaName());
		dto.setCityName(po.getCityDim().getAreaName());
		return dto;
	}

	/**
	 * 
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月9日 上午3:38:51
	 *
	 * @param records
	 * @return
	 */
	public static List<ChinaAreaMedicalDailyDTO> toDTOList(List<ChinaAreaMedicalDailyPO> records) {
		List<ChinaAreaMedicalDailyDTO> list = new ArrayList<>();
		records.stream().forEach(record -> list.add(toDTO(record)));
		return list;
	}

	/**
	 * 分页po转dto
	 * @param page
	 * @return
	 */
	public static Page<ChinaAreaMedicalDailyDTO> toPageDTO(Page<ChinaAreaMedicalDailyPO> page){
		List<ChinaAreaMedicalDailyDTO> content = toDTOList(page.getContent());
		return new PageImpl<>(content, page.getPageable(), page.getTotalElements());
	}
}
