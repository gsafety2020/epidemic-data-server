/**
 * 
 */
package com.gsafety.gemp.wuhanncov.wrapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaRuntimeDTO;
import org.springframework.beans.BeanUtils;

import com.gsafety.gemp.wuhanncov.contract.dto.ChinaHashDataDTO;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaHashDataPO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

/**
 * 该类用于...，用于实现...等能力。
 *
 * @author yangbo
 *
 * @since 2020年2月8日 下午10:24:51
 *
 */
public class ChinaHashDataWrapper {

	/**
	 * 
	 */
	private ChinaHashDataWrapper() {
		super();
	}

	/**
	 * 
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月8日 下午10:26:08
	 *
	 * @param po
	 * @return
	 */
	public static ChinaHashDataDTO toDTO(ChinaHashDataPO po) {
		ChinaHashDataDTO dto = ChinaHashDataDTO.builder().build();
		BeanUtils.copyProperties(po, dto);
		return dto;
	}

	public static Page<ChinaHashDataDTO> toDTOPage(Page<ChinaHashDataPO> page) {
		List<ChinaHashDataDTO> content = toDTOList(page.getContent());
		return new PageImpl<>(content, page.getPageable(), page.getTotalElements());
	}

	/**
	 * 
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月8日 下午10:28:53
	 *
	 * @param records
	 * @return
	 */
	public static List<ChinaHashDataDTO> toDTOList(List<ChinaHashDataPO> records) {
		List<ChinaHashDataDTO> list = new ArrayList<>();
		records.stream().forEach(record -> list.add(toDTO(record)));
		return list;
	}

	/**
	 * 
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月8日 下午10:35:29
	 *
	 * @param records
	 * @return
	 */
	public static Map<String, Object> toMap(List<ChinaHashDataDTO> records) {
		Map<String, Object> map = new HashMap<>();
		records.forEach(record -> map.put(record.key(), record.value()));
		return map;
	}

}
