package com.gsafety.gemp.wuhanncov.controller;

import com.gsafety.gemp.wuhanncov.constant.AppConstant;
import com.gsafety.gemp.wuhanncov.contract.dto.*;
import com.gsafety.gemp.wuhanncov.contract.params.DimQueryParam;
import com.gsafety.gemp.wuhanncov.contract.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * 提供数据的维护能力
 * @author zhoushuyan
 * @date 2020-01-29 9:43
 * @Description
 */
@Api(value = "冠状病毒数据维护能力", tags = {"冠状病毒数据维护能力"})
@RestController
@RequestMapping("/wuhan/ncov/repair")
public class NcovRepairDataController {
    @Resource
    private NcovRepairDataService ncovRepairDataService;

    @Resource
    private NcovDailyDataMaintainService ncovDailyDataMaintainService;

    @Resource
    private NcovLocationDimService ncovLocationDimService;

    @Resource
    private PatientAreaRuntimeService patientAreaRuntimeService;

    @Resource
    private ChinaHashDataService chinaHashDataService;

    @Resource
    private CounthistoryService countHistoryService;

    @Resource
    private LocationTokenService locationTokenService;



    /**
     *
     * 全国各地区例统计列表：areaType：country province city
     * 默认查询所有省
     * @author zhoushuyan
     *
     * @since 2020年1月26日 上午9:23:08
     *
     * @return
     */
    @ApiOperation(value = "全国各地区例统计列表：areaType：country province city")
    @GetMapping("/findAreaCountList")
    public List<ChinaCountStatisticsDTO> findAreaCountList(@RequestParam(name = "areaType", required = false) String areaType) {
        if(StringUtils.isEmpty(areaType)){
            areaType="province";
        }
        return ncovRepairDataService.findAreaCountList(areaType);
    }

    /**
     *
     * 根据名称模糊查询地区病例列表
     * 默认查询所有省
     * @author zhoushuyan
     *
     * @since 2020年1月26日 上午9:23:08
     *
     * @return
     */
    @ApiOperation(value = "根据名称模糊查询地区病例列表")
    @GetMapping("/findAreaCountByNameList")
    public List<ChinaCountStatisticsDTO> findAreaCountByNameList(@RequestParam(name = "areaName", required = false) String areaName) {
        return ncovRepairDataService.findAreaCountByNameList(areaName);
    }

    /**
     *
     * 查询市区列表
     * @author zhoushuyan
     *
     * @since 2020年1月26日 上午9:23:08
     *
     * @return
     */
    @ApiOperation(value = " 查询市区列表")
    @GetMapping("/findAreaCountByProvince")
    public List<ChinaCountStatisticsDTO> findAreaCountByProvince(@RequestParam(name = "province", required = false) String province) {
        return ncovRepairDataService.findAreaCountByProvince(province);
    }

    /**
     *
     * 查询详细信息
     * @author zhoushuyan
     *
     * @since 2020年1月26日 上午9:23:08
     *
     * @return
     */
    @ApiOperation(value = "查询详细信息")
    @GetMapping("/findAreaCountById")
    public ChinaCountStatisticsDTO findAreaCountById(@RequestParam(name = "id", required = false) String id) {
        return ncovRepairDataService.findAreaCountById(id);
    }
    /**
     *
     * 修改详细信息
     * @author zhoushuyan
     *
     * @since 2020年1月26日 上午9:23:08
     *
     * @return
     */
    @ApiOperation(value = "修改详细信息")
    @PostMapping("/updateAreaCount")
    public Result updateAreaCount(@RequestBody ChinaCountStatistics chinaCountStatistics) {
        return ncovRepairDataService.updateAreaCount(chinaCountStatistics);
    }

    /**
     * 查询病患日报
     * 默认查询所有省
     * @author xiaoz
     * @since 2020年1月26日 上午9:23:08
     * @return
     */
    @ApiOperation(value = "根据地区名称模糊查询该地区病患日报")
    @GetMapping("/findDailyReportByArea")
    public List<PatientAreaStatisticDTO> findDailyReportByArea(@RequestParam(name = "area", required = true) String area) {
        return ncovDailyDataMaintainService.findDailyReportByArea(area);
    }

    @ApiOperation(value = "根据地区名称模糊分页查询该地区病患日报")
    @PostMapping("/findDailyReportPage")
    public Result findDailyReportPage(@RequestBody DimQueryParam queryParam) {
        Sort sort = Sort.by(Sort.Direction.DESC, "stasTime");
        Pageable pageable = PageRequest.of( null == queryParam.getCurrentPage() ? AppConstant.DEFAULT_CURRENT_PAGE : queryParam.getCurrentPage() - 1,
                                            null == queryParam.getPageSize() ? AppConstant.DEAULT_PAGE_SIZE : queryParam.getPageSize(),
                                            sort);
        Page<PatientAreaStatisticDTO> page = ncovDailyDataMaintainService.findAllByDim(queryParam, pageable);

        return new Result().success(page);
    }

    /**
     * 新增病患日报
     * @author xiaoz
     * @since 2020年1月26日 上午9:23:08
     * @return
     */
    @ApiOperation(value = "新增地区病患日报")
    @PostMapping("/addDailyReport")
    public Result addDailyReport(@RequestBody PatientAreaStatisticDTO dto) {
        //新增忽略接口文档中传递主键，维护页面不会传递主键
        dto.setId(null);
        return ncovDailyDataMaintainService.dailyReportSave(dto);
    }

    /**
     * 更新病患日报
     * @author xiaoz
     * @since 2020年1月26日 上午9:23:08
     * @return
     */
    @ApiOperation(value = "更新地区病患日报")
    @PostMapping("/updateDailyReport")
    public Result updateDailyReport(@RequestBody PatientAreaStatisticDTO dto) {
        return ncovDailyDataMaintainService.dailyReportSave(dto);
    }

    /**
     * 新增行政区划
     * @author xiaoz
     * @since 2020年1月26日 上午9:23:08
     * @return
     */
    @ApiOperation(value = "新增行政区划")
    @PostMapping("/addLocationDim")
    public Result addLocationDim(@RequestBody LocationDimDTO dto) {
        ncovLocationDimService.save(dto);

        return new Result().success("新增成功");
    }

    /**
     * 更新行政区划
     * @author xiaoz
     * @since 2020年1月26日 上午9:23:08
     * @return
     */
    @ApiOperation(value = "更新行政区划")
    @PostMapping("/updateLocationDim")
    public Result updateLocationDim(@RequestBody LocationDimDTO dto) {
        ncovLocationDimService.save(dto);

        return new Result().success("更新成功");
    }

    /**
     * 删除行政区划
     * @author xiaoz
     * @since 2020年1月26日 上午9:23:08
     * @return
     */
    @ApiOperation(value = "删除行政区划")
    @DeleteMapping("/deleteLocationDim")
    public Result deleteLocationDim(@RequestParam(name = "areaNo") String areaNo) {
        ncovLocationDimService.delete(areaNo);

        return new Result().success("删除成功");
    }

    /**
     * 新增行政区划判断填报的行政区划编码和行政区划名称是否已经存在
     * @author xiaoz
     * @since 2020年1月26日 上午9:23:08
     * @return
     */
    @ApiOperation(value = "判断行政区划是否存在")
    @GetMapping("/existLocationDim")
    public Result existLocationDim(@RequestParam(name = "areaNo", required = false) String areaNo,
                                   @RequestParam(name = "areaName", required = false) String areaName) {

        return new Result().success(String.valueOf(ncovLocationDimService.exist(areaNo, areaName)));
    }

    /**
     * 查询行政区划
     * @author xiaoz
     * @since 2020年1月26日 上午9:23:08
     * @return
     */
    @ApiOperation(value = "行政编码查询行政区划")
    @GetMapping("/locationDimOne")
    public Result locationDimOne(@RequestParam(name = "areaNo") String areaNo) {
        return new Result().success(ncovLocationDimService.load(areaNo));
    }

    /**
     * 查询子行政区划
     * @author xiaoz
     * @since 2020年1月26日 上午9:23:08
     * @return
     */
    @ApiOperation(value = "查询子行政区划，包含父节点")
    @GetMapping("/locationDimAndChild")
    public Result locationDimAndChild(@RequestParam(name = "areaNo") String areaNo) {
        areaNo= AppConstant.getDimCode(areaNo);
        return new Result().success(ncovLocationDimService.loadSelfAndChild(areaNo));
    }

    /**
     * 查询子行政区划
     * @author xiaoz
     * @since 2020年1月26日 上午9:23:08
     * @return
     */
    @ApiOperation(value = "查询子行政区划，不包含父节点")
    @GetMapping("/locationDimChild")
    public Result locationDimChild(@RequestParam(name = "areaNo") String areaNo) {
        areaNo= AppConstant.getDimCode(areaNo);
        return new Result().success(ncovLocationDimService.loadChild(areaNo));
    }

    /**
     * 行政区划名称模糊查询
     * @author xiaoz
     * @since 2020年1月26日 上午9:23:08
     * @return
     */
    @ApiOperation(value = "行政区划名称模糊查询")
    @PostMapping("/locationDimPage")
    public Result locationDimPage(@RequestBody DimQueryParam queryParam) {
        List<Sort.Order> orderlist=new ArrayList<>();
        Sort.Order areaOrder=new Sort.Order(Sort.Direction.ASC, "areaNo");
        Sort.Order seqOrder=new Sort.Order(Sort.Direction.DESC, "seqNum");
        orderlist.add(areaOrder);
        orderlist.add(seqOrder);
        Sort sort = new Sort(orderlist);
        Pageable pageable = PageRequest.of( null == queryParam.getCurrentPage() ? AppConstant.DEFAULT_CURRENT_PAGE : queryParam.getCurrentPage() - 1,
                                            null == queryParam.getPageSize() ? AppConstant.DEAULT_PAGE_SIZE : queryParam.getPageSize(),
                                            sort);
        return new Result().success(ncovLocationDimService.findByAreaNamePage(queryParam.getArea(), pageable));
    }

    /**
     * 实时数据行政区划名称分页模糊查询
     * @author xiaoz
     * @since 2020年1月26日 上午9:23:08
     * @return
     */
    @ApiOperation(value = "实时数据行政区划名称模糊查询")
    @PostMapping("/patientRuntimePage")
    public Result patientRuntimePage(@RequestBody DimQueryParam queryParam) {
        Sort sort = Sort.by(Sort.Direction.DESC, "updateTime");
        Pageable pageable = PageRequest.of( null == queryParam.getCurrentPage() ? AppConstant.DEFAULT_CURRENT_PAGE : queryParam.getCurrentPage() - 1,
                                            null == queryParam.getPageSize() ? AppConstant.DEAULT_PAGE_SIZE : queryParam.getPageSize(),
                                            sort);

        return new Result().success(patientAreaRuntimeService.findPatientRuntimePage(queryParam, pageable));
    }

    /**
     * 爬虫历史查询
     * @author wangwenhai
     * @since 2020年2月12日 下午19:18
     * @return
     */
    @ApiOperation(value = "爬虫历史查询")
    @PostMapping("/getReptileListPage")
    public Result reptilePage(@RequestBody DimQueryParam queryParam) {
        List<Sort.Order> orderlist=new ArrayList<>();
        Sort.Order timeOrder=new Sort.Order(Sort.Direction.DESC, "updateTime");
        Sort.Order areaOrder=new Sort.Order(Sort.Direction.ASC, "areaName");
        Sort.Order provinceOrder=new Sort.Order(Sort.Direction.ASC,"province");
        orderlist.add(timeOrder);
        orderlist.add(provinceOrder);
        orderlist.add(areaOrder);

        Sort sort = new Sort(orderlist);
        Pageable pageable = PageRequest.of( null == queryParam.getCurrentPage() ? AppConstant.DEFAULT_CURRENT_PAGE : queryParam.getCurrentPage() - 1,
                null == queryParam.getPageSize() ? AppConstant.DEAULT_PAGE_SIZE : queryParam.getPageSize(),
                sort);

        return new Result().success(countHistoryService.findRetileInfoPage(queryParam, pageable));
    }


    /**
     * 搜索行政区划父节点名称
     * @author wangwenhai
     * @since 2020年2月15日 上午10:44:08
     * @return
     */
    @ApiOperation(value = "搜索行政区划父节点名称查询")
    @PostMapping("/locationDimParentSearch")
    public Result locationDimParentSearch(@RequestBody DimQueryParam queryParam) {
        return new Result().success(ncovLocationDimService.findLocationDimParentSearch(queryParam));
    }

    @ApiOperation(value = "实时数据新增")
    @PostMapping("/addPatientRuntime")
    public Result addPatientRuntime(@RequestBody PatientAreaRuntimeDTO dto) {
        return patientAreaRuntimeService.patientRuntimeSave(dto);
    }

    @ApiOperation(value = "实时数据新增校验")
    @GetMapping("/addPatientRuntimeValid")
    public Result addPatientRuntimeValid(@NotNull@RequestParam(name = "areaCode") String areaCode) {
        areaCode= AppConstant.getDimCode(areaCode);
        return patientAreaRuntimeService.patientRuntimeValid(areaCode);
    }

    @ApiOperation(value = "实时数据更新")
    @PostMapping("/updatePatientRuntime")
    public Result updatePatientRuntime(@RequestBody PatientAreaRuntimeDTO dto) {
        return patientAreaRuntimeService.patientRuntimeUpdate(dto);
    }

    @ApiOperation(value = "初始化指定区域的疫情数据, 历史折线图和热力图数据")
    @GetMapping("/initPatientHistoryData")
    public Result initPatientHistoryData(@RequestParam(name = "areaNo")String areaNo, @RequestParam(name = "days")Integer days){
        areaNo= AppConstant.getDimCode(areaNo);
        return ncovRepairDataService.initPatientHistoryData(areaNo, days);
    }

    @ApiOperation(value = "锁定")
    @PostMapping("/lockPatientRuntime")
    public Result lockPatientRuntime(@RequestBody PatientAreaRuntimeDTO dto) {
        return patientAreaRuntimeService.patientRuntimeLock(dto);
    }

    @ApiOperation(value = "解锁")
    @PostMapping("/unlockPatientRuntime")
    public Result unlockPatientRuntime(@RequestBody PatientAreaRuntimeDTO dto) {
        return patientAreaRuntimeService.patientRuntimeUnlock(dto);
    }


    @ApiOperation(value = "非病患散列信息实时数据分页")
    @PostMapping("/hashDataPage")
    public Result hashDataPage(@RequestBody DimQueryParam queryParam) {
        Sort sort = Sort.by(Sort.Direction.DESC, "dataCode");
        Pageable pageable = PageRequest.of(null == queryParam.getCurrentPage() ? AppConstant.DEFAULT_CURRENT_PAGE : queryParam.getCurrentPage() - 1,
                null == queryParam.getPageSize() ? AppConstant.DEAULT_PAGE_SIZE : queryParam.getPageSize(),
                sort);

        return new Result().success(chinaHashDataService.hashDataPage(queryParam, pageable));
    }

    @ApiOperation(value = "非病患散列信息实时数据更新")
    @PostMapping("/updateHashData")
    public Result updateHashData(@RequestBody ChinaHashDataDTO dto) {
        chinaHashDataService.updateHashData(dto);
        return new Result().success();
    }


    /**
     * 查询本机及下辖行政区划的登录口令
     * @author wangwenhai
     * @since 2020年2月22日 下午3:42:08
     * @return
     */
    @ApiOperation(value = "查询本机及下辖行政区划的登录口令")
    @PostMapping("/locationTokenPage")
    public Result locationTokenPage(@RequestBody DimQueryParam queryParam) {
        Sort sort = Sort.by(Sort.Direction.ASC, "areaNo");
        Pageable pageable = PageRequest.of( null == queryParam.getCurrentPage() ? AppConstant.DEFAULT_CURRENT_PAGE : queryParam.getCurrentPage() - 1,
                null == queryParam.getPageSize() ? AppConstant.DEAULT_PAGE_SIZE : queryParam.getPageSize(),
                sort);

        return new Result().success(locationTokenService.findTokenPage(queryParam, pageable));
    }



}
