package com.gsafety.gemp.wuhanncov.controller;


import com.gsafety.gemp.wuhanncov.contract.dto.RefreshPatientDailyCond;
import com.gsafety.gemp.wuhanncov.contract.jobdto.JobRunParam;
import com.gsafety.gemp.wuhanncov.contract.jobdto.JobRunResultDTO;
import com.gsafety.gemp.wuhanncov.contract.service.NcovPublicSentimentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * 任务执行接口
 *
 * @author fanlx
 * @since 2020/2/4  15:55
 */
@Api(value = "任务执行接口", tags = {"任务执行接口"})
@RestController
@RequestMapping("/task/")
@Slf4j
public class ScheduleTaskController {

    /**
     *
     */
    private static final int CHECK_POINT = 1;

    /**
     *
     */
    private static final String ENDPOINT_HOUR = "23";

    @Resource
    private NcovPublicSentimentService ncovPublicSentimentService;


    @ApiOperation(value = "计算舆情热力分析")
    @PostMapping("/hot/start")
    public JobRunResultDTO generateStat(@RequestBody JobRunParam param) {
        log.info("run" + DateTime.now());

        JobRunResultDTO resultDTO = new JobRunResultDTO();
        resultDTO.setJobId(param.getJobId());
        resultDTO.setJobRunId(param.getJobRunId());

        try {
            DateTimeFormatter format = DateTimeFormat.forPattern("yyyyMMdd");
            // 查询当日的舆情"pub_sentiment_news"
            String date = DateTime.now().toString(format);
            ncovPublicSentimentService.mergeLocationHot(date);
        } catch (Exception e) {
            resultDTO.setRunResult(2);
            return resultDTO;
        }

        resultDTO.setRunResult(1);
        return resultDTO;
    }


    /**
     * 定时生成患者日报
     *
     * @author yangbo
     * @since 2020年1月25日 下午9:01:30
     */
    @ApiOperation(value = "计算生成患者日报")
    @PostMapping("/patientDaily/start")
    public JobRunResultDTO generatePatientStat(@RequestBody JobRunParam param) {

        JobRunResultDTO resultDTO = JobRunResultDTO.builder().jobId(param.getJobId()).jobRunId(param.getJobRunId()).runResult(1).build();

        try {
            DateTimeFormatter format = DateTimeFormat.forPattern("yyyyMMdd");
            DateTime now = DateTime.now();
            String sampleDate = now.toString(format);
            Integer hour = now.getHourOfDay();
            String sampleHour = hour.toString();

            // 默认取样本日期为报表日期
            RefreshPatientDailyCond cond = new RefreshPatientDailyCond(sampleDate, sampleHour, sampleDate);
            ncovPublicSentimentService.refreshPatientDaily(cond);

            // 如果小于指定采集时间，则生成一次上日23:59:59的数据
            if (hour <= CHECK_POINT) {
                String targetDate = now.plusDays(-1).toString(format);
                RefreshPatientDailyCond lastDateCode = new RefreshPatientDailyCond(targetDate, ENDPOINT_HOUR, targetDate);
                ncovPublicSentimentService.refreshPatientDaily(lastDateCode);
            }
        } catch (Exception e) {
            log.error("调用定时job generatePatientStat 失败", e);
            resultDTO.setRunResult(2);
            return resultDTO;
        }
        return resultDTO;
    }

    /**
     * 定时生成病患实时数据
     *
     * @author fanlx
     * @since
     */
    @ApiOperation(value = "同步患者实时数据")
    @PostMapping("/patientRuntime/start")
    public JobRunResultDTO generatePatientRuntimeStat(@RequestBody JobRunParam param) {
        JobRunResultDTO resultDTO = JobRunResultDTO.builder().jobId(param.getJobId()).jobRunId(param.getJobRunId()).runResult(1).build();
        try {
            ncovPublicSentimentService.syncPatientRuntime();
            //同步患者实时数据-待确定数据同步
            ncovPublicSentimentService.patientUncertainRuntime();
        } catch (Exception e) {
            log.error("调用定时job generatePatientRuntimeStat 失败", e);
            resultDTO.setRunResult(2);
            return resultDTO;
        }
        return resultDTO;
    }
    /**
     * 同步患者实时数据-待确定数据同步
     *
     * @author zhoushuyan
     * @since
     */
    @ApiOperation(value = "同步患者实时数据-待确定数据同步")
    @PostMapping("/patientUncertainRuntime/start")
    public JobRunResultDTO patientUncertainRuntimeStart(@RequestBody JobRunParam param) {
        JobRunResultDTO resultDTO = JobRunResultDTO.builder().jobId(param.getJobId()).jobRunId(param.getJobRunId()).runResult(1).build();
        try {
            ncovPublicSentimentService.patientUncertainRuntime();
        } catch (Exception e) {
            log.error("调用定时job generatePatientUncertainRuntimeStat 失败", e);
            resultDTO.setRunResult(2);
            return resultDTO;
        }
        return resultDTO;
    }


}
