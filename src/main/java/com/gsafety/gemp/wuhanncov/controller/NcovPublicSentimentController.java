package com.gsafety.gemp.wuhanncov.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.gsafety.gemp.wuhanncov.constant.AppConstant;
import com.gsafety.gemp.wuhanncov.contract.dto.*;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gsafety.gemp.wuhanncov.contract.dto.HotwordContent.HotwordText;
import com.gsafety.gemp.wuhanncov.contract.service.NcovPublicSentimentService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * 该类用于武汉冠状病毒疫情大屏,用于实现疫情舆情数据查询等能力
 *
 * @author 86186
 * @since 2020/1/25 15:04
 *        <p>
 *        1.0.0
 */
@Api(value = "武汉冠状病毒舆情数据接口", tags = { "武汉冠状病毒舆情数据接口" })
@RestController
@RequestMapping("/beijing/ncov/pub/sens")
@Slf4j
public class NcovPublicSentimentController {

	@Resource
	private NcovPublicSentimentService ncovPublicSentimentService;

	/**
	 * 
	 * 查询疫情的最新舆情热词数据
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月25日 下午3:42:24
	 *
	 * @return
	 */
	@ApiOperation(value = "查询疫情的最新舆情热词数据")
	@GetMapping("/latestHotword")
	public HotwordDTO findLatestHotword() {
		return ncovPublicSentimentService.findLatestHotword();
	}

	/**
	 * 
	 * 查询疫情的最新舆情热词模型
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月25日 下午4:54:03
	 *
	 * @return
	 */
	@ApiOperation(value = "查询疫情的最新舆情热词接口")
	@GetMapping("/latestHotwordContent")
	public Collection<HotwordText> latestHotwordContent() {
		return ncovPublicSentimentService.findLatestHotwordContent().getWordTexts();
	}

	@ApiOperation(value = "查询疫情的讨论量接口")
	@GetMapping("/pubsensheat")
	public List<PubSensHeatDTO> getListHeatOfPubSens() {
		return ncovPublicSentimentService.getListHeatOfPubSens();
	}

	/**
	 *
	 * 新闻滚动条
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月25日 下午3:52:240
	 *
	 * @return
	 */
	@ApiOperation(value = "新闻滚动条")
	@GetMapping("/rollNows")
	public List<Map<String, String>> findRollNows() {
		log.info("新闻滚动条");
		String title = ncovPublicSentimentService.findRollNows();
		List<Map<String, String>> rollList = new ArrayList<>();
		Map<String, String> map = new HashMap<String, String>();
		map.put("value", title);
		rollList.add(map);
		return rollList;
	}

	/**
	 * 
	 * 查询地区舆情热力数据
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月25日 下午7:17:22
	 *
	 * @return
	 */
	@ApiOperation(value = "查询地区舆情热力数据")
	@GetMapping("/findLocationHot")
	public List<LocationHotDTO> findLocationHot(@RequestParam(name = "value", required = false) String date) {
		if(StringUtils.isEmpty(date)) {
			DateTime dateTime = DateTime.now();
			date =dateTime.toString("yyyyMMdd");
		}
		return ncovPublicSentimentService.findLocationHot(date);
	}
	
	/**
	 *
	 * 情感分析
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月25日 下午3:52:240
	 *
	 * @return
	 */
	@ApiOperation(value = "情感分析")
	@GetMapping("/emotionAnalyzing")
	public Collection<EmotionAnalyzingContent.EmotionAnalyzingText> findEmotionAnalyzing(){
		log.info("情感分析");
		return ncovPublicSentimentService.findEmotionAnalyzing().getEmotionAnalyzingText();
	}
	
	@ApiOperation(value = "更新地区舆情热力数据")
	@GetMapping("/refreshLocationHot")
	public String refreshLocationHot(@RequestParam(name = "date", required = false) String date,
			@RequestParam(name = "areaId") String areaId) {
		if(StringUtils.isEmpty(date)) {
			DateTime dateTime = DateTime.now();
			date =dateTime.toString("yyyyMMdd");
		}
		ncovPublicSentimentService.mergeLocationHot(date, areaId);
		return "OK";
	}
	
	/**
	 * 
	 * 更新当日全国各省舆情热力数据
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月25日 下午9:23:08
	 *
	 * @param date
	 * @return
	 */
	@ApiOperation(value = "更新当日全国各省舆情热力数据")
	@GetMapping("/refreshAllLocationHot")
	public String refreshAllLocationHot(@RequestParam(name = "date", required = false) String date) {
		if(StringUtils.isEmpty(date)) {
			DateTime dateTime = DateTime.now();
			date =dateTime.toString("yyyyMMdd");
		}
		ncovPublicSentimentService.mergeLocationHot(date);
		return "OK";
	}
	/**
	 *
	 * 全国各省病例统计滚动板
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	@ApiOperation(value = "全国各省病例统计滚动板")
	@GetMapping("/findAreaCount")
	public List<ChinaCountStatisticsDTO> findAreaCount() {
		return ncovPublicSentimentService.findAreaCount();
	}


	/**
	 *
	 * 获取指定地区的病例数据
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	@ApiOperation(value = "获取指定地区的病例数据")
	@GetMapping("/findAreaByAreaIdsCount")
	public List<ChinaCountStatisticsDTO> findAreaByAreaIdsCount(@ApiParam(value = "多个区域编码，用'_'分割, 例如：100000_420000")
																	@RequestParam(name = "areaIds", required = true) String areaIds) {
		List<String> areaIdList = new ArrayList<>();
		String[] areaArray = areaIds.split("_");
		for (String area : areaArray) {
			String code= AppConstant.getDimCode(area.trim());
			areaIdList.add(code);
		}
		return ncovPublicSentimentService.findAreaByAreaIdsCount(areaIdList);
	}

	/**
	 *
	 * 全国病例统计
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	@ApiOperation(value = "全国病例统计")
	@GetMapping("/findChinaCount")
	public ChinaCountStatisticsDTO findChinaCount() {
		return ncovPublicSentimentService.findChinaCount();
	}
	/**
	 *
	 * 湖北城市病例统计
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	@ApiOperation(value = "湖北-城市病例统计")
	@GetMapping("/findHBCityCount")
	public List<ChinaCountStatisticsJSON> findHBCityCount() {
		List<ChinaCountStatisticsDTO> list=ncovPublicSentimentService.findHBCityCount();
		List<ChinaCountStatisticsJSON> listResult=new ArrayList<ChinaCountStatisticsJSON>();
		list.stream().forEach(temp -> {
			ChinaCountStatisticsJSON json=new ChinaCountStatisticsJSON();
			BeanUtils.copyProperties(temp,json);
			listResult.add(json);
		});
		return listResult;
	}
	/**
	 *
	 * 北京各城市病例统计
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	@ApiOperation(value = "北京-城市病例统计")
	@GetMapping("/findBJCityCount")
	public List<ChinaCountStatisticsJSON> findBJCityCount() {
		List<ChinaCountStatisticsDTO> list=ncovPublicSentimentService.findBJCityCount();
		List<ChinaCountStatisticsJSON> listResult=new ArrayList<ChinaCountStatisticsJSON>();
		list.stream().forEach(temp -> {
			ChinaCountStatisticsJSON json=new ChinaCountStatisticsJSON();
			BeanUtils.copyProperties(temp,json);
			listResult.add(json);
		});
		return listResult;
	}

	/**
	 *
	 * 广东-城市病例统计
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	@ApiOperation(value = "广东-城市病例统计")
	@GetMapping("/findGDCityCount")
	public List<ChinaCountStatisticsJSON> findGDCityCount() {
		String province="广东";
		List<ChinaCountStatisticsDTO> list=ncovPublicSentimentService.findProvinceCityCount(province);
		List<ChinaCountStatisticsJSON> listResult=new ArrayList<ChinaCountStatisticsJSON>();
		list.stream().forEach(temp -> {
			ChinaCountStatisticsJSON json=new ChinaCountStatisticsJSON();
			BeanUtils.copyProperties(temp,json);
			listResult.add(json);
		});
		return listResult;
	}
	/**
	 *
	 * 省份-城市病例统计
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	@ApiOperation(value = "省份-城市病例统计 例如：province=湖北")
	@GetMapping("/findCityCountByProvince")
	public List<ChinaCountStatisticsJSON> findCityCountByProvince(@RequestParam(name = "province", required = true) String province) {
		List<ChinaCountStatisticsDTO> list=ncovPublicSentimentService.findProvinceCityCount(province);
		List<ChinaCountStatisticsJSON> listResult=new ArrayList<ChinaCountStatisticsJSON>();
		list.stream().forEach(temp -> {
			ChinaCountStatisticsJSON json=new ChinaCountStatisticsJSON();
			BeanUtils.copyProperties(temp,json);
			listResult.add(json);
		});
		return listResult;
	}

	/**
	 *
	 * 查询地区病例统计:例如湖北
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	@ApiOperation(value = "查询地区病例统计:例如湖北")
	@GetMapping("/findAreaNameCount")
	public ChinaCountStatisticsDTO findAreaNameCount(@RequestParam(name = "areaName", required = true) String areaName) {
		return ncovPublicSentimentService.getAreaNameCount(areaName);
	}

	/**
	 *
	 * 河北城市病例统计
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	@ApiOperation(value = "河北-城市病例统计")
	@GetMapping("/findHeBeiCityCount")
	public List<ChinaCountStatisticsJSON> findHeBeiCityCount() {
		String province="河北";
		List<ChinaCountStatisticsDTO> list=ncovPublicSentimentService.findProvinceCityCount(province);
		List<ChinaCountStatisticsJSON> listResult=new ArrayList<ChinaCountStatisticsJSON>();
		list.stream().forEach(temp -> {
			ChinaCountStatisticsJSON json=new ChinaCountStatisticsJSON();
			BeanUtils.copyProperties(temp,json);
			listResult.add(json);
		});
		return listResult;
	}
	/**
	 *
	 * 广东-城市病例统计
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	@ApiOperation(value = "佛山-各市病例统计")
	@GetMapping("/findFSCityCount")
	public List<ChinaCountStatisticsJSON> findFSCityCount() {
		String cityName="佛山";
		List<ChinaCountStatisticsDTO> list=ncovPublicSentimentService.findCityCountyCount(cityName);
		List<ChinaCountStatisticsJSON> listResult=new ArrayList<ChinaCountStatisticsJSON>();
		list.stream().forEach(temp -> {
			ChinaCountStatisticsJSON json=new ChinaCountStatisticsJSON();
			BeanUtils.copyProperties(temp,json);
			listResult.add(json);
		});
		return listResult;
	}

	/**
	 *
	 * 广东-城市病例统计-包含佛山各区
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	@ApiOperation(value = "广东-城市病例统计-包含佛山各区")
	@GetMapping("/findGDAndFsCityCount")
	public List<ChinaCountStatisticsJSON> findGDAndFsCityCount() {
		List<ChinaCountStatisticsJSON> list=findGDCityCount();
		list.addAll(findFSCityCount());
		return list;
	}

	/**
	 *
	 * 湖北病例统计
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	@ApiOperation(value = "湖北病例统计")
	@GetMapping("/findHBCount")
	public ChinaCountStatisticsDTO findHBCount() {
		return ncovPublicSentimentService.findHBCount();
	}

	/**
	 *
	 * 北京病例统计
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	@ApiOperation(value = "北京病例统计")
	@GetMapping("/findBJCount")
	public ChinaCountStatisticsDTO findBJCount() {
		return ncovPublicSentimentService.findBJCount();
	}

	/**
	 *
	 * 武汉病例统计
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	@ApiOperation(value = "武汉病例统计")
	@GetMapping("/findWHCount")
	public ChinaCountStatisticsDTO findWHCount() {
		return ncovPublicSentimentService.findWHCount();
	}

	/**
	 *
	 * 港澳台病例统计
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	@ApiOperation(value = "港澳台病例统计")
	@GetMapping("/findGATCount")
	public List<ChinaCountStatisticsDTO> findGATCount() {
		return ncovPublicSentimentService.findGATCount();
	}
	/**
	 *
	 * 国际病例统计
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	@ApiOperation(value = "国际病例统计")
	@GetMapping("/findInternationalCount")
	public List<WorldInfoStatisticsDTO> findInternationalCount() {
		return ncovPublicSentimentService.findInternationalCount();
	}
	/**
	 *
	 * 获得时间轴数据
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	@ApiOperation(value = "获得时间轴数据")
	@GetMapping("/getTimeAxis")
	public Collection<TextValue.TextValueText> getTimeAxis() {
		return ncovPublicSentimentService.getTimeAxis().getTextValueText();
	}
	/**
	 *
	 * 获得时间省级区划数据
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	@ApiOperation(value = "获得省级区划数据")
	@GetMapping("/getProvinceArea")
	public Collection<TextValue.TextValueText> getProvince() {
		return ncovPublicSentimentService.getProvinceArea().getTextValueText();
	}
	/**
	 *
	 * 获得时间市级区划数据
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	@ApiOperation(value = "获得市级区划数据")
	@GetMapping("/getCityArea")
	public Collection<TextValue.TextValueText> getCityArea(@RequestParam(name = "areaId", required = true) String areaId) {
		areaId=AppConstant.getDimCode(areaId);
		return ncovPublicSentimentService.getCityArea(areaId).getTextValueText();
	}

	/**
	 *
	 * 获得时间市级区划数据
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	@ApiOperation(value = "获得所有的省市地区信息")
	@GetMapping("/getAllArea")
	public Collection<AreaInfo.AreaInfoText> getAllArea() {
		return ncovPublicSentimentService.getAllArea().getAreaInfoText();
	}

	/**
	 * 
	 * 生成指定日期的病患报表
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月26日 下午7:29:28
	 *
	 * @return
	 */
	@ApiOperation(value = "生成指定日期的病患报表")
	@PostMapping("/refreshPatientDaily")
	public String refreshPatientDaily(@RequestBody RefreshPatientDailyCond cond) {
		this.ncovPublicSentimentService.refreshPatientDaily(cond);
		return "OK";
	}
	
	@ApiOperation(value = "查询区域患者人数按天统计数据")
	@GetMapping("/patient_area_daily_count")
	public List<PatientAreaCountDailyDTO> getPatientAreaDailyCount(){
		return ncovPublicSentimentService.getPatientAreaCountDaily();
	}

	@ApiOperation(value = "查询实时视频地址列表数据")
	@GetMapping("/real_video_public")
	public List<RealVideoPublicDTO> getRealVideoPublicList(){
		return ncovPublicSentimentService.getRealVideoPublicList();
	}
	/**
	 * 迁出/迁入的列表接口
	 * @author zhoushuyan
	 * @since 2020年1月26日 上午9:23:08
	 * @param date 时间 格式 yyyy-mm-dd
	 * @param migrate_type:move_out1 市迁出  move_out2省迁出 move_in1市迁入 move_id2省迁入
	 * @param area_id 默认武汉 420100
	 * @return
	 */
	@ApiOperation(value = "迁出/迁入列表查询")
	@GetMapping("/findMigratePeople")
	public Collection<MigratePeopleContent.MigratePeople> findMigratePeople(@RequestParam(name = "date", required = false) String date,
																			@RequestParam(name = "migrate_type", required = false) String migrate_type,
																			@RequestParam(name = "area_id", required = false) String area_id) {
		String areaType="province";
		String migrateType="move_out";
		if(StringUtils.isEmpty(date)||"null".equalsIgnoreCase(date)||"undefined".equalsIgnoreCase(date)) {
			date=ncovPublicSentimentService.getMigrateMapDefaultDate();
		}
		if(StringUtils.isEmpty(migrate_type)||"null".equalsIgnoreCase(migrate_type)||"undefined".equalsIgnoreCase(migrate_type)) {
			migrate_type ="move_out1";
		}
		if(StringUtils.isEmpty(area_id)||"null".equalsIgnoreCase(area_id)||"undefined".equalsIgnoreCase(area_id)) {
			area_id ="420100";
		}
		if(migrate_type.contains("1")){
			areaType="city";
		}
		if(migrate_type.contains("in")){
			migrateType="move_in";
		}
		date=date.replaceAll("-","");
		return ncovPublicSentimentService.getMigratePeople(date,migrateType,areaType,area_id).getMigratePeoples();
	}

	/**
	 * 迁徙图数据
	 * @author zhoushuyan
	 * @since 2020年1月26日 上午9:23:08
	 * @param date 时间 格式 yyyymmdd
	 * @param migrate_type:move_out1 市迁出  move_out2省迁出 move_in1市迁入 move_id2省迁入
	 * @param area_id 默认武汉 420100
	 * @return
	 */
	@ApiOperation(value = "获得迁徙图的数据")
	@GetMapping("/getMigrateMap")
	public Collection<MigrateMapContent.MigrateMap> getMigrateMap(@RequestParam(name = "date", required = false) String date,
																  @RequestParam(name = "migrate_type", required = false) String migrate_type,
																  @RequestParam(name = "area_id", required = false) String area_id) {
		String areaType="province";
		String migrateType="move_out";
		if(StringUtils.isEmpty(date)||"null".equalsIgnoreCase(date)||"undefined".equalsIgnoreCase(date)) {
			date=ncovPublicSentimentService.getMigrateMapDefaultDate();
		}
		if(StringUtils.isEmpty(migrate_type)||"null".equalsIgnoreCase(migrate_type)||"undefined".equalsIgnoreCase(migrate_type)) {
			migrate_type ="move_out1";
		}
		if(StringUtils.isEmpty(area_id)||"null".equalsIgnoreCase(area_id)||"undefined".equalsIgnoreCase(area_id)) {
			area_id ="420100";
		}
		if(migrate_type.contains("1")){
			areaType="city";
		}
		if(migrate_type.contains("in")){
			migrateType="move_in";
		}
		date=date.replaceAll("-","");
		return ncovPublicSentimentService.getMigrateMap(date,migrateType,areaType,area_id).getMigrateMaps();
	}


	@ApiOperation(value = "查询指定父区域下子区域舆情热力值列表")
	@GetMapping("/location_hot_List")
	public List<AreaHotDTO> getAreaHotList(@RequestParam(name = "areaParent", required = true)String areaParent){
		areaParent=AppConstant.getDimCode(areaParent);
		return ncovPublicSentimentService.getAreaHotList(areaParent);
	}

	@ApiOperation(value = "查询指定父区域下子区域患者热力值列表")
	@Deprecated
	@GetMapping("/location_patient_count_List")
	public List<AreaHotDTO> getAreaPatientCoutList(@RequestParam(name = "areaParent", required = true)String areaParent){
		areaParent=AppConstant.getDimCode(areaParent);
		return ncovPublicSentimentService.getAreaPatientCountList(areaParent);
	}

	@ApiOperation(value = "查询区域患者情况按天统计数据")
	@GetMapping("/patient_area_daily_info")
	public List<PatientAreaCountDailyDTO> getPatientAreaDailyInfo(@RequestParam("area_id") String areaId){
		areaId=AppConstant.getDimCode(areaId);
		return ncovPublicSentimentService.getPatientAreaInfoDaily(areaId);
	}

	@ApiOperation(value = "飞线图-武汉到全国省份")
	@GetMapping("/getFlyLineByWutoProvince")
	public Collection<MigrateMapContent.MigrateMap> getFlyLineByWutoProvince(){
		String from = "114.2919,30.5675"; //默认武汉
		return ncovPublicSentimentService.getFlyLineByWutoProvince(from).getMigrateMaps();
	}

	@ApiOperation(value = "冒泡图-全国省份定位和数据")
	@GetMapping("/getChinaCountStatisticsBubbling")
	public List<ChinaCountStatisticsBubbling> getChinaCountStatisticsBubbling(){
		return ncovPublicSentimentService.getChinaCountStatisticsBubbling();
	}
	@Deprecated
	@ApiOperation(value = "全国指定省-市的定位和数据")
	@GetMapping("/getParentCountStatisticsBubbling")
	public List<ChinaCountStatisticsBubbling> getParentCountStatisticsBubbling(@ApiParam(value = "上级区域行政区划编码", example = "北京")
																				  @RequestParam(name = "areaName", required = true) String areaName){
		return ncovPublicSentimentService.getParentCountStatisticsBubbling(areaName);
	}

	@ApiOperation(value = "获得迁徙图最新数据默认时间 yyyy-mm-dd")
	@GetMapping("/getMigrateMapDefaultDate")
	public List<Map<String,String>> getMigrateMapDefaultDate(){
		List<Map<String,String>> list=new ArrayList<>();
		String dateStr=ncovPublicSentimentService.getMigrateMapDefaultDate();
		Map<String,String> map=new HashMap<String, String>();
		map.put("time",dateStr);
		list.add(map);
		return list;
	}
	/**
	 *
	 * 省-城市病例统计
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	@ApiOperation(value = "省-城市病例统计")
	@GetMapping("/findProvinceCityCount")
	public List<ChinaCountStatisticsJSON> findProvinceCityCount(@RequestParam(name = "province", required = true) String province) {
		List<ChinaCountStatisticsDTO> list=ncovPublicSentimentService.findProvinceCityCount(province);
		List<ChinaCountStatisticsJSON> listResult=new ArrayList<ChinaCountStatisticsJSON>();
		list.stream().forEach(temp -> {
			ChinaCountStatisticsJSON json=new ChinaCountStatisticsJSON();
			BeanUtils.copyProperties(temp,json);
			listResult.add(json);
		});
		return listResult;
	}


}
