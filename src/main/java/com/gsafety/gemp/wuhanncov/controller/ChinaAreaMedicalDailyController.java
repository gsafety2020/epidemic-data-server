package com.gsafety.gemp.wuhanncov.controller;

import com.gsafety.gemp.wuhanncov.constant.AppConstant;
import com.gsafety.gemp.wuhanncov.contract.dto.ChinaAreaMedicalDailyDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.Result;
import com.gsafety.gemp.wuhanncov.contract.params.DimQueryParam;
import com.gsafety.gemp.wuhanncov.contract.service.ChinaAreaMedicalDailyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

/**
 * 该类用于维护地区医院日报
 *
 * @author wuhongling
 * @since 2020/02/18
 */
@Api(value = "地区医院日报接口", tags = {"地区医院日报接口"})
@RestController
@RequestMapping("/ncov/v3/areaMedicalDaily/")
public class ChinaAreaMedicalDailyController {

    @Autowired
    private ChinaAreaMedicalDailyService chinaAreaMedicalDailyService;


    /**
     * 地区医院日报查询
     * @param queryParam
     * @return
     */
    @ApiOperation(value = "地区医院日报分页查询")
    @PostMapping(value = "/findAreaMedicalDailyPage")
    public Result findAreaMedicalDailyPage(@RequestBody DimQueryParam queryParam){
        Sort sort = Sort.by(Sort.Direction.DESC, "stasDate","updateTime");
        Pageable pageable = PageRequest.of( null == queryParam.getCurrentPage() ? AppConstant.DEFAULT_CURRENT_PAGE : queryParam.getCurrentPage() - 1,
                null == queryParam.getPageSize() ? AppConstant.DEAULT_PAGE_SIZE : queryParam.getPageSize(),
                sort);
        Page<ChinaAreaMedicalDailyDTO> page = chinaAreaMedicalDailyService.findAreaMedicalDailyPage(queryParam, pageable);

        return new Result().success(page);
    }

    /**
     * 新增地区医院日报
     * @param dto
     * @return
     */
    @ApiOperation(value = "新增地区医院日报")
    @PostMapping("/addMedicalDaily")
    public Result addMedicalDailyPage(@RequestBody ChinaAreaMedicalDailyDTO dto) {
        //新增忽略接口文档中传递主键，维护页面不会传递主键
        dto.setId(null);
        return new Result().success("保存成功", chinaAreaMedicalDailyService.areaMedicalDailySave(dto).getId());
    }

    /**
     * 编辑地区医院日报
     * @param dto
     * @return
     */
    @ApiOperation(value = "编辑地区医院日报")
    @PostMapping("/updateMedicalDaily")
    public Result updateMedicalDailyPage(@RequestBody ChinaAreaMedicalDailyDTO dto) {

        return new Result().success("更新成功", chinaAreaMedicalDailyService.areaMedicalDailytUpdate(dto).getId());
    }


}
