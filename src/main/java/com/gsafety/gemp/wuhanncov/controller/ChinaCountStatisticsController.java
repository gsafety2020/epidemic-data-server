package com.gsafety.gemp.wuhanncov.controller;


import com.gsafety.gemp.wuhanncov.constant.AppConstant;
import com.gsafety.gemp.wuhanncov.contract.dto.ChinaCountStatisticsDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.Result;
import com.gsafety.gemp.wuhanncov.contract.params.ChinaCountStatisticsParam;
import com.gsafety.gemp.wuhanncov.contract.service.ChinaCountStatisticsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(value = "查询爬虫实时数据接口", tags = {"查询爬虫实时数据接口"})
@RestController
@RequestMapping("/ncov/chinaCountStats/")
public class ChinaCountStatisticsController {

    @Resource
    private ChinaCountStatisticsService chinaCountStatisticsService;

    @ApiOperation(value = "佛山-china_count_statistics更新接口")
    @PostMapping("/updateFSCityCount")
    public String findFSCityCount(@RequestBody ChinaCountStatisticsDTO dto) {
        chinaCountStatisticsService.update(dto);
        return "OK";
    }

    @ApiOperation(value = "查询爬虫实时数据")
    @PostMapping(value = "/chinaCountStat")
    public Result findChinaCountStatisticsPage(@RequestBody ChinaCountStatisticsParam chinaCountStatisticsParam){
        Sort sort = Sort.by(Sort.Direction.DESC, "updateTime");
        Pageable pageable = PageRequest.of( null == chinaCountStatisticsParam.getCurrentPage() ? AppConstant.DEFAULT_CURRENT_PAGE : chinaCountStatisticsParam.getCurrentPage() - 1,
                null == chinaCountStatisticsParam.getPageSize() ? AppConstant.DEAULT_PAGE_SIZE : chinaCountStatisticsParam.getPageSize(),
                sort);
        Page<ChinaCountStatisticsDTO> page = chinaCountStatisticsService.findChinaCountStatPage(chinaCountStatisticsParam, pageable);

        return new Result().success(page);
    }
}
