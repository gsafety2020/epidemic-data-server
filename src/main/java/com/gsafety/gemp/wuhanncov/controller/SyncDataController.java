package com.gsafety.gemp.wuhanncov.controller;

import com.gsafety.gemp.wuhanncov.contract.dto.Result;
import com.gsafety.gemp.wuhanncov.contract.service.SyncDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 数据同步加工任务控制类
 * @author liyanhong
 * @since 2020/2/12  14:23
 */
@Api(value = "数据同步加工任务接口", tags = {"数据同步加工任务接口"})
@RestController
@RequestMapping("/data/sync/")
@Slf4j
public class SyncDataController {

    @Resource
    private SyncDataService syncDataService;

    @ApiOperation(value = "同步加工疫情日报历史数据")
    @PostMapping("/area/daily")
    public Result syncAreaDailyData(String fileId){

        syncDataService.handleAreaDailyCsvfile(fileId);

        return  Result.builder().msg("OK").build();
    }
}
