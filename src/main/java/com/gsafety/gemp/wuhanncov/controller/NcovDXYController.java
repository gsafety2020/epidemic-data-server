package com.gsafety.gemp.wuhanncov.controller;

import com.gsafety.gemp.wuhanncov.constant.AppConstant;
import com.gsafety.gemp.wuhanncov.contract.dto.*;
import com.gsafety.gemp.wuhanncov.contract.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.*;

/**
 * 该类用于疫情大屏北京专用丁香园数据,用于实现疫情通用接口Rest API等能力
 *
 * @author zhoushuyan
 * @since 2020/2/7  13:10
 */
@Api(value = "丁香园数据疫情数据接口", tags = {"丁香园数据疫情数据接口"})
@RestController
@RequestMapping("/dxy/ncov/pub/sens/")
public class NcovDXYController {
    @Resource
    private NcovPublicSentimentDxyService ncovPublicSentimentDxyService;

    @Resource
    BasicReportInfoService basicReportInfoService;

    @Resource
    ChinaAreaCaseInfoService chinaAreaCaseInfoService;
    /**
     *
     * 获取指定地区的病例数据
     *
     * @author zhoushuyan
     *
     * @since 2020年1月26日 上午9:23:08
     *
     * @return
     */
    @ApiOperation(value = "获取指定地区的病例数据")
    @GetMapping("/findAreaByAreaIdsCount")
    public List<ChinaCountStatisticsDTO> findAreaByAreaIdsCount(@ApiParam(value = "多个区域编码，用'_'分割, 例如：100000_420000" ,example ="100000_420000")
                                                                @RequestParam(name = "areaIds", required = true) String areaIds) {
        List<String> areaIdList = new ArrayList<>();
        String[] areaArray = areaIds.split("_");
        for (String area : areaArray) {
            areaIdList.add(area.trim());
        }
        return ncovPublicSentimentDxyService.findAreaByAreaIdsCount(areaIdList);
    }
    /**
     *
     * 湖北城市病例统计
     *
     * @author zhoushuyan
     *
     * @since 2020年1月26日 上午9:23:08
     *
     * @return
     */
    @ApiOperation(value = "湖北-城市病例统计")
    @GetMapping("/findHBCityCount")
    public List<ChinaCountStatisticsJSON> findHBCityCount() {
        List<ChinaCountStatisticsDTO> list=ncovPublicSentimentDxyService.findHBCityCount();
        List<ChinaCountStatisticsJSON> listResult=new ArrayList<ChinaCountStatisticsJSON>();
        list.stream().forEach(temp -> {
            ChinaCountStatisticsJSON json=new ChinaCountStatisticsJSON();
            BeanUtils.copyProperties(temp,json);
            listResult.add(json);
        });
        return listResult;
    }

    /**
     *  用于地图上的患者数量热力图展示，查询父区域下各子区域的患者确诊数量接口
     *  数据来源 爬虫，数据可能不准确，表：china_count_statistics，location_dim
     * @author lyh
     * @since 2020/1/30 14:21
     * @param   areaParent 父区域行政区划编码
     * @return List<AreaHotDTO>
     */
    @ApiOperation(value = "查询指定父区域下子区域患者热力值列表，数据来源爬虫")
    @GetMapping("/location_patient_count_List")
    public List<AreaHotDTO> getAreaPatientCoutList(@ApiParam(value = "上级区域行政区划编码", example = "420000")
                                                   @RequestParam(name = "areaParent", required = true) String areaParent) {
        areaParent= AppConstant.getDimCode(areaParent);
        return ncovPublicSentimentDxyService.getAreaPatientCountListByParent(areaParent);
    }
    @ApiOperation(value = "全国指定省-市的定位和数据")
    @GetMapping("/getParentCountStatisticsBubbling")
    public List<ChinaCountStatisticsBubbling> getParentCountStatisticsBubbling(@ApiParam(value = "上级区域行政区划编码", example = "北京")
                                                                               @RequestParam(name = "areaName", required = true) String areaName){
        return ncovPublicSentimentDxyService.getParentCountStatisticsBubbling(areaName);
    }
    @ApiOperation(value = "北京市的专题新闻列表")
    @GetMapping("/getReportInfos")
    public List<BasicReportInfoDTO> getReportInfos(@ApiParam(value = "渠道 1:12345热线 2:北京市应急局 110119:延庆区", example = "1")
                                                                               @RequestParam(name = "channel", required = true) String channel,
                                                   @ApiParam(value="返回最新的N条信息，-1所有的信息",example = "10" ) @RequestParam(name="topSize",required = true) int topSize){
        return basicReportInfoService.getReportInfos(channel,topSize);
    }

    /**
     * 查询X地区最新的前N位病例
     * 数据来源表：china_area_case_info
     * @author zhoushuyan
     * @since 2020/2/6 14:21
     * @param   areaName 排查区域行政区划编码列表
     * @param   date 前N位
     * @return java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.ChinaAreaCaseInfoValue>
     * @throws
     */
    @ApiOperation(value = "查询X地区按照名称匹配最新病例数据")
    @GetMapping("/getChinaAreaCaseInfoListByAreaName")
    public List<ChinaAreaCaseInfoValue> getChinaAreaCaseInfoList(@ApiParam(value = "区域编码, 例如北京市", example = "北京市")
                                                                  @RequestParam(value = "areaName",required = true) String areaName,
                                                                  @ApiParam(value = "时间日期yyyy-mm-dd, -1就是所有的时间", example = "2020-02-05") @RequestParam(value = "date", required = true) String date) {

        return chinaAreaCaseInfoService.getChinaAreaCaseInfoListByAreaName(areaName,date);

    }

    @ApiOperation(value = "获得病例最新数据默认时间 yyyy-mm-dd")
    @GetMapping("/getCaseInfoDefaultDate")
    public List<Map<String,String>> getCaseInfoDefaultDate(@ApiParam(value = "区域编码, 例如北京市：110000", example = "110000")
                                                               @RequestParam(value = "areaNo",required = true) String areaNo){
        List<Map<String,String>> list=new ArrayList<>();
        String dateStr=chinaAreaCaseInfoService.getCaseInfoDefaultDate(areaNo);
        Map<String,String> map=new HashMap<String, String>();
        map.put("time",dateStr);
        list.add(map);
        return list;
    }

}
