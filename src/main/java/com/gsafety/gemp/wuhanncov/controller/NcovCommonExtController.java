package com.gsafety.gemp.wuhanncov.controller;


import com.gsafety.gemp.wuhanncov.constant.AppConstant;
import com.gsafety.gemp.wuhanncov.contract.dto.*;
import com.gsafety.gemp.wuhanncov.contract.service.PatientAreaStatisticsExtService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import java.util.List;
import java.util.Map;

/**
 * 该类用于疫情大屏的扩展接口
 *
 * @author fanlx
 * @since 2020/02/07  16:30
 */
@Api(value = "疫情数据服务通用扩展接口", tags = {"疫情数据服务通用扩展接口"})
@RestController
@RequestMapping("/ncov/v3/common/")
public class NcovCommonExtController {

    @Resource
    private PatientAreaStatisticsExtService patientAreaStatisticsExtService;


    /**
     * 查询单个城市的新增确诊病例变化图，折线图展示
     * 数据来源表：patient_area_statistic
     * @param   areaId 指定区域行政区划编码
     * @author Luoganggang
     * @since 2020/2/8 12:08
     */
    @ApiOperation(value = "查询单个城市的新增确诊病例变化图，主要用于折线图")
    @GetMapping("/new_patient_areas_history_count")
    public List<PatientAreaCountDailyDTO> getNewPatientAreaListDailyCount(@ApiParam(value = "区域编码, 例如：420100", example = "420100")
                                                                          @RequestParam(value = "area_id") String areaId) {
        areaId= AppConstant.getDimCode(areaId);
        return patientAreaStatisticsExtService.getNewAreaListPatientCountHistory(areaId);
    }


    /**
     * localhost:3001/ncov/v3/common/showCitySickRiseRate?areaCode=420100
     * @param areaCode
     * @return
     */
    @ApiOperation(value = "查询市-省-全国累计病例数增长率")
    @GetMapping("/showCitySickRiseRate")
    public Map<String,Object> showCitySickRiseRate(@RequestParam(value = "areaCode") String areaCode){
        areaCode= AppConstant.getDimCode(areaCode);
        return patientAreaStatisticsExtService.showCitySickRiseRate(areaCode);
    }

    /**
     * localhost:3001/ncov/v3/common/showCitySickDeathRate?areaCode=420100
     * @param areaCode
     * @return
     */
    @ApiOperation(value = "查询市-省-全国死亡比例对比图")
    @GetMapping("/showCitySickDeathRate")
    public Map<String,Object> showCitySickDeathRate(@RequestParam(value = "areaCode") String areaCode) {
        areaCode= AppConstant.getDimCode(areaCode);
        return patientAreaStatisticsExtService.showCitySickDeathRate(areaCode);
    }


    /**
     * 多个行政区病患确诊数据占全国确诊病例比例变化接口，查询多个区域患者确诊数量比例历史数据
     * 可以用于查询指定多个区域的患者确诊数量历史比例情况，折线图展示
     * 数据来源表：patient_area_statistic_ext
     * @author dusiwei
     * @since 2020/2/7 14:21
     * @param   areaId 指定城市行政区划编码
     * @return java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaCaseRateDailyDTO>
     * @throws
     */
    @ApiOperation(value = "查询多个行政区病患确诊数据比例变化接口，主要用于折线图")
    @GetMapping("/patient_areas_case_rate")
    public List<PatientAreaCaseRateDailyDTO> getAreaListPatientCaseRate(@ApiParam(value = "城市编码，例如：420100", example = "420100")
                                                                        @RequestParam(value = "area_id") String areaId) {
        areaId= AppConstant.getDimCode(areaId);
        return patientAreaStatisticsExtService.getAreaListPatientCaseRate(areaId);
    }

    /**
     * 多个行政区新增病例数与累计病例数比例变化接口
     * 可以用于查询多个行政区新增病例数与累计病例数比例变化接口，主要用于折线图
     * 数据来源表：patient_area_statistic_ext
     * @author dusiwei
     * @since 2020/2/7 14:21
     * @param   areaId 指定区域行政区划编码列表
     * @return java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaNewTotalRateDailyDTO>
     * @throws
     */
    @ApiOperation(value = "查询多个行政区新增病例数与累计病例数比例变化接口，主要用于折线图")
    @GetMapping("/patient_areas_new_total_rate")
    public List<PatientAreaNewTotalRateDailyDTO> getAreaListPatientNewTotalRate(@ApiParam(value = "城市编码, 例如：420100", example = "420100")
                                                                                @RequestParam(value = "area_id") String areaId) {
        areaId= AppConstant.getDimCode(areaId);
        return patientAreaStatisticsExtService.getAreaListPatientNewTotalRate(areaId);
    }

}
