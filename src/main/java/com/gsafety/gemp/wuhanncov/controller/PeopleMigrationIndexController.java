package com.gsafety.gemp.wuhanncov.controller;

import com.google.common.collect.Lists;
import com.gsafety.gemp.wuhanncov.contract.dto.MigrationIndex;
import com.gsafety.gemp.wuhanncov.contract.dto.MigrationIndexDTO;
import com.gsafety.gemp.wuhanncov.contract.service.PeopleMigrationIndexService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 全国人口迁移指数
 *
 * @author: WangGang
 * @since 2020-02-06 21:10
 */
@Api(value = "全国人口迁移指数", tags = {"全国人口迁移指数"})
@RestController
@RequestMapping("/peoplemigrationindex")
@Slf4j
public class PeopleMigrationIndexController {

    @Resource
    private PeopleMigrationIndexService popleMigrationIndexService;

    @ApiOperation(value = "查询全国人口迁移指数趋势")
    @PostMapping(value = "/list")
    public List<MigrationIndex> popleMigrationTrend(@RequestBody MigrationIndexDTO migrationIndexDTO) {
        return popleMigrationIndexService.findMigrationIndexList(migrationIndexDTO);
    }
}
