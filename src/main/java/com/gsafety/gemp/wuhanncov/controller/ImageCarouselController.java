package com.gsafety.gemp.wuhanncov.controller;


import com.google.common.collect.Lists;
import com.gsafety.gemp.wuhanncov.constant.AppConstant;
import com.gsafety.gemp.wuhanncov.contract.dto.ImageCarouselDTO;
import com.gsafety.gemp.wuhanncov.contract.service.CityImageCarouselService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(value = "图片轮播接口接口", tags = {"图片轮播接口接口"})
@RestController
@RequestMapping("/ncov/carousel/")
public class ImageCarouselController {

    @Autowired
    private CityImageCarouselService cityImageCarouselService;


    @ApiOperation(value = "图片轮播接口接口")
    @GetMapping("/foshanCity")
    public List<ImageCarouselDTO> carouselList(@RequestParam("areaCode") String areaCode) {
        areaCode= AppConstant.getDimCode(areaCode);
        List<ImageCarouselDTO>  list = cityImageCarouselService.listCarouselByAreaCode(areaCode);
        return list;
    }

}
