package com.gsafety.gemp.wuhanncov.controller;

import com.gsafety.gemp.wuhanncov.constant.AppConstant;
import com.gsafety.gemp.wuhanncov.contract.dto.*;
import com.gsafety.gemp.wuhanncov.contract.service.NcovPublicSentimentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 世界ncov疫情数据接口控制类
 *
 * @author lyh
 * @since 2020/1/27  20:36
 * <p>
 * 1.0.0
 */
@Api(value = "新型冠状病毒世界数据接口", tags = { "新型冠状病毒世界数据接口" })
@RestController
@RequestMapping("/world/ncov/")
@Slf4j
public class NcovWorldController {

    @Resource
    private NcovPublicSentimentService ncovPublicSentimentService;

    @ApiOperation(value = "查询区域患者人数按天统计数据")
    @GetMapping("/patient_area_daily_count")
    public List<PatientAreaCountDailyDTO> getPatientAreaDailyCount(@RequestParam(name="areaCode", required = false)String areaCode,
                                                                   @RequestParam(name="type", required = false)String type){
        if(StringUtils.isEmpty(areaCode)){
            areaCode = "F";
        }

        if(StringUtils.isEmpty(type)){
            type = "1";
        }
        areaCode= AppConstant.getDimCode(areaCode);
        return ncovPublicSentimentService.getPatientAreaCountDaily(areaCode, type);
    }

    @ApiOperation(value = "获得世界热力图数据")
    @GetMapping("/getWorldHotMap")
    public List<WorldHotMap> getWorldHotMap(){
        return ncovPublicSentimentService.getWorldHotMap();
    }

    @ApiOperation(value = "获得国际国家信息图数据")
    @GetMapping("/getInternationalLocationValue")
    public List<LocationValue> getInternationalLocationValue(){
//        ChinaCountStatisticsDTO china=ncovPublicSentimentService.findChinaCount();
//        ChinaCountStatisticsDTO beijing=ncovPublicSentimentService.findBJCount();
//        ChinaCountStatisticsDTO wuhan=ncovPublicSentimentService.findWHCount();
        List<LocationValue> list=ncovPublicSentimentService.findChinaProvinceLocationValue();
//        LocationValue chinaValue=new LocationValue();
//        chinaValue.setLat(new BigDecimal(114.291900));
//        chinaValue.setLng(new BigDecimal(30.567500));
//        chinaValue.setValue(china.getConfirmCase());
//        list.add(chinaValue);
//        LocationValue beijingValue=new LocationValue();
//        beijingValue.setLat(new BigDecimal(116.380900));
//        beijingValue.setLng(new BigDecimal(39.923600));
//        beijingValue.setValue(beijing.getConfirmCase());
//        list.add(beijingValue);
//        LocationValue wuhanValue=new LocationValue();
//        wuhanValue.setLat(new BigDecimal(114.291900));
//        wuhanValue.setLng(new BigDecimal(30.567500));
//        wuhanValue.setValue(wuhan.getConfirmCase());
//        list.add(wuhanValue);
        list.addAll(ncovPublicSentimentService.getInternationalLocationValue());
        return list;
    }
    @ApiOperation(value = "飞线图-武汉到国际")
    @GetMapping("/getFlyLineByWutoInternationalLocation")
    public Collection<MigrateMapContent.MigrateMap> getFlyLineByWutoInternationalLocation(){
        String from = "114.2919,30.5675"; //默认武汉
        return ncovPublicSentimentService.getFlyLineByWutoInternationalLocation(from).getMigrateMaps();
    }
    @ApiOperation(value = "获得国际舆论前50最新列表")
    @GetMapping("/getWorldRealBroadcast")
    public List<WorldRealBroadcastDTO> getWorldRealBroadcastTop(){
        return ncovPublicSentimentService.getWorldRealBroadcastTop();
    }
}
