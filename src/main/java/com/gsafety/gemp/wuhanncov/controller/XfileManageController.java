package com.gsafety.gemp.wuhanncov.controller;

import com.gsafety.gemp.wuhanncov.constant.AppConstant;
import com.gsafety.gemp.wuhanncov.contract.dto.*;
import com.gsafety.gemp.wuhanncov.contract.enums.FileBnTypeEnum;
import com.gsafety.gemp.wuhanncov.contract.params.BasicFileQueryParam;
import com.gsafety.gemp.wuhanncov.contract.params.FileParam;
import com.gsafety.gemp.wuhanncov.contract.service.BasicFileInfoService;
import com.gsafety.gemp.wuhanncov.contract.service.XfileManageService;
import com.gsafety.gemp.wuhanncov.dao.po.FilesManagePO;
import com.gsafety.gemp.wuhanncov.utils.CsvUtils;
import com.gsafety.gemp.wuhanncov.utils.DateUtils;
import com.gsafety.gemp.wuhanncov.utils.UUIDUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 静态文件素材管理
 *
 * @author liyanhong
 * @since 2020/2/5  19:54
 */
@Api(value = "静态文件素材管理", tags = {"静态文件素材管理接口"})
@RestController
@RequestMapping("/ncov/file")
public class XfileManageController {
    @Resource
    BasicFileInfoService basicFileInfoService;
    @Resource
    private XfileManageService xfileManageService;

    @Value("${static.file.upload.path}")
    private String filePrefixPath;

    @Value("${static.file.url.prefix}")
    private String accessUrlPrefix;

    @Value("${alioos.ObjectName}")
    private String objectName;

    @Value("${alioos.ReportObjectName}")
    private String reportObjectName;


    @PostMapping("/upload")
    @ApiOperation(value = "上传静态素材文件")
    public Result fileUpload(@RequestParam MultipartFile upFile, FileParam fileParam) throws Exception {

        FilesManagePO po = new FilesManagePO();
        BeanUtils.copyProperties(fileParam, po);
        po.setId(UUIDUtils.getUUID());
        po.setOrigName(upFile.getOriginalFilename());
        po.setExtension(getFileExtension(upFile.getOriginalFilename()));

        //上传文件到oss服务器，并保存返回文件可访问url
        String fileName=generateFileServerName(po);
        String ossFileUrl = basicFileInfoService.ossFileUpdate(upFile, objectName,fileName);
        po.setPath(ossFileUrl);

        //文件信息保存
        xfileManageService.addFileInfo(po);

        FileInfoDTO dto = FileInfoDTO.builder()
                .fileId(po.getId())
                .fileUrl(po.getPath())
                .build();

        if (FileBnTypeEnum.Area_daily_csv.getTypeCode().equals(fileParam.getBnType())){
            dto.setData(CsvUtils.ReadCsv(upFile.getInputStream()));
        }

        return Result.builder().msg("OK").data(dto).build();
    }

    @PostMapping("/info")
    @ApiOperation(value = "查询静态素材文件信息")
    public List<FileInfoDTO> getFileInfo(@RequestBody FileParam fileParam) {
        List<FilesManagePO> poList = xfileManageService.findFiles(fileParam);

        List<FileInfoDTO> resultList = new ArrayList<>();
        FileInfoDTO dto = null;
        for (FilesManagePO po : poList) {
            dto = new FileInfoDTO();
            dto.setFileId(po.getId());
            dto.setFileUrl(po.getPath());
        }
        return resultList;
    }

    @PostMapping("/edit")
    @ApiOperation(value = "更改静态素材文件信息")
    public Result updateFileInfo(@RequestBody FileParam fileParam) {
        xfileManageService.updateFileInfo(fileParam);
        return Result.builder().msg("OK").build();
    }

    private String getFileExtension(String fileName) {
        return fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
    }

    private String buildFilePath(FilesManagePO po) {
        StringBuilder strB = new StringBuilder();
        strB.append(File.separator).append(po.getAreaCode())
                .append(File.separator).append(po.getBnType())
                .append(File.separator).append(po.getExtension())
                .append(File.separator).append(DateUtils.format(new Date(), "yyyyMMddHHmmss"))
                .append("_").append(RandomStringUtils.randomNumeric(5))
                .append(".").append(po.getExtension());
        return strB.toString();
    }

    private String generateFileServerName(FilesManagePO po) {
        StringBuilder strB = new StringBuilder();
        strB.append(DateUtils.format(new Date(), "yyyyMMddHHmmss"))
                .append("_").append(RandomStringUtils.randomNumeric(5))
                .append(".").append(po.getExtension());
        return strB.toString();
    }

    /**
     *
     * 获取疫情分析文件
     *
     * @author zhoushuyan
     *
     * @since 2020年2月11日 下午11:07:01
     *
     * @param
     * @return
     */
    @ApiOperation(value = "获取当前疫情文件")
    @GetMapping("/getNcovOOSFile")
    public List<OSSFileDto> getNcovOSSFile(@ApiParam(value = "文件类型", example = "0")
                                           @RequestParam(value = "fileType", required = false) String fileType) {
        if(StringUtils.isEmpty(fileType)){
            fileType="0";
        }
        return basicFileInfoService.getNcovOSSFile(fileType);
    }

    /**
     *
     * 获取疫情分析文件
     *
     * @author zhoushuyan
     *
     * @since 2020年2月11日 下午11:07:01
     *
     * @param
     * @return
     */
    @ApiOperation(value = "变更当前疫情趋势与疫情分析文件")
    @PostMapping("/updateNcovOOSFile")
    public Result updateNcovOSSFile(@ApiParam(value = "文件信息对象")
                                    @RequestBody(required = true) BasicFileInfoParam fileInfo) {
        return basicFileInfoService.updateNcovOSSFile(fileInfo);
    }
    @PostMapping("/oosReportupload")
    @ApiOperation(value = "上传文件到oos服务器-分析与趋势文件专用")
    public Result ossFxFileUpload(@RequestParam MultipartFile upFile,
                                @ApiParam(value = "文件名称")
                                @RequestParam(value="fileName",required = false) String  fileName) {
        String suffix = upFile.getOriginalFilename().substring(upFile.getOriginalFilename().lastIndexOf("."));
        if(StringUtils.isNotEmpty(fileName)){
            if(fileName.indexOf(".")<0) {
                fileName = fileName + suffix;//自动加上后缀名
            }
        }else{
//            return Result.builder().msg("请输入正确的文件名").build();
            fileName = upFile.getOriginalFilename();
        }
        String fileUrl=basicFileInfoService.ossFileUpdate(upFile,reportObjectName,fileName);
        return new Result().success("OK", fileUrl);
    }

    @PostMapping("/oosupload")
    @ApiOperation(value = "上传文件到oos服务器")
    public Result oosFileUpload(@RequestParam MultipartFile upFile,
                                @ApiParam(value = "文件名称")
                                @RequestParam(value="fileName",required = true) String  fileName) {
        String fileUrl=basicFileInfoService.ossFileUpdate(upFile,objectName,fileName);
        return Result.builder().msg("OK").data(fileUrl).build();
    }
    @GetMapping("/findAreaFile")
    @ApiOperation(value = "获得趋势地区的各类文件")
    public List<OSSFileDto> findAreaFile(@ApiParam(value = "地区可以是多个用_隔开，例如全国和四川：100000_510000",example = "100000_510000")
                                @RequestParam(value="areaIds",required = true) String  areaIds,
                                         @ApiParam(value = "文件类型,趋势分析：0，疫情分析:1   趋势分析建议图:2 趋势分析折线图:3", example = "0")
                                         @RequestParam(value = "fileType", required = true) String fileType) {
        List<String> areaIdList = new ArrayList<>();
        String[] areaArray = areaIds.split("_");
        for (String area : areaArray) {
            String code= AppConstant.getDimCode(area.trim());
            areaIdList.add(code);
        }
        return basicFileInfoService.findAreaFile(areaIdList,fileType);
    }

    @PostMapping("/findAllFileInfo")
    @ApiOperation(value = "获得各类文件分页列表")
    public Result findAllFileInfo(/*@ApiParam(value = "文件名称或者文件名称", example = "趋势")
                                         @RequestParam(value = "areaName", required = false) String areaName,
                                               @ApiParam(value = "每页数量 第一页传0 ",required = true, example = "0")
                                          @RequestParam("pageNum") int pageNum,
                                               @ApiParam(value = "每页数量, 例如：10,不限制传-1", example = "10")
                                           @RequestParam("pageSize")  int pageSize*/@RequestBody BasicFileQueryParam basicFileQueryParam) {
        Page<OSSAllFileDto> page = basicFileInfoService.findAllFileInfo(basicFileQueryParam);
        return new Result().success(page);
    }
}
