package com.gsafety.gemp.wuhanncov.controller;

import com.gsafety.gemp.wuhanncov.constant.AppConstant;
import com.gsafety.gemp.wuhanncov.contract.dto.DataSyncLogDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaStatisticDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.Result;
import com.gsafety.gemp.wuhanncov.contract.params.DataSyncLogParam;
import com.gsafety.gemp.wuhanncov.contract.service.DataSyncLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 该类用于数据同步任务的日志查询,用于实现提供Rest API等能力
 *
 * @author xiongjinhui
 * @since 2020/2/12  13:14
 * <p>
 * 查询数据同步任务日志控制器
 */

@Api(value = "查询数据同步任务日志", tags = {"查询数据同步任务日志"})
@RestController
@RequestMapping("/wuhan/ncov/")
public class DataSyncLogController {

    @Resource
    DataSyncLogService dataSyncLogService;

    @ApiOperation(value = "查询数据同步任务日志")
    @PostMapping("/dataSyncLog")
    public Result findDataSyncLogPage(@RequestBody DataSyncLogParam dataSyncLogParam){
        Sort sort = Sort.by(Sort.Direction.DESC, "updateTime");
        Pageable pageable = PageRequest.of( null == dataSyncLogParam.getCurrentPage() ? AppConstant.DEFAULT_CURRENT_PAGE : dataSyncLogParam.getCurrentPage() - 1,
                null == dataSyncLogParam.getPageSize() ? AppConstant.DEAULT_PAGE_SIZE : dataSyncLogParam.getPageSize(),
                sort);
        Page<DataSyncLogDTO> page = dataSyncLogService.findDataSyncLogPage(dataSyncLogParam, pageable);

        return new Result().success(page);
    }
}
