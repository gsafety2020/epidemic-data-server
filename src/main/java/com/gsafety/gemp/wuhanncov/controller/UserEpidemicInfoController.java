package com.gsafety.gemp.wuhanncov.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.gsafety.gemp.wuhanncov.contract.dto.Result;
import com.gsafety.gemp.wuhanncov.contract.dto.UserEpidemicInfoDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.UserEpidemicInfoPartDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.in.UserEpidemicInfoQueryDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.in.UserEpidemicInfoQueryUserDTO;
import com.gsafety.gemp.wuhanncov.contract.service.UserEpidemicInfoService;
import com.gsafety.gemp.wuhanncov.contract.service.UserEpidemicListService;
import com.gsafety.gemp.wuhanncov.dao.po.UserEpidemicInfoPO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 
* @ClassName: UserEpidemicInfoController 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author luoxiao
* @date 2020年2月3日 上午3:32:26 
*
 */
@Api(value = "新型冠状病毒用户信息上报接口", tags = { "新型冠状病毒用户信息上报接口" })
@RestController
@RequestMapping("/user/epidemic/")
public class UserEpidemicInfoController {

	@Autowired
	private UserEpidemicInfoService userEpidemicInfoService;
	
	@Autowired
	private UserEpidemicListService userEpidemicListService;
		
	@ApiOperation(value = "信息上报保存接口")
	@PostMapping("/add")
	public Result add(@RequestBody UserEpidemicInfoPO po){
		try{
			userEpidemicInfoService.saveUserEpidemicInfo(po);
			return new Result().success("信息上报成功!");
		}catch(Exception e){
			return new Result().fail(e.getMessage());
		}
	}
	
	@ApiOperation(value = "信息上报分页接口")
	@PostMapping("/findByPage")
	public Result findByPage(@RequestBody UserEpidemicInfoQueryDTO dto,Integer pageSize,
			Integer pageNumber){
		try{
			Map<String,Object>  map = userEpidemicInfoService.findAllByPage(dto, pageSize, pageNumber);
			return new Result().success("查询成功!", map);
		}catch(Exception e){
			return new Result().fail(e.getMessage());
		}
	}
	
	@ApiOperation(value = "查询详细信息")
	@PostMapping("/findByEpidemicInfoId")
	public Result findByEpidemicInfoId(@RequestParam String epidemicInfoId){
		try{
			UserEpidemicInfoDTO  dto = userEpidemicInfoService.findByEpidemicInfoId(epidemicInfoId);
			return new Result().success("查询成功!", dto);
		}catch(Exception e){
			return new Result().fail(e.getMessage());
		}
	}
	
	@ApiOperation(value = "查询人员已经填写过的信息")
	@PostMapping("/getReportedMessage")
	public Result getReportedMessage(@RequestParam String userId){
		try{
			UserEpidemicInfoPartDTO  dto = userEpidemicInfoService.getReportedMessage(userId);
			return new Result().success("查询成功!", dto);
		}catch(Exception e){
			return new Result().fail(e.getMessage());
		}
	}
	
	@ApiOperation(value = "查询所有的部门名称")
	@PostMapping("/getAllDepartmentName")
	public Result getAllDepartmentName(String mobile){
		try{
			List<String>  departments= userEpidemicInfoService.getAllDepartmentName(mobile);
			return new Result().success("查询成功!", departments);
		}catch(Exception e){
			return new Result().fail(e.getMessage());
		}
	}
	
	@ApiOperation(value = "查询所有的异常人员")
	@PostMapping("/getAllNotNormalUser")
	public Result getAllNotNormalUser(@RequestBody UserEpidemicInfoQueryUserDTO dto){
		try{
			Map<String,Object>  map = userEpidemicInfoService.getAllNotNormalUser(dto);
			return new Result().success("查询成功!", map);
		}catch(Exception e){
			return new Result().fail(e.getMessage());
		}
	}
	
	@ApiOperation(value = "查询人员当天是否已经填报")
	@PostMapping("/isReportedByToday")
	public Result isReportedByToday(@RequestParam("userId")String userId){
		try{
			Boolean flag = userEpidemicInfoService.isReportToday(userId);
			return new Result().success("查询成功!", flag);
		}catch(Exception e){
			return new Result().fail(e.getMessage());
		}
	}
	
	
	@ApiOperation(value = "折线图统计")
	@PostMapping("/brokenLine")
	public Result isAlreadyReported(String mobile,int pastDays){
		try{
			JSONObject jsonObject = userEpidemicInfoService.statisticHistoryBrokenLine(mobile, pastDays);
			return new Result().success("查询成功!", jsonObject);
		}catch(Exception e){
			return new Result().fail(e.getMessage());
		}
	}
	
	@ApiOperation(value = "同步综合平台用户数据到上报表")
	@PostMapping("/synchroData")
	public Result synchroData(){
		try{
			userEpidemicListService.synchroListToInfo();
			return new Result().success("同步成功!");
		}catch(Exception e){
			return new Result().fail(e.getMessage());
		}
	}
}
