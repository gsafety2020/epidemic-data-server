package com.gsafety.gemp.wuhanncov.controller;

import com.gsafety.gemp.wuhanncov.constant.AppConstant;
import com.gsafety.gemp.wuhanncov.contract.dto.ChinaAreaMaterialDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.MaterialRequirementDTO;
import com.gsafety.gemp.wuhanncov.contract.service.ChinaAreaMaterialService;
import com.gsafety.gemp.wuhanncov.contract.service.CityMaterialService;
import com.gsafety.gemp.wuhanncov.contract.service.MaterialRequirementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * 物资需求controller
 * @author zhangay
 * @date 2020-01-31 20:25:17
 */
@Api(value = "物资需求接口", tags = {"物资需求接口"})
@RestController
@RequestMapping("/wuhan/material_requirement/")
public class MaterialRequirementController {

    @Autowired
    private MaterialRequirementService materialRequirementService;

    @Autowired
    private ChinaAreaMaterialService chinaAreaMaterialService;


    @Autowired
    private CityMaterialService cityMaterialService;

    /***
     * 指定区域及其下级区域的物资需求情况列表
     * 数据来源表：material_requirement
     * @author zhangay
     * @since 2020-01-31 20:26:27
     * @param   areaNo 指定区域行政区划编码
     * @return java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.MaterialRequirementDTO>
     *     status=1 新增;status=2 进行中;status=9 已完成;
     * @throws
     */
    @ApiOperation(value = "指定区域及其下级区域的物资需求情况列表")
    @GetMapping("/include_sub_list")
    public List<MaterialRequirementDTO> getListIncludeSubByAreaNo(@ApiParam(value = "单个区域行政区划编码", example = "420115")
                                                                  @RequestParam("areaNo") String areaNo) {
        areaNo= AppConstant.getDimCode(areaNo);
        return materialRequirementService.selectIncludeSubByAreaNo(areaNo);
    }
//    @ApiOperation(value = "指定区域及物资情况列表")
//    @GetMapping("/findAreaMaterials")
//    public Page<ChinaAreaMaterialDTO> findAreaMaterials(@ApiParam(value = "页码",required = true, example = "1")
//                                                            @RequestParam("pageNum") int pageNum,
//                                                        @ApiParam(value = "每页数量",required = true, example = "10")
//                                                            @RequestParam("pageSize")  int pageSize,
//                                                        @ApiParam(value = "单个区域行政区划编码",required = true, example = "440000_440600")
//                                                            @RequestParam("areaNos") String areaNos) {
//        if(pageNum>0){
//            pageNum=pageNum-1;
//        }
//        List<String> areaIdList = new ArrayList<>();
//        String[] areaArray = areaNos.split("_");
//        for (String area : areaArray) {
//            areaIdList.add(area.trim());
//        }
//        return chinaAreaMaterialService.findAreaMaterialsPage(pageNum,pageSize,areaIdList);
//    }
    @ApiOperation(value = "指定区域及物资情况列表")
    @GetMapping("/findAreaMaterials")
    public List<ChinaAreaMaterialDTO> findAreaMaterials(@ApiParam(value = "单个区域行政区划编码",required = true, example = "440000_440600")
                                                            @RequestParam("areaNos") String areaNos,
                                                        @ApiParam(value = "物资状态，默认是5",required = false, example = "5" )
                                                        @RequestParam("statusCd") String statusCd) {
        List<String> areaIdList = new ArrayList<>();
        String[] areaArray = areaNos.split("_");
        for (String area : areaArray) {
            areaIdList.add(area.trim());
        }
        if(StringUtils.isEmpty(statusCd)){
            statusCd="5";
        }
        List<ChinaAreaMaterialDTO> list=chinaAreaMaterialService.findAreaMaterials(areaIdList,statusCd);
        List<ChinaAreaMaterialDTO> materials=new ArrayList<ChinaAreaMaterialDTO>();
        List<ChinaAreaMaterialDTO> materialList=new ArrayList<ChinaAreaMaterialDTO>();
        for (int i=0;i<list.size();i++) {
            ChinaAreaMaterialDTO material=list.get(i);
            String mname=material.getMaterialName();
            if(mname.contains("其他")){
                materials.add(material);
             }else{
                materialList.add(material);
            }
        }
        materialList.addAll(materials);
        return materialList;
    }

    @ApiOperation(value = "佛山区域区域及物资情况列表")
    @GetMapping("/findFsCityMaterials")
    public List<ChinaAreaMaterialDTO> findFsCityMaterials() {
        List<String> areaIdList = new ArrayList<>();
        String[] areaArray = "440000_440600".split("_");
        for (String area : areaArray) {
            areaIdList.add(area.trim());
        }
        List<ChinaAreaMaterialDTO> list=cityMaterialService.findAreaMaterials(areaIdList);
        List<ChinaAreaMaterialDTO> materials=new ArrayList<>();
        List<ChinaAreaMaterialDTO> materialList=new ArrayList<>();
        for (int i=0;i<list.size();i++) {
            ChinaAreaMaterialDTO material=list.get(i);
            String mname=material.getMaterialName();
            if(mname.contains("其他")){
                materials.add(material);
            }else{
                materialList.add(material);
            }
        }
        materialList.addAll(materials);
        return materialList;
    }
}
