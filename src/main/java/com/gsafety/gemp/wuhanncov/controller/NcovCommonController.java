package com.gsafety.gemp.wuhanncov.controller;

import com.gsafety.gemp.wuhanncov.constant.AppConstant;
import com.gsafety.gemp.wuhanncov.contract.dto.*;
import com.gsafety.gemp.wuhanncov.contract.service.*;
import com.gsafety.gemp.wuhanncov.dao.po.LocationDimPO;
import com.gsafety.gemp.wuhanncov.utils.DateUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.*;

/**
 * 该类用于疫情大屏数据,用于实现疫情通用接口Rest API等能力
 *
 * @author lyh
 * @since 2020/1/30  13:10
 */
@Api(value = "疫情数据服务通用接口", tags = {"疫情数据服务通用接口"})
@RestController
@RequestMapping("/ncov/common/")
public class NcovCommonController {

    @Resource
    private NcovCommonDataService ncovCommonDataService;

    @Resource
    private PatientAreaRuntimeService patientAreaRuntimeService;
    
    @Resource
    private ChinaHashDataService chinaHashDataService;

    @Resource
    private ChinaAreaCaseInfoService chinaAreaCaseInfoService;

    @Resource
    private NcovLocationDimService ncovLocationDimService;

    /**
     * 行政区病患状态拟合接口，查询区域患者情况按天统计历史数据
     * 可以用于查询指定单个区域的患者历史情况，折线图或者柱状图展示
     * 数据来源表：patient_area_statistic
     * @author lyh
     * @since 2020/1/30 14:21
     * @param   areaId 指定区域行政区划编码
     * @param   days 需要返回最近多少天的数据（不包含当天）
     * @param   types 指定数据类型列表，见枚举类NumberTypeEnum
     * @return java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaCountDailyDTO>
     *     s=1 确诊，s=2 治愈, s=3 重症, s=4 死亡, s=5 新增确诊
     * @throws
     */
    @ApiOperation(value = "查询单个区域患者情况按天统计历史数据")
    @GetMapping("/patient_area_history_info")
    public List<PatientAreaInfoDailyDTO> getPatientAreaDailyInfo(@ApiParam(value = "单个区域行政区划编码", example = "420000")
                                                                  @RequestParam("area_id") String areaId,
                                                                 @ApiParam(value = "需要展示最近多少天的历史数据（不包含当日）", example = "7") @RequestParam("days") Integer days,
                                                                 @ApiParam(value = "数据类型编码，用'_'分割, 例如：confirmed_cured_severe_death_new", example = "confirmed_cured_severe_death_new")
                                                                     @RequestParam(value = "types", required = false) String types) {
        if(StringUtils.isBlank(types)){
            types = "confirmed_cured_severe_death_new";
        }

        List<String> typesList = new ArrayList<>();
        String[] typeArray = types.split("_");
        for (String type : typeArray) {
            typesList.add(type.trim());
        }
        areaId=AppConstant.getDimCode(areaId);
        return ncovCommonDataService.getPatientAreaInfoHistory(areaId, days, typesList);
    }

    /**
     * 多个行政区病患确诊数据历史对比接口，查询多个区域患者确诊数量历史数据
     * 可以用于查询指定多个区域的患者确诊数量历史情况，折线图展示
     * 数据来源表：patient_area_statistic
     * @author lyh
     * @since 2020/1/30 14:21
     * @param   areaIds 指定区域行政区划编码列表
     * @return java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaCountDailyDTO>
     * @throws
     */
    @ApiOperation(value = "查询多个行政区病患确诊数据历史对比接口，主要用于折线图")
    @GetMapping("/patient_areas_history_count")
    public List<PatientAreaCountDailyDTO> getPatientAreaListDailyCount(@ApiParam(value = "多个区域编码，用'_'分割, 例如：100000_420000", example = "100000_420000")
                                                                       @RequestParam(value = "area_ids") String areaIds) {

        List<String> areaIdList = new ArrayList<>();
        String[] areaArray = areaIds.split("_");
        for (String area : areaArray) {
            String code=AppConstant.getDimCode(area.trim());
            areaIdList.add(code);
        }
        return ncovCommonDataService.getAreaListPatientCountHistory(areaIdList);
    }
    @ApiOperation(value = "查询多个行政区新增确诊数据历史对比接口，主要用于折线图")
    @GetMapping("/patient_areas_history_new_count")
    public List<PatientAreaCountDailyDTO> getPatientAreaListDailyNewCount(@ApiParam(value = "多个区域编码，用'_'分割, 例如：100000_420000", example = "100000_420000")
                                                                       @RequestParam(value = "area_ids") String areaIds) {

        List<String> areaIdList = new ArrayList<>();
        String[] areaArray = areaIds.split("_");
        for (String area : areaArray) {
            String code=AppConstant.getDimCode(area.trim());
            areaIdList.add(code);
        }
        List<PatientAreaCountDailyDTO> dtos=ncovCommonDataService.getAreaListPatientNewCountHistory(areaIdList);
        Collections.reverse(dtos);
        return  dtos;
    }
    /**
     * 多个行政区病患确诊数据历史对比接口，查询多个区域患者确诊数量历史数据
     * 可以用于查询指定多个区域的患者确诊数量历史情况，折线图展示
     * 数据来源表：patient_area_runtime
     * @author zhoushuyan
     * @since 2020/2/6 14:21
     * @param   excludeAreaIds 排查区域行政区划编码列表
     * @param   topSize 前N位
     * @return java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaCountDailyDTO>
     * @throws
     */
    @ApiOperation(value = "查询患病的前N位省份")
    @GetMapping("/patient_areas_top")
    public Collection<TopArea.TopAreaInfo>getPatientAreaTop(@ApiParam(value = "排除哪些省多个区域编码，用'_'分割, 例如：420000", example = "420000")
                                                                       @RequestParam(value = "area_ids",required = false) String excludeAreaIds,
                                @ApiParam(value = "前多少位排名, 例如：10,不限制传-1", example = "10") @RequestParam(value = "top_size") int topSize) {

        List<String> areaIdList = new ArrayList<>();
        if(StringUtils.isNotEmpty(excludeAreaIds)){
            String[] areaArray = excludeAreaIds.split("_");
            for (String area : areaArray) {
                areaIdList.add(area.trim());
            }
        }
        return ncovCommonDataService.getPatientAreaTop(areaIdList,0,topSize).getTopAreaInfo();
    }
    /**
     * 查询患病的前N位省份的确诊柱状图
     * 数据来源表：patient_area_statistic
     * @author zhoushuyan
     * @since 2020/2/6 14:21
     * @param   excludeAreaIds 排查区域行政区划编码列表
     * @param   week 前N位
     * @return java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaCountDailyDTO>
     * @throws
     */
    @ApiOperation(value = "查询患病的前N位省份的每周新增确诊柱状图")
    @GetMapping("/getPatientAreaListTop")
    public List<AreaWeekValue>getPatientAreaListTop(@ApiParam(value = "排除哪些省多个区域编码，用'_'分割, 例如：420000", example = "420000")
                                                            @RequestParam(value = "area_ids",required = false) String excludeAreaIds,
                                                           @ApiParam(value = "每页数量 第一页传0 ",required = true, example = "0")
                                                           @RequestParam("pageNum") int pageNum,
                                                            @ApiParam(value = "每页数量, 例如：16,不限制传-1", example = "16")
                                                            @RequestParam("pageSize")  int pageSize,
                                                            @ApiParam(value = "多少星期 0 本周 ，1上周 2 上上周", example = "0")
                                                             @RequestParam(value = "week",required = true) int week) {

        List<String> excludelist = new ArrayList<>();
        if(StringUtils.isNotEmpty(excludeAreaIds)){
            String[] areaArray = excludeAreaIds.split("_");
            for (String area : areaArray) {
                excludelist.add(area.trim());
            }
        }
//        //获得排名前x的省份
//        Collection<TopArea.TopAreaInfo> list=ncovCommonDataService.getPatientAreaTop(excludelist,pageNum,pageSize).getTopAreaInfo();
//        List<String> areaIdList = new ArrayList<>();
//        for (TopArea.TopAreaInfo info:list) {
//            areaIdList.add(info.getCode());
//        }
        return ncovCommonDataService.getPatientAreaListTop(excludelist,pageNum,pageSize,week);
    }

    /**
     * 多个行政区病患确诊数据历史对比接口，查询多个区域患者确诊数量历史数据
     * 可以用于查询指定多个区域的患者确诊数量历史情况，折线图展示
     * 数据来源表：patient_area_runtime
     * @author zhoushuyan
     * @since 2020/2/6 14:21
     * @param   excludeAreaIds 排查区域行政区划编码列表
     * @param   topSize 前N位
     * @return java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaCountDailyDTO>
     * @throws
     */
    @ApiOperation(value = "查询患病的前N位省份病患确诊数据历史对比接口，主要用于折线图")
    @GetMapping("/getPatientAreaTopDailyCount")
    public List<PatientAreaCountDailyDTO> getPatientAreaTopDailyCount(@ApiParam(value = "排除哪些省多个区域编码，用'_'分割, 例如：420000", example = "420000")
                                                                          @RequestParam(value = "area_ids",required = false) String excludeAreaIds,
                                                                      @ApiParam(value = "前多少位排名, 例如：10,不限制传-1", example = "10") @RequestParam(value = "top_size") int topSize) {

        List<String> exdAreaIdList = new ArrayList<>();
        if(StringUtils.isNotEmpty(excludeAreaIds)){
            String[] areaArray = excludeAreaIds.split("_");
            for (String area : areaArray) {
                exdAreaIdList.add(area.trim());
            }
        }
        Collection<TopArea.TopAreaInfo> list=ncovCommonDataService.getPatientAreaTop(exdAreaIdList,0,topSize).getTopAreaInfo();
        List<String> areaIdList = new ArrayList<>();
        for (TopArea.TopAreaInfo info:list) {
            areaIdList.add(info.getCode());
        }
        return ncovCommonDataService.getAreaListPatientCountHistory(areaIdList);

    }

    /**
     * 查询X地区最新的前N位病例
     * 数据来源表：china_area_case_info
     * @author zhoushuyan
     * @since 2020/2/6 14:21
     * @param   areaIds 排查区域行政区划编码列表
     * @param   topSize 前N位
     * @return java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaCountDailyDTO>
     * @throws
     */
    @ApiOperation(value = "查询X地区最新的前N位病例")
    @GetMapping("/getChinaAreaCaseInfoTopList")
    public List<ChinaAreaCaseInfoDTO> getChinaAreaCaseInfoTopList(@ApiParam(value = "多个区域编码，用'_'分割, 例如：420000", example = "420000")
                                                                      @RequestParam(value = "area_ids",required = false) String areaIds,
                                                                      @ApiParam(value = "前多少位排名, 例如：10,不限制传-1", example = "-1") @RequestParam(value = "top_size") int topSize) {

        List<String> areaIdList = new ArrayList<>();
        if(StringUtils.isNotEmpty(areaIds)){
            String[] areaArray = areaIds.split("_");
            for (String area : areaArray) {
                String code=AppConstant.getDimCode(area.trim());
                areaIdList.add(code);
            }
        }
        return chinaAreaCaseInfoService.getChinaAreaCaseInfoTopList(areaIdList,topSize);

    }

    /**
     *  用于地图上的患者数量热力图展示，查询父区域下各子区域的患者确诊数量接口
     *  数据来源 爬虫，数据可能不准确，表：china_count_statistics，location_dim
     * @author lyh
     * @since 2020/1/30 14:21
     * @param   areaParent 父区域行政区划编码
     * @return List<AreaHotDTO>
     */
    @Deprecated
    @ApiOperation(value = "查询指定父区域下子区域患者热力值列表，数据来源爬虫")
    @GetMapping("/location_patient_count_List")
    public List<AreaHotDTO> getAreaPatientCoutList(@ApiParam(value = "上级区域行政区划编码", example = "420000")
                                                   @RequestParam(name = "areaParent", required = true) String areaParent) {
        areaParent=AppConstant.getDimCode(areaParent);
        return ncovCommonDataService.getAreaPatientCountListByParent(areaParent);
    }

    /**
     *  用于地图上的患者数量热力图展示，查询父区域下各子区域的患者确诊数量接口
     *  数据来源人工维护，较准确，表：patient_area_runtime
     * @author lyh
     * @since 2020/1/30 14:21
     * @param   areaParent 父区域行政区划编码
     * @return List<AreaHotDTO>
     */
    @ApiOperation(value = "查询指定父区域下子区域患者热力值列表, 数据来源手工维护")
    @GetMapping("/locations_patient_count_runtime")
    public List<AreaHotDTO> getAreaPatientCoutFromRuntime(@ApiParam(value = "上级区域行政区划编码", example = "440300")
                                                   @RequestParam(name = "areaParent", required = true) String areaParent) {
        areaParent=AppConstant.getDimCode(areaParent);
        return ncovCommonDataService.getAreaPatientCountFromRuntimeByParent(areaParent);
    }

    /**
     * 查询指定区域实时病患情况
     *
     * @param areaCode
     * @return
     * @author yangbo
     * @since 2020年1月30日 下午9:30:49
     */
    @ApiOperation(value = "查询指定区域实时病患情况")
    @GetMapping("/runtime_patient_area")
    public ChinaCountStatisticsJSON runtimePatientArea(@RequestParam(name = "areaCode", required = true) String areaCode) {
        areaCode=AppConstant.getDimCode(areaCode);
        return patientAreaRuntimeService.findByAreaCode(areaCode);
    }

    /**
     * 查询指定区域下级区域的病患情况
     *
     * @param parentNo
     * @return
     * @author yangbo
     * @since 2020年1月30日 下午9:30:46
     */
    @ApiOperation(value = "查询指定区域下级区域的病患情况")
    @GetMapping("/runtime_patient_area_list")
    public List<ChinaCountStatisticsJSON> runtimePatientAreaList(@RequestParam(name = "parentNo", required = true) String parentNo) {
        parentNo=AppConstant.getDimCode(parentNo);
        return patientAreaRuntimeService.findByParentNo(parentNo);
    }

    /**
     * 查询指定区域下的新闻信息
     *
     * @param keyworlds
     * @param size
     * @return
     * @author zhoushuyan
     * @since 2020年02月06日 下午9:30:46
     */
    @ApiOperation(value = "查询指定区域下的新闻信息")
    @GetMapping("/findNewsInfo")
    public List<NewsInfo> findNewsInfo(@ApiParam(value = "新闻关键字可以用_隔开，例如 河南_信阳", example = "信阳")
                                           @RequestParam(name = "keyworks", required = false) String keyworlds,
                                       @ApiParam(value = "返回最新新闻的条数", example = "10")
                                       @RequestParam(name = "size", required = true) int size) {
        List<String> worldList = new ArrayList<>();
        if(StringUtils.isNotEmpty(keyworlds)){
            String[] areaArray = keyworlds.split("_");
            for (String area : areaArray) {
                worldList.add(area.trim());
            }
        }
        return ncovCommonDataService.findNewsInfo(worldList,size);
    }
    @ApiOperation(value = "全国指定省-市的定位和数据-气泡层")
    @GetMapping("/getParentCountStatisticsBubbling")
    public List<ChinaCountStatisticsBubbling> getParentCountStatisticsBubbling(@ApiParam(value = "上级区域行政区划编码 珠海市：440402", example = "440402")
                                                                               @RequestParam(name = "parentNo", required = true) String parentNo){
        parentNo=AppConstant.getDimCode(parentNo);
        return ncovCommonDataService.getParentCountStatisticsBubbling(parentNo);
    }
    
    /**
     * 
     * 获取当前行政区划的散列数据
     *
     * @author yangbo
     * 
     * @since 2020年2月8日 下午11:07:01
     *
     * @param areaCode
     * @return
     */
	@ApiOperation(value = "获取当前行政区划的散列数据")
	@GetMapping("/getHashData")
	public Map<String, Object> findHashData(
			@ApiParam(value = "本机行政区划", example = "420100") @RequestParam(name = "areaCode", required = true) String areaCode) {
        areaCode=AppConstant.getDimCode(areaCode);
		return this.chinaHashDataService.findMapByAreaCode(areaCode);
	}

    /**
     *
     * 获取当前行政区下的各个区域信息，可以聚合多个父区域
     *
     * @author yangbo
     *
     * @since 2020年2月8日 下午11:07:01
     *
     * @param parentNos
     * @return
     */
    @ApiOperation(value = "获取当前行政区下的各个区域信息，可以聚合多个父区域")
    @GetMapping("/findStatisticsByParentAreaCode")
    public List<AreaHotDTO> findStatisticsByParentAreaCode(
            @ApiParam(value = "多个父节区域用_隔开", example = "340102")
            @RequestParam(name = "parentNos", required = true) String parentNos) {
        List<String> areaIdList = new ArrayList<>();
        if(StringUtils.isNotEmpty(parentNos)){
            String[] areaArray = parentNos.split("_");
            for (String area : areaArray) {
                String code=AppConstant.getDimCode(area.trim());
                areaIdList.add(code);
            }
        }
        return this.patientAreaRuntimeService.findMapByParentAreaCode(areaIdList);
    }
    /**
     *
     * 获取当前行政区下的各个区域信息，可以聚合多个父区域
     *
     * @author yangbo
     *
     * @since 2020年2月8日 下午11:07:01
     *
     * @param parentNo
     * @return
     */
    @ApiOperation(value = "获取当前行政区下各个小区信息")
    @GetMapping("/findCommunityStatisticsByCityCode")
    public List<AreaHotDTO> findStatisticsByCityCode(
            @ApiParam(value = "区域code比如合肥市", example = "340100")
            @RequestParam(name = "parentNo", required = true) String parentNo) {
        List<LocationDimDTO> dtos=ncovLocationDimService.findByParentNo(parentNo);
        List<String> areaIdList = new ArrayList<>();
       for(LocationDimDTO dto:dtos){
           areaIdList.add(dto.getAreaNo());
       }
        return this.patientAreaRuntimeService.findMapByParentAreaCode(areaIdList);
    }

}
