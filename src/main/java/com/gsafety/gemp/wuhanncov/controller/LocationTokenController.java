package com.gsafety.gemp.wuhanncov.controller;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.gsafety.gemp.wuhanncov.constant.AppConstant;
import com.gsafety.gemp.wuhanncov.contract.dto.Result;
import com.gsafety.gemp.wuhanncov.contract.service.LocationTokenService;
import com.gsafety.gemp.wuhanncov.dao.po.LocationTokenPO;
import com.gsafety.gemp.wuhanncov.utils.TokenUtils;
import io.swagger.annotations.Api;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author Administrator
 * @Title: UserController
 * @ProjectName wuhan-ncov
 * @Description: TODO
 * @date 2020/2/1017:15
 */

@RestController
@RequestMapping("/location")
@Api(value = "行政区划校验", tags = {"行政区划校验"})
public class LocationTokenController {

    @Resource
    private LocationTokenService locationTokenService;

    @PostMapping("/valid")
//    @PassToken
    public Result valid(String areaNo, String token) {
        areaNo= AppConstant.getDimCode(areaNo);
        LocationTokenPO lnt = locationTokenService.getOne(areaNo);
        if (null == lnt) {
            return new Result().fail("区域账号不存在！");
        }

        if (!StringUtils.equalsIgnoreCase(token, lnt.getToken())) {
            return new Result().fail("token错误！");
        }

        // 生成 token
        final String geToken = TokenUtils.create(lnt);
        JSONObject jsonObject = JSONUtil.createObj().put("token", geToken);
        return new Result().success(jsonObject);
}
}
