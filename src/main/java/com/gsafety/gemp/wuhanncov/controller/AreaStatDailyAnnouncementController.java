package com.gsafety.gemp.wuhanncov.controller;

import com.gsafety.gemp.wuhanncov.constant.AppConstant;
import com.gsafety.gemp.wuhanncov.contract.dto.AreaStatDailyAnnouncementDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.Result;
import com.gsafety.gemp.wuhanncov.contract.params.DimQueryParam;
import com.gsafety.gemp.wuhanncov.contract.service.AreaStatDailyAnnouncementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import java.util.List;
import java.util.Map;

/**
 * 该类用于区域公告信息公布
 *
 * @author ganbo
 * @since 2020/02/09
 */
@Api(value = "区域公告信息公布接口", tags = {"区域公告信息公布接口"})
@RestController
@RequestMapping("/ncov/v3/announcement/")
public class AreaStatDailyAnnouncementController {

    @Autowired
    private AreaStatDailyAnnouncementService areaStatDailyAnnouncementService;

    /**
     * localhost:3001/ncov/v3/announcement/getLatestAnnouncementData?areaCode=420100&type=1
     * @param type
     * @param areaCode
     * @return
     */
    @ApiOperation(value = "根据类型和区域编码查询最新一条公告信息数据")
    @GetMapping("/getLatestAnnouncementData")
    public Map<String,Object> getLatestAnnouncementData(@ApiParam(value = "区域编码, 例如：420100") @RequestParam(value = "areaCode") String areaCode,
                                                        @ApiParam(value = "公告类型, 例如：1") @RequestParam(value = "type") String type){
        areaCode= AppConstant.getDimCode(areaCode);
        return areaStatDailyAnnouncementService.getLatestAnnouncementData(areaCode, type);
    }

    /**
     * localhost:3001/ncov/v3/announcement/showAnnouncementDataList?areaCode=420100&type=1
     * @param areaCode
     * @param type
     * @return
     */
    @ApiOperation(value = "根据类型和区域编码查询公告信息数据列表")
    @GetMapping("/showAnnouncementDataList")
    public List<Map<String,Object>> showAnnouncementDataList(@ApiParam(value = "区域编码, 例如：420100") @RequestParam(value = "areaCode") String areaCode,
                                                             @ApiParam(value = "公告类型, 例如：1") @RequestParam(value = "type") String type){
        return areaStatDailyAnnouncementService.showAnnouncementDataList(areaCode, type);
    }

    /**
     * 地区公告分页查询
     * @param queryParam
     * @return
     */
    @ApiOperation(value = "地区公告分页查询")
    @PostMapping(value = "/findAreaAnnouncementPage")
    public Result findAreaAnnouncementPage(@RequestBody DimQueryParam queryParam){
        Sort sort = Sort.by(Sort.Direction.DESC, "statDate","updateTime");
        Pageable pageable = PageRequest.of( null == queryParam.getCurrentPage() ? AppConstant.DEFAULT_CURRENT_PAGE : queryParam.getCurrentPage() - 1,
                null == queryParam.getPageSize() ? AppConstant.DEAULT_PAGE_SIZE : queryParam.getPageSize(),
                sort);
        Page<AreaStatDailyAnnouncementDTO> page = areaStatDailyAnnouncementService.findAreaAnnouncementPage(queryParam, pageable);

        return new Result().success(page);
    }

    /**
     * 新增地区公告
     * @param dto
     * @return
     */
    @ApiOperation(value = "新增地区公告")
    @PostMapping("/addAreaAnnouncement")
    public Result addAreaAnnouncement(@RequestBody AreaStatDailyAnnouncementDTO dto) {
        //新增忽略接口文档中传递主键，维护页面不会传递主键
        dto.setId(null);
        return new Result().success("保存成功", areaStatDailyAnnouncementService.areaAnnouncementSave(dto).getId());
    }

    /**
     * 编辑地区公告
     * @param dto
     * @return
     */
    @ApiOperation(value = "编辑地区公告")
    @PostMapping("/updateAreaAnnouncement")
    public Result updateAreaAnnouncement(@RequestBody AreaStatDailyAnnouncementDTO dto) {

        return new Result().success("更新成功", areaStatDailyAnnouncementService.areaAnnouncementUpdate(dto).getId());
    }

    /**
     * 删除地区公告
     * @param dto
     * @return
     */
    @ApiOperation(value = "删除地区公告")
    @PostMapping("/deleteAreaAnnouncement")
    public Result deleteAreaAnnouncement(@RequestBody AreaStatDailyAnnouncementDTO dto) {
        areaStatDailyAnnouncementService.areaAnnouncementDelete(dto.getId());
        return new Result().success("删除成功");
    }

}
