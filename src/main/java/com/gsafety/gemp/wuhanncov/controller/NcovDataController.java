package com.gsafety.gemp.wuhanncov.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gsafety.gemp.wuhanncov.contract.dto.ChinaRealBroadcastDTO;
import com.gsafety.gemp.wuhanncov.contract.service.NcovDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * 该类用于武汉冠状病毒疫情数据查询,用于实现提供Rest API等能力
 *
 * @author lyh
 * @since 2020/1/23  22:06
 * <p>
 * 武汉冠状病毒数据查控制器
 */
@Api(value = "武汉冠状病毒数据", tags = {"武汉冠状病毒数据"})
@RestController
@RequestMapping("/wuhan/ncov/")
public class NcovDataController {

    public RestTemplate restTemplate;

    @Resource
    private NcovDataService ncovDataService;

    @ApiOperation(value = "查询疫情的实时播报数据-来源辰安爬虫数据库")
    @GetMapping("/broadcast")
    public List<ChinaRealBroadcastDTO> getRealBroadcast(){
        return ncovDataService.getAllRealBroadcast();
    }

    @ApiOperation(value = "查询疫情的实时播报数据-来源网易")
    @GetMapping("/report")
    public Object getDataVirusReportData() throws UnsupportedEncodingException {

        restTemplate = new RestTemplate();

        String dataString = restTemplate.getForObject("https://news.163.com/special/00018IRU/virus_report_data.js", String.class);

        dataString.replaceAll(" ", "");
        dataString = dataString.replaceAll("\\n","");
        dataString = dataString.replaceAll("\\t","");
        dataString = dataString.substring("callback(".length() + 1, dataString.length() - 1);
        dataString = dataString.replaceAll(",}","}");

        JSONObject dataJson = JSONObject.parseObject(dataString);
        JSONArray listArray = dataJson.getJSONArray("list");

        for(int i = 0 ; i < listArray.size(); i++){
            JSONObject jsonObject =  (JSONObject)listArray.get(i);
            //String title = new String(jsonObject.getString("title").getBytes("GBK"));
            jsonObject.put("title", new String(jsonObject.getString("title").getBytes("UTF-8")) );
            //String detail = new String(jsonObject.getString("detail").getBytes("GBK"));
            jsonObject.put("detail", new String(jsonObject.getString("detail").getBytes("UTF-8")));
        }

        return  listArray;
    }

    @GetMapping("/epidemic_data")
    public String getEpidemicData() throws UnsupportedEncodingException{
        restTemplate = new RestTemplate();

        String dataString = restTemplate.getForObject("https://news.163.com/special/00019HSN/epidemic_data.js", String.class);
        dataString.replaceAll(" ", "");
        dataString = dataString.replaceAll("\\n","");
        dataString = dataString.replaceAll("\\t","");
        dataString = dataString.substring("window.dataList=".length(), dataString.length());
        JSONArray jsonArray = JSONArray.parseArray(dataString);

        for(int i = 0 ; i < jsonArray.size(); i++){
            JSONObject jsonObject =  (JSONObject)jsonArray.get(i);
            String name = new String(jsonObject.getString("name").getBytes("GBK"), "GBK");
            jsonObject.put("area", new String(name.getBytes(StandardCharsets.UTF_8), StandardCharsets.UTF_8));
            jsonObject.put("sure",jsonObject.getString("value"));
            jsonObject.put("debt","0");
            jsonObject.put("death","0");
        }

        String result = jsonArray.toJSONString();
        return result;
    }

}
