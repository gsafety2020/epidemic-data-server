package com.gsafety.gemp.wuhanncov.controller;

import com.gsafety.gemp.wuhanncov.constant.AppConstant;
import com.gsafety.gemp.wuhanncov.contract.dto.Result;
import com.gsafety.gemp.wuhanncov.contract.params.DimQueryParam;
import com.gsafety.gemp.wuhanncov.contract.service.HealthCodeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 用户微信相关信息
 * @author wangwenhai
 * @date 2020-02-25 15:47
 * @Description
 */
@Api(value = "用户微信相关信息", tags = {"用户微信相关信息"})
@RestController
@RequestMapping("/wuhan/ncov/wechat")
public class WechatDataController {


    @Resource
    private HealthCodeService healthCodeService;

    /**
     * 健康码列表
     * @author wangwenhai
     * @since 2020年2月25日 上午11:39:08
     * @return
     */
    @ApiOperation(value = "")
    @PostMapping("/healthCodePage")
    public Result healthCodePage(@RequestBody DimQueryParam queryParam) {
        Sort sort = Sort.by(Sort.Direction.ASC, "healthCodeId");
        Pageable pageable = PageRequest.of( null == queryParam.getCurrentPage() ? AppConstant.DEFAULT_CURRENT_PAGE : queryParam.getCurrentPage() - 1,
                null == queryParam.getPageSize() ? AppConstant.DEAULT_PAGE_SIZE : queryParam.getPageSize(),
                sort);

        return new Result().success(healthCodeService.findHealthCodePage(queryParam, pageable));
    }

}
