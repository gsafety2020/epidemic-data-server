package com.gsafety.gemp.wuhanncov.constant;

import lombok.Getter;

/**
 * 公告类型枚举类
 * 1：速报；2：问题困难；3：措施
 *
 * @author xiongjinhui
 * @since 2020/2/12 16:37
 */
@Getter
public enum AreaAnnouncementEnum {
    /**
     * 全部成功
     */
    ALL_SUCCESS("速报", 1),
    /**
     * 部分成功
     */
    SOME_SUCCESS("问题困难", 2),
    /**
     * 全部异常
     */
    ALL_EXCEPTION("措施", 3);

    public static String getNameByCode(Integer code) {
        for (AreaAnnouncementEnum announcementEnum : AreaAnnouncementEnum.values()) {
            if (code == announcementEnum.code) {
                return announcementEnum.name;
            }
        }
        return null;
    }

    AreaAnnouncementEnum(String name, Integer code) {
        this.name = name;
        this.code = code;
    }

    private String name;
    private Integer code;

}

