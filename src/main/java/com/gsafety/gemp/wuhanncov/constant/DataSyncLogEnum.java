package com.gsafety.gemp.wuhanncov.constant;

import lombok.Getter;

/**
 * 同步任务整体是否成功枚举类
 * 1：全部成功；2：部分成功；3：全部异常
 *
 * @author xiongjinhui
 * @since 2020/2/12 16:37
 */
@Getter
public enum DataSyncLogEnum {
    /**
     * 全部成功
     */
    ALL_SUCCESS("全部成功", 1),
    /**
     * 部分成功
     */
    SOME_SUCCESS("部分成功", 2),
    /**
     * 全部异常
     */
    ALL_EXCEPTION("全部异常", 3);

    public static String getNameByCode(Integer code) {
        for (DataSyncLogEnum dataSyncLogEnum : DataSyncLogEnum.values()) {
            if (code == dataSyncLogEnum.code) {
                return dataSyncLogEnum.name;
            }
        }
        return null;
    }

    DataSyncLogEnum(String name, Integer code) {
        this.name = name;
        this.code = code;
    }

    private String name;
    private Integer code;

}

