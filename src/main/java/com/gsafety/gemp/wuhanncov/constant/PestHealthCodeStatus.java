package com.gsafety.gemp.wuhanncov.constant;

/**
 * 
* @ClassName: PestHealthCodeStatus 
* @Description: 健康码状态枚举
* @author luoxiao
* @date 2020年2月21日 下午7:13:56 
*
 */
public class PestHealthCodeStatus {

	public enum QuestionEnum{
		
		NO("0","否"),YES("1","是");
		
		private String code;
		
		private String value;

		private QuestionEnum(String code, String value) {
			this.code = code;
			this.value = value;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

	}
	
	public enum CodeColorEnum{
		
		RED("red","红码"),GREEN("green","绿码"),YELLOW("yellow","黄码");
		
		private String code;
		
		private String value;
		
		private CodeColorEnum(String code, String value) {
			this.code = code;
			this.value = value;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}
	
	public enum DeleteEnum{
		
		NOT_DELETE("0","未删除"),DELETED("1","已删除");
		
		private String code;
		
		private String value;

		private DeleteEnum(String code, String value) {
			this.code = code;
			this.value = value;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

	}
}
