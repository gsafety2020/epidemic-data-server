package com.gsafety.gemp.wuhanncov.constant;

/**
 * 
* @ClassName: PestWechatUserStatus 
* @Description: 微信用户状态枚举类
* @author luoxiao
* @date 2020年2月26日 上午12:27:32 
*
 */
public class PestWechatUserStatus {

	public enum AuthorizeEnum{
		
		NOT_AUTHORIZE("0","未授权"),AUTORIZED("1","已授权");
		
		private String code;
		
		private String value;

		private AuthorizeEnum(String code, String value) {
			this.code = code;
			this.value = value;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}
	
	public enum GenderEnum{
		
		UNKONW("0","未知"),MALE("1","男性"),FEMALE("2","女性");
		
		private String code;
		
		private String value;

		private GenderEnum(String code, String value) {
			this.code = code;
			this.value = value;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}
}
