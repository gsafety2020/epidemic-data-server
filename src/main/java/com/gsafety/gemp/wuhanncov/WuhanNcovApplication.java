package com.gsafety.gemp.wuhanncov;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(scanBasePackages = { "com.gsafety.gemp.wuhanncov", "com.gsafety.gemp.d420110",
		"com.gsafety.gemp.d420115", "com.gsafety.gemp.wechatmp" })
@EnableJpaRepositories(basePackages = { "com.gsafety.gemp.wuhanncov.dao" })
@EntityScan(basePackages = { "com.gsafety.gemp.wuhanncov.dao.po" })
@EnableScheduling
public class WuhanNcovApplication {

	public static void main(String[] args) {
		SpringApplication.run(WuhanNcovApplication.class, args);
	}

}
