package com.gsafety.gemp.wuhanncov.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
  * 该类用于swagger配置
  *
  * @author lyh
  *
  * @since 2020/1/25 12:08
  *
  *
  */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket templateApi() {

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(false)
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("武汉冠状病毒疫情数据服务接口")
                .version("0.0.1")
                .build();
    }
}
