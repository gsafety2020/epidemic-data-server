package com.gsafety.gemp.wuhanncov.config;

import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;

/**
 * @author Administrator
 * @Title: JacksonConfig
 * @ProjectName wuhan-ncov
 * @Description: TODO
 * @date 2020/2/517:05
 */

@Configuration
public class JacksonConfig {
    @Bean
    public HttpMessageConverters fastJsonHttpMessageConverters() {
        return new HttpMessageConverters((HttpMessageConverter<?>) new JacksonHttpMessageCoverter());
    }

}
