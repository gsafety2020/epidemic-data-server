/**
 * 
 */
package com.gsafety.gemp.wuhanncov.handler;

import com.gsafety.gemp.wuhanncov.contract.dto.RefreshPatientDailyCond;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaCountStatisticsHistoryPO;
import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaStatisticPO;

/**
 * 该类用于...，用于实现...等能力。
 *
 * @author yangbo
 *
 * @since 2020年1月26日 下午9:14:38
 *
 */
public class NullPatientAreaStatDataHandler implements PatientAreaStatDataHandler {

	@Override
	public PatientAreaStatisticPO transData(ChinaCountStatisticsHistoryPO po, RefreshPatientDailyCond cond) {
		return null;
	}

	@Override
	public boolean isEnabled() {
		return false;
	}

}
