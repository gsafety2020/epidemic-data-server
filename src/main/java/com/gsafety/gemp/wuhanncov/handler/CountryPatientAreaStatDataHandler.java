/**
 * 
 */
package com.gsafety.gemp.wuhanncov.handler;

import com.gsafety.gemp.wuhanncov.constant.AppConstant;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.gsafety.gemp.wuhanncov.contract.dto.RefreshPatientDailyCond;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaCountStatisticsHistoryPO;
import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaStatisticPO;
import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaStatisticPO.PatientAreaStatisticPOBuilder;

/**
 * 该类用于...，用于实现...等能力。
 *
 * @author yangbo
 *
 * @since 2020年1月26日 下午9:10:06
 *
 */
public class CountryPatientAreaStatDataHandler implements PatientAreaStatDataHandler {

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public PatientAreaStatisticPO transData(ChinaCountStatisticsHistoryPO po, RefreshPatientDailyCond cond) {
		PatientAreaStatisticPOBuilder builder = PatientAreaStatisticPO.builder();
		String areaCode = AppConstant.DIM_ChINA_CODE;
		String id = cond.getTargetDate() + areaCode;
		
		// 查询当日的舆情
		DateTimeFormatter format = DateTimeFormat.forPattern("yyyyMMddHHmmss");
		String hour = "235959";
		DateTime dateTime = DateTime.parse(cond.getTargetDate() + hour, format);
		builder.id(id).area("中国").areaCode(areaCode).stasTime(dateTime.toDate()).stasDate(cond.getTargetDate())
				.stasHour(hour).patientNumber(po.getConfirmCase()).deathNumber(po.getDeadCase())
				.curedNumber(po.getCureCase()).debtNumber(po.getProbableCase());
		return builder.build();
	}
}
