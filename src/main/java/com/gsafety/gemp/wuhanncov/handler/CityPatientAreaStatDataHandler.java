/**
 * 
 */
package com.gsafety.gemp.wuhanncov.handler;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.gsafety.gemp.wuhanncov.contract.dto.RefreshPatientDailyCond;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaCountStatisticsHistoryPO;
import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaStatisticPO;
import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaStatisticPO.PatientAreaStatisticPOBuilder;

/**
 * 默认的市级加工器
 *
 * @author yangbo
 *
 * @since 2020年1月27日 下午1:50:10
 *
 */
public class CityPatientAreaStatDataHandler implements PatientAreaStatDataHandler {

	/**
	 * 
	 */
	private static final String DTF = "yyyyMMddHHmmss";

	/**
	 * 
	 */
	private static final String LAST_TIME = "235959";

	/**
	 * 
	 */
	private String areaCode;

	/**
	 * 
	 */
	private String area;

	/**
	 * 
	 * @param areaCode
	 * @param area
	 */
	public CityPatientAreaStatDataHandler(String areaCode, String area) {
		super();
		this.areaCode = areaCode;
		this.area = area;
	}

	@Override
	public PatientAreaStatisticPO transData(ChinaCountStatisticsHistoryPO po, RefreshPatientDailyCond cond) {
		PatientAreaStatisticPOBuilder builder = PatientAreaStatisticPO.builder();
		String id = cond.getTargetDate() + areaCode;

		// 查询当日的舆情
		DateTimeFormatter format = DateTimeFormat.forPattern(DTF);
		DateTime dateTime = DateTime.parse(cond.getTargetDate() + LAST_TIME, format);

		builder.id(id).area(this.area).areaCode(areaCode).stasTime(dateTime.toDate()).stasDate(cond.getTargetDate())
				.stasHour(LAST_TIME).patientNumber(po.getConfirmCase()).deathNumber(po.getDeadCase())
				.curedNumber(po.getCureCase()).debtNumber(po.getProbableCase());
		return builder.build();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.gsafety.gemp.wuhanncov.handler.PatientAreaStatDataHandler#isEnabled()
	 */
	@Override
	public boolean isEnabled() {
		return true;
	}
}
