/**
 *
 */
package com.gsafety.gemp.wuhanncov.handler;

import com.gsafety.gemp.wuhanncov.contract.service.NcovLocationDimService;
import com.gsafety.gemp.wuhanncov.utils.SpringContextUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 该类用于...，用于实现...等能力。
 * NcovRepairDataController需要暴露一个刷新API接口，
 * @author yangbo
 *
 * @since 2020年1月26日 下午9:16:14
 *
 */
public class PatientAreaStatDataHandlerFactory {

    private static Map<String, PatientAreaStatDataHandler> handlerMap = new HashMap<>();

    static {
        if (handlerMap.isEmpty()) {
            initHanderMap();
        }
    }


    private static void initHanderMap() {
        NcovLocationDimService ncovLocationDimService = SpringContextUtil.getBean(NcovLocationDimService.class);
        ncovLocationDimService.findByAutoJobOn().stream()
                .forEach(dim -> handlerMap.put(dim.getShortName(),
                        new CityPatientAreaStatDataHandler(dim.getAreaNo(), dim.getAreaName())));
    }

    public static void resfreshMap(){
        handlerMap.clear();
        initHanderMap();
    }


    /**
     *
     */
    private PatientAreaStatDataHandlerFactory() {
        super();
    }

    /**
     *
     * 该方法用于实现...的功能
     *
     * @author yangbo
     *
     * @since 2020年1月26日 下午9:18:38
     *
     * @param name
     * @return
     */
    public static PatientAreaStatDataHandler get(String name) {
        switch (name) {
            case "全国":
                return new CountryPatientAreaStatDataHandler();
            default:
                return handlerMap.containsKey(name) ? handlerMap.get(name) : new NullPatientAreaStatDataHandler();
        }
    }


}
