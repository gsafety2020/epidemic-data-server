/**
 * 
 */
package com.gsafety.gemp.wuhanncov.handler;

import com.gsafety.gemp.wuhanncov.contract.dto.RefreshPatientDailyCond;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaCountStatisticsHistoryPO;
import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaStatisticPO;

/**
 * 该类用于...，用于实现...等能力。
 *
 * @author yangbo
 *
 * @since 2020年1月26日 下午9:07:52
 *
 */
public interface PatientAreaStatDataHandler {

	/**
	 * 
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月26日 下午9:13:34
	 *
	 * @param po
	 * @param cond
	 * @return
	 */
	PatientAreaStatisticPO transData(ChinaCountStatisticsHistoryPO po, RefreshPatientDailyCond cond);

	/**
	 * 
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月26日 下午9:11:56
	 *
	 * @return
	 */
	boolean isEnabled();
}
