package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


@Entity
@Table(name = "city_image_carousel")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CityImageCarouselPO implements Serializable {

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "num")
    private Integer num;

    @Column(name = "area_code")
    private String areaCode;

    @Column(name = "description")
    private String description;

    @Column(name = "url")
    private String url;

}
