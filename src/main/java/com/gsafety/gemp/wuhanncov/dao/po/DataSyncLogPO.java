package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@Table(name = "data_sync_log")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DataSyncLogPO implements Serializable {
    /**主键id**/
    @Id
    @Column(name = "id")
    private String id;

    /**数据源文件id**/
    @Column(name = "src_file_id")
    private String srcFileId;

    /**数据目标表**/
    @Column(name = "target_table")
    private String targetTable;

    /**数据同步开始时间**/
    @Column(name = "start_time")
    private Date startTime;

    /**数据同步结束时间**/
    @Column(name = "end_time")
    private Date endTime;

    /**同步成功的记录条数**/
    @Column(name = "success_record")
    private Integer successRecord;

    /**同步总记录条数**/
    @Column(name = "total_record")
    private Integer totalRecord;

    /**同步任务整体是否成功。1：全部成功；2：部分成功；3：全部异常**/
    @Column(name = "status")
    private Integer status;

    /**创建时间**/
    @Column(name = "create_time")
    private Date createTime;

    /**更新时间**/
    @Column(name = "update_time")
    private Date updateTime;

    /**创建人**/
    @Column(name = "create_by")
    private String createBy;

    /**更新人**/
    @Column(name = "update_by")
    private String updateBy;
}
