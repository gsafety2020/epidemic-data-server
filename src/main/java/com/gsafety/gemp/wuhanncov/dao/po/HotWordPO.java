package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "hot_word")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HotWordPO implements Serializable {
    private static final long serialVersionUID = -7520125219903501125L;
    @Id
    @Column(name = "word_time")
    private String wordTime;

    @Column(name = "part_word")
    private String partWord;

    @Column(name = "create_time")
    private Date createTime;

}
