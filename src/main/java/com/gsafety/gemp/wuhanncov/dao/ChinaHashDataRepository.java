/**
 * 
 */
package com.gsafety.gemp.wuhanncov.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gsafety.gemp.wuhanncov.dao.po.ChinaHashDataPO;

/**
 * ChinaHashData DAO
 *
 * @author yangbo
 *
 * @since 2020年2月8日 下午10:01:21
 *
 */
public interface ChinaHashDataRepository
		extends JpaRepository<ChinaHashDataPO, String>, JpaSpecificationExecutor<ChinaHashDataPO> {

}
