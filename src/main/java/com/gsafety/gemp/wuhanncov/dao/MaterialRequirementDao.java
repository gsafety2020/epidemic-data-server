package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.ChinaCountStatisticsPO;
import com.gsafety.gemp.wuhanncov.dao.po.MaterialRequirementPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 *
 *
 * @author zhangay
 * @since 2020-01-31 20:01:28
 * <p>
 * 1.0.0
 */
public interface MaterialRequirementDao extends JpaRepository<MaterialRequirementPO, String>,
        JpaSpecificationExecutor<MaterialRequirementPO> {

    @Query( " select mp from MaterialRequirementPO mp " +
            " where mp.status <> '9' " +
            " and mp.areaNo = ?1 or mp.cityNo = ?1 or mp.provinceNo = ?1" +
            " order by mp.areaNo,mp.publishDate desc,mp.createTime desc ")
    List<MaterialRequirementPO> findIncludeSubByAreaNo(String areaNo);
}
