package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table( name = "basic_report_statistic_day",schema = "data_collection",catalog = "data_collection")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BasicReportStatisticDayPO implements Serializable {
    private static final long serialVersionUID = -4410827708484530174L;

    @Id
    @Column(name = "report_id")
    private String reportId;

    @Column(name = "area_name")
    private String areaName;

    @Column(name = "area_no")
    private String areaNo;

    @Column(name = "report_date")
    private String reportDate;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "hot_num")
    private Integer hotNum;

    @Column(name = "community_num")
    private Integer communityNum;

    @Column(name = "report_num")
    private Integer reportNum;

    @Column(name = "lock_flag")
    private String lockFlag;

    @Column(name = "update_time")
    private Date updateTime;
}
