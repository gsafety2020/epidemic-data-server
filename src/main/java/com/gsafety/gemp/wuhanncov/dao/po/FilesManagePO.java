package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "files_manage")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FilesManagePO implements Serializable {

    private static final long serialVersionUID = -4648247826378016222L;

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "area_code")
    private String areaCode;

    @Column(name = "area_name")
    private String areaName;

    @Column(name = "bn_type")
    private String bnType;

    @Column(name = "seq_num")
    private Integer seqNum;

    @Column(name = "status")
    private Integer status;

    @Column(name = "orig_name")
    private String origName;

    @Column(name = "extension")
    private String extension;

    @Column(name = "tags")
    private String tags;

    @Column(name = "path")
    private String path;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "create_by")
    private String createBy;

    @Column(name = "update_by")
    private String updateBy;
}
