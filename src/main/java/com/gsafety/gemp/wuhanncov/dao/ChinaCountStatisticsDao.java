package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.contract.dto.ChinaCountStatisticsDTO;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaCountStatisticsPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

/**
 *
 *
 * @author lyh
 * @since 2020/1/28  0:04
 * <p>
 * 1.0.0
 */
public interface ChinaCountStatisticsDao extends JpaRepository<ChinaCountStatisticsPO, String>,
        JpaSpecificationExecutor<ChinaCountStatisticsPO> {

    List<ChinaCountStatisticsPO> findByAreaType(String areaType);

    List<ChinaCountStatisticsPO> findByAreaTypeAndProvinceLike(String areaType, String province);

    List<ChinaCountStatisticsPO> findByAreaTypeAndCityLike(String areaType, String city);

    List<ChinaCountStatisticsPO> findByProvince(String province);

    @Query(value = "from ChinaCountStatisticsPO where updateTime between :beginDate and :endDate and (areaName like '%待确认%' or areaName like '%待确定%')")
    List<ChinaCountStatisticsPO> findUncertainAll(@Param("beginDate") Date beginDate, @Param("endDate") Date endDate);
}
