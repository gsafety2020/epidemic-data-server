package com.gsafety.gemp.wuhanncov.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gsafety.gemp.wuhanncov.dao.po.CodeBasDistrictPO;

/**
 * @author dusiwei
 */
public interface CodeBasDistrictDao extends JpaRepository<CodeBasDistrictPO, String>,
        JpaSpecificationExecutor<CodeBasDistrictPO> {
	
	@Query("SELECT T FROM CodeBasDistrictPO T WHERE districtcode in (:districtCodes)")
	List<CodeBasDistrictPO> findByDistrictCodes(@Param("districtCodes")List<String> districtCodes);

	@Query("SELECT T FROM CodeBasDistrictPO T WHERE parentCode in (:parentCodes)")
	List<CodeBasDistrictPO> findByParentCode(@Param("parentCodes")List<String> parentCodes);

}
