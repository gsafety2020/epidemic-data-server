package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 佛山医用物资信息表
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "city_material" )
public class CityMaterialPO {
    @Id
    @Column(name = "material_id")
    private String id;

    @Column(name = "area_no")
    private String areaNo;

    @Column(name = "area_name")
    private String areaName;

    @Column(name = "material_name")
    private String materialName;

    @Column(name = "unit_name")
    private String unitName;

    @Column(name = "storage_num")
    private BigDecimal storageNum;

    @Column(name = "can_num")
    private BigDecimal canNum;

    @Column(name = "lock_num")
    private BigDecimal lockNum;

    @Column(name = "total")
    private BigDecimal total;

    @Column(name = "org_name")
    private String orgName;

    @Column(name = "org_code")
    private String orgCode;

    @Column(name = "warehouse")
    private String warehouse;

    @Column(name = "warehouse_code")
    private String warehouseCode;

    @Column(name = "address")
    private String address;

    @Column(name = "link_name")
    private String linkName;

    @Column(name = "mobile")
    private String mobile;

    @Column(name = "status_cd")
    private String statusCd;

    @Column(name = "longitude")
    private BigDecimal longitude;

    @Column(name = "latitude")
    private BigDecimal latitude;

    @Column(name = "city_no")
    private String cityNo;

    @Column(name = "city_name")
    private String cityName;

    @Column(name = "province_no")
    private String provinceNo;

    @Column(name = "province_name")
    private String provinceName;

    @Column(name = "parent_no")
    private String parentNo;

    @Column(name = "parent_name")
    private String parentName;

    @Column(name = "in_time")
    private Date inTime;

    @Column(name = "create_by")
    private String createBy;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "seq_num")
    private Integer seqNum;
}
