package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "location_dim")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LocationDimPO implements Serializable {
    private static final long serialVersionUID = -1672349999792115277L;
    @Id
    @Column(name = "area_no")
    private String areaNo;

    @Column(name = "area_name")
    private String areaName;

    @Column(name = "parent_no")
    private String parentNo;

    @Column(name = "seq_num")
    private Integer seqNum;

    @Column(name = "latitude")
    private BigDecimal latitude;

    @Column(name = "longitude")
    private BigDecimal longitude;

    @Column(name = "short_name")
    private String shortName;

    @Column(name = "auto_job")
    private String autoJob;

    /**
     * 
     * 该方法用于判断记录是省还是市
     *
     * @author yangbo
     * 
     * @since 2020年1月27日 下午5:06:29
     *
     * @return
     */
    public String getAreaType() {
    	return StringUtils.equals(this.parentNo, "0") ? "province" : "city";
    }

    public String getAreaType2() {
        return Integer.valueOf(this.parentNo) == 0 ? "province" : "city";
    }
}
