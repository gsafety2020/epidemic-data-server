package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaStatisticPO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

/**
 * 该类用于...,用于实现...等能力
 *
 * @author lyh
 * @since 2020/1/26  19:23
 * <p>
 * 1.0.0
 */
public interface PatientAreaStatisticDao extends JpaRepository<PatientAreaStatisticPO, String>,
        JpaSpecificationExecutor<PatientAreaStatisticPO> {

	/**
	 * 
	 * 查询指定区域的病患统计数据
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月28日 下午5:38:24
	 *
	 * @param areaCode
	 * @return
	 */
	List<PatientAreaStatisticPO> findByAreaCodeOrderByStasTimeDesc(String areaCode);
	
    List<PatientAreaStatisticPO> findByAreaCodeAndStasDateLessThanOrderByStasTimeDesc(String areaCode, String today);

    List<PatientAreaStatisticPO> findByAreaCodeAndStasDateGreaterThanAndStasDateLessThanOrderByStasTimeAsc(String areaCode, String startDate, String endDate);

	/**
	 * 分页查询日报
	 * @param area
	 * @param pageable
	 * @return
	 */
	@Query(value = "select t from PatientAreaStatisticPO t where t.area like %:area%")
	Page<PatientAreaStatisticPO> findAllByArea(@Param("area") String area, Pageable pageable);

	@Query(value = "select areaCode,area ,max(patientNumber) as patientNumber from PatientAreaStatisticPO t where t.areaCode in :areaIdList and stas_date between :beginStr and :endStr   group by areaCode,area order by patientNumber desc")
	List<Object[]> findWeekAreaCount(@Param("areaIdList") List<String> areaIdList, @Param("beginStr") String beginStr,  @Param("endStr") String endStr);

	@Query(value = "select area_Code,area ,max(patient_Number) as patientNumber from Patient_Area_Statistic t where t.area_Code=:areaId and stas_date between :beginStr and :endStr   group by area_Code,area",nativeQuery = true)
	List<Object[]> findWeekArea(@Param("areaId") String areaId, @Param("beginStr") String beginStr,  @Param("endStr") String endStr);

	@Query(value = "select areaCode,area ,max(patientNumber) as patientNumber from PatientAreaStatisticPO t where t.areaCode =:areaCode and stas_date between :beginStr and :endStr   group by areaCode,area")
	List<Object[]> findWeekAreaCountByareaCode(String areaCode, String beginStr, String endStr);

    List<PatientAreaStatisticPO> findByAreaCodeAndStasDateLessThanOrderByStasTimeAsc(String areaCode, String todayDate);
}
