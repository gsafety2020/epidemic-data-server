package com.gsafety.gemp.wuhanncov.dao.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.gsafety.gemp.wuhanncov.dao.po.UserEpidemicListPO.UserEpidemicListPOBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
* @ClassName: UserEpidemicDepartmentPO 
* @Description: 综合平台机构表
* @author luoxiao
* @date 2020年2月6日 下午7:59:04 
*
 */
@Entity
@Table(name = "user_epidemic_department")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserEpidemicDepartmentPO implements Serializable{
	
	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "department_code")
	private String departmentCode;
	
	@Column(name = "department_name")
	private String departmentName;
	
	@Column(name = "parent_code")
	private String parentCode;

}
