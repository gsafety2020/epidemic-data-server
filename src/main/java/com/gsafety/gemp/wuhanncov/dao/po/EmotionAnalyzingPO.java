package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "emotion_analyzing")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EmotionAnalyzingPO implements Serializable {
    private static final long serialVersionUID = -4199246833205967320L;
    @Id
    @Column(name = "analysis_time")
    private String analysisTime;

    @Column(name = "positive")
    private Integer positive;

    @Column(name = "negative")
    private Integer negative;

    @Column(name = "neutral")
    private Integer neutral;
}
