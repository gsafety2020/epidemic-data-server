package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.BasicReportPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

/**
 * @author liyanhong
 * @since 2020/2/7  23:24
 */
public interface BasicReportDao extends JpaRepository<BasicReportPO, Integer>,
        JpaSpecificationExecutor<BasicReportPO> {

    @Query(value = "SELECT COUNT(*) FROM data_collection.basic_report b WHERE b.fever = 1 and Date(CREATE_TIME) = CURRENT_DATE;",nativeQuery = true)
    Integer getTolalFeverByDate();

    @Query(value = "SELECT COUNT(*) FROM data_collection.basic_report b WHERE Date(CREATE_TIME) = CURRENT_DATE; ", nativeQuery = true)
    Integer getTotalReportByDate();

    @Query(value = "SELECT COUNT(DISTINCT RESIDENCE) FROM data_collection.basic_report b WHERE Date(CREATE_TIME) = CURRENT_DATE;", nativeQuery = true)
    Integer getTotalDistrictByDate();

}
