package com.gsafety.gemp.wuhanncov.dao.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The persistent class for the location_sentiment_daily database table.
 * 
 */
@Entity
@Table(name = "location_sentiment_daily")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LocationSentimentDailyPO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "stat_id")
	private String statId;

	@Column(name = "area_name")
	private String areaName;

	@Column(name = "area_no")
	private String areaNo;
	
	@Column(name = "area_type")
	private String areaType;
	
	@Column(name = "parent_no")
	private String parentNo;
	
	@Column(name = "parent_name")
	private String parentName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time")
	private Date createTime;

	private int egative;

	@Column(name = "hot_value")
	private int hotValue;

	private int neutral;

	private int positive;

	@Column(name = "stat_date")
	private String statDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_time")
	private Date updateTime;

}