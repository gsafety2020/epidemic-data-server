package com.gsafety.gemp.wuhanncov.dao.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
* @ClassName: UserEpidemicDeployPO 
* @Description: 机构查询配置表 
* @author luoxiao
* @date 2020年2月8日 下午5:14:20 
*
 */
@Entity
@Table(name = "user_epidemic_deploy")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserEpidemicDeployPO implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "deploy_id")
	private Long deployId;
	
	@Column(name = "user_name")
	private String userName;
	
	@Column(name = "department_name")
	private String departmentName;
	
	@Column(name = "department_code")
	private String departmentCode;
	
	@Column(name = "mobile")
	private String mobile;
}
