package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @Author ganbo
 * @Since 2020-02-09
 */
@Entity
@Table(name = "area_stat_daily_announcement")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@DynamicInsert
@DynamicUpdate
public class AreaStatDailyAnnouncementPO implements Serializable {

    /** 主键ID  **/
    @Id
    @Column(name = "id")
    private String id;

    /** 行政编码 **/
    @Column(name="area_code")
    private String areaCode;

    /** 统计日期（yyyymmdd） **/
    @Column(name="stat_date")
    private String statDate;

    /** 公告标题 **/
    @Column(name="title")
    private String title;

    /** 公告内容 **/
    @Column(name="content")
    private String content;

    /** 公告发布部门 **/
    @Column(name="department")
    private String department;

    /** 公告类型 **/
    @Column(name="announcement_type")
    private String announcementType;

    /** 公告排序 **/
    @Column(name="order_num")
    private Integer orderNum;

    /** 创建时间 **/
    @Column(name="create_time")
    private Date createTime;

    /** 修改时间 **/
    @Column(name="update_time")
    private Date updateTime;

    @ManyToOne(targetEntity = LocationDimPO.class)
    @JoinColumn(name = "area_code" ,referencedColumnName = "area_no", insertable = false, updatable = false)
    private LocationDimPO dim;
}
