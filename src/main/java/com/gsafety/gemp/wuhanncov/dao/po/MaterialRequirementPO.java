package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * 物资需求
 * @author zhangay
 * @date 2020-01-31 20:00:33
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "material_requirement" )
public class MaterialRequirementPO {
    @Id
    @Column(name = "id", nullable = false, length = 32)
    private String id;

    @Basic
    @Column(name = "area_no", nullable = true, length = 32)
    private String areaNo;

    @Basic
    @Column(name = "area_name", nullable = true, length = 32)
    private String areaName;

    @Basic
    @Column(name = "department_name", nullable = true, length = 255)
    private String departmentName;

    @Basic
    @Column(name = "address", nullable = true, length = 255)
    private String address;

    @Basic
    @Column(name = "publish_date", nullable = true)
    private LocalDate publishDate;

    @Basic
    @Column(name = "person_name", nullable = true, length = 32)
    private String personName;

    @Basic
    @Column(name = "person_tel", nullable = true, length = 32)
    private String personTel;

    @Basic
    @Column(name = "requirement", nullable = true, length = 2000)
    private String requirement;

    @Basic
    @Column(name = "status", nullable = true, length = 1)
    private String status;

    @Basic
    @Column(name = "longitude", nullable = true, precision = 6)
    private BigDecimal longitude;

    @Basic
    @Column(name = "latitude", nullable = true, precision = 6)
    private BigDecimal latitude;

    @Basic
    @Column(name = "city_no", nullable = true, length = 32)
    private String cityNo;

    @Basic
    @Column(name = "city_name", nullable = true, length = 32)
    private String cityName;

    @Basic
    @Column(name = "province_no", nullable = true, length = 32)
    private String provinceNo;

    @Basic
    @Column(name = "province_name", nullable = true, length = 32)
    private String provinceName;

    @Basic
    @Column(name = "parent_no", nullable = true, length = 32)
    private String parentNo;

    @Basic
    @Column(name = "parent_name", nullable = true, length = 32)
    private String parentName;

    @Basic
    @Column(name = "create_by", nullable = true, length = 32)
    private String createBy;

    @Basic
    @Column(name = "update_by", nullable = true, length = 32)
    private String updateBy;

    @Basic
    @Column(name = "create_time", nullable = true)
    private LocalDateTime createTime;

    @Basic
    @Column(name = "update_time", nullable = true)
    private LocalDateTime updateTime;
}
