package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.BasicFileInfoPO;
import com.gsafety.gemp.wuhanncov.dao.po.BasicReportPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author zhoushuyan
 * @date 2020-02-11 10:44
 * @Description
 */
public interface BasicFileInfoDao  extends JpaRepository<BasicFileInfoPO, String>,
        JpaSpecificationExecutor<BasicFileInfoPO> {
    List<BasicFileInfoPO> findAllByStatusAndFileTypeOrderByFileDateDesc(String s, String fileType);
}
