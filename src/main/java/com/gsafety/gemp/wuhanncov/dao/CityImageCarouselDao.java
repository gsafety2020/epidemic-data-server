package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.CityImageCarouselPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface CityImageCarouselDao extends JpaRepository<CityImageCarouselPO, Integer>,
        JpaSpecificationExecutor<CityImageCarouselPO> {

    List<CityImageCarouselPO> findAllByAreaCodeOrderByNumAsc(String areaCode);
}
