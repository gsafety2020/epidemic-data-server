package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.ChinaCountStatisticsPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * 该类用于...,用于实现...等能力
 *
 * @author 86186
 * @since 2020/1/25  14:55
 * <p>
 * 1.0.0
 */
public interface CountDao extends JpaRepository<ChinaCountStatisticsPO, String>,
        JpaSpecificationExecutor<ChinaCountStatisticsPO> {
    @Query(value="from ChinaCountStatisticsPO where areaType='province' and confirmCase>0")
    List<ChinaCountStatisticsPO> getFlyLineByWutoProvince();

    @Query(value="from ChinaCountStatisticsPO where areaType='province'")
    List<ChinaCountStatisticsPO> getProvinces();
}
