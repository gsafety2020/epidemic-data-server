package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.CityMaterialPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 *
 *
 * @author zhangay
 * @since 2020-01-31 20:01:28
 * <p>
 * 1.0.0
 */
public interface CityMaterialDao extends JpaRepository<CityMaterialPO, String>,
        JpaSpecificationExecutor<CityMaterialPO> {
}
