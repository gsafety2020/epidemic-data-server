package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author zhoushuyan
 * @date 2020-02-11 9:59
 * @Description
 */
@Entity
@Table(name = "basic_file_info")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BasicFileInfoPO {
    private static final long serialVersionUID = -2491816292647860668L;
    @Id
    @Column(name = "file_id")
    private String fileId;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "file_date")
    private Date fileDate;

    @Column(name = "status")
    private String status;

    @Column(name = "url")
    private String url;

    @Column(name = "area_code")
    private String areaCode;

    @Column(name = "area_name")
    private String areaName;


    @Column(name = "oth_url")
    private String othUrl;

    @Column(name = "file_type")
    private String fileType;

    @Column(name = "update_date")
    private Date updateDate;

    @Column(name = "create_date")
    private Date createDate;
}
