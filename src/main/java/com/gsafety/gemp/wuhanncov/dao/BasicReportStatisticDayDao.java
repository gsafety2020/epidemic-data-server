package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.BasicReportStatisticDayPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author liyanhong
 * @since 2020/2/7  23:25
 */
public interface BasicReportStatisticDayDao extends JpaRepository<BasicReportStatisticDayPO, String>,
        JpaSpecificationExecutor<BasicReportStatisticDayPO> {
}
