package com.gsafety.gemp.wuhanncov.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gsafety.gemp.wuhanncov.dao.po.UserEpidemicDeployPO;

/**
 * 
* @ClassName: UserEpidemicDeployDao 
* @Description: 机构查询配置数据层
* @author luoxiao
* @date 2020年2月8日 下午5:17:53 
*
 */
public interface UserEpidemicDeployDao extends JpaRepository<UserEpidemicDeployPO, Long>,
JpaSpecificationExecutor<UserEpidemicDeployPO>{

	UserEpidemicDeployPO findByMobile(String mobile);
}
