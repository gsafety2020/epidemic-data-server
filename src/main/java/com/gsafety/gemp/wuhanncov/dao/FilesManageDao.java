package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.FilesManagePO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author liyanhong
 * @since 2020/2/6  16:59
 */
public interface FilesManageDao extends JpaRepository<FilesManagePO, String>,
        JpaSpecificationExecutor<FilesManagePO> {
}
