package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.PubSentimentNewsPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

/**
 * 该类用于...,用于实现...等能力
 *
 * @author 86186
 * @since 2020/1/25  14:56
 * <p>
 * 1.0.0
 */
public interface PubSentimentNewsDao extends JpaRepository<PubSentimentNewsPO, Long>,
        JpaSpecificationExecutor<PubSentimentNewsPO> {

    @Query(nativeQuery = true, value = "SELECT DATE_FORMAT(DATE(news_time),'%Y/%m/%d')as date, count(*) as number FROM pub_sentiment_news where DATE_FORMAT(DATE(news_time),'%Y/%m/%d')<DATE_FORMAT(DATE(now()),'%Y/%m/%d') group by DATE(news_time) ORDER BY date desc limit 7")
    List getCountListGroupByDate();

	List<PubSentimentNewsPO> findByNewsTimeBetweenAndNewsContentLike(Date startDate, Date endDate, String newsContentLike);
}
