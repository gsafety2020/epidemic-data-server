package com.gsafety.gemp.wuhanncov.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gsafety.gemp.wuhanncov.dao.po.PestHealthCodePO;

/**
 *
 * @ClassName: PestHealthCodeDao
 * @Description: 健康码Dao层
 * @author luoxiao
 * @date 2020年2月21日 下午4:44:12
 *
 */
public interface PestHealthCodeDao extends JpaRepository<PestHealthCodePO, String>,
		JpaSpecificationExecutor<PestHealthCodePO> {

	PestHealthCodePO findByHealthCodeId(String healthCodeId);

	@Query("SELECT T FROM PestHealthCodePO T WHERE openId=:openId AND areaCode=:areaCode AND isDelete=:isDelete")
	PestHealthCodePO findByOpenIdAndAreaCodeAndIsDelete(@Param("openId")String openId,@Param("areaCode")String areaCode,@Param("isDelete")String isDelete);
}