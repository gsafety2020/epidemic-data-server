package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.BasicReportInfoPO;
import com.gsafety.gemp.wuhanncov.dao.po.BasicReportPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

/**
 * @author liyanhong
 * @since 2020/2/7  23:24
 */
public interface BasicReportInfoDao extends JpaRepository<BasicReportInfoPO, Integer>,
        JpaSpecificationExecutor<BasicReportInfoPO> {
}
