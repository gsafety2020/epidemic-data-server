package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "pub_sentiment_news")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubSentimentNewsPO implements Serializable {
    private static final long serialVersionUID = 3173729276859795004L;
    @Id
    @Column(name = "news_id")
    private Long newsId;

    @Column(name = "news_time")
    private Date newsTime;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "news_url")
    private String newsUrl;

    @Column(name = "news_title")
    private String newsTitle;

    @Column(name = "keywords")
    private String keywords;

    @Column(name = "news_content")
    private String newsContent;

    @Column(name = "from_chananel")
    private String fromChananel;
    
}
