package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.ReportCommunityStatDailyPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author dusiwei
 */
public interface ReportCommunityStatDailyDao extends JpaRepository<ReportCommunityStatDailyPO, String>,
        JpaSpecificationExecutor<ReportCommunityStatDailyPO> {

    /**
     * 查询最新统计时间
     * @return
     */
    @Query(nativeQuery = true,value = " select max(t.stat_date) from report_community_stat_daily t ")
    String getLatestStatDate();

    /**
     * 通过行政编码查询社区发热信息列表
     * @param areaCode
     * @return
     */
    List<ReportCommunityStatDailyPO> findByAreaCode(String areaCode);
}
