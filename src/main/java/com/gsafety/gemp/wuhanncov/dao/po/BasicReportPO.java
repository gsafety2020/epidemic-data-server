package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "basic_report", schema = "data_collection",catalog = "data_collection")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BasicReportPO implements Serializable {
    private static final long serialVersionUID = -2491816292322360668L;
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "ID_number")
    private String idNumber;

    @Column(name = "sex")
    private Integer sex;

    @Column(name = "age")
    private Integer age;

    @Column(name = "residence")
    private String residence;

    @Column(name = "community")
    private String community;

    @Column(name = "building")
    private String building;

    @Column(name = "unit")
    private String unit;

    @Column(name = "room_number")
    private String roomNumber;

    @Column(name = "phone")
    private String phone;

    @Column(name = "back_to_wuhan")
    private String backToWuhan;

    @Column(name = "back_date")
    private String backDate;

    @Column(name = "fever")
    private Integer fever;

    @Column(name = "temperature")
    private String temperature;

    @Column(name = "touch_person_isolation")
    private Integer touchPersonIsolation;

    @Column(name = "touch_person_name")
    private String touchPersonName;

    @Column(name = "touch_person_phone")
    private String touchPersonPhone;

    @Column(name = "touch_person_location")
    private String touchPersonLocation;

    @Column(name = "symptom")
    private String symptom;

    @Column(name = "medical_advice")
    private Integer medicalAdvice;

    @Column(name = "remark")
    private String remark;

    @Column(name = "pregnant")
    private Integer pregnant;

    @Column(name = "widows")
    private Integer widows;

    @Column(name = "community_tour")
    private Integer communityTour;

    @Column(name = "disinfection")
    private Integer disinfection;

    @Column(name = "access_control")
    private Integer accessControl;

    @Column(name = "concrete_measure")
    private String concreteMeasure;

    @Column(name = "special_personnel_type")
    private String specialPersonnelType;

    @Column(name = "objective")
    private String objective;

    @Column(name = "temporary_residence")
    private String temporaryResidence;

    @Column(name = "service_places")
    private String servicePlaces;

    @Column(name = "life_needs")
    private String lifeNeeds;

    @Column(name = "other_life_needs")
    private String otherLifeNeeds;

    @Column(name = "fixed_work")
    private Integer fixedWork;

    @Column(name = "work_org")
    private String workOrg;

    @Column(name = "unemployment")
    private Integer unemployment;

    @Column(name = "fixed_income")
    private Integer fixedIncome;

    @Column(name = "medical_history")
    private String medicalHistory;

    @Column(name = "relatives_diagnosed")
    private Integer relativesDiagnosed;

    @Column(name = "relatives_died")
    private Integer relativesDied;

    @Column(name = "divorced")
    private Integer divorced;

    @Column(name = "clue_type")
    private String clueType;

    @Column(name = "clue_describe")
    private String clueDescribe;

    @Column(name = "reporter_type")
    private Integer reporterType;

    @Column(name = "reporter_phone")
    private String reporterPhone;

    @Column(name = "reporter_name")
    private String reporterName;

    @Column(name = "reporter_number")
    private String reporterNumber;

    @Column(name = "create_time")
    private Date createTime;

}
