package com.gsafety.gemp.wuhanncov.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gsafety.gemp.wuhanncov.dao.po.UserEpidemicInfoPO;

/**
 * 
* @ClassName: UserEpidemicInfoDao 
* @Description: 新型冠状病毒-用户上报Dao
* @author luoxiao
* @date 2020年2月3日 上午12:11:47 
*
 */
public interface UserEpidemicInfoDao extends JpaRepository<UserEpidemicInfoPO, Long>,
JpaSpecificationExecutor<UserEpidemicInfoPO> {
	
	UserEpidemicInfoPO findByEpidemicInfoId(String epidemicInfoId);
	
	UserEpidemicInfoPO findByUserIdAndReportDate(String userId,Date reportDate);
	
	UserEpidemicInfoPO findByMobileAndReportDate(String mobile,Date reportDate);
	
	List<UserEpidemicInfoPO> findByUserId(String userId);
	
	List<UserEpidemicInfoPO> findByMobile(String mobile);
	
	List<UserEpidemicInfoPO> findByReportDate(Date reportDate);
	
	@Query("SELECT T FROM UserEpidemicInfoPO T WHERE reportDate =:reportDate AND departmentCode in (:departmentCodes)")
	List<UserEpidemicInfoPO> findUserInfoByReprotDateAndDepartmentCode(@Param("reportDate")Date reportDate,@Param("departmentCodes")List<String> departmentCodes);
	
	@Query("SELECT DISTINCT departmentName FROM UserEpidemicInfoPO WHERE departmentCode in (:departmentCodes)")
	List<String> findDpeartmentNameByDepartmentCode(@Param("departmentCodes")List<String> departmentCodes);
	
	@Query("SELECT DISTINCT userName FROM UserEpidemicInfoPO WHERE reportDate =:reportDate AND userCondition in (:userConditions)")
	List<String> findUserNameByUserCondition(@Param("reportDate")Date reportDate,@Param("userConditions")List<String> userConditions);

	@Query("SELECT DISTINCT userId FROM UserEpidemicInfoPO WHERE userCondition=:userCondition AND departmentName = :departmentName")
	List<String> findUserNameByUserConditionAndDepartmentName(@Param("userCondition")String userCondition,@Param("departmentName")String departmentName);
	
	@Query("SELECT DISTINCT userId FROM UserEpidemicInfoPO WHERE userCondition=:userCondition")
	List<String> findUserNameByUserCondition(@Param("userCondition")String userCondition);
	
	@Modifying
	@Query("UPDATE UserEpidemicInfoPO SET userName=:userName,departmentName=:departmentName,departmentCode=:departmentCode "
			+ "WHERE mobile=:mobile")
	void updateInfoByMobile(@Param("userName")String userName,@Param("departmentName")String departmentName,
			@Param("departmentCode")String departmentCode,@Param("mobile")String mobile);
}
