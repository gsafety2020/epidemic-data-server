package com.gsafety.gemp.wuhanncov.dao.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

/**
 * The persistent class for the china_hash_data database table.
 * 
 */
@Entity
@Table(name = "china_hash_data")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@DynamicUpdate
public class ChinaHashDataPO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@Column(name = "area_code")
	private String areaCode;

	@Column(name = "data_code")
	private String dataCode;

	@Column(name = "data_label")
	private String dataLabel;

	@Column(name = "data_type")
	@Enumerated(EnumType.STRING)
	private EnumChinaHashDataType dataType;

	@Column(name = "data_value")
	private String dataValue;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_time")
	private Date updateTime;

	/**
	 *
	 * @author yangbo
	 *
	 * @since 2020年2月8日 下午10:10:49
	 *
	 */
	public enum EnumChinaHashDataType {
		/**
		 * 
		 */
		STRING,
		/**
		 * 
		 */
		INT,
		/**
		 * 
		 */
		NUMBER;
	}
}