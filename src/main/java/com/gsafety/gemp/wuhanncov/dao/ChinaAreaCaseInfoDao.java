package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.BasicReportInfoPO;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaAreaCaseInfoPO;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

/**
 * @author liyanhong
 * @since 2020/2/7  23:24
 */
public interface ChinaAreaCaseInfoDao extends JpaRepository<ChinaAreaCaseInfoPO, Integer>,
        JpaSpecificationExecutor<ChinaAreaCaseInfoPO> {
    @Query(value="select max(updateTime) as updateTime from ChinaAreaCaseInfoPO where areaNo=:areaNo or parentNo=:areaNo")
    Date getCaseInfoDefaultDate(@Param("areaNo") String areaNo);

    @Query(value=" from ChinaAreaCaseInfoPO where (areaName =:areaName or parentName=:areaName) and  updateTime between  :date1 and  :date2 order by updateTime desc")
    List<ChinaAreaCaseInfoPO> getChinaAreaCaseInfoListByAreaName(@Param("areaName") String areaName, @Param("date1") Date date1, @Param("date2") Date date2);

    @Query(value=" from ChinaAreaCaseInfoPO where (areaName =:areaName or parentName=:areaName) order by updateTime desc")
    List<ChinaAreaCaseInfoPO> getChinaAreaCaseInfoList( @Param("areaName") String areaName);
}
