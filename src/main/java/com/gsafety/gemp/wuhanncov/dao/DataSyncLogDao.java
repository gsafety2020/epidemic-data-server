package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.DataSyncLogPO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * 该类用于数据同步任务的日志dao层
 *
 * @author xiongjinhui
 * @since 2020/2/12  12:56
 * <p>
 * 1.0.0
 */
public interface DataSyncLogDao extends JpaRepository<DataSyncLogPO, String>, JpaSpecificationExecutor<DataSyncLogPO> {

//    @Query("select po from DataSyncLogPO po where po.targetTable like %:targetTable%")
//    Page<DataSyncLogPO> findAllByTargetTable(@Param("target_table") String targerTable, Pageable pageable);
}
