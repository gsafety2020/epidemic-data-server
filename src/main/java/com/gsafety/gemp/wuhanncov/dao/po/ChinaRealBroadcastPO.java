package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "china_real_broadcast")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChinaRealBroadcastPO implements Serializable {

    private static final long serialVersionUID = 3103882548815704200L;
    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "time")
    private Date time;

    @Column(name = "title")
    private String title;

    @Column(name = "detail")
    private String detail;

    @Column(name = "link")
    private String link;

    @Column(name = "memo")
    private String memo;

}
