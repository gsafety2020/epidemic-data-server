package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.contract.dto.WorldRealBroadcastDTO;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaRealBroadcastPO;
import com.gsafety.gemp.wuhanncov.dao.po.WorldRealBroadcastPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * 该类用于疫情实时播报数据层访问,用于实现对疫情实时播报数据数据库访问等能力
 *
 * @author 86186
 * @since 2020/1/25  11:43
 * <p>
 * 1.0.0
 */
public interface WorldRealBroadcastDao extends JpaRepository<WorldRealBroadcastPO, String>,
        JpaSpecificationExecutor<WorldRealBroadcastPO> {
}
