/**
 * 
 */
package com.gsafety.gemp.wuhanncov.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gsafety.gemp.wuhanncov.dao.po.ChinaAreaMedicalDailyPO;

/**
 * 该类用于...，用于实现...等能力。
 *
 * @author yangbo
 *
 * @since 2020年2月9日 上午2:31:19
 *
 */
public interface ChinaAreaMedicalDailyDao
		extends JpaRepository<ChinaAreaMedicalDailyPO, String>, JpaSpecificationExecutor<ChinaAreaMedicalDailyPO> {

}
