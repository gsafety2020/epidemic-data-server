package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.ChinaMigrationInfoPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author zhoushuyan
 * @date 2020-01-26 17:24
 * @Description
 */
public interface ChinaMigrationInfoDao  extends JpaRepository<ChinaMigrationInfoPO, Integer>,
        JpaSpecificationExecutor<ChinaMigrationInfoPO> {
    /**
     * 迁徙图数据和迁入迁出省份汇总
     * @param date
     * @param migrateType
     * @param areaId
     * @return
     */
    @Query(nativeQuery = true, value = "select max(area_id) area_id,max(create_date) create_date," +
            "max(migrate_type) migrate_type," +
            "max(area_type) area_type," +
            "max(area_name) area_name," +
            "province_name,max(city_name) city_name," +
            "sum(value) value ," +
            "max(id) id from china_migration_info where migrate_type=:migrateType and  area_id=:areaId and create_date=:date group by province_name order by sum(value) desc")
    List<ChinaMigrationInfoPO> findMigrateByProvince(@Param("date") String date, @Param("migrateType") String migrateType, @Param("areaId") String areaId);

    /**
     * 武汉的迁徙图默认时间
     * @return
     */
    @Query(value="select max(createDate) from ChinaMigrationInfoPO where areaId='420100'")
    String getMigrateMapDefaultDate();
}
