package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 地区病患实时统计信息
 * 
 */
@Entity
@Table(name = "patient_area_runtime")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@DynamicInsert
@DynamicUpdate
public class PatientAreaRuntimePO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@Column(name = "area_code")
	private String areaCode;

	@Column(name = "area_name")
	private String areaName;

	@Column(name = "area_type")
	private String areaType;

	@Column(name = "city_no")
	private String cityNo;

	@Column(name = "confirm_case")
	private Integer confirmCase;

	@Column(name = "cure_case")
	private Integer cureCase;

	@Column(name = "dead_case")
	private Integer deadCase;

	@Column(name = "new_case")
	private Integer newCase;

	@Column(name = "new_cure")
	private Integer newCure;

	@Column(name = "new_dead")
	private Integer newDead;

	@Column(name = "new_probable")
	private Integer newProbable;

	@Column(name = "parent_no")
	private String parentNo;

	@Column(name = "probable_case")
	private Integer probableCase;

	@Column(name = "province_no")
	private String provinceNo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_time")
	private Date updateTime;

	/**
	 * 病危数量
	 */
	@Column(name = "dying_case")
	private Integer dyingCase;

	/**
	 * 接受医学观察者数量
	 */
	@Column(name = "observed_case")
	private Integer observedCase;

	/**
	 * 密切接触者数量
	 */
	@Column(name = "close_contacts_case")
	private Integer closeContactsCase;

	/**
	 * 解除接受医学观察者数量
	 */
	@Column(name = "release_observed_case")
	private Integer releaseObservedCase;

	/**
	 * 新增病危数量
	 */
	@Column(name = "new_dying")
	private Integer newDying;

	/**
	 * 新增接受医学观察者数量
	 */
	@Column(name = "new_observed")
	private Integer newObserved;

	/**
	 * 新增密切接触者数量
	 */
	@Column(name = "new_close_contacts")
	private Integer newCloseContacts;

	/**
	 * 新增解除接受医学观察者数量
	 */
	@Column(name = "new_release_observed")
	private Integer newReleaseObserved;

	/**
	 * 发热数量
	 */
	@Column(name = "hot_case")
	private Integer hotCase;

	/**
	 * 新增发热数量
	 */
	@Column(name = "new_hot")
	private Integer newHot;

	/**
	 * 重症数量
	 */
	@Column(name = "severe_case")
	private Integer severeCase;

	/**
	 * 新增重症数量
	 */
	@Column(name = "new_severe")
	private Integer newSevere;

	/**
	 * 当前为确诊状态的总数量，非累计
	 */
	@Column(name = "cur_confirm")
	private Integer curConfirm;

	/**
	 * 
	 */
	@Column(name = "lock_Flag")
	@Enumerated(EnumType.STRING)
	private LockFlag lockFlag;

	/**
	 * 
	 * 该类用于...，用于实现...等能力。
	 *
	 * @author yangbo
	 *
	 * @since 2020年2月6日 下午2:10:16
	 *
	 */
	public enum LockFlag {
		/**
		 * 
		 */
		LOCK,
		/**
		 * 
		 */
		UNLOCK;
	}
}