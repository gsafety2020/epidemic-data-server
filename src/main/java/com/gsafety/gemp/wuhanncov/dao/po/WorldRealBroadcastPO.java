package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "world_real_broadcast")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WorldRealBroadcastPO implements Serializable {

    private static final long serialVersionUID = 3103882438815704200L;
    @Id
    @Column(name = "publish_time")
    private String publishTime;

    @Column(name = "publish_date")
    private Date publish_date;

    @Column(name = "title")
    private String title;

    @Column(name = "outline")
    private String outline;

    @Column(name = "weblink")
    private String weblink;

    @Column(name = "memo")
    private String memo;

    @Column(name = "data_source")
    private String dataSource;

    @Column(name = "timestamp")
    private String timestamp;

    @Column(name = "urlid")
    private String urlid;

}
