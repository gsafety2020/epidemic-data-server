package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 每日情况汇总表,汇总江夏区每日上报情况
 * @author Luoganggang
 * @since 2020/2/9 16:05
 */
@Entity
@Table(name = "report_area_stat_daily")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ReportAreaStatDailyPO implements Serializable{

    private static final long serialVersionUID = -4005865752795119390L;
    /**主键（日期 + 行政编码）**/
    @Id
    @Column(name = "id")
    private String id;
    /**统计日期**/
    @Column(name = "stas_date")
    private String stasDate;
   /**行政编码**/
    @Column(name = "area_code")
    private String areaCode;
   /**上级编码*/
    @Column(name = "parent_code")
    private String parentCode;
   /**辖区总人数**/
    @Column(name = "people_number")
    private BigDecimal peopleNumber;
   /**今日登记人数**/
    @Column(name = "register_number")
    private BigDecimal registerNumber;
    /**发热人数**/
    @Column(name = "hot_number")
    private BigDecimal hotNumber;
    /**确诊患者**/
    @Column(name = "patient_number")
    private BigDecimal patientNumber;
   /**疑似患者**/
    @Column(name = "debt_number")
    private BigDecimal debtNumber;
   /**CT诊断肺炎患者**/
    @Column(name = "ct_patient_number")
    private BigDecimal ctPatientNumber;
   /**一般发热患者**/
    @Column(name = "general_hot_number")
    private BigDecimal generalHotNumber;
   /**密切接触者**/
    @Column(name = "close_contacts_number")
    private BigDecimal closeContactsNumber;
   /**更新时间**/
    @Column(name = "update_time")
    private Date updateTime;


}
