package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.ReportAreaStatDailyPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * 每日情况汇总表,汇总江夏区每日上报情况
 * @author Luoganggang
 * @since 2020/2/9 16:22
 */
public interface ReportAreaStatDailyDao
        extends JpaRepository<ReportAreaStatDailyPO, String>, JpaSpecificationExecutor<ReportAreaStatDailyPO> {
}
