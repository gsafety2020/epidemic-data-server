package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/** china_count_statistics 改成 china_count_statistics_v 这个视图，从两个表里面来，北京地区的数据从丁香园来
 * @author zhoushuyan
 * @date 2020-01-25 20:31
 * @Description
 */
@Entity
@Table(name = "china_count_statistics_dxy")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChinaCountStatisticsDxyPO implements Serializable {
    private static final long serialVersionUID = -4199267833205968820L;
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "area_name")
    private String areaName;

    @Column(name = "area_type")
    private String areaType;

    @Column(name = "province")
    private String province;

    @Column(name = "confirm_case")
    private Integer confirmCase;

    @Column(name = "probable_case")
    private Integer probableCase;

    @Column(name = "cure_case")
    private Integer cureCase;

    @Column(name = "dead_case")
    private Integer deadCase;


    @Column(name = "new_case")
    private Integer newCase;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "memo")
    private  String memo;

    @Column(name = "from_source")
    private String fromSource;

    @Column(name = "city")
    private String city;


}
