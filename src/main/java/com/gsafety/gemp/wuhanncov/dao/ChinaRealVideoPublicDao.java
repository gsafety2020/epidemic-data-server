package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.ChinaRealVideoPublicPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * 该类用于疫情大屏,用于实现查询实时疫情视频信息等能力
 *
 * @author lyh
 * @since 2020/1/26  20:47
 * <p>
 * 1.0.0
 */
public interface ChinaRealVideoPublicDao extends JpaRepository<ChinaRealVideoPublicPO, String>,
        JpaSpecificationExecutor<ChinaRealVideoPublicPO> {

}
