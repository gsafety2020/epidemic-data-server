package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author zhoushuyan
 * @date 2020-01-27 13:53
 * @Description
 */
@Entity
@Table(name = "world_info_statistics")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WorldInfoStatisticsPO implements Serializable {
    private static final long serialVersionUID = 3323729276859795004L;
    @Id
    @Column(name = "country")
    private String country;

    @Column(name = "count_time")
    private String countTime;

    @Column(name = "count_date")
    private Date countDate;


    @Column(name = "confirmcase")
    private Integer confirmCase;

    @Column(name = "probablecase")
    private Integer probableCase;

    @Column(name = "curecase")
    private Integer cureCase;

    @Column(name = "deadcase")
    private Integer deadCase;


    @Column(name = "newcase")
    private Integer newCase;

    @Column(name = "memo")
    private  String memo;

}
