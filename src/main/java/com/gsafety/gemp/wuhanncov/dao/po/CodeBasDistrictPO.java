package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author dusiwei
 */
@Entity
@Table(name = "code_bas_district")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@DynamicInsert
@DynamicUpdate
public class CodeBasDistrictPO implements Serializable {

    private static final long serialVersionUID = 3149560722628135894L;

    /**
     * 行政编码
     */
    @Id
    @Column(name = "districtcode")
    private String districtCode;

    /**
     * 行政名称
     */
    @Column(name = "districtname")
    private String districtName;

    /**
     * 上级编码
     */
    @Column(name = "parentcode")
    private String parentCode;

    /**
     * 简称
     */
    @Column(name = "shortname")
    private String shortName;

    /**
     * 全称
     */
    @Column(name = "fullname")
    private String fullName;

    /**
     * 经度
     */
    @Column(name = "longitude")
    private BigDecimal longitude;

    /**
     * 纬度
     */
    @Column(name = "latitude")
    private BigDecimal latitude;

    /**
     * 简拼
     */
    @Column(name = "spelling")
    private String spelling;

    /**
     * 备注
     */
    @Column(name = "notes")
    private String notes;

    /**
     * 排序
     */
    @Column(name = "ordercolum")
    private BigDecimal orderColum;

    /**
     * 级别
     */
    @Column(name = "levelflag")
    private String levelFlag;

}
