package com.gsafety.gemp.wuhanncov.dao.po;

import com.gsafety.gemp.wuhanncov.contract.service.BasicReportInfoService;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author zhoushuyan
 * @date 2020-02-07 0:11
 * @Description
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "china_area_case_info" )
public class ChinaAreaCaseInfoPO {
    private static final long serialVersionUID = 2303345548815704200L;
    @Id
    @Column(name = "case_id")
    private String caseId;

    @Column(name = "area_no")
    private String areaNo;

    @Column(name = "area_name")
    private String areaName;

    @Column(name = "age")
    private BigDecimal age;

    @Column(name = "gender")
    private String gender;

    @Column(name = "case_detail")
    private String caseDetail;

    @Column(name = "hospital")
    private String hospital;

    @Column(name = "community")
    private String community;

    @Column(name = "illness_date")
    private Date illnessDate;


    @Column(name = "diagnosis_date")
    private Date diagnosisDate;


    @Column(name = "illness_type")
    private String illnessType;

    @Column(name = "remark")
    private String remark;

    @Column(name = "seq_num")
    private Integer seqNum;

    @Column(name = "city_no")
    private String cityNo;

    @Column(name = "city_name")
    private String cityName;

    @Column(name = "province_name")
    private String provinceName;


    @Column(name = "province_no")
    private String provinceNo;


    @Column(name = "parent_no")
    private String parentNo;


    @Column(name = "parent_name")
    private String parentName;

    @Column(name = "status")
    private String status;


    @Column(name = "create_by")
    private String createBy;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

}
