package com.gsafety.gemp.wuhanncov.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gsafety.gemp.wuhanncov.dao.po.UserEpidemicListPO;

/**
 * 
* @ClassName: UserEpidemicListDao 
* @Description: 综合信息用户DAO层
* @author luoxiao
* @date 2020年2月6日 下午8:05:39 
*
 */
public interface UserEpidemicListDao extends JpaRepository<UserEpidemicListPO, String>,
JpaSpecificationExecutor<UserEpidemicListPO> {

	UserEpidemicListPO findByMobile(String mobile);
}
