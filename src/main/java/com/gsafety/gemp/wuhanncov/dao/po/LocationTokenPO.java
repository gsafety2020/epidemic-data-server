package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 地区用户
 * 
 */
@Entity
@Table(name = "location_token")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@DynamicInsert
@DynamicUpdate
public class LocationTokenPO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "area_code")
	private String areaCode;

	@Column(name = "token")
	private String token;

	@Column(name = "token_status")
	private String tokenStatus;

	/**
	 * 更新时间
	 */
	@Column(name = "update_time")
	private Date updateTime;

}