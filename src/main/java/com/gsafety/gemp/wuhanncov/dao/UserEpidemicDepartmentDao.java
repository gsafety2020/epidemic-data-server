package com.gsafety.gemp.wuhanncov.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gsafety.gemp.wuhanncov.dao.po.UserEpidemicDepartmentPO;

/**
 * 
* @ClassName: UserEpidemicDepartmentDao 
* @Description: 综合平台组织结构DAO层
* @author luoxiao
* @date 2020年2月6日 下午8:04:11 
*
 */
public interface UserEpidemicDepartmentDao extends JpaRepository<UserEpidemicDepartmentPO, String>,
JpaSpecificationExecutor<UserEpidemicDepartmentPO>{

	UserEpidemicDepartmentPO findByDepartmentCode(String departmentCode);
	
	List<UserEpidemicDepartmentPO> findByParentCode(String parentCode);
}
