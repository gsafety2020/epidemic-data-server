package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * @author zhoushuyan
 * @date 2020-01-27 13:53
 * @Description
 */
@Entity
@Table(name = "world_country_info")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WorldCountryInfoPO implements Serializable {
    private static final long serialVersionUID = -7520125259903501125L;
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "country")
    private String country;

    @Column(name = "latitude")
    private BigDecimal latitude;

    @Column(name = "longitude")
    private BigDecimal longitude;

}
