package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.ChinaCountStatisticsDxyPO;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaCountStatisticsPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 *
 *
 * @author lyh
 * @since 2020/1/28  0:04
 * <p>
 * 1.0.0
 */
public interface ChinaCountStatisticsDxyDao extends JpaRepository<ChinaCountStatisticsDxyPO, String>,
        JpaSpecificationExecutor<ChinaCountStatisticsDxyPO> {
    @Query(value="from ChinaCountStatisticsPO where areaType='province' and confirmCase>0")
    List<ChinaCountStatisticsPO> getFlyLineByWutoProvince();

    @Query(value="from ChinaCountStatisticsPO where areaType='province'")
    List<ChinaCountStatisticsPO> getProvinces();

    List<ChinaCountStatisticsDxyPO> findByAreaType(String areaType);

    List<ChinaCountStatisticsDxyPO> findByAreaTypeAndProvinceLike(String areaType, String province);

    List<ChinaCountStatisticsDxyPO> findByAreaTypeAndCityLike(String areaType, String city);

    List<ChinaCountStatisticsDxyPO> findByProvince(String province);
}
