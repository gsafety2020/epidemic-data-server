package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.ChinaMigrationScalePO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * 全国人口迁移指数
 *
 * @author: WangGang
 * @since 2020-02-07 07:45
 */
public interface PeopleMigrationIndexDao extends JpaRepository<ChinaMigrationScalePO, String>,
        JpaSpecificationExecutor<ChinaMigrationScalePO> {

    /**
     * @return
     */
    @Override
    List<ChinaMigrationScalePO> findAll();

    @Query(value = "select id, areaId,createDate ,migrateType,areaType,areaName,value from ChinaMigrationScalePO t where t.areaId =:areaId and t.migrateType =:migrateType and create_date between :beginStr and :endStr order by createDate asc")
    List<Object[]> findMigrationIndexList(String areaId, String migrateType, String beginStr, String endStr);
}
