package com.gsafety.gemp.wuhanncov.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gsafety.gemp.wuhanncov.dao.po.PestWechatUserPO;

/**
 * 
* @ClassName: PestWechatUserDao 
* @Description: 微信用户DAO层
* @author luoxiao
* @date 2020年2月26日 上午12:02:37 
*
 */
public interface PestWechatUserDao extends JpaRepository<PestWechatUserPO, String>,
JpaSpecificationExecutor<PestWechatUserPO> {

}
