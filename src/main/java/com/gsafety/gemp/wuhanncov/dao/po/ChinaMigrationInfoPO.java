package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author zhoushuyan
 * @date 2020-01-26 14:58
 * @Description
 */
@Entity
@Table(name = "china_migration_info")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChinaMigrationInfoPO implements Serializable {
    private static final long serialVersionUID = 3103345548815704200L;
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "area_id")
    private String areaId;

    @Column(name = "create_date")
    private String createDate;

    @Column(name = "migrate_type")
    private String migrateType;

    @Column(name = "area_type")
    private String areaType;

    @Column(name = "area_name")
    private String areaName;

    @Column(name = "province_name")
    private String provinceName;

    @Column(name = "city_name")
    private String cityName;

    @Column(name = "value")
    private BigDecimal value;

    @Column(name = "level")
    private String level;

}
