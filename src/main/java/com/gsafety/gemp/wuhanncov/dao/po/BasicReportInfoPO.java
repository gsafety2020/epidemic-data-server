package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * @author zhoushuyan
 * @date 2020-02-09 10:09
 * @Description
 */
@Entity
@Table(name = "basic_report_info")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BasicReportInfoPO implements Serializable {
    private static final long serialVersionUID = -2491816292645360668L;
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "report_time",nullable = true)
    private LocalDateTime reportTime;

    @Column(name = "report_title")
    private String reportTitle;

    @Column(name = "report_info")
    private String reportInfo;

    @Column(name = "remark")
    private String remark;

    @Column(name = "channel")
    private String channel;

    @Column(name = "area_no")
    private String areaNo;

    @Column(name = "area_name")
    private String areaName;

    @Column(name = "parent_no")
    private String parentNo;

    @Column(name = "parent_name")
    private String parentName;

    @Column(name = "community")
    private String community;

    @Column(name = "status")
    private String status;

    @Column(name = "seq_num")
    private Integer seqNum;

    @Column(name = "create_by")
    private String createBy;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "create_time",nullable = true)
    private LocalDateTime createTime;

    @Column(name = "update_time",nullable = true)
    private LocalDateTime updateTime;

}
