package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 地区病患历史统计信息
 *
 */
@Entity
@Table(name = "patient_area_statistic")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@DynamicInsert
@DynamicUpdate
public class PatientAreaStatisticPO implements Serializable {
    private static final long serialVersionUID = 3349970726978134105L;
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "area")
    private String area;

    @Column(name = "area_code")
    private String areaCode;

    @Column(name = "stas_time")
    private Date stasTime;

    @Column(name = "stas_date")
    private String stasDate;

    @Column(name = "stas_hour")
    private String stasHour;

    @Column(name = "patient_number")
    private Integer patientNumber;

    @Column(name = "debt_number")
    private Integer debtNumber;

    @Column(name = "death_number")
    private Integer deathNumber;

    @Column(name = "severe_number")
    private Integer severeNumber;

    @Column(name = "cured_number")
    private Integer curedNumber;

    @Column(name = "data_src")
    private String dataSrc;

    @Column(name = "create_by")
    private String createBy;

    @Column(name = "upate_by")
    private String upateBy;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "post_date")
    private String postDate;

    /**
     * 病危数量
     */
    @Column(name = "dying_number")
    private Integer dyingNumber;

    /**
     * 接受医学观察者数量
     */
    @Column(name = "observed_number")
    private Integer observedNumber;

    /**
     * 密切接触者数量
     */
    @Column(name = "close_contacts_number")
    private Integer closeContactsNumber;

    /**
     * 解除被医学观察者数量
     */
    @Column(name = "release_observed_number")
    private Integer releaseObservedNumber;

    /**
     * 发热数量
     */
    @Column(name = "hot_number")
    private Integer hotNumber;

    /**
     * 当日新增疑似数量
     */
    @Column(name = "new_debt")
    private Integer newDebt;

    /**
     * 当前为确诊状态的总数量，非累计
     */
    @Column(name = "cur_confirm")
    private Integer curConfirm;
}
