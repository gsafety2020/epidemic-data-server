package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaStatisticsExtPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;


/**
 * 病患统计信息扩展表
 *
 * @author fanlx
 * @since 2020/02/02  16:23
 *
 */
public interface PatientAreaStatisticsExtDao extends JpaRepository<PatientAreaStatisticsExtPO, String>,
        JpaSpecificationExecutor<PatientAreaStatisticsExtPO> {

    List<PatientAreaStatisticsExtPO> findByAreaCodeOrderByStasTimeDesc(String areaCode);

    /**
     * 根据区域查询
     * @param areaCode
     * @param today
     * @return
     */
    List<PatientAreaStatisticsExtPO> findByAreaCodeAndStasDateLessThanAndStasDateGreaterThanEqualOrderByStasTimeAsc(String areaCode, String today ,String endDate);




}
