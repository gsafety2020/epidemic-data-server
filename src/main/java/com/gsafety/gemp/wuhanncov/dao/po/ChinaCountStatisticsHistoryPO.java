package com.gsafety.gemp.wuhanncov.dao.po;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the china_count_statistics_history database table.
 * 
 */
@Entity
@Table(name="china_count_statistics_history")
public class ChinaCountStatisticsHistoryPO implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	@Column(name="area_name")
	private String areaName;

	@Column(name="area_type")
	private String areaType;

	@Column(name="confirm_case")
	private Integer confirmCase;

	@Column(name="cure_case")
	private Integer cureCase;

	@Column(name="dead_case")
	private Integer deadCase;

	@Column(name="from_source")
	private String fromSource;

	private String memo;

	@Column(name="new_case")
	private Integer newCase;

	@Column(name="probable_case")
	private Integer probableCase;

	private String province;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="update_time")
	private Date updateTime;

	public ChinaCountStatisticsHistoryPO() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAreaName() {
		return this.areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getAreaType() {
		return this.areaType;
	}

	public void setAreaType(String areaType) {
		this.areaType = areaType;
	}

	public Integer getConfirmCase() {
		return this.confirmCase;
	}

	public void setConfirmCase(Integer confirmCase) {
		this.confirmCase = confirmCase;
	}

	public Integer getCureCase() {
		return this.cureCase;
	}

	public void setCureCase(Integer cureCase) {
		this.cureCase = cureCase;
	}

	public Integer getDeadCase() {
		return this.deadCase;
	}

	public void setDeadCase(Integer deadCase) {
		this.deadCase = deadCase;
	}

	public String getFromSource() {
		return this.fromSource;
	}

	public void setFromSource(String fromSource) {
		this.fromSource = fromSource;
	}

	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Integer getNewCase() {
		return this.newCase;
	}

	public void setNewCase(Integer newCase) {
		this.newCase = newCase;
	}

	public Integer getProbableCase() {
		return this.probableCase;
	}

	public void setProbableCase(Integer probableCase) {
		this.probableCase = probableCase;
	}

	public String getProvince() {
		return this.province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public Date getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return "ChinaCountStatisticsHistoryPO [id=" + id + ", areaName=" + areaName + ", areaType=" + areaType
				+ ", confirmCase=" + confirmCase + ", deadCase=" + deadCase + ", province=" + province + ", updateTime="
				+ updateTime + "]";
	}

}