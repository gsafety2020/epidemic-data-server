/**
 * 
 */
package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaRuntimePO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * 地区病患实时统计信息数据服务
 *
 * @author yangbo
 *
 * @since 2020年1月30日 下午8:55:26
 *
 */
public interface PatientAreaRuntimeDao
		extends JpaRepository<PatientAreaRuntimePO, String>, JpaSpecificationExecutor<PatientAreaRuntimePO> {


	List<PatientAreaRuntimePO> findByParentNo(String parentNo);

	/**
	 * 实时日报地区名称分页查询
	 * @param areaName
	 * @param pageable
	 * @return
	 */
	@Query(" select po from PatientAreaRuntimePO po where po.areaName like %:areaName%")
    Page<PatientAreaRuntimePO> findByAreaNamePage(@Param("areaName") String areaName, Pageable pageable);

	@Query("  from PatientAreaRuntimePO po where po.areaName like '%待确认%'")
    List<PatientAreaRuntimePO> findUncertainAll();
}
