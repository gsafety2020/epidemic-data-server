package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.ChinaAreaMaterialPO;
import com.gsafety.gemp.wuhanncov.dao.po.MaterialRequirementPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 *
 *
 * @author zhangay
 * @since 2020-01-31 20:01:28
 * <p>
 * 1.0.0
 */
public interface ChinaAreaMaterialDao extends JpaRepository<ChinaAreaMaterialPO, String>,
        JpaSpecificationExecutor<ChinaAreaMaterialPO> {
}
