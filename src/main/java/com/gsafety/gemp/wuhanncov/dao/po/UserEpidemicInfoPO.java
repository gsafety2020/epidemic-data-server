package com.gsafety.gemp.wuhanncov.dao.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
* @ClassName: UserEpidemicInfoPO 
* @Description: 新型冠状病毒-用户上报PO
* @author luoxiao
* @date 2020年2月2日 下午11:44:42 
*
 */
@Entity
@Table(name = "user_epidemic_info")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("上报信息字段接受对象")
public class UserEpidemicInfoPO implements Serializable{
	
	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	/**主键ID*/
	@Id
	@Column(name = "epidemic_info_id")
	@ApiModelProperty(value="主键UUID,自动生成")
	private String epidemicInfoId;
	
	/**用户ID*/
    @Column(name = "user_id")
    @ApiModelProperty(value="用户Id,暂时前端传入mobile字段")
	private String userId;
	
	/**姓名*/
	@Column(name = "user_name")
	@ApiModelProperty(value="用户姓名")
	private String userName;
	
	/**科室名*/
	@Column(name = "department_name")
	@ApiModelProperty(value="科室名称")
	private String departmentName;
	
	/**科室code*/
	@Column(name = "department_code")
	@ApiModelProperty(value="科室code,暂时前端传入科室名称")
	private String departmentCode;
	
	/**电话*/
	@Column(name = "mobile")
	@ApiModelProperty(value="电话号码")
	private String mobile;
	
	/**常住地址*/
	@Column(name = "permanent_address")
	@ApiModelProperty(value="常住地址")
	private String parmanentAddress;

	/**现住地址*/
	@Column(name = "present_address")
	@ApiModelProperty(value="现住地址")
	private String presentAddress;
	
	/**本人状况（0-确诊/1-疑似/2-密接/3-正常）*/
	@Column(name = "user_condition")
	@ApiModelProperty(value="本人状况（0-确诊/1-疑似/2-密接/3-正常）")
	private String userCondition;
	
	/**本人详情*/
	@Column(name = "user_detail")
	@ApiModelProperty(value="本人详情")
	private String userDetail;
	
	/**居家隔离日期，表单填写*/
	@Column(name = "home_quarantine_date")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")  
	@ApiModelProperty(value="居家隔离日期，取第一次填写的隔离日期")
	private Date homeQuarantineDate;
	
	/**居家隔离天数，起始日至今，后台计算*/
	@Column(name = "home_quarantine_days")
	@ApiModelProperty(value="居家隔离天数,后台计算")
	private Integer homeQuarantineDays;
	
	/**家庭成员数*/
	@Column(name = "family_numbers")
	@ApiModelProperty(value="家庭成员数")
	private Integer familyNumbers;
	
	/**家庭成员状况（0-确诊/1-疑似/2-密接/3-正常）*/
	@Column(name = "family_condition")
	@ApiModelProperty(value="家庭成员状况（0-确诊/1-疑似/2-密接/3-正常）")
	private String familyCondition;
	
	/**家庭详情*/
	@Column(name = "family_detail")
	@ApiModelProperty(value="家庭详情")
	private String familyDetail;
	
	/**填报日期*/
	@Column(name = "report_date")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
	@ApiModelProperty(value="填报日期，后台自动生成")
	private Date reportDate;
	
	/**创建时间*/
	@Column(name = "create_time")
	@ApiModelProperty(value="创建时间，后台自动生成")
	private Date createTime;
	
	/**其他*/
	@Column(name = "ext")
	@ApiModelProperty(value="其他")
	private String ext;
}
