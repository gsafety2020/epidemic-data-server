/**
 * 
 */
package com.gsafety.gemp.wuhanncov.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gsafety.gemp.wuhanncov.dao.po.LocationSentimentDailyPO;
import org.springframework.data.jpa.repository.Query;

/**
 * LocationSentimentDailyPO的DAO
 *
 * @author yangbo
 *
 * @since 2020年1月25日 下午6:56:45
 *
 */
public interface LocationSentimentDailyDao
		extends JpaRepository<LocationSentimentDailyPO, String>, JpaSpecificationExecutor<LocationSentimentDailyPO> {

	/**
	 * 
	 * findByStatDate
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月25日 下午7:12:13
	 *
	 * @param statDate
	 * @return
	 */
	List<LocationSentimentDailyPO> findByStatDate(String statDate);


	@Query(nativeQuery = true, value = "SELECT area_no, SUM(hot_value) FROM location_sentiment_daily WHERE parent_no = :parentNo GROUP BY area_no")
	List getAreaHotList(String parentNo);
}
