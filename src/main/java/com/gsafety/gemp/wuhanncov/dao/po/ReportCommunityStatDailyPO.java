package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 社区人员情况汇总
 * @author dusiwei
 */
@Entity
@Table(name = "report_community_stat_daily")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@DynamicInsert
@DynamicUpdate
public class ReportCommunityStatDailyPO implements Serializable {
    private static final long serialVersionUID = 3349970726978135894L;

    @Id
    @Column(name = "id")
    private String id;

    /**
     * 行政编码
     */
    @Column(name = "area_code")
    private String areaCode;

    /**
     * 上级编码
     */
    @Column(name = "parent_area_code")
    private String parentAreaCode;

    /**
     * 统计日期（yyyymmdd）
     */
    @Column(name = "stat_date")
    private String statDate;

    /**
     * 累计发热
     */
    @Column(name = "total_fever_number")
    private Integer totalFeverNumber;

    /**
     * 新增发热
     */
    @Column(name = "new_fever_number")
    private Integer newFeverNumber;

    /**
     * 累计隔离
     */
    @Column(name = "total_isolated_number")
    private Integer totalIsolatedNumber;

    /**
     * 新增隔离
     */
    @Column(name = "new_isolated_number")
    private Integer newIsolatedNumber;

    /**
     * 累计疑似
     */
    @Column(name = "total_suspected_number")
    private Integer totalSuspectedNumber;

    /**
     * 新增疑似
     */
    @Column(name = "new_suspected_number")
    private Integer newSuspectedNumber;

    /**
     * 累计确诊
     */
    @Column(name = "total_diagnosed_number")
    private Integer totalDiagnosedNumber;

    /**
     * 新增确诊
     */
    @Column(name = "new_diagnosed_number")
    private Integer newDiagnosedNumber;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    private Date updateTime;

}
