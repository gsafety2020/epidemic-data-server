/**
 * 
 */
package com.gsafety.gemp.wuhanncov.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gsafety.gemp.wuhanncov.dao.po.ChinaCountStatisticsHistoryPO;

/**
 * 病患历史数据
 *
 * @author yangbo
 *
 * @since 2020年1月26日 下午8:05:03
 *
 */
public interface ChinaCountStatisticsHistoryDAO extends JpaRepository<ChinaCountStatisticsHistoryPO, String>,
		JpaSpecificationExecutor<ChinaCountStatisticsHistoryPO> {

	/**
	 * 
	 * 查询指定区间的病患历史数据
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月26日 下午9:04:18
	 *
	 * @param startDate
	 * @param endDate
	 * @param areaTypes
	 * @return
	 */
	List<ChinaCountStatisticsHistoryPO> findByUpdateTimeBetweenAndAreaTypeIn(Date startDate, Date endDate,
			String[] areaTypes);

	/**
	 * 查询指定区间的病患历史数据-渠道百度
	 * @param startDate
	 * @param endDate
	 * @param areaTypes
	 * @param fromSource
	 * @return
	 */
    List<ChinaCountStatisticsHistoryPO> findByUpdateTimeBetweenAndAreaTypeInAndFromSource(Date startDate, Date endDate, String[] areaTypes, String fromSource);
}
