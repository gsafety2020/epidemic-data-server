package com.gsafety.gemp.wuhanncov.dao.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
* @ClassName: PestWechatUserPO 
* @Description: 微信用户表
* @author luoxiao
* @date 2020年2月25日 下午11:53:09 
*
 */
@Entity
@Table(name = "pest_wechat_user")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PestWechatUserPO implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "open_id")
	/**用户微信openId*/
	private String openId;
	
	@Column(name = "nick_name")
	/**昵称*/
	private String nickName;
	
	@Column(name = "gender")
	/**性别，0-未知，1-男性，2-女性*/
	private String gender;
	
	@Column(name = "city")
	/**城市*/
	private String city;
	
	@Column(name = "province")
	/**省*/
	private String province;
	
	@Column(name = "country")
	/**国家*/
	private String country;
	
	@Column(name = "avatar_url")
	/**头像*/
	private String avatarUrl;
	
	@Column(name = "union_id")
	/**唯一标识*/
	private String unionId;
	
	@Column(name = "with_credentials")
	/**是否授权，0-未授权，1-已授权*/
	private String withCredentials;
	
	@Column(name = "first_access_time")
	/**首次访问时间*/
	private Date firstAccessTime;
	
	@Column(name = "credentials_time")
	/**授权时间*/
	private Date credentialsTime;
	
	@Column(name = "update_time")
	/**最后更新时间*/
	private Date updateTime;
	
	@Column(name = "appid")
	/**appid*/
	private String appid;



}
