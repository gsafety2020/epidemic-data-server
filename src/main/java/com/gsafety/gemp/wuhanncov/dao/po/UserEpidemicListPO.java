package com.gsafety.gemp.wuhanncov.dao.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
* @ClassName: UserEpidemicListPO 
* @Description: 综合应用平台用户表
* @author luoxiao
* @date 2020年2月6日 下午7:55:25 
*
 */
@Entity
@Table(name = "user_epidemic_list")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserEpidemicListPO implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "user_id")
	private String userId;
	
	@Column(name = "user_name")
	private String userName;
	
	@Column(name = "mobile")
	private String mobile;
	
	@Column(name = "department_code")
	private String departmentCode;
	
	@Column(name = "leader")
	private String leader;
	
}
