package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "china_real_video_public")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChinaRealVideoPublicPO implements Serializable {
    private static final long serialVersionUID = -617070448265373881L;
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "date")
    private Date date;

    @Column(name = "title")
    private String title;

    @Column(name = "source_url")
    private String sourceUrl;

    @Column(name = "is_public")
    private Integer isPublic;

    @Column(name = "video_url")
    private String videoUrl;

    @Column(name = "video_big_url")
    private String videoBigUrl;

    @Column(name = "memo")
    private String memo;
}
