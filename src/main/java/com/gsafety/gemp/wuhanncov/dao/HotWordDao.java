package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.HotWordPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * 该类用于...,用于实现...等能力
 *
 * @author 86186
 * @since 2020/1/25  14:55
 * <p>
 * 1.0.0
 */
public interface HotWordDao extends JpaRepository<HotWordPO, String>,
        JpaSpecificationExecutor<HotWordPO> {
}
