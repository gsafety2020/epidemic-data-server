package com.gsafety.gemp.wuhanncov.dao.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The persistent class for the chinar_area_medical_daily database table.
 * 
 * 各地区医院情况日报
 * 
 */
@Entity
@Table(name = "china_area_medical_daily")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChinaAreaMedicalDailyPO implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@Id
	private String id;

	/**
	 * 地区编码
	 */
	@Column(name = "area_code")
	private String areaCode;

	/**
	 * 地区名称
	 */
	@Column(name = "area_name")
	private String areaName;

	/**
	 * 市编码
	 */
	@Column(name = "city_no")
	private String cityNo;

	/**
	 * 发热门诊数fever clinic
	 */
	@Column(name = "number_of_fc")
	private int numberOfFc;

	/**
	 * 发热门诊接诊数fever clinic reception
	 */
	@Column(name = "number_of_fcr")
	private int numberOfFcr;

	/**
	 * 收治病人数Number of patients admitted
	 */
	@Column(name = "number_of_pa")
	private int numberOfPa;

	/**
	 * 上级编码
	 */
	@Column(name = "parent_no")
	private String parentNo;

	/**
	 * 省编码
	 */
	@Column(name = "province_no")
	private String provinceNo;

	/**
	 * 报表日
	 */
	@Column(name = "stas_date")
	private String stasDate;

	/**
	 * 最后更新时间
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_time")
	private Date updateTime;

	@ManyToOne(targetEntity = LocationDimPO.class)
	@JoinColumn(name = "parent_no" ,referencedColumnName = "area_no", insertable = false, updatable = false)
	private LocationDimPO parentDim;

	@ManyToOne(targetEntity = LocationDimPO.class)
	@JoinColumn(name = "province_no" ,referencedColumnName = "area_no", insertable = false, updatable = false)
	private LocationDimPO provinceDim;

	@ManyToOne(targetEntity = LocationDimPO.class)
	@JoinColumn(name = "city_no" ,referencedColumnName = "area_no", insertable = false, updatable = false)
	private LocationDimPO cityDim;

	@Transient
	private String parentName;
	@Transient
	private String provinceName;
	@Transient
	private String cityName;
}