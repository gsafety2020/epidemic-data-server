package com.gsafety.gemp.wuhanncov.dao.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 全国人口迁移指数po
 *
 * @author: WangGang
 * @since 2020-02-06 21:10
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "china_migration_scale")
public class ChinaMigrationScalePO implements Serializable {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "area_id")
    private String areaId;

    @Column(name = "create_date")
    private String createDate;

    @Column(name = "migrate_type")
    private String migrateType;

    @Column(name = "area_type")
    private String areaType;

    @Column(name = "area_name")
    private String areaName;

    @Column(name = "value")
    private BigDecimal value;

}
