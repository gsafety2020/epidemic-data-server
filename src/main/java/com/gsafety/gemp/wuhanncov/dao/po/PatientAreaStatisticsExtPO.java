package com.gsafety.gemp.wuhanncov.dao.po;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 地区病患历史统计信息扩展表
 * @author fanlx
 * @since 2020-02-07 16:21:21
 */
@Entity
@Table(name = "patient_area_statistic_ext")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@DynamicInsert
@DynamicUpdate
public class PatientAreaStatisticsExtPO implements Serializable {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "area")
    private String area;

    @Column(name = "area_code")
    private String areaCode;

    @Column(name = "stas_time")
    private Date stasTime;

    @Column(name = "stas_date")
    private String stasDate;

    @Column(name = "stas_hour")
    private String stasHour;

    /**
     * 当日确诊比例=当前城市当日确诊数量/上级当日确诊数量
     */
    @Column(name = "new_case_rate")
    private BigDecimal newCaseRate;

    /**
     * 当日确诊比例=当前城市当日确诊数量/全国当日确诊数量
     */
    @Column(name = "new_case_all_rate")
    private BigDecimal newCaseAllRate;


    /**
     * 累计确诊比例=当前城市累计确诊数量/上级累计确诊数量
     */
    @Column(name = "total_case_rate")
    private BigDecimal totalCaseRate;

    /**
     * 累计确诊比例=当前城市累计确诊数量/全国累计确诊数量
     */
    @Column(name = "total_case_all_rate")
    private BigDecimal totalCaseAllRate;

    /**
     * 当日增长率=当日累计确诊/前一天累计确诊-1
     */
    @Column(name = "case_growth_rate")
    private BigDecimal caseGrowthRate;


    /**
     * 其他城市当日增长率=当日累计确诊/前一天累计确诊-1
     */
    @Column(name = "other_growth_rate")
    private BigDecimal otherGrowthRate;

    /**
     * 当前城市当日新增/当日累计
     */
    @Column(name = "new_total_rate")
    private BigDecimal newTotalRate;


    /**
     * 其他城市当日新增/当日累计
     */
    @Column(name = "other_new_total_rate")
    private BigDecimal otherNewTotalRate;

    /**
     * 死亡率=截止到当天累计死亡人数/截止到当天累计确诊
     */
    @Column(name = "death_rate")
    private BigDecimal deathRate;


    /**
     * 其他城市死亡率=截止到当天累计死亡人数/截止到当天累计确诊
     */
    @Column(name = "other_death_rate")
    private BigDecimal otherDeathRate;

    @Column(name = "data_src")
    private String dataSrc;

    @Column(name = "create_by")
    private String createBy;

    @Column(name = "upate_by")
    private String upateBy;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

}
