/**
 * 
 */
package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.LocationTokenPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * 地区登录
 *
 * @author xiaoz
 *
 * @since 2020年1月30日 下午8:55:26
 *
 */
public interface LocationTokenDao
		extends JpaRepository<LocationTokenPO, String>, JpaSpecificationExecutor<LocationTokenPO> {


	/**
	 * 验证口令和地区编码匹不匹配
	 * @param areaCode
	 * @param token
	 * @return
	 */
	List<LocationTokenPO> findByAreaCodeAndToken(String areaCode, String token);

}
