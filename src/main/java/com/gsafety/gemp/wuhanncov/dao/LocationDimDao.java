package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.LocationDimPO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * 该类用于...,用于实现...等能力
 *
 * @author 86186
 * @since 2020/1/25  14:56
 * <p>
 * 1.0.0
 */
public interface LocationDimDao extends JpaRepository<LocationDimPO, String>,
        JpaSpecificationExecutor<LocationDimPO> {
	
	/**
	 *
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 *
	 * @since 2020年1月25日 下午9:09:29
	 *
	 * @param parentNo
	 * @return
	 */
	List<LocationDimPO> findByParentNo(String parentNo,Sort sort);

	@Query(value=" from LocationDimPO where parentNo=0 or parentNo='100000'")
	List<LocationDimPO> findAllProvince();

	@Query(value=" from LocationDimPO where areaName like %:areaName% and parentNo='100000'")
	List<LocationDimPO> getLocationDimByProvinceName(@Param("areaName") String areaName);

	@Query(value=" from LocationDimPO where areaName = :province or shortName= :province")
	List<LocationDimPO> getLocationDimProvinceByName(@Param("province") String province);

	@Query(value=" from LocationDimPO where areaName like %:areaName%")
	List<LocationDimPO> getLocationDimByCityName(@Param("areaName") String areaName);

	@Query(value=" from LocationDimPO where autoJob=1")
	List<LocationDimPO> getAutoJobOn();

	/**
	 * 行政区划名称模糊查询
	 * @param areaName
	 * @return
	 */
	Page<LocationDimPO> findByAreaNameLike(String areaName, Pageable pageable);

	/**
	 * 查询当前节点和子节点
	 * @param parentNo
	 * @param areaNo
	 * @param sort
	 * @return
	 */
	List<LocationDimPO> findByParentNoOrAreaNo(String parentNo, String areaNo, Sort sort);

	LocationDimPO findByAreaNo(String areaNo);

	List<LocationDimPO> findByParentNo(String parentNo);
}
