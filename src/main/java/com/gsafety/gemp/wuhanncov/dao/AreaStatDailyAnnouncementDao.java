package com.gsafety.gemp.wuhanncov.dao;

import com.gsafety.gemp.wuhanncov.dao.po.AreaStatDailyAnnouncementPO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @Author ganbo
 * @Since 2020-02-09
 */
public interface AreaStatDailyAnnouncementDao extends JpaRepository<AreaStatDailyAnnouncementPO, String>,
        JpaSpecificationExecutor<AreaStatDailyAnnouncementPO> {

    AreaStatDailyAnnouncementPO findFirstByAreaCodeAndAnnouncementTypeOrderByUpdateTimeDesc(String areaCode, String type);


    List<AreaStatDailyAnnouncementPO> findByAreaCodeAndAnnouncementTypeOrderByUpdateTimeDesc(String areaCode, String type);

}
