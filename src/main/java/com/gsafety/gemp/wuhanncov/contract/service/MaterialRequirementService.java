package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.contract.dto.MaterialRequirementDTO;

import java.util.List;

public interface MaterialRequirementService {
    /**
     * 查询 指定区域及其下级区域的物资需求情况列表
     * @param areaNo 区域代码
     * @return
     */
    List<MaterialRequirementDTO> selectIncludeSubByAreaNo(String areaNo);
}
