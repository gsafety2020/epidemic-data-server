package com.gsafety.gemp.wuhanncov.contract.jobdto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 定时任务调用入参
 *
 * @author lyh
 * @since 2020/2/5  11:11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class JobRunParam implements Serializable {

    @ApiModelProperty("任务Id")
    private Integer jobId;

    @ApiModelProperty("任务执行Id")
    private Long jobRunId;

    @ApiModelProperty("任务执行数据参数")
    private String data;
}
