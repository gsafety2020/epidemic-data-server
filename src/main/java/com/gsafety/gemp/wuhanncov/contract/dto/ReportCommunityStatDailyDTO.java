package com.gsafety.gemp.wuhanncov.contract.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 社区人员情况汇总
 * @author dusiwei
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description="社区人员情况汇总")
public class ReportCommunityStatDailyDTO implements Serializable {
    private static final long serialVersionUID = 3349970726973655894L;

    @ApiModelProperty(value = "id", example = "\"20200209420115001\"")
    private String id;

    /**
     * 行政编码
     */
    @ApiModelProperty(value = "行政编码", example = "\"420115001\"")
    private String areaCode;

    @ApiModelProperty(value = "行政名称", example = "纸坊街道")
    private String areaName;

    /**
     * 上级编码
     */
    @ApiModelProperty(value = "上级编码", example = "\"420115\"")
    private String parentAreaCode;

    /**
     * 统计日期（yyyymmdd）
     */
    @ApiModelProperty(value = "统计日期（yyyymmdd）", example = "\"20200209\"")
    private String statDate;

    /**
     * 累计发热
     */
    @ApiModelProperty(value = "累计发热", example = "0")
    private Integer totalFeverNumber;

    /**
     * 新增发热
     */
    @ApiModelProperty(value = "新增发热", example = "0")
    private Integer newFeverNumber;

    /**
     * 累计隔离
     */
    @ApiModelProperty(value = "累计隔离", example = "0")
    private Integer totalIsolatedNumber;

    /**
     * 新增隔离
     */
    @ApiModelProperty(value = "新增隔离", example = "0")
    private Integer newIsolatedNumber;

    /**
     * 累计疑似
     */
    @ApiModelProperty(value = "累计疑似", example = "0")
    private Integer totalSuspectedNumber;

    /**
     * 新增疑似
     */
    @ApiModelProperty(value = "新增疑似", example = "0")
    private Integer newSuspectedNumber;

    /**
     * 累计确诊
     */
    @ApiModelProperty(value = "累计确诊", example = "0")
    private Integer totalDiagnosedNumber;

    /**
     * 新增确诊
     */
    @ApiModelProperty(value = "新增确诊", example = "0")
    private Integer newDiagnosedNumber;

    /**
     * 更新时间
     */
    @ApiModelProperty(hidden = true)
    private Date updateTime;

}
