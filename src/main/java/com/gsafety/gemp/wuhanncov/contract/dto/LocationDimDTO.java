/**
 * 
 */
package com.gsafety.gemp.wuhanncov.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.math.BigDecimal;

/**
 * 地区热力模型
 *
 * @author yangbo
 *
 * @since 2020年1月25日 下午7:05:51
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LocationDimDTO {

	@ApiModelProperty(value = "区域编码")
	private String areaNo;

	@ApiModelProperty(value = "区域名称")
	private String areaName;

	@ApiModelProperty(value = "父区域编码")
	private String parentNo;

	@ApiModelProperty(value = "排序")
	private Integer seqNum;

	@ApiModelProperty(value = "维度")
	private BigDecimal latitude;

	@ApiModelProperty(value = "经度")
	private BigDecimal longitude;

	@ApiModelProperty(value = "简称")
	private String shortName;

	@ApiModelProperty(value = "是否自动加工")
	private String autoJob;
}
