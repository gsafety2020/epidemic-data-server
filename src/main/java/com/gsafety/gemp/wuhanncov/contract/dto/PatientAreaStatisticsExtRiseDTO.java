package com.gsafety.gemp.wuhanncov.contract.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author ganbo
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description="病例增长率对象")
public class PatientAreaStatisticsExtRiseDTO implements Serializable {

    private static final long serialVersionUID = 1856130557343517618L;

    @ApiModelProperty(value = "日期。 格式：yyyy/MM/dd")
    private String x;

    @ApiModelProperty(value = "根据s的类型显示的增长率")
    private String y;

    @ApiModelProperty(value = "数据类型。 1:某市增长率;2:某省其他城市增长率;3:全国其他省份的增长率")
    private String s;



}
