package com.gsafety.gemp.wuhanncov.contract.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
* @ClassName: UserEpidemicInfoDTO 
* @Description: 新型冠状病毒-用户上报DTO
* @author luoxiao
* @date 2020年2月3日 上午12:18:50 
*
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("返回人员的详细信息")
public class UserEpidemicInfoDTO implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value="主键UUID,自动生成")
	private String epidemicInfoId;
	
	@ApiModelProperty(value = "用户Id")
	private String userId;
	
	@ApiModelProperty(value = "用户姓名")
	private String userName;
	
	@ApiModelProperty(value = "科室名")
	private String departmentName;
	
	@ApiModelProperty(value = "科室code")
	private String departmentCode;
	
	@ApiModelProperty(value = "电话")
	private String mobile;
	
	@ApiModelProperty(value = "常住地址")
	private String parmanentAddress;
	
	@ApiModelProperty(value = "现住地址")
	private String presentAddress;
	
	@ApiModelProperty(value = "本人状况（0-确诊/1-疑似/2-密接/3-正常）")
	private String userCondition;
	
	@ApiModelProperty(value = "本人详情")
	private String userDetail;
	
	@ApiModelProperty(value = "居家隔离日期，表单填写")
	@JsonFormat(pattern = "yyyy-MM-dd",locale = "zh",timezone = "GMT+8")
	private Date homeQuarantineDate;
	
	@ApiModelProperty(value = "居家隔离天数，起始日至今，后台计算")
	private Integer homeQuarantineDays;
	
	@ApiModelProperty(value = "家庭成员数")
	private Integer familyNumbers;
	
	@ApiModelProperty(value = "家庭成员状况（0-确诊/1-疑似/2-密接/3-正常）")
	private String familyCondition;
	
	@ApiModelProperty(value = "家庭详情")
	private String familyDetail;
	
	@ApiModelProperty(value = "上报日期")
	@JsonFormat(pattern = "yyyy-MM-dd",locale = "zh",timezone = "GMT+8")
	private Date reportDate;
	
	@ApiModelProperty(value = "其他")
	private String ext;
}
