package com.gsafety.gemp.wuhanncov.contract.enums;

import lombok.Getter;

/**
 * patient_area_statistic表数量对应的类型枚举
 *
 * @author lyh
 * @since 2020/2/2  14:46
 */
@Getter
public enum NumberTypeEnum {

    CONFIRMED("confirmed","getPatientNumber", "确诊"),
    CURED("cured","getCuredNumber", "治愈"),
    SEVERE("severe","getSevereNumber", "重症"),
    DEATH("death","getDeathNumber", "死亡"),
    DEBT("debt","getDebtNumber", "疑似病例"),
    DYING("dying","getDyingNumber", "病危"),
    OBSERVED("observed","getObservedNumber", "接受医学观察者"),
    CLOSE_CONTACTS("clsconts","getCloseContactsNumber", "密切接触者"),
    RELEASE_OBSERVED("rlsobs","getReleaseObservedNumber", "解除接受医学观察者"),
    NEW_DEBT("newdebt","getNewDebt", "新增疑似"),
    CUR_CONFIRMED("curconfr","getCurConfirm", "当前为确诊状态的总数量，非累计"),
    HOT("hot","getHotNumber", "当日接诊发热数量"),
    NEW_CURED("newcured","getNewcured", "新增出院"),
    NEW_DEATH("newdeath","getNewdeath", "新增死亡"),
    NEW_DYING("newdying","getNewdying", "新增病危"),
    NEW_SEVERE("newsevere","getNewsevere", "新增重症"),
    INC("new","getNewNumber", "新增确诊");

    String type;
    String methodName;
    String description;

    NumberTypeEnum(String t, String m, String d){
        this.type = t;
        this.methodName = m;
        this.description = d;
    }

    public static String getFiledNameByType(String t){
        for(NumberTypeEnum en : NumberTypeEnum.values()){
            if(en.type.equals(t)){
                return en.methodName;
            }
        }
        return null;
    }

}
