/**
 * 
 */
package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.contract.dto.BasicReportDTO;

/**
 * 该类用于...，用于实现...等能力。
 *
 * @author yangbo
 *
 * @since 2020年2月7日 下午11:25:08
 *
 */
public interface BasicReportService {

	/**
	 * 
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月7日 下午11:31:32
	 *
	 * @param date
	 * @return
	 */
	BasicReportDTO findByDate(String date);
}
