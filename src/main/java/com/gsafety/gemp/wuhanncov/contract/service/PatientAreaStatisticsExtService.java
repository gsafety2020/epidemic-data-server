package com.gsafety.gemp.wuhanncov.contract.service;


import com.gsafety.gemp.wuhanncov.contract.dto.*;

import java.util.List;
import java.util.Map;

/**
 * 地区病患统计信息扩展服务
 *
 * @author fanlx
 * @since 2020/02/02  16:25
 *
 */
public interface PatientAreaStatisticsExtService {



    /**
     * 查询市-省-全国累计病例数增长率
     * @return
     */
    Map<String,Object> showCitySickRiseRate(String areaCode);


    /**
     * 查询市-省-全国死亡比例对比图
     * @return
     */
    Map<String,Object> showCitySickDeathRate(String areaCode);



    /***
     * 多个行政区病患比例变化趋势接口
     * 可以用于查询指定多个区域的患者确诊比例变化趋势情况，折线图展示
     * 数据来源表：patient_area_statistic_ext
     * @author  dusiwei
     * @since   2020/2/7 14:21
     * @param   areaId 指定区域行政区划编码
     * @return  java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaCaseRateDailyDTO>
     */
    List<PatientAreaCaseRateDailyDTO> getAreaListPatientCaseRate(String areaId);

    /***
     * 查询多个行政区新增病例数与累计病例数比例变化接口
     * 可以用于查询多个行政区新增病例数与累计病例数比例变化，折线图展示
     * 数据来源表：patient_area_statistic_ext
     * @author  dusiwei
     * @since   2020/2/7 14:21
     * @param   areaId 指定区域行政区划编码
     * @return  java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaNewTotalRateDailyDTO>
     */
    List<PatientAreaNewTotalRateDailyDTO> getAreaListPatientNewTotalRate(String areaId);

    /**
     * 查询单个城市的新增确诊病例变化图，主要用于折线图
     * @param areaId
     * @return
     */
    List<PatientAreaCountDailyDTO> getNewAreaListPatientCountHistory(String areaId);

    /**
     * 数据同步接口
     * @param date
     */
    void syncData(String date);
}
