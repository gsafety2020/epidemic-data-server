/**
 * 
 */
package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.contract.dto.LocationTokenDTO;
import com.gsafety.gemp.wuhanncov.contract.params.DimQueryParam;
import com.gsafety.gemp.wuhanncov.dao.po.LocationTokenPO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * 地区用户服务
 *
 * @author yangbo
 *
 * @since 2020年1月30日 下午8:58:48
 *
 */
public interface LocationTokenService {

    /**
     * 根据主键查询locatiotoken
     * @param id
     * @return
     */
    LocationTokenPO getOne(String id);

    /**
     * token更新
     * @param ltp
     * @return
     */
    LocationTokenPO updateToken(LocationTokenPO ltp);

    /**
     * 查询系统登录口令
     * @param queryParam
     * @param sort
     * @return
     */
    Page<LocationTokenDTO> findTokenPage(DimQueryParam queryParam, Pageable sort);
}
