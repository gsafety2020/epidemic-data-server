package com.gsafety.gemp.wuhanncov.contract.enums;

import lombok.Getter;

/**
 * @author liyanhong
 * @since 2020/2/12  10:21
 */
@Getter
public enum FileBnTypeEnum {

    Area_daily_csv("area_daily_csv","区域疫情日报csv文件");

    private String typeCode;

    private String typeName;

    FileBnTypeEnum(String c, String n){
        this.typeCode = c;
        this.typeName = n;
    }
}
