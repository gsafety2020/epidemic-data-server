package com.gsafety.gemp.wuhanncov.contract.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class DataSyncLogDTO {

    /**主键id**/
    @ApiModelProperty(name = "主键id")
    private String id;

    /**数据源文件id**/
    @ApiModelProperty(name = "数据源文件id")
    private String srcFileId;

    /**数据目标表**/
    @ApiModelProperty(name = "数据目标表")
    private String targetTable;

    /**数据同步开始时间**/
    @ApiModelProperty(name = "数据同步开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startTime;

    /**数据同步结束时间**/
    @ApiModelProperty(name = "数据同步结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date endTime;

    /**同步成功的记录条数**/
    @ApiModelProperty(name = "同步成功的记录条数")
    private Integer successRecord;

    /**同步总记录条数**/
    @ApiModelProperty(name = "同步总记录条数")
    private Integer totalRecord;

    /**同步任务整体是否成功。1：全部成功；2：部分成功；3：全部异常**/
    @ApiModelProperty(name = "同步任务整体是否成功")
    private Integer status;
    /**同步任务整体是否成功。1：全部成功；2：部分成功；3：全部异常**/
    @ApiModelProperty(name = "同步任务整体是否成功")
    private String statusName;

    /**创建时间**/
    @ApiModelProperty(name = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    /**更新时间**/
    @ApiModelProperty(name = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    /**创建人**/
    @ApiModelProperty(name = "创建人")
    private String createBy;

    /**更新人**/
    @ApiModelProperty(name = "更新人")
    private String updateBy;
}
