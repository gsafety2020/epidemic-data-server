package com.gsafety.gemp.wuhanncov.contract.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author zhoushuyan
 * @date 2020-02-07 0:22
 * @Description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChinaAreaCaseInfoValue implements Serializable {
    private static final long serialVersionUID = 8208621638289374062L;

    @JsonProperty("region")
    @ApiModelProperty(value = "地区")
    private String areaName;


    @JsonProperty("place")
    @ApiModelProperty(value = "社区")
    private String community;

    @ApiModelProperty(value = "时间", hidden = true)
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @JsonProperty("date")
    private Date updateTime;

}
