/**
 * 
 */
package com.gsafety.gemp.wuhanncov.contract.dto;

/**
 * 热词等级
 *
 * @author yangbo
 *
 * @since 2020年1月25日 下午4:41:41
 *
 */
public enum HotwordLevel {
	/**
	 * 
	 */
	LEVEL01(1) {
		@Override
		public int randomValue() {
			return 10000;
		}
	},
	/**
	 * LEVEL
	 */
	LEVEL02(2) {
		@Override
		public int randomValue() {
			return 9000;
		}
	},
	/** LEVEL */
	LEVEL03(3) {
		@Override
		public int randomValue() {
			return 8000;
		}
	},
	/** LEVEL */
	LEVEL04(4) {
		@Override
		public int randomValue() {
			return 7000;
		}
	},
	/** LEVEL */
	LEVEL05(5) {
		@Override
		public int randomValue() {
			return 6000;
		}
	},
	/** LEVEL */
	LEVEL06(6) {
		@Override
		public int randomValue() {
			return 5000;
		}
	},
	/** LEVEL */
	LEVEL07(7) {
		@Override
		public int randomValue() {
			return 4000;
		}
	},
	/** LEVEL */
	LEVEL08(8) {
		@Override
		public int randomValue() {
			return 3000;
		}
	},
	/** LEVEL */
	LEVEL09(9) {
		@Override
		public int randomValue() {
			return 2000;
		}
	},
	/** LEVEL */
	LEVEL10(10) {
		@Override
		public int randomValue() {
			return 1000;
		}
	};

	private final int hotVal;

	private HotwordLevel(int hotVal) {
		this.hotVal = hotVal;
	}

	public int getHotVal() {
		return hotVal;
	}

	/**
	 * 
	 * randomValue
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月25日 下午4:59:03
	 *
	 * @return
	 */
	public abstract int randomValue();

	/**
	 * 
	 * 获取指定索引的HotwordLevel
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月25日 下午4:47:21
	 *
	 * @param i
	 * @return
	 */
	public static HotwordLevel get(int i) {
		return values()[i];
	}

}
