/**
 * 
 */
package com.gsafety.gemp.wuhanncov.contract.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gsafety.gemp.wuhanncov.dao.po.BasicReportPO;
import com.gsafety.gemp.wuhanncov.dao.po.BasicReportPO.BasicReportPOBuilder;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 该类用于...，用于实现...等能力。
 *
 * @author yangbo
 *
 * @since 2020年2月7日 下午11:30:46
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BasicReportStaticsDTO {

	@JsonProperty("x")
	@ApiModelProperty(value = "时间")
	private String date;

	@JsonProperty("y")
	@ApiModelProperty(value = "数量")
	private int value;

	@JsonProperty("s")
	@ApiModelProperty(value = "类型  1 上报 2 发热 3 社区")
	private String type;
	
}
