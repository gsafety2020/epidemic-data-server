/**
 * 
 */
package com.gsafety.gemp.wuhanncov.contract.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 热词模型。
 *
 * @author yangbo
 *
 * @since 2020年1月25日 下午4:31:00
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TopArea {

	/**
	 * DEFAULT_SEPARATORCHARS
	 */
	public static final String DEFAULT_SEPARATORCHARS = ";";

	List<TopAreaInfo> topAreaInfo;

	/**
	 * 
	 * 增加情感分析
	 *
	 * @author zhoushuyan
	 * 
	 * @since 2020年1月25日 下午4:36:07
	 *
	 * @param value
	 * @param name
	 */
	public void add(String code, String name,int value) {
		if (CollectionUtils.isEmpty(topAreaInfo)) {
			this.topAreaInfo = new ArrayList<>();
		}
		this.topAreaInfo.add(new TopAreaInfo(code,name, value));
	}

	@Data
	@AllArgsConstructor
	public class TopAreaInfo {

		private String code;

		private String name;

		private int value;
	}
}
