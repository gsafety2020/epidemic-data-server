package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.contract.dto.ChinaRealBroadcastDTO;

import java.util.List;

/**
 * 该类用于武汉冠状病毒数据服务,用于实现业务查询等能力
 *
 * @author 86186
 * @since 2020/1/25  11:01
 * <p>
 * 武汉冠状病毒数据服务
 */
public interface NcovDataService {

    /**
     * 该方法用于武汉冠状病毒疫情大屏，用于实现查询实时播报等能力。
     * @author  lyh
     * @since   2020/1/25 12:28
     * @return  java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.ChinaRealBroadcastDTO>
     * @throws
     */
    List<ChinaRealBroadcastDTO> getAllRealBroadcast();
}
