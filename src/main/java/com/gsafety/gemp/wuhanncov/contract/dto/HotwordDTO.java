/**
 * 
 */
package com.gsafety.gemp.wuhanncov.contract.dto;

import java.io.Serializable;
import java.util.Date;

import com.gsafety.gemp.wuhanncov.dao.po.HotWordPO;
import com.gsafety.gemp.wuhanncov.dao.po.HotWordPO.HotWordPOBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * HotwordDTO实体
 *
 * @author yangbo
 *
 * @since 2020年1月25日 下午3:24:58
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HotwordDTO implements Serializable {

	private static final long serialVersionUID = -1L;

    private String wordTime;

    private String partWord;

    private Date createTime;
}
