package com.gsafety.gemp.wuhanncov.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 该类用于疫情大屏,用于实现封装疫情讨论量趋势数据等能力
 *
 * @author 86186
 * @since 2020/1/25  17:52
 * <p>
 * 1.0.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PubSensHeatDTO implements Serializable {

    private static final long serialVersionUID = 4635064662584383265L;

    @ApiModelProperty(value = "日期")
    private String x;

    @ApiModelProperty(value = "讨论量")
    private Integer y;

    @ApiModelProperty(value = "类型")
    private String s = "1";
}
