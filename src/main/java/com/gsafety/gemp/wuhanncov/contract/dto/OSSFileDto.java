package com.gsafety.gemp.wuhanncov.contract.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author zhoushuyan
 * @date 2020-02-10 19:05
 * @Description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OSSFileDto {

    @JsonProperty("value")
    @ApiModelProperty(value = "文件名称")
    private String fileName;

    @JsonProperty("url")
    @ApiModelProperty(value = "文件http路径")
    private String url;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @JsonProperty("date")
    @ApiModelProperty(value = "文件时间")
    private Date fileDate;
}
