package com.gsafety.gemp.wuhanncov.contract.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zhoushuyan
 * @date 2020-01-26 14:21
 * @Description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AreaInfo {

    public static final String DEFAULT_SEPARATORCHARS = ";";

    List<AreaInfoText> areaInfoText;

    /**
     *
     * 增加情感分析
     *
     * @author zhoushuyan
     *
     * @since 2020年1月25日 下午4:36:07
     *
     * @param value
     * @param name
     */
    public void add(String name,String abcode,String level,String parent) {
        if (CollectionUtils.isEmpty(areaInfoText)) {
            this.areaInfoText = new ArrayList<>();
        }
        this.areaInfoText.add(new AreaInfoText(name, abcode,level,parent));
    }

    @Data
    @AllArgsConstructor
    public class AreaInfoText {

        private String name;

        private String adcode;

        private String level;

        private String parent;
    }
}
