package com.gsafety.gemp.wuhanncov.contract.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author zhoushuyan
 * @date 2020-02-09 10:57
 * @Description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BasicReportInfoDTO {
    @ApiModelProperty(value = "更新时间")
    String id;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    LocalDateTime reportTime;

    @JsonProperty("value")
    @ApiModelProperty(value = "内容")
    String reportInfo;
}
