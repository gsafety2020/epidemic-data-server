package com.gsafety.gemp.wuhanncov.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

public class ChinaRealBroadcastDTO implements Serializable {
    private static final long serialVersionUID = -3947094354912040759L;

    @ApiModelProperty(value = "记录主键")
    private Integer id;

    @ApiModelProperty(value = "播报时间")
    private Date time;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "详情")
    private String detail;

    @ApiModelProperty(value = "原文链接")
    private String link;

    @ApiModelProperty(value = "备注")
    private String memo;
}
