package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.contract.dto.ReportAreaStatDailyDTO;

/**
 * 每日情况汇总表,汇总江夏区每日上报情况
 * @author Luoganggang
 * @since 2020/2/9 16:27
 */
public interface ReportAreaStatDailyService {

    /**
     * 集中医学观察每日情况汇总
     * @param areaCode 行政编码
     * @return ReportAreaStatDailyDTO
     */
    ReportAreaStatDailyDTO dailySummary(String areaCode);
}
