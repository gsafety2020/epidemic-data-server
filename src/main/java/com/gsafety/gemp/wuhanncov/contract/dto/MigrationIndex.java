package com.gsafety.gemp.wuhanncov.contract.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 全国地区迁移指数返回实体
 *
 * @author: WangGang
 * @since 2020-02-06 21:25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MigrationIndex {

    /**
     * 公历日期
     */
    private String migrateDate;

    /**
     * 农历日期
     */
    private String lunarCalendar;

    /**
     * 今年指数
     */
    private BigDecimal thisYear;

    /**
     * 去年指数
     */
    private BigDecimal lastYear;
}
