package com.gsafety.gemp.wuhanncov.contract.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("返回已经上报过人员的部分信息")
public class UserEpidemicInfoPartDTO implements Serializable{
	
	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "常住地址")
	private String parmanentAddress;
	
	@ApiModelProperty(value = "现住地址")
	private String presentAddress;
	
	@ApiModelProperty(value = "居家隔离日期，表单填写")
	@JsonFormat(pattern = "yyyy-MM-dd",locale = "zh",timezone = "GMT+8")
	private Date homeQuarantineDate;
	
	@ApiModelProperty(value = "居家隔离天数，起始日至今，后台计算")
	private Integer homeQuarantineDays;
	
	@ApiModelProperty(value = "家庭成员数")
	private Integer familyNumbers;
	
	@ApiModelProperty(value = "用户姓名")
	private String userName;
	
	@ApiModelProperty(value = "科室")
	private String departmentName;
	
	@ApiModelProperty(value = "科室code")
	private String departmentCode;
}
