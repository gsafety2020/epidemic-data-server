package com.gsafety.gemp.wuhanncov.contract.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WorldRealBroadcastDTO implements Serializable {

    private static final long serialVersionUID = 3103886738815704200L;

    private String publishTime;

//    private Date publish_date;
//
//    private String title;

    private String outline;

    private String weblink;

//    private String memo;

    private String dataSource;

    private String timestamp;

//    private String urlid;

}
