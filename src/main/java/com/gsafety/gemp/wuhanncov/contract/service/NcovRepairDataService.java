package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.contract.dto.ChinaCountStatistics;
import com.gsafety.gemp.wuhanncov.contract.dto.ChinaCountStatisticsDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.Result;
import io.swagger.annotations.ApiOperation;

import java.util.List;

/**
 * @author zhoushuyan
 * @date 2020-01-29 9:46
 * @Description
 */
public interface NcovRepairDataService {
    /**
     *
     * 全国各地区例统计列表：areaType：country province city
     *
     * @author zhoushuyan
     *
     * @since 2020年1月26日 上午9:23:08
     *
     * @return
     */
    List<ChinaCountStatisticsDTO> findAreaCountList(String areaType);
    /**
     *
     * 根据名称模糊查询地区病例列表
     * 默认查询所有省
     * @author zhoushuyan
     *
     * @since 2020年1月26日 上午9:23:08
     *
     * @return
     */
    List<ChinaCountStatisticsDTO> findAreaCountByNameList(String areaName);

    /**
     *
     * 查询市区列表
     * @author zhoushuyan
     *
     * @since 2020年1月26日 上午9:23:08
     *
     * @return
     */
    @ApiOperation(value = " 查询市区列表")
    List<ChinaCountStatisticsDTO> findAreaCountByProvince(String province);

    /**
     *
     * 查询市区列表
     * @author zhoushuyan
     *
     * @since 2020年1月26日 上午9:23:08
     *
     * @return
     */
    ChinaCountStatisticsDTO findAreaCountById(String id);

    /**
     *
     * 修改详细信息
     * @author zhoushuyan
     *
     * @since 2020年1月26日 上午9:23:08
     *
     * @return
     */
    Result updateAreaCount(ChinaCountStatistics chinaCountStatistics);

    /**
     * 初始化指定区域的疫情统计历史信息
     * @param areaCode
     * @param days
     * @return
     */
    Result initPatientHistoryData(String areaCode, Integer days);
}
