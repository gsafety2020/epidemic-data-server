package com.gsafety.gemp.wuhanncov.contract.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.util.Date;

/**
 * @author zhoushuyan
 * @date 2020-02-10 19:05
 * @Description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OSSAllFileDto {

    @ApiModelProperty(value = "文件ID-主键")
    private String fileId;

    @ApiModelProperty(value = "文件名称")
    private String fileName;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "文件时间")
    private Date fileDate;

    @ApiModelProperty(value = "状态 -1 删除 5 正常")
    private String status;

    @ApiModelProperty(value = "文件http路径")
    private String url;

    @ApiModelProperty(value = "地区code")
    private String areaCode;

    @ApiModelProperty(value = "地区名称")
    private String areaName;

    @ApiModelProperty(value = "文件类型 默认:0 分析报告 1 是趋势分析")
    private String fileType;

    @ApiModelProperty(value = "文件类型 默认:0 分析报告 1 是趋势分析")
    private String fileTypeName;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
    private Date updateDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private Date createDate;



}
