package com.gsafety.gemp.wuhanncov.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhoushuyan
 * @date 2020-02-06 17:24
 * @Description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NewsInfo {
    @ApiModelProperty(value = "标题")
    String title;

    @ApiModelProperty(value = "时间")
    String time;

    @ApiModelProperty(value = "新闻url地址")
    String link;

    @ApiModelProperty(value = "详细内容")
    String detail;
}
