package com.gsafety.gemp.wuhanncov.contract.service;

/**
 * 数据同步加工服务
 * @author liyanhong
 * @since 2020/2/12  14:28
 */
public interface SyncDataService {

    /**
     * 处理已经上传的区域疫情日报csv文件数据
     * @param fileId
     * @return
     */
    Integer handleAreaDailyCsvfile(String fileId);
}
