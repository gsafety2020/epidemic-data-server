package com.gsafety.gemp.wuhanncov.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 该类用于疫情大屏,用于实现封装区域按天统计患者人数数据等能力
 *
 * @author lyh
 * @since 2020/1/26  19:29
 * <p>
 * 1.0.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PatientAreaCountDailyDTO implements Serializable {
    @ApiModelProperty(value = "日期。日期格式：yyyy/MM/dd")
    private String x;

    @ApiModelProperty(value = "确诊患者数量")
    private Integer y;

    @ApiModelProperty(value = "区域在折线图上的代号：根据区域的编码入参顺序从1开始递增")
    private String s;
}
