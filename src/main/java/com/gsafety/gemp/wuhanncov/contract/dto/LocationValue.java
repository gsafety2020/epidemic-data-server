package com.gsafety.gemp.wuhanncov.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author zhoushuyan
 * @date 2020-01-30 21:52
 * @Description
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LocationValue {
    private static final long serialVersionUID = -4199246899895998820L;

    @ApiModelProperty(value = "lng")
    BigDecimal lng;

    @ApiModelProperty(value = "lat")
    BigDecimal lat;

    @ApiModelProperty(value = "value")
    Integer value;
}
