package com.gsafety.gemp.wuhanncov.contract.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("城市编码返回参数")
public class PestHealthCodeSaveDTO implements Serializable{
	
	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value="简称")
	private String nickName;
	
	@ApiModelProperty(value="用户姓名")
	private String userName;
	
	@ApiModelProperty(value="二维码路径")
	private String healthCodeUrl;
	
	@ApiModelProperty(value="健康码颜色(后台自动生成,red：红色，green：绿色，yellow：黄色)")
	private String healthCodeColor;
	
	@ApiModelProperty(value="创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd",locale = "zh",timezone = "GMT+8")
	private Date createTime;
	
	@ApiModelProperty(value="更新时间")
	@JsonFormat(pattern = "yyyy-MM-dd",locale = "zh",timezone = "GMT+8")
	private Date updateTime;

}
