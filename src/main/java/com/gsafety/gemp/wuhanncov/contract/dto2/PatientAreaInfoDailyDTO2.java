/**
 * 
 */
package com.gsafety.gemp.wuhanncov.contract.dto2;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 该类用于...，用于实现...等能力。
 *
 * @author yangbo
 *
 * @since 2020年2月1日 下午11:12:24
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PatientAreaInfoDailyDTO2 {

	private String id;

	private String area;

	private String areaCode;

	private Date stasTime;

	private String stasDate;

	private String stasHour;

	private Integer patientNumber;

	private Integer debtNumber;

	private Integer deathNumber;

	private Integer newDebt;

	private Integer severeNumber;

	private Integer curedNumber;

	private String dataSrc;

	private String createBy;

	private String upateBy;

	private Date createTime;

	private Date updateTime;

	private String postDate;
}
