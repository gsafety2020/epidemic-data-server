/**
 * 
 */
package com.gsafety.gemp.wuhanncov.contract.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;

/**
 * 热词模型。
 *
 * @author yangbo
 *
 * @since 2020年1月25日 下午4:31:00
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EmotionAnalyzingContent {

	/**
	 * DEFAULT_SEPARATORCHARS
	 */
	public static final String DEFAULT_SEPARATORCHARS = ";";

	Collection<EmotionAnalyzingText> emotionAnalyzingText;

	/**
	 * 
	 * 增加情感分析
	 *
	 * @author zhoushuyan
	 * 
	 * @since 2020年1月25日 下午4:36:07
	 *
	 * @param value
	 * @param type
	 */
	public void add(int value, String type) {
		if (CollectionUtils.isEmpty(emotionAnalyzingText)) {
			this.emotionAnalyzingText = new ArrayList<>();
		}
		this.emotionAnalyzingText.add(new EmotionAnalyzingText(value, type));
	}

	@Data
	@AllArgsConstructor
	public class EmotionAnalyzingText {

		private int value;

		private String type;
	}
}
