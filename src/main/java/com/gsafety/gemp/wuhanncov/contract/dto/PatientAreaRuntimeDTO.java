package com.gsafety.gemp.wuhanncov.contract.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 该类用于疫情大屏,用于实现封装区域按天统计患者人数数据等能力
 *
 * @author lyh
 * @since 2020/1/26  19:29
 * <p>
 * 1.0.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "实时病患数据对象")
public class PatientAreaRuntimeDTO implements Serializable {

    @ApiModelProperty(value = "主键")
    private String id;

    @ApiModelProperty(value = "地区编码", example = "\"420115404000\"", required = true)
    private String areaCode;

    @ApiModelProperty(value = "地区类型", hidden = true)
    private String areaType;

    @ApiModelProperty(value = "地区名称")
    private String areaName;

    @ApiModelProperty(value = "新增死亡", example = "0")
    private Integer newDead;

    @ApiModelProperty(value = "新增治愈", example = "0")
    private Integer newCure;

    @ApiModelProperty(value = "新增疑似", example = "0")
    private Integer newProbable;

    @ApiModelProperty(value = "新增确诊", example = "0")
    private Integer newCase;

    @ApiModelProperty(value = "累计治愈", example = "0")
    private Integer cureCase;

    @ApiModelProperty(value = "累计死亡", example = "0")
    private Integer deadCase;

    @ApiModelProperty(value = "累计疑似", example = "0")
    private Integer probableCase;

    @ApiModelProperty(value = "累计确诊", example = "0")
    private Integer confirmCase;

    @ApiModelProperty(value = "更新时间", hidden = true)
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    @ApiModelProperty(value = "上级编码", hidden = true)
    private String parentNo;

    @ApiModelProperty(value = "上级行政区划名称", hidden = true)
    private String parent;

    @ApiModelProperty(value = "省级编码", hidden = true)
    private String provinceNo;

    @ApiModelProperty(value = "省、直辖市名称", hidden = true)
    private String province;

    @ApiModelProperty(value = "市级编码", hidden = true)
    private String cityNo;

    @ApiModelProperty(value = "城市名称", hidden = true)
    private String city;

    @ApiModelProperty(value = "病危数量", example = "0")
    private Integer dyingCase;

    @ApiModelProperty(value = "接受医学观察者数量", example = "0")
    private Integer observedCase;

    @ApiModelProperty(value = "密切接触者数量", example = "0")
    private Integer closeContactsCase;

    @ApiModelProperty(value = "解除接受医学观察者数量", example = "0")
    private Integer releaseObservedCase;

    @ApiModelProperty(value = "新增病危数量", example = "0")
    private Integer newDying;

    @ApiModelProperty(value = "新增接受医学观察者数量", example = "0")
    private Integer newObserved;

    @ApiModelProperty(value = "新增密切接触者数量", example = "0")
    private Integer newCloseContacts;

    @ApiModelProperty(value = "新增解除接受医学观察者数量", example = "0")
    private Integer newReleaseObserved;

    @ApiModelProperty(value = "发热数量", example = "0")
    private Integer hotCase;

    @ApiModelProperty(value = "新增发热数量", example = "0")
    private Integer newHot;

    @ApiModelProperty(value = "重症数量", example = "0")
    private Integer severeCase;

    @ApiModelProperty(value = "新增重症数量", example = "0")
    private Integer newSevere;

    @ApiModelProperty(value = "当前为确诊状态的总数量，非累计", example = "0")
    private Integer curConfirm;

    @ApiModelProperty(value = "锁定标识", example = "LOCK")
    private String lockFlag;
}
