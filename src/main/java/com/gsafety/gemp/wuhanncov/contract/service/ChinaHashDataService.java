/**
 * 
 */
package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.contract.dto.ChinaHashDataDTO;
import com.gsafety.gemp.wuhanncov.contract.params.DimQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * ChinaHashData业务服务
 *
 * @author yangbo
 *
 * @since 2020年2月8日 下午10:04:20
 *
 */
public interface ChinaHashDataService {

	/**
	 * 
	 * 按行政区划查询散列数据
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月8日 下午10:13:03
	 *
	 * @param areaCode
	 * @return
	 */
	List<ChinaHashDataDTO> findByAreaCode(String areaCode);

	/**
	 * 
	 * 按行政区划查询散列数据,并转为map
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月8日 下午10:13:03
	 *
	 * @param areaCode
	 * @return
	 */
	Map<String, Object> findMapByAreaCode(String areaCode);

	/**
	 * 非病患散列信息实时数据分页查询
	 * @param queryParam
	 * @param sort
	 * @return
	 */
    Page<ChinaHashDataDTO> hashDataPage(DimQueryParam queryParam, Pageable sort);

	/**
	 * 非病患散列信息实时数据更新
	 * @param dto
	 * @return
	 */
	void updateHashData(ChinaHashDataDTO dto);
}
