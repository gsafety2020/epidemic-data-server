package com.gsafety.gemp.wuhanncov.contract.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author zhoushuyan
 * @date 2020-02-06 19:11
 * @Description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AreaWeekValue implements Serializable {
    private static final long serialVersionUID = 8408623638289374062L;

    @JsonProperty("areaId")
    @ApiModelProperty(value = "地区编号")
    private String areaCode;

    @JsonProperty("x")
    @ApiModelProperty(value = "省份名称")
    private String areaName;

    @JsonProperty("y")
    @ApiModelProperty(value = "确诊人数")
    private Integer value;

    @JsonProperty("s")
    @ApiModelProperty(value = "标记")
    private String s;

}
