package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaCountDailyDTO;

import java.util.List;
import java.util.Map;

/**
 * @author dusiwei
 */
public interface ReportCommunityStatDailyService {

    /**
     * 获取社区发热人员每日汇总
     * @param areaCode
     * @return
     */
    Map<String,Object> getCommunityStatDaily(String areaCode);

    /**
     * 社区发热人员发展趋势
     * @param areaCode 行政编码
     * @return List<PatientAreaCountDailyDTO>
     */
    List<PatientAreaCountDailyDTO> communityHotPeopleStat(String areaCode);
}
