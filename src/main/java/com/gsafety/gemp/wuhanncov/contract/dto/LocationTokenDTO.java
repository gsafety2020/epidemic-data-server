/**
 * 
 */
package com.gsafety.gemp.wuhanncov.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 系统登录口令模型
 *
 * @author wangwenhai
 *
 * @since 2020年2月22日 下午5:40:51
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LocationTokenDTO {

	@ApiModelProperty(value = "区域编码")
	private String areaNo;

	@ApiModelProperty(value = "区域名称")
	private String areaName;

	@ApiModelProperty(value = "父区域编码")
	private String parentNo;

	@ApiModelProperty(value = "排序")
	private Integer seqNum;

	@ApiModelProperty(value = "token")
	private String token;

	@ApiModelProperty(value = "父区域名称")
	private String parentAreaName;

}
