package com.gsafety.gemp.wuhanncov.contract.params;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author liyanhong
 * @since 2020/2/6  17:02
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description="素材文件参数对象")
public class FileParam implements Serializable {

    @ApiModelProperty(value = "文件ID, 上传文件时不用输入；查询和更新时可输入")
    private String id;

    @ApiModelProperty(value = "区域编码", example = "420000")
    private String areaCode;

    @ApiModelProperty(value = "业务类型", example = "area_daily_csv")
    private String bnType;

    @ApiModelProperty(value = "排序号", example = "1")
    private Integer seqNum;

    @ApiModelProperty(value = "文件状态", example = "1")
    private Integer status;

    @ApiModelProperty(value = "文件标签，用半角逗号分隔", example = "区域疫情日报")
    private String tags;

}
