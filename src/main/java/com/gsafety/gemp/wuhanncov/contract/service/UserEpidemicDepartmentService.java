package com.gsafety.gemp.wuhanncov.contract.service;

import java.util.List;

/**
 * 
* @ClassName: UserEpidemicDepartmentService 
* @Description: 机构服务类
* @author luoxiao
* @date 2020年2月6日 下午11:03:33 
*
 */
public interface UserEpidemicDepartmentService {

	public List<String> getLowerAllDepartmentCode(String departmentCode);
}
