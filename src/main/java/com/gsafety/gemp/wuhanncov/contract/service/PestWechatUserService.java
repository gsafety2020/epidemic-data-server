package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wechatmp.client.controller.WechatAuthController.WechatUserinfo;
import com.gsafety.gemp.wuhanncov.dao.po.PestWechatUserPO;

/**
 * 
* @ClassName: PestWechatUserService 
* @Description: 微信用户服务层
* @author luoxiao
* @date 2020年2月26日 上午12:04:22 
*
 */
public interface PestWechatUserService {

	/**
	 * 
	* @Title: saveUpdate 
	* @Description: 保存用户
	* @param @param po    设定文件 
	* @return void    返回类型 
	* @throws
	 */
	void saveUpdate(PestWechatUserPO po);
	
	/**
	 * 
	* @Title: authorizeUser 
	* @Description: 用户授权
	* @param @param po    设定文件 
	* @return void    返回类型 
	* @throws
	 */
	void authorizeUser(PestWechatUserPO po,WechatUserinfo userInfo);
	
	/**
	 * 
	* @Title: getPestWechatUserById 
	* @Description: 通过主键获取详细信息
	* @param @param openId
	* @param @return    设定文件 
	* @return PestWechatUserPO    返回类型 
	* @throws
	 */
	PestWechatUserPO getPestWechatUserById(String openId);
}
