package com.gsafety.gemp.wuhanncov.contract.dto.in;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("是否已上报查询参数")
public class UserEpidemicInfoReportedDTO implements Serializable{
	
	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value="用户名")
	private String userName;
	
	@ApiModelProperty(value="科室")
	private String departmentName;
	
	@ApiModelProperty(value="手机号")
	private String mobile;

}
