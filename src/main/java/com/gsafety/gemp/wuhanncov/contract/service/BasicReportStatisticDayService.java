/**
 * 
 */
package com.gsafety.gemp.wuhanncov.contract.service;

import java.util.List;

import com.gsafety.gemp.wuhanncov.contract.dto.BasicReportDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.BasicReportStaticsDTO;

/**
 * 该类用于...，用于实现...等能力。
 *
 * @author yangbo
 *
 * @since 2020年2月7日 下午11:24:24
 *
 */
public interface BasicReportStatisticDayService {

	/**
	 * 
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月7日 下午11:32:36
	 *
	 * @param topSize
	 * @return
	 */
	List<BasicReportStaticsDTO> findReportHistroySum(int topSize);
}
