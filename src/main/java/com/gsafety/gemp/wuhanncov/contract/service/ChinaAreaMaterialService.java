package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.contract.dto.ChinaAreaMaterialDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.MaterialRequirementDTO;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ChinaAreaMaterialService {
    /**
     * 查询 指定区域及其下级区域的物资情况列表
     * @param areaNos  区域代码
     * @return
     */
    Page<ChinaAreaMaterialDTO> findAreaMaterialsPage(int pageNum, int pageSize, List<String> areaNos);

    List<ChinaAreaMaterialDTO> findAreaMaterials(List<String> areaNos,String statusCd);
}
