package com.gsafety.gemp.wuhanncov.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author zhoushuyan
 * @date 2020-01-30 11:37
 * @Description
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChinaCountStatisticsBubbling {
    private static final long serialVersionUID = -4199246833295998820L;

    @ApiModelProperty(value = "lng")
    BigDecimal lng;

    @ApiModelProperty(value = "lat")
    BigDecimal lat;

    @ApiModelProperty(value = "info")
    String info;

    @ApiModelProperty(value = "name")
    String name;

    @ApiModelProperty(value = "确诊")
    int confirmCase;

    @ApiModelProperty(value = "治愈")
    int cureCase;

    @ApiModelProperty(value = "死亡")
    int deadCase;


}
