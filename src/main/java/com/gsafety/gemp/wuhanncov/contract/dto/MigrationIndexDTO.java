package com.gsafety.gemp.wuhanncov.contract.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * 全国地区迁移指数请求实体
 *
 * @author: WangGang
 * @since 2020-02-06 21:15
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MigrationIndexDTO {

    /**
     * 迁移类型move_in迁入，move_out迁出
     */
    @ApiModelProperty("迁移类型move_in迁入，move_out迁出")
    private String migrateType;

    /**
     * 行政区划/地区Code值
     */
    @ApiModelProperty("行政区划/地区Code值")
    private String areaCode;

    @ApiModelProperty("查询开始时间/yyyy-MM-dd格式")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @ApiModelProperty("查询结束时间/yyyy-MM-dd格式")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

}
