package com.gsafety.gemp.wuhanncov.contract.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

/**
 * 迁徙地图数据
 * @author zhoushuyan
 * @date 2020-01-26 16:55
 * @Description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MigrateMapContent {
    Collection<MigrateMap> migrateMaps;
    public void add(String from, String to,BigDecimal value,String fromName,String toName) {
        if (CollectionUtils.isEmpty(migrateMaps)) {
            this.migrateMaps = new ArrayList<>();
        }
        this.migrateMaps.add(new MigrateMap(from, to,value,fromName,toName));
    }
    public void add(String from, String to) {
        if (CollectionUtils.isEmpty(migrateMaps)) {
            this.migrateMaps = new ArrayList<>();
        }
        this.migrateMaps.add(new MigrateMap(from, to,new BigDecimal(0),"",""));
    }
    public void init() {
        this.migrateMaps = new ArrayList<>();
    }
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public class MigrateMap {

        private String from;

        private String to;

        private BigDecimal value;

        private String fromName;

        private String toName;
    }
}
