/**
 * 
 */
package com.gsafety.gemp.wuhanncov.contract.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 各地区医院情况日报DTO
 *
 * @author yangbo
 *
 * @since 2020年2月9日 上午2:38:05
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChinaAreaMedicalDailyDTO {

	/**
	 * ID
	 */
	@ApiModelProperty(value = "ID")
	private String id;

	/**
	 * 地区编码
	 */
	@ApiModelProperty(value = "地区编码")
	private String areaCode;

	/**
	 * 地区名称
	 */
	@ApiModelProperty(value = "地区名称")
	private String areaName;

	/**
	 * 市编码
	 */
	@ApiModelProperty(value = "市编码")
	private String cityNo;

	/**
	 * 城市名称
	 */
	@ApiModelProperty(value = "城市")
	private String cityName;

	/**
	 * 发热门诊数fever clinic
	 */
	@JsonProperty("number_of_fc")
	@ApiModelProperty(value = "发热门诊数fever clinic")
	private int numberOfFc;

	/**
	 * 发热门诊接诊数fever clinic reception
	 */
	@JsonProperty("number_of_fcr")
	@ApiModelProperty(value = "发热门诊接诊数fever clinic reception")
	private int numberOfFcr;

	/**
	 * 收治病人数Number of patients admitted
	 */
	@JsonProperty("number_of_pa")
	@ApiModelProperty(value = "收治病人数Number of patients admitted")
	private int numberOfPa;

	/**
	 * 上级编码
	 */
	@ApiModelProperty(value = "上级编码")
	private String parentNo;

	/**
	 * 上级地区
	 */
	@ApiModelProperty(value = "上级地区")
	private String parentName;

	/**
	 * 省编码
	 */
	@ApiModelProperty(value = "省编码")
	private String provinceNo;

	/**
	 * 省份
	 */
	@ApiModelProperty(value = "省份")
	private String provinceName;

	/**
	 * 报表日
	 */
	@ApiModelProperty(value = "报表日")
	private String stasDate;

	/**
	 * 最后更新时间
	 */
	@ApiModelProperty(value = "最后更新时间")
	private Date updateTime;
}
