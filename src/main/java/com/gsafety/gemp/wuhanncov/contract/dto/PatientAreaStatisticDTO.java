package com.gsafety.gemp.wuhanncov.contract.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 该类用于疫情大屏,用于实现封装区域按天统计患者人数数据等能力
 *
 * @author lyh
 * @since 2020/1/26  19:29
 * <p>
 * 1.0.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description="日报数据对象")
public class PatientAreaStatisticDTO implements Serializable {

    @ApiModelProperty(value = "id", example = "\"20200131429004\"")
//    @JsonIgnore  无效
    private String id;

    @ApiModelProperty(hidden = true)
    private String area;

    @NotNull(message = "地区编码不能为空")
    @ApiModelProperty(value = "地区编码", example = "\"429004\"", required = true)
    private String areaCode;

    @ApiModelProperty(value = "患者人数", example = "0")
    private Integer patientNumber;

    @ApiModelProperty(value = "疑似人数", example = "0")
    private Integer debtNumber;

    @ApiModelProperty(value = "死亡人数", example = "0")
    private Integer deathNumber;


    @ApiModelProperty(value = "重症人数", example = "0")
    private Integer severeNumber;

    @ApiModelProperty(value = "治愈人数", example = "0")
    private Integer curedNumber;

    @NotNull(message = "统计时间不能为空")
    @ApiModelProperty(value = "统计时间", name="stasTime", example="2020-01-31", required = true)
    private String stasTime;

//    @ApiModelProperty(value = "报表日")
    @ApiModelProperty(hidden = true)
    private String stasDate;

//    @ApiModelProperty(value = "更新时间")
    @ApiModelProperty(hidden = true)
    private String updateTime;

//    @ApiModelProperty(value = "披露日期")
    @ApiModelProperty(hidden = true)
    private String postDate;

    @ApiModelProperty(value = "病危数量", example = "0")
    private Integer dyingNumber;

    @ApiModelProperty(value = "接受医学观察者数量", example = "0")
    private Integer observedNumber;

    @ApiModelProperty(value = "密切接触者数量", example = "0")
    private Integer closeContactsNumber;

    @ApiModelProperty(value = "解除被医学观察者数量", example = "0")
    private Integer releaseObservedNumber;

    @ApiModelProperty(value = "发热数量", example = "0")
    private Integer hotNumber;

    @ApiModelProperty(value = "当日新增疑似数量", example = "0")
    private Integer newDebt;

    @ApiModelProperty(value = "当前为确诊状态的总数量，非累计", example = "0")
    private Integer curConfirm;

}
