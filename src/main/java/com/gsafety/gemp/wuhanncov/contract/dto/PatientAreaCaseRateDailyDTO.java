package com.gsafety.gemp.wuhanncov.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 该类用于疫情大屏,用于实现封装区域按天确诊病例比例数据等能力
 *
 * @author dusiwei
 * @since 2020/2/7  19:29
 * <p>
 * 1.0.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PatientAreaCaseRateDailyDTO implements Serializable {
    @ApiModelProperty(value = "日期。日期格式：yyyy/MM/dd")
    private String x;

    @ApiModelProperty(value = "比例")
    private Double y;

    @ApiModelProperty(value = "类型（1：新增，2：累计）")
    private String t;

    @ApiModelProperty(value = "占比类型（1：市对省，2：市对国，3：省对国）")
    private String s;
}
