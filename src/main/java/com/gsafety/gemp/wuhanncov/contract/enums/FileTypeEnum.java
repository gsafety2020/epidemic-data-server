package com.gsafety.gemp.wuhanncov.contract.enums;

/**
 * 疫情文件类型枚举类
 *
 * @author xiongjinhui
 * @date 2020年2月18日 上午10:56:35
 *
 */
public enum FileTypeEnum {

	/**
	 * 0：分析文件
	 */
	FX("0","分析文件"),

	/**
	 * 1：趋势文件
	 */
	QS("1","趋势文件"),

	/**
	 * 2：趋势分析建议图
	 */
	JY("2","趋势分析建议图"),

	/**
	 * 3：趋势分析折线图
	 */
	ZX("3","趋势分析折线图");

	private String code;

	private String value;

	private FileTypeEnum(String code, String value) {
		this.code = code;
		this.value = value;
	}

	public static String getNameByCode(String code) {
		for (FileTypeEnum fileTypeEnum : FileTypeEnum.values()) {
			if (code.equals(fileTypeEnum.code) ) {
				return fileTypeEnum.value;
			}
		}
		//如果入参code与枚举类不匹配，则返回默认值"0：分析文件"。
		return FX.value;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
}
