package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.contract.dto.BasicFileInfoParam;
import com.gsafety.gemp.wuhanncov.contract.dto.OSSAllFileDto;
import com.gsafety.gemp.wuhanncov.contract.dto.OSSFileDto;
import com.gsafety.gemp.wuhanncov.contract.dto.Result;
import com.gsafety.gemp.wuhanncov.contract.params.BasicFileQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author zhoushuyan
 * @date 2020-02-11 10:51
 * @Description
 */
public interface BasicFileInfoService {
    List<OSSFileDto> getNcovOSSFile(String fileType);

    Result updateNcovOSSFile(BasicFileInfoParam fileInfo);

    String ossFileUpdate(MultipartFile upFile, String objName, String fileName);

    List<OSSFileDto> findAreaFile(List<String> areaIdList, String fileType);

    Page<OSSAllFileDto> findAllFileInfo(BasicFileQueryParam basicFileQueryParam);
}
