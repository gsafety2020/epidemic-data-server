/**
 * 
 */
package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.contract.dto.*;
import com.gsafety.gemp.wuhanncov.contract.params.DimQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 地区病患实时统计报表服务
 *
 * @author yangbo
 *
 * @since 2020年1月30日 下午8:58:48
 *
 */
public interface PatientAreaRuntimeService {

	/**
	 * 查询单个区域的实时数据
	 *
	 * @param areaCode
	 * @return
	 * @author yangbo
	 * @since 2020年1月30日 下午9:11:58
	 */
	public ChinaCountStatisticsJSON findByAreaCode(String areaCode);

	/**
	 * 查询单个区域的实时数据
	 *
	 * @param areaCode
	 * @return
	 * @author yangbo
	 * @since 2020年1月30日 下午9:11:58
	 */
	public List<ChinaCountStatisticsJSON> findByParentNo(String areaCode);

	/**
	 * 区域名称模糊查询实时数据，以分页形式返回
	 * @param dimQuery
	 * @param pageable
	 * @return
	 */
	Page<PatientAreaRuntimeDTO> findPatientRuntimePage(DimQueryParam dimQuery, Pageable pageable);

	/**
	 * 保存病患实时数据
	 * @param dto
	 * @return
	 */
	Result patientRuntimeSave(PatientAreaRuntimeDTO dto);

	/**
	 * 实时数据更新
	 * @param dto
	 * @return
	 */
	Result patientRuntimeUpdate(PatientAreaRuntimeDTO dto);

	/**
	 * 实时日报校验
	 * @param areaCode
	 * @return
	 */
	Result patientRuntimeValid(@NotNull String areaCode);

	/**
	 * 实时数据锁定
	 * @param dto
	 * @return
	 */
	Result patientRuntimeLock(PatientAreaRuntimeDTO dto);

	/**
	 * 实时数据解锁
	 * @param dto
	 * @return
	 */
	Result patientRuntimeUnlock(PatientAreaRuntimeDTO dto);

	List<AreaHotDTO> findMapByParentAreaCode(List<String> areaIdList);
}
