package com.gsafety.gemp.wuhanncov.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 物资需求
 * @author zhangay
 * @date 2020-01-31 20:00:33
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MaterialRequirementDTO {
    @ApiModelProperty(value = "主键id")
    private String id;

    @ApiModelProperty(value = "所属区域编码")
    private String areaNo;

    @ApiModelProperty(value = "所属区域名称")
    private String areaName;

    @ApiModelProperty(value = "单位名称")
    private String departmentName;

    @ApiModelProperty(value = "单位地址")
    private String address;

    @ApiModelProperty(value = "发布日期")
    private LocalDate publishDate;

    @ApiModelProperty(value = "联系人姓名")
    private String personName;

    @ApiModelProperty(value = "联系电话")
    private String personTel;

    @ApiModelProperty(value = "需求内容")
    private String requirement;

    @ApiModelProperty(value = "需求状态(1新建;2调配中;9已完成)")
    private String status;

    @ApiModelProperty(value = "经度")
    private BigDecimal longitude;

    @ApiModelProperty(value = "纬度")
    private BigDecimal latitude;

    @ApiModelProperty(value = "所属市级区域编码")
    private String cityNo;

    @ApiModelProperty(value = "所属市级区域名称")
    private String cityName;

    @ApiModelProperty(value = "所属省级区域编码")
    private String provinceNo;

    @ApiModelProperty(value = "所属省级区域名称")
    private String provinceName;

    @ApiModelProperty(value = "上级区域编码")
    private String parentNo;

    @ApiModelProperty(value = "上级区域名称")
    private String parentName;

    private String createBy;

    private String updateBy;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;
}
