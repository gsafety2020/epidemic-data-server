package com.gsafety.gemp.wuhanncov.contract.jobdto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author liyanhong
 * @ClassName: com.gsafety.gemp.scheduler.dto.JobRunResultDTO
 * @Description:
 * @date: 2019/10/17
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class JobRunResultDTO implements Serializable {
    private static final long serialVersionUID = 3086248193054476377L;

    @ApiModelProperty("任务Id")
    private Integer jobId;

    @ApiModelProperty("任务执行Id")
    private Long jobRunId;

    @ApiModelProperty("任务执行结果:1.执行成功；2.执行失败")
    private Integer runResult;

    @ApiModelProperty("任务执行结果详情")
    private String resultDetail;
}
