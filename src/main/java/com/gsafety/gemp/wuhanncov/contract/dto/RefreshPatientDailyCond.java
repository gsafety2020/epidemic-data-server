package com.gsafety.gemp.wuhanncov.contract.dto;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * 生成指定日期的病患报表。
 *
 * @author yangbo
 *
 * @since 2020年1月26日 下午7:33:41
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RefreshPatientDailyCond {

	/**
	 * 样本日期，格式YYYYMMDD
	 */
	@ApiModelProperty(value = "样本日期，格式YYYYMMDD", required = true, example = "20200126", dataType = "String")
	private String sampleDate;

	/**
	 * 样本时点，格式HH
	 */
	@ApiModelProperty(value = "样本时点，格式HH", required = true, example = "10", dataType = "String")
	private String sampleHour;

	/**
	 * 目标报表日期，格式YYYYMMDD
	 */
	@ApiModelProperty(value = "目标报表日期，格式YYYYMMDD", required = true, example = "20200125", dataType = "String")
	private String targetDate;

	@Override
	public String toString() {
		return "RefreshPatientDailyCond [sampleDate=" + sampleDate + ", sampleHour=" + sampleHour + ", targetDate="
				+ targetDate + "]";
	}

	/**
	 * 
	 * 披露日期(实际发生日期)，默认为报表日-1
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月28日 下午12:13:24
	 *
	 * @return
	 */
	public String getPostDate() {
		DateTimeFormatter format = DateTimeFormat.forPattern("yyyyMMdd");
		return DateTime.parse(this.targetDate, format).minusDays(1).toString(format);
	}
	
}