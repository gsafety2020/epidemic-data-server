package com.gsafety.gemp.wuhanncov.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 服务器文件信息对象
 *
 * @author liyanhong
 * @since 2020/2/6  21:12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FileInfoDTO implements Serializable {

    private static final long serialVersionUID = -6404671878335926254L;
    @ApiModelProperty("文件标识")
    private String fileId;

    @ApiModelProperty("文件URL")
    private String fileUrl;

    @ApiModelProperty("文件json格式化内容")
    private Object data;
}
