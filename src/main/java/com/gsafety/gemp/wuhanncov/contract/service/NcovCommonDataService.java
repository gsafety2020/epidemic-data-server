package com.gsafety.gemp.wuhanncov.contract.service;

import java.util.Collection;
import java.util.List;

import com.gsafety.gemp.wuhanncov.contract.dto.*;
import com.gsafety.gemp.wuhanncov.contract.dto2.PatientAreaInfoDailyDTO2;

/**
 * 该类用于疫情大屏数据服务,用于实现疫情通用数据查询等能力
 *
 * @author lyh
 * @since 2020/1/30  13:08
 * <p>
 * 1.0.0
 */
public interface NcovCommonDataService {

    /***
     * 行政区病患状态拟合接口，查询指定行政区划内时间窗口的累计确诊、新增确诊、重症、死亡、治愈数据。参数：时间窗口、行政区划编码
     * @author  lyh
     * @since   2020/1/30 13:15
     * @param   areaId 区域行政编码
     * @param   days 要返回的数据天数
     * @param   types 指定数据类型列表，见枚举类NumberTypeEnum
     * @return  java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaCountDailyDTO>
     */
    List<PatientAreaInfoDailyDTO> getPatientAreaInfoHistory(String areaId, Integer days, List<String> types);
    
    /**
     * 
     * 该方法用于实现...的功能
     *
     * @author yangbo
     * 
     * @since 2020年2月1日 下午11:57:27
     *
     * @param areaId
     * @param days
     * @return
     */
    List<PatientAreaInfoDailyDTO2> getPatientAreaInfoHistoryV2(String areaId, Integer days);

    /***
     * 多个行政区病患状态对比接口，查询多个区域患者确诊数量历史数据
     * 可以用于查询指定多个区域的患者确诊数量历史情况，折线图展示
     * 数据来源表：patient_area_statistic
     * @author  lyh
     * @since   2020/1/30 14:21
     * @param   areaIds 指定区域行政区划编码列表
     * @return  java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaCountDailyDTO>
     */
    List<PatientAreaCountDailyDTO> getAreaListPatientCountHistory(List<String> areaIds);

    /***
     *  用于地图上的患者数量热力图展示，查询父区域下各子区域的患者确诊数量接口
     *  数据来源表：china_count_statistics，location_dim
     * @author  lyh
     * @since   2020/1/30 14:21
     * @param   areaParent 父区域行政区划编码
     * @return  List<AreaHotDTO>
     */
    List<AreaHotDTO> getAreaPatientCountListByParent(String areaParent);

    /**
     *  用于地图上的患者数量热力图展示，查询父区域下各子区域的患者确诊数量接口
     *  数据来源人工维护，较准确，表：patient_area_runtime
     * @author lyh
     * @since 2020/1/30 14:21
     * @param   areaParent 父区域行政区划编码
     * @return List<AreaHotDTO>
     */
    List<AreaHotDTO> getAreaPatientCountFromRuntimeByParent(String areaParent);

    TopArea getPatientAreaTop(List<String> areaIdList,  int pageNum, int pageSize);

    List<AreaWeekValue> getPatientAreaListTop(List<String> excludeAreaIds,int pageNum,int pageSize, int week);

    List<NewsInfo> findNewsInfo(List<String> keyworlds, int size);


    List<ChinaCountStatisticsBubbling> getParentCountStatisticsBubbling(String parentNo);

    List<PatientAreaCountDailyDTO> getAreaListPatientNewCountHistory(List<String> areaIdList);
}
