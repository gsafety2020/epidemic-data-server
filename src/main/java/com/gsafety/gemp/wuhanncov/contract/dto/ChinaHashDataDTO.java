/**
 * 
 */
package com.gsafety.gemp.wuhanncov.contract.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang.math.NumberUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaHashDataPO.EnumChinaHashDataType;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 该类用于...，用于实现...等能力。
 *
 * @author yangbo
 *
 * @since 2020年2月8日 下午10:05:20
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChinaHashDataDTO {

	@JsonProperty("id")
	@ApiModelProperty(value = "业务主键")
	private String id;

	@JsonProperty("area_id")
	@ApiModelProperty(value = "地区编码")
	private String areaCode;

	@JsonProperty("data_code")
	@ApiModelProperty(value = "数据编码")
	private String dataCode;

	@JsonProperty("data_label")
	@ApiModelProperty(value = "数据标签")
	private String dataLabel;

	@JsonProperty("caseId")
	@ApiModelProperty(value = "数据类型")
	private EnumChinaHashDataType dataType;

	@JsonProperty("data_value")
	@ApiModelProperty(value = "数据描述")
	private String dataValue;

	@JsonProperty("update_time")
	@ApiModelProperty(value = "更新时间", example = "2020-02-20 10:00:00")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateTime;

	public String key() {
		return this.dataCode;
	}

	/**
	 * 
	 * 该方法用于实现...的功能
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月8日 下午10:55:59
	 *
	 * @return
	 */
	public Object value() {
		switch (this.dataType) {
		case NUMBER:
			return NumberUtils.toDouble(this.dataValue);
		case INT:
			BigDecimal bd = NumberUtils.createBigDecimal(this.dataValue);
			return bd.setScale(0, BigDecimal.ROUND_FLOOR).intValue();
		default:
			return this.dataValue;
		}
	}

	public static void main(String[] args) {
		ChinaHashDataDTO dto = new ChinaHashDataDTO();
		dto.setDataType(EnumChinaHashDataType.NUMBER);
		dto.setDataValue("12.135");
		System.out.println(dto.value());
	}
}
