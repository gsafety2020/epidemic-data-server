package com.gsafety.gemp.wuhanncov.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 封装单个区域患者历史信息数据的对象类
 *
 * @author 86186
 * @since 2020/2/1  13:15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PatientAreaInfoDailyDTO implements Serializable {
    private static final long serialVersionUID = 8408676967289374062L;

    @ApiModelProperty(value = "日期。 格式：yyyy/MM/dd")
    private String x;

    @ApiModelProperty(value = "根据s的类型显示对应数量。s=1 确诊，s=2 治愈, s=3 重症, s=4 死亡, s=5 新增确诊")
    private Integer y;

    @ApiModelProperty(value = "数据类型。 1:确诊;2:治愈;3:重症;4:死亡;5:新增确诊")
    private String s;
}
