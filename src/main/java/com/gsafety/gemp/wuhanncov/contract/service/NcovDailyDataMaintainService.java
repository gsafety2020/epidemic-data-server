package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.contract.dto.ChinaCountStatistics;
import com.gsafety.gemp.wuhanncov.contract.dto.ChinaCountStatisticsDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaStatisticDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.Result;
import com.gsafety.gemp.wuhanncov.contract.params.DimQueryParam;
import com.gsafety.gemp.wuhanncov.dao.po.PatientAreaStatisticPO;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author xiaoz
 * @date 2020-01-29 9:46
 * @Description
 */
public interface NcovDailyDataMaintainService {

    /**
     * 查询地区病患数据日报
     * @param areaCode
     * @return
     */
    List<PatientAreaStatisticDTO> findDailyReportByArea(String areaCode);

    /**
     * 新增、修改病患日报数据
     * @param dto
     */
    Result dailyReportSave(PatientAreaStatisticDTO dto);


    /**
     * 新增区划名称、编码分页查询日报
     * @param queryParam
     * @param pageable
     * @return
     */
    Page<PatientAreaStatisticDTO> findAllByDim(DimQueryParam queryParam, Pageable pageable);
}
