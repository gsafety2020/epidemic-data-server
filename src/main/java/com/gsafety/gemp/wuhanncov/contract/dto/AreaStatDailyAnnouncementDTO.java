package com.gsafety.gemp.wuhanncov.contract.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gsafety.gemp.wuhanncov.dao.po.LocationDimPO;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.io.Serializable;
import java.util.Date;

/**
 * @author GANBO
 * @date 2020-02-09
 * @Description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AreaStatDailyAnnouncementDTO implements Serializable {
    /** 主键ID **/
    @ApiModelProperty(value = "id", example = "\"20200131429004\"")
    private String id;

    /** 统计日期 **/
    @ApiModelProperty(value = "统计日期")
    private String statDate;

    /** 地区编码 **/
    @ApiModelProperty(value = "地区编码")
    private String areaCode;

    /** 地区名称 **/
    @ApiModelProperty(value = "地区名称")
    private String areaName;

    /** 公告标题 **/
    @ApiModelProperty(value = "公告标题")
    private String title;

    /** 公告类型。1：速报；2：问题困难；3：措施 **/
    @ApiModelProperty(value = "公告类型")
    private String announcementType;

    /** 公告类型名称 **/
    @ApiModelProperty(value = "公告类型名称")
    private String announcementTypeName;

    /** 公告内容 **/
    @ApiModelProperty(value = "公告内容")
    private String content;

    /** 公告发布部门 **/
    @ApiModelProperty(value = "公告发布部门")
    private String department;

    /** 公告排序 **/
    @ApiModelProperty(value = "公告排序")
    private Integer orderNum;

    /**更新时间**/
    @ApiModelProperty(name = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    /**更新时间**/
    @ApiModelProperty(name = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

}
