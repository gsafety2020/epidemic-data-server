package com.gsafety.gemp.wuhanncov.contract.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import jdk.nashorn.internal.objects.annotations.Property;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 物资列表
 * @author zhoushuyan
 * @date 2020-01-31 20:00:33
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChinaAreaMaterialDTO {

    @JsonProperty("name")
    @ApiModelProperty(value = "物资名称")
    private String materialName;

    @JsonProperty("storageName")
    @ApiModelProperty(value = "仓库名称")
    private String warehouse;

    @ApiModelProperty(value = "单位")
    private String unitName;

    @JsonProperty("count")
    @ApiModelProperty(value = "存储数量")
    private int storageNum;

    @JsonProperty("total")
    @ApiModelProperty(value = "物资累计数")
    private int total;

    @JsonProperty("department")
    @ApiModelProperty(value = "管理部门")
    private String orgName;

    @JsonProperty("address")
    @ApiModelProperty(value = "地址")
    private String address;

    @JsonProperty("contacts")
    @ApiModelProperty(value = "联系人")
    private String linkName;

    @JsonProperty("tel")
    @ApiModelProperty(value = "联系电话")
    private String mobile;


}
