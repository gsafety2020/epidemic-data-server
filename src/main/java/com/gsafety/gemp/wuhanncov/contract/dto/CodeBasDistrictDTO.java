package com.gsafety.gemp.wuhanncov.contract.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
* @ClassName: CodeBasDistrictDTO 
* @Description: 城市输出参数
* @author luoxiao
* @date 2020年2月21日 下午4:52:05 
*
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("城市编码返回参数")
public class CodeBasDistrictDTO implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	/**行政编码*/
	@ApiModelProperty(value="地区编码")
	private String districtCode;
	
	@ApiModelProperty(value="地区名称")
	private String districtName;
	
	@ApiModelProperty(value="父节点编码")
	private String parentCode;
	
	@ApiModelProperty(value="地区简称")
	private String shortName;

}
