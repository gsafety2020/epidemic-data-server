package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.dao.po.UserEpidemicDeployPO;

/**
 * 
* @ClassName: UserEpidemicDeployService 
* @Description: 机构查询配置服务
* @author luoxiao
* @date 2020年2月8日 下午5:33:50 
*
 */
public interface UserEpidemicDeployService {
	
	UserEpidemicDeployPO getDeployPOByMobile(String mobile);
}
