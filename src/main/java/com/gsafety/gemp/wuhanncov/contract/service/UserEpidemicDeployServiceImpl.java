package com.gsafety.gemp.wuhanncov.contract.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.gsafety.gemp.wuhanncov.dao.UserEpidemicDeployDao;
import com.gsafety.gemp.wuhanncov.dao.po.UserEpidemicDeployPO;

/**
 * 
* @ClassName: UserEpidemicDeployServiceImpl 
* @Description: 机构查询配置服务实现类
* @author luoxiao
* @date 2020年2月8日 下午5:37:03 
*
 */
@Service("userEpidemicDeployService")
public class UserEpidemicDeployServiceImpl implements UserEpidemicDeployService{

	@Autowired
	private  UserEpidemicDeployDao userEpidemicDeployDao;
	
	@Override
	public UserEpidemicDeployPO getDeployPOByMobile(String mobile) {
		if(StringUtils.isEmpty(mobile)){
			throw new RuntimeException("手机号不能为空!");
		}
		return userEpidemicDeployDao.findByMobile(mobile);
	}

}
