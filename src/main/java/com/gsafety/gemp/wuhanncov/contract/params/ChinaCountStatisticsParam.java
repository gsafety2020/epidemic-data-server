package com.gsafety.gemp.wuhanncov.contract.params;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description="爬虫实时数据分页查询参数")
public class ChinaCountStatisticsParam {

    @ApiModelProperty(value = "地区名称")
    private String areaName;

    /**
     * 分页参数-当前页面
     */
    @ApiModelProperty(value = "第几页", example = "1")
    private Integer currentPage;

    /**
     * 分页参数-页面容量
     */
    @ApiModelProperty(value = "一页多少条数据", example = "10")
    private Integer pageSize;
}
