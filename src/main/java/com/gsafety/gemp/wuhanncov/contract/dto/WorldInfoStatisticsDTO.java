package com.gsafety.gemp.wuhanncov.contract.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author zhoushuyan
 * @date 2020-01-27 13:53
 * @Description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WorldInfoStatisticsDTO implements Serializable {
    private static final long serialVersionUID = 3323729276859795004L;
    private String countTime;

    private String countDate;

    private String country;

    private Integer confirmCase;

    private Integer probableCase;

    private Integer cureCase;

    private Integer deadCase;

    private Integer newCase;

    private  String memo;

}
