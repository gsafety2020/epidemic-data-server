package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.contract.dto.LocationDimDTO;
import com.gsafety.gemp.wuhanncov.contract.params.DimQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author xiaoz
 * @date 2020-01-29 9:46
 * @Description
 */
public interface NcovLocationDimService {

    void save(LocationDimDTO dto);

    void delete(String areaNo);

    boolean exist(String areaNo, String areaName);

    /**
     * 根据行政区划编码查询行政区划对象
     * @param areaNo
     */
    LocationDimDTO load(String areaNo);

    /**
     * 查询当前节点和子节点
     * @param areaNo
     * @return
     */
    List<LocationDimDTO> loadSelfAndChild(String areaNo);

    /**
     * 行政区划名称分页查询
     * @param areaName 区域名称
     * @param pageable 分页查询
     * @return
     */
    Page<LocationDimDTO> findByAreaNamePage(String areaName, Pageable pageable);

    /**
     * 查询所有需要自动处理的数据
     * @return
     */
    List<LocationDimDTO> findByAutoJobOn();

    /**
     * 查询所有子节点
     * @param areaNo
     * @return
     */
    List<LocationDimDTO> loadChild(String areaNo);

    /**
     * 查询搜索行政区划父节点名称
     * @param queryParam
     * @return
     */
    List<LocationDimDTO> findLocationDimParentSearch(DimQueryParam queryParam);

    List<LocationDimDTO> findByParentNo(String parentNo);
}
