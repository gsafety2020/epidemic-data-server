package com.gsafety.gemp.wuhanncov.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author zhoushuyan
 * @date 2020-01-25 20:31
 * @Description
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChinaCountStatistics implements Serializable{
    private static final long serialVersionUID = -4199246833295998820L;

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "确诊人数")
    private Integer confirmCase;

    @ApiModelProperty(value = "疑似人数")
    private Integer probableCase;

    @ApiModelProperty(value = "治愈人数")
    private Integer cureCase;

    @ApiModelProperty(value = "死亡人数")
    private Integer deadCase;

    @ApiModelProperty(value = "新增人数")
    private Integer newCase;

}
