package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.contract.dto.MigrationIndex;
import com.gsafety.gemp.wuhanncov.contract.dto.MigrationIndexDTO;

import java.util.List;

/**
 * 全国人口迁移指数接口
 *
 * @author: WangGang
 * @since 2020-02-06 21:37
 */
public interface PeopleMigrationIndexService {

    /**
     * 查询所有需要自动处理的数据
     *
     * @return
     */
    List<MigrationIndex> findMigrationIndexList(MigrationIndexDTO migrationIndexDTO);
}
