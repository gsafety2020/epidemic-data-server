/**
 * 
 */
package com.gsafety.gemp.wuhanncov.contract.dto;

import java.util.Date;

import com.gsafety.gemp.wuhanncov.dao.po.BasicReportPO;
import com.gsafety.gemp.wuhanncov.dao.po.BasicReportPO.BasicReportPOBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 该类用于...，用于实现...等能力。
 *
 * @author yangbo
 *
 * @since 2020年2月7日 下午11:29:41
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BasicReportDTO {

	private Integer totalReport;
	
	private Integer tolalFever;
	
	private Integer totalDistrict;
}
