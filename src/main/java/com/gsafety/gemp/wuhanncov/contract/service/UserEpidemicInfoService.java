package com.gsafety.gemp.wuhanncov.contract.service;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.gsafety.gemp.wuhanncov.contract.dto.UserEpidemicInfoDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.UserEpidemicInfoPartDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.in.UserEpidemicInfoQueryDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.in.UserEpidemicInfoQueryUserDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.in.UserEpidemicInfoReportedDTO;
import com.gsafety.gemp.wuhanncov.dao.po.UserEpidemicInfoPO;


/**
 * 
* @ClassName: UserEpidemicInfoService 
* @Description: 新型冠状病毒-用户上报Service
* @author luoxiao
* @date 2020年2月3日 上午12:29:54 
*
 */
public interface UserEpidemicInfoService {
	
	
	/**
	 * 
	* @Title: findAllByPage 
	* @Description: 分页查询全部 
	* @param userEpidemicInfoPO
	* @param pageSize  每页大小
	* @param pageNumber  当前页
	* @return Page<UserEpidemicInfoDTO>    返回类型 
	* @throws
	 */
	Map<String,Object> findAllByPage(UserEpidemicInfoQueryDTO dto,Integer pageSize,Integer pageNumber);
	
	/**
	 * 
	* @Title: saveUserEpidemicInfo 
	* @Description: 保存上报信息
	* @param userEpidemicInfoPO
	* @throws
	 */
	void saveUserEpidemicInfo(UserEpidemicInfoPO userEpidemicInfoPO);
	
	/**
	 * 
	* @Title: findByEpidemicInfoId 
	* @Description: 通过主键查询详细信息
	* @param @param epidemicInfoId
	* @param @return    设定文件 
	* @return UserEpidemicInfoDTO    返回类型 
	* @throws
	 */
	UserEpidemicInfoDTO findByEpidemicInfoId(String epidemicInfoId);
	
	/**
	 * 
	* @Title: getReportedMessage 
	* @Description:获取已经填写过的数据
	* @param @param userId
	* @param @return    设定文件 
	* @return UserEpidemicInfoPartDTO    返回类型 
	* @throws
	 */
	UserEpidemicInfoPartDTO getReportedMessage(String userId);
	
	/**
	 * 
	* @Title: getAllDepartmentName 
	* @Description: 获取所有的部门 
	* @param @return    设定文件 
	* @return List<String>    返回类型 
	* @throws
	 */
	List<String> getAllDepartmentName(String mobile);
	
	/**
	 * 
	* @Title: isReportToday 
	* @Description: 今天是否已经报送
	* @param @param userId
	* @param @return    设定文件 
	* @return Boolean    返回类型 
	* @throws
	 */
	Boolean isReportToday(String userId);
	
	Map<String,Object> getAllNotNormalUser(UserEpidemicInfoQueryUserDTO dto);
	
	/**
	 * 
	* @Title: isAlreadyReported 
	* @Description: 是否已经上报
	* @param @param dto
	* @param @return    设定文件 
	* @return Boolean    返回类型 
	* @throws
	 */
	Boolean isAlreadyReported(UserEpidemicInfoReportedDTO dto);
	
	/**
	 * 
	* @Title: statisticHistoryBrokenLine 
	* @Description: 折线图统计
	* @param @param mobile
	* @param @return    设定文件 
	* @return JSONObject    返回类型 
	* @throws
	 */
	JSONObject statisticHistoryBrokenLine(String mobile,int pastDays);
}
