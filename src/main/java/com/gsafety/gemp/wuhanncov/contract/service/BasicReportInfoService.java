package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.contract.dto.BasicReportInfoDTO;

import java.util.List;

/**
 * @author zhoushuyan
 * @date 2020-02-09 11:12
 * @Description
 */
public interface BasicReportInfoService {
    List<BasicReportInfoDTO> getReportInfos(String channel, int topSize);
}
