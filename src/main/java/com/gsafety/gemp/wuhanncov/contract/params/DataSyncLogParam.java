package com.gsafety.gemp.wuhanncov.contract.params;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description="数据同步任务日志分页查询参数")
public class DataSyncLogParam {

    /**
     * 分页参数-当前页面
     */
    @ApiModelProperty(value = "第几页", example = "1")
    private Integer currentPage;

    /**
     * 分页参数-页面容量
     */
    @ApiModelProperty(value = "一页多少条数据", example = "10")
    private Integer pageSize;

    /**
     * 查询数据同步日志同步时间-开始时间
     */
    @ApiModelProperty(value = "查询数据同步时间开始", example = "2020-02-12 16:05:13")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startTime;

    /**
     * 查询数据同步日志同步时间-结束时间
     */
    @ApiModelProperty(value = "查询数据同步时间结束", example = "2020-02-13 16:05:13")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date endTime;
}
