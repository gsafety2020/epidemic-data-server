package com.gsafety.gemp.wuhanncov.contract.params;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 疫情文件维护查询参数
 *
 * @author xiongjinhui
 * @date 2020年2月18日 上午10:56:35
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description="疫情文件维护查询参数")
public class BasicFileQueryParam {
    /**
     * 文件名称或者区域名称
     */
    @ApiModelProperty(value = "文件名称或者区域名称", example = "趋势")
    private String areaName;

    /**
     * 分页参数-当前页面
     */
    @ApiModelProperty(value = "第几页", example = "0")
    private Integer currentPage;

    /**
     * 分页参数-页面容量
     */
    @ApiModelProperty(value = "一页多少条数据", example = "10")
    private Integer pageSize;


}
