package com.gsafety.gemp.wuhanncov.contract.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author zhoushuyan
 * @date 2020-01-28 12:37
 * @Description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WorldHotMap implements Serializable {
    private static final long serialVersionUID = 3323729997859795004L;

    private String id;

    private String name;

    private Integer value;
}
