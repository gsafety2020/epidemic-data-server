package com.gsafety.gemp.wuhanncov.contract.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description="死亡比例数据图")
public class PatientAreaStatisticsExtDeathDTO  implements Serializable {

    private static final long serialVersionUID = 7135446312448789601L;

    @ApiModelProperty(value = "日期。 格式：yyyy/MM/dd")
    private String x;

    @ApiModelProperty(value = "根据s的类型显示的死亡比例")
    private String y;

    @ApiModelProperty(value = "数据类型。 1:某市死亡率;2:某省死亡率;3:全国死亡率;4:某省对应某市死亡率;5全国对应某省死亡率")
    private String s;
}
