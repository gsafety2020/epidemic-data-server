package com.gsafety.gemp.wuhanncov.contract.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * @author zhoushuyan
 * @date 2020-02-07 0:22
 * @Description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChinaAreaCaseInfoDTO implements Serializable {
    private static final long serialVersionUID = 8208621638289374062L;

    @JsonProperty("caseId")
    @ApiModelProperty(value = "病例编号")
    private String caseId;

    @JsonProperty("age")
    @ApiModelProperty(value = "年龄")
    private String age;

    @JsonProperty("areaName")
    @ApiModelProperty(value = "地区")
    private String areaName;

    @JsonProperty("areaNo")
    @ApiModelProperty(value = "地区编号")
    private String areaNo;

    @JsonProperty("gender")
    @ApiModelProperty(value = "性别")
    private String gender;

    @JsonProperty("case")
    @ApiModelProperty(value = "排序")
    private Integer seqNum;

    @JsonProperty("attribute")
    @ApiModelProperty(value = "病例描述")
    private String caseDetail;

    @ApiModelProperty(value = "生病时间", hidden = true)
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date illnessDate;

}
