package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.contract.dto.AreaHotDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.ChinaCountStatisticsBubbling;
import com.gsafety.gemp.wuhanncov.contract.dto.ChinaCountStatisticsDTO;

import java.util.List;

/**
 * 用于丁香园的数据实现
 * @author zhoushuyan
 * @date 2020-02-07 13:27
 * @Description
 */
public interface NcovPublicSentimentDxyService {
    public List<ChinaCountStatisticsDTO> findAreaByAreaIdsCount(List<String> areaIdList);
    /**
     *
     * 湖北病例城市统计
     *
     * @author zhoushuyan
     *
     * @since 2020年1月26日 上午9:23:08
     *
     * @return
     */
    List<ChinaCountStatisticsDTO> findHBCityCount();

    /***
     *  用于地图上的患者数量热力图展示，查询父区域下各子区域的患者确诊数量接口
     *  数据来源表：china_count_statistics，location_dim
     * @author  lyh
     * @since   2020/1/30 14:21
     * @param   areaParent 父区域行政区划编码
     * @return  List<AreaHotDTO>
     */
    List<AreaHotDTO> getAreaPatientCountListByParent(String areaParent);

    List<ChinaCountStatisticsBubbling> getParentCountStatisticsBubbling(String areaName);

    public List<ChinaCountStatisticsDTO> findProvinceCityCount(String province);
}
