package com.gsafety.gemp.wuhanncov.contract.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import jdk.nashorn.internal.objects.annotations.Property;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author zhoushuyan
 * @date 2020-01-25 20:31
 * @Description
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChinaCountStatisticsDTO implements Serializable{
    private static final long serialVersionUID = -4199246833295998820L;

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "地区")
    private String areaName;

    @ApiModelProperty(value = "确诊人数")
    private Integer confirmCase;

    @ApiModelProperty(value = "疑似人数")
    private Integer probableCase;

    @ApiModelProperty(value = "治愈人数")
    private Integer cureCase;

    @ApiModelProperty(value = "死亡人数")
    private Integer deadCase;

    @ApiModelProperty(value = "新增人数")
    private Integer newCase;

    @ApiModelProperty(value = "统计时间")
    private String countTime;

    @ApiModelProperty(value = "地区类型")
    private String areaType;

    @ApiModelProperty(value = "省份")
    private String province;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private String updateTime;

    @ApiModelProperty(value = "数据渠道")
    private String fromSource;

}
