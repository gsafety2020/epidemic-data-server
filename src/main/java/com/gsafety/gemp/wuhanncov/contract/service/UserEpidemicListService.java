package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.dao.po.UserEpidemicListPO;

/**
 * 
* @ClassName: UserEpidemicListService 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author luoxiao
* @date 2020年2月6日 下午8:06:54 
*
 */
public interface UserEpidemicListService {

	public UserEpidemicListPO getUserByMobile(String mobile);
	
	public void synchroListToInfo();
}
