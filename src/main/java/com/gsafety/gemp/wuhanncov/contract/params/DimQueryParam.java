package com.gsafety.gemp.wuhanncov.contract.params;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author Administrator
 * @Title: DimQueryParam
 * @ProjectName wuhan-ncov
 * @Description: TODO
 * @date 2020/2/215:22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description="日报数据查询对象")
public class DimQueryParam {

    @ApiModelProperty(value = "地区名称")
    private String area;

    @ApiModelProperty(value = "地区编码")
    private String areaNo;

    @ApiModelProperty(value = "验证token")
    private String token;

    @ApiModelProperty(value = "爬虫历史数据搜索日期")
    private String searchDate;


    @ApiModelProperty(value = "关键字，健康码列表使用")
    private String keywords;

    @ApiModelProperty(value = "健康码颜色，健康码列表使用")
    private String healthCodeColor;


    /**
     * 分页参数-当前页面
     */
    @ApiModelProperty(value = "第几页", example = "1")
    private Integer currentPage;

    /**
     * 分页参数-页面容量
     */
    @ApiModelProperty(value = "一页多少条数据", example = "10")
    private Integer pageSize;


    /**
     * 搜索下拉框搜索区域提示
     */
    @ApiModelProperty(value = "搜索下拉框搜索区域提示")
    private String searchAreaName;
}
