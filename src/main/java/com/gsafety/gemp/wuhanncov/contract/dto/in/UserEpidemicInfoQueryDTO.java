package com.gsafety.gemp.wuhanncov.contract.dto.in;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
* @ClassName: UserEpidemicInfoQueryDTO 
* @Description: 用户上报-分页查询入参
* @author luoxiao
* @date 2020年2月3日 下午5:53:36 
*
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("分页查询参数-入参")
public class UserEpidemicInfoQueryDTO implements Serializable{

	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value="填报日期")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")  
	private Date reportDate;
	
	@ApiModelProperty(value="用户名")
	private String userName;
	
	@ApiModelProperty(value="科室")
	private String departmentName;
	
	@ApiModelProperty(value="手机号")
	private String mobile;
	
	@ApiModelProperty(value="0:异常,1:未填报,2:全部")
	private String userCondition;
	

}
