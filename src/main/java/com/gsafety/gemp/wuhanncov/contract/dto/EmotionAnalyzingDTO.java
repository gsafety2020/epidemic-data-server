package com.gsafety.gemp.wuhanncov.contract.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EmotionAnalyzingDTO implements Serializable {
    private static final long serialVersionUID = -4199246833205967320L;
    private String analysisTime;

    private Integer positive;

    private Integer negative;

    private Integer neutral;
}
