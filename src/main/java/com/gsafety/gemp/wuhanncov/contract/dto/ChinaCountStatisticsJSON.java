package com.gsafety.gemp.wuhanncov.contract.dto;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author zhoushuyan
 * @date 2020-01-27 18:03
 * @Description
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChinaCountStatisticsJSON {
    private static final long serialVersionUID = -4199246833295998820L;
    @JsonProperty("name")
    @ApiModelProperty(value = "地区")
    private String areaName;

    @JsonProperty("shortName")
    @ApiModelProperty(value = "地区简称")
    private String areaShortName;

    @JsonProperty("areaCode")
    @ApiModelProperty(value = "地区Code")
    @Column(name = "areaCode")
    private String areaCode;

    @JsonProperty("confirm")
    @ApiModelProperty(value = "确诊人数")
    private Integer confirmCase;

    @JsonProperty("probable")
    @ApiModelProperty(value = "疑似人数")
    private Integer probableCase;

    @JsonProperty("cure")
    @ApiModelProperty(value = "治愈人数")
    private Integer cureCase;

    @JsonProperty("dead")
    @ApiModelProperty(value = "死亡人数")
    private Integer deadCase;

    @JsonProperty("new")
    @ApiModelProperty(value = "新增人数")
    private Integer newCase;

    @JsonProperty("new_cure")
    @ApiModelProperty(value = "新增治愈")
    private Integer new_cure=0;

    @JsonProperty("new_dead")
    @ApiModelProperty(value = "新增死亡")
    private Integer new_dead=0;

    @JsonProperty("new_probable")
    @ApiModelProperty(value = "新增疑似")
    private Integer new_probable=0;
    
    /**
     * 病危数量
     */
    @JsonProperty("dying")
    @ApiModelProperty(value = "病危数量")
	private Integer dyingCase;
	
	/**
	 * 新增病危数量
	 */
    @JsonProperty("new_dying")
    @ApiModelProperty(value = "新增病危数量")
	private Integer newDying;

	/**
	 * 接受医学观察者数量
	 */
    @JsonProperty("observed")
    @ApiModelProperty(value = "接受医学观察者数量")
	private Integer observedCase;

	/**
	 * 新增接受医学观察者数量
	 */
    @JsonProperty("new_observed")
    @ApiModelProperty(value = "新增接受医学观察者数量")
	private Integer newObserved;
	
	/**
	 * 密切接触者数量
	 */
    @JsonProperty("closeContacts")
    @ApiModelProperty(value = "密切接触者数量")
	private Integer closeContactsCase;

	/**
	 * 新增密切接触者数量
	 */
    @JsonProperty("new_closeContacts")
    @ApiModelProperty(value = "新增密切接触者数量")
	private Integer newCloseContacts;
	
	/**
	 * 解除接受医学观察者数量
	 */
    @JsonProperty("release")
    @ApiModelProperty(value = "解除接受医学观察者数量")
	private Integer releaseObservedCase;

	/**
	 * 新增解除接受医学观察者数量
	 */
    @JsonProperty("new_release")
    @ApiModelProperty(value = "新增解除接受医学观察者数量")
	private Integer newReleaseObserved;

    /**
     * 发热数量
     */
    @JsonProperty("hot_case")
    @ApiModelProperty(value = "发热数量")
    private Integer hotCase;

    /**
     * 新增发热数量
     */
    @JsonProperty("new_hot")
    @ApiModelProperty(value = "新增发热数量")
    private Integer newHot;

    /**
     * 重症数量
     */
    @JsonProperty("severe_case")
    @ApiModelProperty(value = "重症数量")
    private Integer severeCase;

    /**
     * 新增重症数量
     */
    @JsonProperty("new_severe")
    @ApiModelProperty(value = "新增重症数量")
    private Integer newSevere;

    /**
     * 当前为确诊状态的总数量，非累计
     */
    @JsonProperty("cur_confirm")
    @ApiModelProperty(value = "当前为确诊状态的总数量，非累计")
    private Integer curConfirm;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
	
}
