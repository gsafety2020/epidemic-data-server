package com.gsafety.gemp.wuhanncov.contract.service;


import com.gsafety.gemp.wuhanncov.contract.dto.AreaStatDailyAnnouncementDTO;
import com.gsafety.gemp.wuhanncov.contract.params.DimQueryParam;
import com.gsafety.gemp.wuhanncov.dao.po.AreaStatDailyAnnouncementPO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @Author ganbo
 * @Since 2020-02-09
 */
public interface AreaStatDailyAnnouncementService {

    Map<String,Object> getLatestAnnouncementData(String areaCode, String type);

    List<Map<String,Object>> showAnnouncementDataList(String areaCode, String type);

    /**
     * 分页查询地区公告信息
     * @param queryParam
     * @param pageable
     * @return
     */
    Page<AreaStatDailyAnnouncementDTO> findAreaAnnouncementPage(DimQueryParam queryParam, Pageable pageable);

    /**
     * 新增地区公告信息
     * @param dto
     */
    AreaStatDailyAnnouncementPO areaAnnouncementSave(AreaStatDailyAnnouncementDTO dto);

    /**
     * 编辑地区公告信息
     * @param dto
     */
    AreaStatDailyAnnouncementPO areaAnnouncementUpdate(AreaStatDailyAnnouncementDTO dto);

    /**
     * 删除地区公告信息
     * @param id
     */
    void areaAnnouncementDelete(String id);

}
