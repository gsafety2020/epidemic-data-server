package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.contract.dto.ChinaCountStatisticsDTO;
import com.gsafety.gemp.wuhanncov.contract.params.ChinaCountStatisticsParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ChinaCountStatisticsService {

    boolean update(ChinaCountStatisticsDTO dto);

    Page<ChinaCountStatisticsDTO> findChinaCountStatPage(ChinaCountStatisticsParam queryParam, Pageable pageable);

}
