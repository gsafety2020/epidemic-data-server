/**
 * 
 */
package com.gsafety.gemp.wuhanncov.contract.dto;

import java.util.Date;

import com.gsafety.gemp.wuhanncov.dao.po.LocationSentimentDailyPO;
import com.gsafety.gemp.wuhanncov.dao.po.LocationSentimentDailyPO.LocationSentimentDailyPOBuilder;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 地区热力模型
 *
 * @author yangbo
 *
 * @since 2020年1月25日 下午7:05:51
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LocationHotDTO {

	/**
	 * 区域编码
	 */
	@ApiModelProperty(value = "区域编码")
	private int areaId;

	/**
	 * 舆情热点值
	 */
	@ApiModelProperty(value = "舆情热点值")
	private int value;
}
