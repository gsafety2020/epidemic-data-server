package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.contract.dto.DataSyncLogDTO;
import com.gsafety.gemp.wuhanncov.contract.params.DataSyncLogParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * 数据同步日志查询service层
 *
 * @author xiongjinhui
 * @since 2020/2/12 16:37
 */
public interface DataSyncLogService {

    /**
     * 数据同步日志分页查询
     * @param  dataSyncLogParam, pageable
     * @return Page<DataSyncLogDTO>
     */
    Page<DataSyncLogDTO> findDataSyncLogPage(DataSyncLogParam dataSyncLogParam, Pageable pageable);
}
