package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.contract.params.FileParam;
import com.gsafety.gemp.wuhanncov.dao.po.FilesManagePO;

import java.util.List;

/**
 * 素材文件管理服务
 *
 * @author liyanhong
 * @since 2020/2/6  16:57
 */
public interface XfileManageService {

    /**
     * 保存图片信息到数据库
     *
     * @param po
     */
    Integer addFileInfo(FilesManagePO po);

    /**
     * 根据指定条件查询静态资源信息
     *
     * @param fileParam
     * @return
     */
    List<FilesManagePO> findFiles(FileParam fileParam);

    /**
     * 更新静态文件信息
     *
     * @param fileParam
     */
    Integer updateFileInfo(FileParam fileParam);
}
