/**
 * 
 */
package com.gsafety.gemp.wuhanncov.contract.service;

import java.util.List;

import com.gsafety.gemp.wuhanncov.contract.dto.ChinaAreaMedicalDailyDTO;
import com.gsafety.gemp.wuhanncov.contract.params.DimQueryParam;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaAreaMedicalDailyPO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * 各地区医院情况日报Service。
 *
 * @author yangbo
 *
 * @since 2020年2月9日 上午2:37:22
 *
 */
public interface ChinaAreaMedicalDailyService {

	/**
	 * 
	 * 按报表日查询各地区医院情况日报
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月9日 上午2:49:57
	 *
	 * @param areaCode
	 * @param from
	 * @param to
	 * @return
	 */
	List<ChinaAreaMedicalDailyDTO> findByStasDateBetween(String areaCode, String from, String to);

	/**
	 * 分页查询地区医院日报
	 * @param queryParam
	 * @param pageable
	 * @return
	 */
	Page<ChinaAreaMedicalDailyDTO> findAreaMedicalDailyPage(DimQueryParam queryParam, Pageable pageable);

	/**
	 * 新增地区医院日报
	 * @param dto
	 */
	ChinaAreaMedicalDailyPO areaMedicalDailySave(ChinaAreaMedicalDailyDTO dto);

	/**
	 * 编辑地区医院日报
	 * @param dto
	 */
	ChinaAreaMedicalDailyPO areaMedicalDailytUpdate(ChinaAreaMedicalDailyDTO dto);
}
