/**
 * 
 */
package com.gsafety.gemp.wuhanncov.contract.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 热词模型。
 *
 * @author yangbo
 *
 * @since 2020年1月25日 下午4:31:00
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TextValue {

	/**
	 * DEFAULT_SEPARATORCHARS
	 */
	public static final String DEFAULT_SEPARATORCHARS = ";";

	List<TextValueText> textValueText;

	/**
	 * 
	 * 增加情感分析
	 *
	 * @author zhoushuyan
	 * 
	 * @since 2020年1月25日 下午4:36:07
	 *
	 * @param value
	 * @param name
	 */
	public void add(int value, String name) {
		if (CollectionUtils.isEmpty(textValueText)) {
			this.textValueText = new ArrayList<>();
		}
		this.textValueText.add(new TextValueText(value, name));
	}

	@Data
	@AllArgsConstructor
	public class TextValueText {

		private int value;

		private String name;
	}
}
