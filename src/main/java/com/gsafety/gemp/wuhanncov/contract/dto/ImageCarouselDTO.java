package com.gsafety.gemp.wuhanncov.contract.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ImageCarouselDTO {

    @JsonProperty("description")
    @ApiModelProperty(value = "描述")
    private String description;

    @JsonProperty("url")
    @ApiModelProperty(value = "链接")
    private String url;
}
