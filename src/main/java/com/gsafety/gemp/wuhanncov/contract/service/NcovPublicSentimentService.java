package com.gsafety.gemp.wuhanncov.contract.service;

import java.util.Collection;
import java.util.List;

import com.gsafety.gemp.wuhanncov.contract.dto.*;

/**
 * 该类用于...,用于实现...等能力
 *
 * @author 86186
 * @since 2020/1/25  15:07
 * <p>
 * 1.0.0
 */
public interface NcovPublicSentimentService {
	
	/**
	 * 
	 * 查询最热的新词
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月25日 下午3:28:02
	 *
	 * @return
	 */
	public HotwordDTO findLatestHotword();
	
	/**
	 * 
	 * 查询最热的新词内容
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月25日 下午3:28:02
	 *
	 * @return
	 */
	public HotwordContent findLatestHotwordContent();


	/***
	 * 疫情讨论量趋势数据
	 * @author  lyh
	 * @since   2020/1/25 17:56
	 * @return  java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.PubSensHeatDTO>
	 * @throws
	 */
	List<PubSensHeatDTO> getListHeatOfPubSens();

	/**
	 *
	 * 查询最热的新词
	 *
	 * @author yangbo
	 *
	 * @since 2020年1月25日 下午3:28:02
	 *
	 * @return
	 */
	String findRollNows();
	
	/**
	 * 
	 * 查询指定日期的热力值
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月25日 下午7:10:11
	 *
	 * @param date
	 * @return
	 */
	List<LocationHotDTO> findLocationHot(String date);

	/**
	 *
	 * 情感分析
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月25日 下午5:28:02
	 *
	 * @return
	 */
	public EmotionAnalyzingContent findEmotionAnalyzing();

	/**
	 * 
	 * 计算指定日期指定区域舆情热力分析
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月25日 下午7:54:57
	 *
	 * @param date
	 * @param areaId
	 */
	void mergeLocationHot(String date, String areaId);
	
	/**
	 * 
	 * 计算指定日期舆情热力分析
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月25日 下午7:54:57
	 *
	 * @param date
	 * @param areaId
	 */
	void mergeLocationHot(String date);

	/**
	 *
	 * 病例统计滚动板
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	List<ChinaCountStatisticsDTO> findAreaCount();

	/**
	 *
	 * 获得时间轴数据
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	TextValue getTimeAxis();

	/**
	 *
	 * 获得时间省级区划数据
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	TextValue getProvinceArea();
	/**
	 *
	 * 获得市级区划数据
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	TextValue getCityArea(String areaId);

	/**
	 *
	 * 获得所有区划数据
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	AreaInfo getAllArea();

	/**
	 * 迁出/迁入的列表接口
	 * @author zhoushuyan
	 * @since 2020年1月26日 上午9:23:08
	 * @param date 时间 格式 yyyy-mm-dd
	 * @param migrate_type:move_out1 市迁出  move_out2省迁出 move_in1市迁入 move_id2省迁入
	 * @param area_id 默认武汉 420100
	 * @return
	 */
	MigratePeopleContent getMigratePeople(String date, String migrate_type,String areaType, String area_id);

	/**
	 * 迁徙图数据
	 * @author zhoushuyan
	 * @since 2020年1月26日 上午9:23:08
	 * @param date 时间 格式 yyyymmdd
	 * @param migrate_type:move_out1 市迁出  move_out2省迁出 move_in1市迁入 move_id2省迁入
	 * @param area_id 默认武汉 420100
	 * @return
	 */
	MigrateMapContent getMigrateMap(String date, String migrate_type, String areaType,String area_id);
	
	/**
	 * 
	 * 生成指定日期的病患报表
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月26日 下午7:48:34
	 *
	 * @param cond
	 */
	void refreshPatientDaily(RefreshPatientDailyCond cond);

	/***
	 * 查询区域患者人数按天统计数据列表
	 * @author  lyh
	 * @since   2020/1/26 19:41
	 * @return  java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaCountDailyDTO>
	 * @throws
	 */
	List<PatientAreaCountDailyDTO> getPatientAreaCountDaily();

	/***
	 * 查询疫情实时视频地址列表
	 * @author  lyh
	 * @since   2020/1/26 21:02
	 * @return  java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.RealVideoPublicDTO>
	 * @throws
	 */
	List<RealVideoPublicDTO> getRealVideoPublicList();

	/**
	 *
	 * 全国病例统计
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	ChinaCountStatisticsDTO findChinaCount();
	/**
	 *
	 * 全国港澳台病例
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	List<ChinaCountStatisticsDTO> findGATCount();

	/**
	 *
	 * 国际病例统计
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	List<WorldInfoStatisticsDTO> findInternationalCount();
	/**
	 *
	 * 湖北病例统计
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	ChinaCountStatisticsDTO findHBCount();

	/**
	 *
	 * 北京病例统计
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	ChinaCountStatisticsDTO findBJCount();

	/**
	 *
	 * 武汉病例统计
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	ChinaCountStatisticsDTO findWHCount();

	/**
	 *
	 * 湖北病例城市统计
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	List<ChinaCountStatisticsDTO> findHBCityCount();
	/**
	 *
	 * 北京病例城市统计
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	List<ChinaCountStatisticsDTO> findBJCityCount();

	/***
	 * 查询指定区域内的舆情热力值
	 * @author  lyh
	 * @since   2020/1/27 17:57
	 * @param   areaParent
	 * @return  java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.AreaHotDTO>
	 * @throws
	 */
	List<AreaHotDTO> getAreaHotList(String areaParent);

	/***
	 * 查询指定区域患者人数按天统计数据列表
	 * @author  lyh
	 * @since   2020/1/26 19:41
	 * @param   areaCode 区域编码
	 * @param   type 类型
	 * @return  java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaCountDailyDTO>
	 * @throws
	 */
	List<PatientAreaCountDailyDTO> getPatientAreaCountDaily(String areaCode, String type);

	/***
	 * 查询子区域内患者热力值
	 * @author  86186
	 * @since   2020/1/27 23:56
	 * @param   areaParent
	 * @return  java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.AreaHotDTO>
	 * @throws
	 */
	List<AreaHotDTO> getAreaPatientCountList(String areaParent);

	/***
	 * 查询区域患者信息按天统计的柱状图数据
	 * @author  lyh
	 * @since   2020/1/28 9:24
	 * @param   areaId
	 * @return  java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaCountDailyDTO>
	 * @throws
	 */
	List<PatientAreaCountDailyDTO> getPatientAreaInfoDaily(String areaId);

	/**
	 * 获得世界热力地图
	 * @return
	 */
	List<WorldHotMap> getWorldHotMap();

	/**
	 * 武汉到全国飞线图
	 * @return
	 */
	MigrateMapContent getFlyLineByWutoProvince(String from);

	/**
	 * 获得省的数据
	 * @param province
	 * @return
	 */
	List<ChinaCountStatisticsDTO> findProvinceCityCount(String province);

	/**
	 * 获得市各区的数据
	 * @param city
	 * @return
	 */
	List<ChinaCountStatisticsDTO> findCityCountyCount(String city);

	/**
	 * 冒泡图-全国省份定位和数据
	 * @return
	 */
	List<ChinaCountStatisticsBubbling> getChinaCountStatisticsBubbling();

	/**
	 * 获得迁徙图最新数据时间 yyyy-mm-dd
	 * @return
	 */
	String getMigrateMapDefaultDate();

	/**
	 * 获得国际国家信息图数据
	 * @return
	 */
	List<LocationValue> getInternationalLocationValue();
	/**
	 * 获得各省信息图数据
	 * @return
	 */
	List<LocationValue> findChinaProvinceLocationValue();

	MigrateMapContent getFlyLineByWutoInternationalLocation(String from);


	List<WorldRealBroadcastDTO> getWorldRealBroadcastTop();

	ChinaCountStatisticsDTO getAreaNameCount(String areaName);
	/**
	 *
	 * 获取指定地区的病例数据
	 *
	 * @author zhoushuyan
	 *
	 * @since 2020年1月26日 上午9:23:08
	 *
	 * @return
	 */
	List<ChinaCountStatisticsDTO> findAreaByAreaIdsCount(List<String> areaIdList);

	List<ChinaCountStatisticsBubbling> getParentCountStatisticsBubbling(String province);

	/**
	 * 更新病患实时数据表
	 */
	void syncPatientRuntime();

    void patientUncertainRuntime();
}
