package com.gsafety.gemp.wuhanncov.contract.dto.in;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel("分页查询参数-查询感染人员下拉框入参")
public class UserEpidemicInfoQueryUserDTO implements Serializable{

	
	/** 
	* @Fields serialVersionUID : TODO(用一句话描述这个变量表示什么) 
	*/ 
	private static final long serialVersionUID = 1L;
	
	
	@ApiModelProperty(value="填报日期")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")  
	private Date reportDate;
}
