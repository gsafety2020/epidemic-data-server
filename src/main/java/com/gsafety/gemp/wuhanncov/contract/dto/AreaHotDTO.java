package com.gsafety.gemp.wuhanncov.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.criteria.CriteriaBuilder;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 封装区域疫情舆情热力数据
 *
 * @author 86186
 * @since 2020/1/27  17:50
 * <p>
 * 1.0.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AreaHotDTO implements Serializable {

    private static final long serialVersionUID = -888245401835554457L;
    @ApiModelProperty("区域编码")
    private String area_id;

    @ApiModelProperty("热力值(默认为 累计确诊人数)")
    private Integer value;

    @ApiModelProperty("区域名称")
    private String areaName;

    @ApiModelProperty("(累计)确诊人数")
    private Integer confirmCase;

    @ApiModelProperty("(累计)治愈人数-出院")
    private Integer cureCase;

    @ApiModelProperty("(累计)死亡人数")
    private Integer deadCase;

    @ApiModelProperty("(累计)疑似人数")
    private Integer probableCase;

    @ApiModelProperty("新增确诊人数")
    private Integer newCase;

    @ApiModelProperty("当前为确诊状态的总数量")
    private Integer curConfirm;

    @ApiModelProperty(value = "精度")
    BigDecimal lng;

    @ApiModelProperty(value = "纬度")
    BigDecimal lat;
}
