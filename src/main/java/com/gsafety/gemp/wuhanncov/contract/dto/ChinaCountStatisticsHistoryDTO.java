package com.gsafety.gemp.wuhanncov.contract.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 该类用于爬虫历史数据分析
 *
 * @author wangwenhai
 * @since 2020/2/12  19:55
 * <p>
 * 1.0.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description = "爬虫历史数据分析")
public class ChinaCountStatisticsHistoryDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键")
	private Integer id;

	@ApiModelProperty(value = "地区名称")
	private String areaName;

	@ApiModelProperty(value = "地区类型")
	private String areaType;

	@ApiModelProperty(value = "累计确诊", example = "0")
	private Integer confirmCase;

	@ApiModelProperty(value = "累计治愈", example = "0")
	private Integer cureCase;

	@ApiModelProperty(value = "累计死亡", example = "0")
	private Integer deadCase;

	@ApiModelProperty(value = "数据来源")
	private String fromSource;

	@ApiModelProperty(value = "注释，说明数据来源")
	private String memo;

	@ApiModelProperty(value = "新增确诊", example = "0")
	private Integer newCase;

	@ApiModelProperty(value = "累计疑似", example = "0")
	private Integer probableCase;

	@ApiModelProperty(value = "省、直辖市名称", hidden = true)
	private String province;

	@ApiModelProperty(value = "更新时间", hidden = true)
	@JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date updateTime;



}