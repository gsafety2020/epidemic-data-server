package com.gsafety.gemp.wuhanncov.contract.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 该类用于疫情大屏,用于实现查询播放的视频链接地址等能力
 *
 * @author lyh
 * @since 2020/1/26  20:59
 * <p>
 * 1.0.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RealVideoPublicDTO implements Serializable {

    private static final long serialVersionUID = 2771638262514351804L;

    @ApiModelProperty(value = "视频地址完整url")
    String url;
}
