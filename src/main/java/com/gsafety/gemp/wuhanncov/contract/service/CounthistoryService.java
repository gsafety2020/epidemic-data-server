package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.contract.dto.ChinaCountStatisticsHistoryDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaRuntimeDTO;
import com.gsafety.gemp.wuhanncov.contract.params.DimQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * 爬虫历史数据查询service层
 *
 * @author wangwenhai
 * @since 2020/2/13 12:02
 */
public interface CounthistoryService {
    /**
     * 爬虫历史数据分页查询
     * @param  queryParam, pageable
     * @return Page<ChinaCountStatisticsHistoryDTO>
     */
    Page<ChinaCountStatisticsHistoryDTO> findRetileInfoPage(DimQueryParam queryParam, Pageable pageable);
}
