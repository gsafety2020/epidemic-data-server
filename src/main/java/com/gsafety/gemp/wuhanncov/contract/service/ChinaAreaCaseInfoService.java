package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.contract.dto.ChinaAreaCaseInfoDTO;
import com.gsafety.gemp.wuhanncov.contract.dto.ChinaAreaCaseInfoValue;

import java.util.List;

/**
 * @author zhoushuyan
 * @date 2020-02-09 13:23
 * @Description
 */
public interface ChinaAreaCaseInfoService {
    /**
     * 查询X地区最新的前N位病例
     * 数据来源表：china_area_case_info
     * @author zhoushuyan
     * @since 2020/2/6 14:21
     * @param   areaIdList 排查区域行政区划编码列表
     * @param   topSize 前N位
     * @return java.util.List<com.gsafety.gemp.wuhanncov.contract.dto.PatientAreaCountDailyDTO>
     * @throws
     */
    List<ChinaAreaCaseInfoDTO> getChinaAreaCaseInfoTopList(List<String> areaIdList, int topSize);

    /**
     * 查询父节点下的所有病例
     * @param areaName
     * @param date
     * @return
     */
    List<ChinaAreaCaseInfoValue> getChinaAreaCaseInfoListByAreaName(String areaName, String date);

    String getCaseInfoDefaultDate(String parentNo);
}
