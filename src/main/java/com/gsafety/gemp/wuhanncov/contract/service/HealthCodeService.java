/**
 * 
 */
package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.contract.dto.PestHealthCodeDTO;
import com.gsafety.gemp.wuhanncov.contract.params.DimQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * 健康码服务
 *
 * @author wangwenhai
 *
 * @since 2020年2月25日 上午 11:58:36
 *
 */
public interface HealthCodeService {



    /**
     * 查询健康码列表
     * @param queryParam
     * @param sort
     * @return
     */
    Page<PestHealthCodeDTO> findHealthCodePage(DimQueryParam queryParam, Pageable sort);
}
