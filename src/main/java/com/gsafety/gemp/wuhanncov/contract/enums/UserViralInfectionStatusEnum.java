package com.gsafety.gemp.wuhanncov.contract.enums;

/**
 * 
* @ClassName: UserViralInfectionStatusEnum 
* @Description: 感染状态
* @author luoxiao
* @date 2020年2月4日 上午10:56:35 
*
 */
public enum UserViralInfectionStatusEnum {

	
	DIAGONSE("0","确诊"),
	SUSPECTED("1","疑似"),
	CONTIGUITY("2","密接"),
	NORMAL("3","正常"),
	NOT_REPORTE("99","未填报"),
	ALL("100","全部");

	private String code;
	
	private String value;

	private UserViralInfectionStatusEnum(String code, String value) {
		this.code = code;
		this.value = value;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
}
