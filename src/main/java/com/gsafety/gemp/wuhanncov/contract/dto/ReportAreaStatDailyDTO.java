package com.gsafety.gemp.wuhanncov.contract.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 每日情况汇总表,汇总江夏区每日上报情况
 * @author Luoganggang
 * @since 2020/2/9 18:14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(description="每日情况汇总表,汇总江夏区每日上报情况")
public class ReportAreaStatDailyDTO implements Serializable{

    /**主键（日期 + 行政编码）**/
    @ApiModelProperty(value = "主键（日期 + 行政编码）")
    private String id;
    /**统计日期**/
    @ApiModelProperty(value = "统计日期")
    private String stasDate;
    /**行政编码**/
    @ApiModelProperty(value = "行政编码")
    private String areaCode;
    /**上级编码*/
    @ApiModelProperty(value = "上级编码")
    private String parentCode;
    /**辖区总人数**/
    @ApiModelProperty(value = "辖区总人数")
    private BigDecimal peopleNumber;
    /**今日登记人数**/
    @ApiModelProperty(value = "今日登记人数")
    private BigDecimal registerNumber;
    /**发热人数**/
    @ApiModelProperty(value = "发热人数")
    private BigDecimal hotNumber;
    /**确诊患者**/
    @ApiModelProperty(value = "确诊患者")
    private BigDecimal patientNumber;
    /**疑似患者**/
    @ApiModelProperty(value = "疑似患者")
    private BigDecimal debtNumber;
    /**CT诊断肺炎患者**/
    @ApiModelProperty(value = "CT诊断肺炎患者")
    private BigDecimal ctPatientNumber;
    /**一般发热患者**/
    @ApiModelProperty(value = "一般发热患者")
    private BigDecimal generalHotNumber;
    /**密切接触者**/
    @ApiModelProperty(value = "密切接触者")
    private BigDecimal closeContactsNumber;
    /**更新时间**/
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

}
