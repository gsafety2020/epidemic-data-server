package com.gsafety.gemp.wuhanncov.contract.service;

import com.gsafety.gemp.wuhanncov.contract.dto.ImageCarouselDTO;

import java.util.List;

public interface CityImageCarouselService {
    List<ImageCarouselDTO> listCarouselByAreaCode(String areaCode);
}
