/**
 * 
 */
package com.gsafety.gemp.wuhanncov.contract.dto;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.util.CollectionUtils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 热词模型。
 *
 * @author yangbo
 *
 * @since 2020年1月25日 下午4:31:00
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HotwordContent {

	/**
	 * DEFAULT_SEPARATORCHARS
	 */
	public static final String DEFAULT_SEPARATORCHARS = ";";

	Collection<HotwordText> wordTexts;

	/**
	 * 
	 * 增加热词
	 *
	 * @author yangbo
	 * 
	 * @since 2020年1月25日 下午4:36:07
	 *
	 * @param name
	 * @param value
	 * @param type
	 */
	public void add(String name, int value, int type) {
		if (CollectionUtils.isEmpty(wordTexts)) {
			this.wordTexts = new ArrayList<>();
		}
		this.wordTexts.add(new HotwordText(name, value, type));
	}

	@Data
	@AllArgsConstructor
	public class HotwordText {

		private String name;

		private int value;

		private int type;
	}
}
