package com.gsafety.gemp.wuhanncov.contract.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;

/**
 * 人口迁徙数据
 * @author zhoushuyan
 * @date 2020-01-26 16:55
 * @Description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MigratePeopleContent {
    Collection<MigratePeople> migratePeoples;
    public void add(String area, String percent,String latitude,String longitude) {
        if (CollectionUtils.isEmpty(migratePeoples)) {
            this.migratePeoples = new ArrayList<>();
        }
        this.migratePeoples.add(new MigratePeople(area, percent,latitude,longitude));
    }
    public void init() {
        this.migratePeoples = new ArrayList<>();
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public class MigratePeople {

        private String area;

        private String percent;

        private String latitude;

        private String longitude;
    }
}
