package com.gsafety.gemp.wuhanncov.contract.dto;

import com.gsafety.gemp.wuhanncov.contract.enums.FileTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Column;
import javax.persistence.Id;
import java.util.Date;

/**
 * @author zhoushuyan
 * @date 2020-02-11 14:24
 * @Description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BasicFileInfoParam {
    @ApiModelProperty(value = "ID")
    private String fileId;

    @ApiModelProperty(value = "文件名称")
    private String fileName;


    @ApiModelProperty(value = "状态值")
    private String status;

    @ApiModelProperty(value = "文件URL")
    private String url;

    @ApiModelProperty(value = "文件类型 趋势分析：0，疫情分析:1")
    private String fileType;

    @ApiModelProperty(value = "地区Code")
    private String areaCode;

}
