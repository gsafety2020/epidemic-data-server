package com.gsafety.gemp.wuhanncov.contract.service;

public interface DimService {

    /**
     * 上级行政区划编码
     * @return
     */
    String getParent();

    /**
     * 省、直辖市、自治区行政编码
     * @return
     */
    String getProvince();

    /**
     * 城市编码
     * @return
     */
    String getCity();

    /**
     * 行政区划类型
     * @return
     */
    String getType();
}
