package com.gsafety.gemp.wuhanncov.utils;

import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import com.gsafety.gemp.wuhanncov.dao.po.LocationTokenPO;
import org.springframework.stereotype.Component;


@Component
public class TokenUtils {

    /**
     * 默认盐
     */
    private static final String DEFALT_SALT = "www.gsafety.com.";

    private static final String DEFALT_LINK_CHAR = "|";

    private static final SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, DEFALT_SALT.getBytes());

    /**
     * 生成token
     * @param ltp
     * @return
     */
    public static String create(LocationTokenPO ltp) {
        return encryptHex(ltp.getAreaCode() + DEFALT_LINK_CHAR + ltp.getToken());
    }

    /**
     * 加密
     * @param content
     * @return
     */
    private static String encryptHex(String content) {
        return aes.encryptHex(content);
    }

    /**
     * 解密
     * @param content
     * @return
     */
    public static String decryptStr(String content) {
        return aes.decryptStr(content);
    }

    public static void main(String[] args) {
        String s1 =  TokenUtils.encryptHex("420000|420000");
        System.out.println(s1);
        String s2 = "20fa30fef712fde624196df4a742f9fc";
        System.out.println(TokenUtils.decryptStr(s2));
    }
}
