package com.gsafety.gemp.wuhanncov.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Administrator
 * @Title: RegExUtil
 * @ProjectName wuhan-ncov
 * @Description: 正则表达式工具
 * @date 2020/2/313:22
 */
public class RegExUtil {

    /**
     * 中国行政区划6、12位数字
     */
    private static final String CHINA_DIM_PATTERN = "^\\d{6}$|^\\d{12}$";

    public static Boolean isChinaDim(String areaCode) {
        Pattern pattern = Pattern.compile(CHINA_DIM_PATTERN);
        Matcher isChinaDim = pattern.matcher(areaCode);

        return isChinaDim.matches();
    }

}
