package com.gsafety.gemp.wuhanncov.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * csv文件操作工具类
 *
 * @author liyanhong
 * @since 2020/2/12  10:58
 */
public class CsvUtils {

    /**
     * 将文件内容读取成Json格式
     * @param in csv文件输入流
     * @return
     */
    public static JSONArray ReadCsv(InputStream in) {

        JSONArray result = new JSONArray();

        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(in));
            String line = "";
            JSONObject jsonObject = null;
            while ((line = br.readLine()) != null) {
                String[] datas = line.split(",");

                jsonObject = new JSONObject();
                for (int i = 0; i < datas.length; i++) {
                    jsonObject.put(String.valueOf(i), datas[i]);
                }
                result.add(jsonObject);
            }
        } catch (Exception e) {
        } finally {
            if (br != null) {
                try {
                    br.close();
                    br = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }
}
