/**
 * 
 */
package com.gsafety.gemp.wuhanncov.specification;

import com.gsafety.gemp.wuhanncov.dao.po.AreaStatDailyAnnouncementPO;
import com.gsafety.gemp.wuhanncov.dao.po.ChinaHashDataPO;
import org.springframework.data.jpa.domain.Specification;

/**
 * 该类用于...，用于实现...等能力。
 *
 * @author wuhongling
 *
 * @since 2020年2月14日 下午18:00:00
 *
 */
public class AreaStatDailyAnnouncementSpecifications {

	/**
	 *
	 */
	public static final String FIELD_AREA_CODE = "areaCode";

	/**
	 *
	 */
	private AreaStatDailyAnnouncementSpecifications() {
		super();
	}

	/**
	 * 
	 * areaCodeIs
	 *
	 * @author wuhongling
	 * 
	 * @since 2020年2月14日 下午18:00:00
	 *
	 * @param areaCode
	 * @return
	 */
	public static Specification<AreaStatDailyAnnouncementPO> areaCodeIs(String areaCode) {
		return (root, query, builder) -> builder.equal(root.get(FIELD_AREA_CODE), areaCode);
	}
}
