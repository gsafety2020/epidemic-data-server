/**
 * 
 */
package com.gsafety.gemp.wuhanncov.specification;

import com.gsafety.gemp.wuhanncov.dao.po.ChinaCountStatisticsHistoryPO;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.jpa.domain.Specification;

/**
 * 该类用于爬虫历史数据分析
 *
 * @author wangwenhai
 *
 * @since 2020年2月13日 下午2:35:24
 *
 */
public class CountHistorySpecifications {

	/**
	 * 更新时间
	 */
	public static final String FIELD_UPDATE_TIME = "updateTime";

	/**
	 *
	 */
	public static final String FIELD_AREA_NAME = "areaName";




	/**
	 *
	 */
	private CountHistorySpecifications() {
		super();
	}

	/**
	 * 
	 * areaNameEqual
	 *
	 * @author wangwenhai
	 * 
	 * @since 2020年2月13日 下午2:39:16
	 *
	 * @param areaName
	 * @return
	 */
	public static Specification<ChinaCountStatisticsHistoryPO> areaNameEqual(String areaName) {

		return (root, query, builer) ->{
			if(StringUtils.isEmpty(areaName)){
				return null;
			}
			return builer.like(root.get(FIELD_AREA_NAME), "%" + areaName + "%");
		};
	}

	/**
	 * 
	 * updateTimeBetween
	 *
	 * @author wangwenhai
	 * 
	 * @since 2020年2月13日 下午2:39:19
	 *
	 * @param searchDate
	 * @return
	 */
	public static Specification<ChinaCountStatisticsHistoryPO> updateTimeBetween(String searchDate) {

		return (root, query, builer) ->{
			if(StringUtils.isEmpty(searchDate)){
				return null;
			}
			String startTime=searchDate+" 00:00:00";
			String endTime=searchDate+" 23:59:59";
			return builer.between(root.get(FIELD_UPDATE_TIME).as(String.class), startTime,endTime);
		};
	}



}
