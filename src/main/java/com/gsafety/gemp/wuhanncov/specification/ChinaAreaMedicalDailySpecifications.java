/**
 * 
 */
package com.gsafety.gemp.wuhanncov.specification;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.jpa.domain.Specification;

import com.gsafety.gemp.wuhanncov.dao.po.ChinaAreaMedicalDailyPO;

/**
 * 各地区医院情况日报Specifications
 *
 * @author yangbo
 *
 * @since 2020年2月9日 上午2:52:55
 *
 */
public class ChinaAreaMedicalDailySpecifications {

	/**
	 * 
	 */
	public static final String FIELD_AREA_CODE = "areaCode";

	/**
	 * 
	 */
	public static final String FIELD_STAS_DATE = "stasDate";

	/**
	 *
	 */
	public static final String FIELD_AREA_NAME = "areaName";

	/**
	 * 
	 */
	private ChinaAreaMedicalDailySpecifications() {
		super();
	}

	/**
	 * 
	 * areaCodeIs
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月8日 下午10:20:24
	 *
	 * @param areaCode
	 * @return
	 */
	public static Specification<ChinaAreaMedicalDailyPO> areaCodeIs(String areaCode) {
		return (root, query, builder) -> builder.equal(root.get(FIELD_AREA_CODE), areaCode);
	}

	/**
	 * 
	 * stasDateBetween
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月9日 上午3:37:08
	 *
	 * @param from
	 * @param to
	 * @return
	 */
	public static Specification<ChinaAreaMedicalDailyPO> stasDateBetween(String from, String to) {
		return (root, query, builder) -> builder.between(root.get(FIELD_STAS_DATE), from, to);
	}

	public static Specification<ChinaAreaMedicalDailyPO> areaNameLike(String areaName) {
		return (root, query, builer) ->{
			if(StringUtils.isEmpty(areaName)){
				return null;
			}
			return builer.like(root.get(FIELD_AREA_NAME), "%" + areaName + "%");
		};
	}
}
