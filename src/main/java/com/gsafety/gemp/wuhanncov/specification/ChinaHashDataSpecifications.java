/**
 * 
 */
package com.gsafety.gemp.wuhanncov.specification;

import org.springframework.data.jpa.domain.Specification;

import com.gsafety.gemp.wuhanncov.dao.po.ChinaHashDataPO;

/**
 * 该类用于...，用于实现...等能力。
 *
 * @author yangbo
 *
 * @since 2020年2月8日 下午10:18:33
 *
 */
public class ChinaHashDataSpecifications {

	/**
	 * 
	 */
	public static final String FIELD_AREA_CODE = "areaCode";

	/**
	 * 
	 */
	private ChinaHashDataSpecifications() {
		super();
	}

	/**
	 * 
	 * areaCodeIs
	 *
	 * @author yangbo
	 * 
	 * @since 2020年2月8日 下午10:20:24
	 *
	 * @param areaCode
	 * @return
	 */
	public static Specification<ChinaHashDataPO> areaCodeIs(String areaCode) {
		return (root, query, builder) -> builder.equal(root.get(FIELD_AREA_CODE), areaCode);
	}
}
