package com.gsafety.gemp.wuhanncov.specification;

import com.gsafety.gemp.wuhanncov.dao.po.ReportCommunityStatDailyPO;
import org.springframework.data.jpa.domain.Specification;

/**
 * @author dusiwei
 */
public class ReportCommunityStatDailySpecifications {

    private ReportCommunityStatDailySpecifications() {
        super();
    }

    public static final String FIELD_AREA_CODE = "areaCode";
    public static final String FIELD_PARENT_AREA_CODE = "parentAreaCode";
    public static final String FIELD_STAT_DATE = "statDate";

    /**
     * areaCodeEqual
     * @param areaCode
     * @return
     */
    public static Specification<ReportCommunityStatDailyPO> areaCodeEqual(String areaCode) {
        return (root, query, builder) -> builder.equal(root.get(FIELD_AREA_CODE), areaCode);
    }

    /**
     * parentAreaCodeEqual
     * @param parentAreaCode
     * @return
     */
    public static Specification<ReportCommunityStatDailyPO> parentAreaCodeEqual(String parentAreaCode) {
        return (root, query, builder) -> builder.equal(root.get(FIELD_PARENT_AREA_CODE), parentAreaCode);
    }

    /**
     * statDateEqual
     * @param statDate
     * @return
     */
    public static Specification<ReportCommunityStatDailyPO> statDateEqual(String statDate) {
        return (root, query, builder) -> builder.equal(root.get(FIELD_STAT_DATE), statDate);
    }

}
