/**
 * 
 */
package com.gsafety.gemp.wuhanncov.specification;


import com.gsafety.gemp.wuhanncov.dao.po.LocationDimPO;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.jpa.domain.Specification;

/**
 * 该类用于搜索行政区划父节点名称查询
 *
 * @author wangwenhai
 *
 * @since 2020年2月15日 上午11:18:24
 *
 */
public class LocationDimSpecifications {

	/**
	 * 更新时间
	 */
	public static final String FIELD_AREA_NO = "areaNo";

	/**
	 * 地区名称
	 */
	public static final String FIELD_AREA_NAME = "areaName";

	/**
	 * 父级节点
	 */
	public static final String FIELD_PARENT_NO = "parentNo";



	/**
	 *
	 */
	private LocationDimSpecifications() {
		super();
	}

	/**
	 * 
	 * areaNameEqual
	 *
	 * @author wangwenhai
	 * 
	 * @since 2020年2月15日 上午11:20:16
	 *
	 * @param searchAreaName
	 * @return
	 */
	public static Specification<LocationDimPO> areaNameEqual(String searchAreaName) {

		return (root, query, builer) ->{
			if(StringUtils.isEmpty(searchAreaName)){
				return null;
			}
			return builer.like(root.get(FIELD_AREA_NAME), "%" + searchAreaName + "%");
		};
	}


	/**
	 *
	 * areaNoNotEqual,搜索条件
	 *
	 * @author wangwenhai
	 *
	 * @since 2020年2月15日 上午11:20:16
	 *
	 * @param areaNo
	 * @return
	 */
	public static Specification<LocationDimPO> areaNoNotEqual(String areaNo) {

		return (root, query, builer) ->{
			if(StringUtils.isEmpty(areaNo)){
				return null;
			}
			return builer.notEqual(root.get(FIELD_AREA_NO),areaNo);
		};
	}


	/**
	 * areaNoEqual 地区编码相同条件
	 *
	 *
	 * @author wangwenhai
	 *
	 * @since 2020年2月22日 下午8:26:16
	 *
	 * @param areaNo
	 * @return
	 */
	public static Specification<LocationDimPO> areaNoEqual(String areaNo) {

		return (root, query, builer) ->{
			if(StringUtils.isEmpty(areaNo)){
				return null;
			}
			return builer.equal(root.get(FIELD_AREA_NO),areaNo);
		};
	}

	/**
	 * parentNoEqual 地区编码父级节点相同条件
	 *
	 *
	 * @author wangwenhai
	 *
	 * @since 2020年2月22日 下午8:26:16
	 *
	 * @param areaNo
	 * @return
	 */
	public static Specification<LocationDimPO> parentNoEqual(String areaNo) {

		return (root, query, builer) ->{
			if(StringUtils.isEmpty(areaNo)){
				return null;
			}
			return builer.equal(root.get(FIELD_PARENT_NO),areaNo);
		};
	}


}
