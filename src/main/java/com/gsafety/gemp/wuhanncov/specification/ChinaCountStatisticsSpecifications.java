/**
 * 
 */
package com.gsafety.gemp.wuhanncov.specification;

import com.gsafety.gemp.wuhanncov.dao.po.ChinaCountStatisticsPO;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.jpa.domain.Specification;

/**
 * 该类用于爬虫实时数据分析
 *
 * @author wuhongling
 *
 * @since 2020年2月13日 下午5:35:24
 *
 */
public class ChinaCountStatisticsSpecifications {

	/**
	 *
	 */
	public static final String FIELD_AREANAME = "areaName";


	/**
	 *
	 */
	private ChinaCountStatisticsSpecifications() {
		super();
	}

	/**
	 * 
	 * areaNameEqual
	 *
	 * @author wuhongling
	 * 
	 * @since 2020年2月13日 下午5:39:16
	 *
	 * @param areaName
	 * @return
	 */
	public static Specification<ChinaCountStatisticsPO> areaNameEqual(String areaName) {

		return (root, query, builer) ->{
			if(StringUtils.isEmpty(areaName))
				return null;
			return builer.like(root.get(FIELD_AREANAME), "%" + areaName + "%");
		};
	}

}
