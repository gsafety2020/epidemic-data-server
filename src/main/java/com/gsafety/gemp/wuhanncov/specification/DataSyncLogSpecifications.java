package com.gsafety.gemp.wuhanncov.specification;

import com.gsafety.gemp.wuhanncov.dao.po.DataSyncLogPO;
import org.springframework.data.jpa.domain.Specification;

import java.util.Date;

/**
 * 数据同步日志Specifications
 *
 * @author xiongjinhui
 *
 * @since 2020年2月13日 上午10:52:55
 *
 */
public class DataSyncLogSpecifications {

    public static final String FIELD_START_DATE = "startTime";



    private DataSyncLogSpecifications(){
        super();
    }


    /**
     * 更新时间区间条件过滤
     *
     * @param startTime
     * @param endTime
     * @return
     */
    public static Specification<DataSyncLogPO> timeBetween(Date startTime, Date endTime) {
        return Specification.where(timeMore(startTime)).and(timeLess(endTime));
    }

    /**
     * 查询更新时间小于endTime
     *
     * @param endTime
     * @return
     */
    private static Specification<DataSyncLogPO> timeLess(Date endTime) {
        if (null == endTime) {
            return null;
        }
        return (root, query, cb) -> cb.lessThanOrEqualTo(root.get(FIELD_START_DATE), endTime);
    }

    /**
     * 查询更新时间大于startTime
     *
     * @param startTime
     * @return
     */
    private static Specification<DataSyncLogPO> timeMore(Date startTime) {
        if (null == startTime) {
            return null;
        }
        return (root, query, cb) -> cb.greaterThanOrEqualTo(root.get(FIELD_START_DATE), startTime);
    }
}
