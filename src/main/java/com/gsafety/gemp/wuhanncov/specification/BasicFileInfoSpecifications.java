package com.gsafety.gemp.wuhanncov.specification;

import com.gsafety.gemp.wuhanncov.dao.po.BasicFileInfoPO;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.jpa.domain.Specification;

/**
 * 疫情文件Specification
 *
 * @author xiongjinhui
 *
 * @since 2020年2月18日 16:02:45
 */
public class BasicFileInfoSpecifications {
    private BasicFileInfoSpecifications(){}

    /**
     *  地区名称
     */
    public static final String FIELD_AREA_NAME = "areaName";

    /**
     *  文件名称
     */
    public static final String FIELD_FILE_NAME = "fileName";

    /**
     *  文件状态
     */
    public static final String FIELD_FILE_STATUS = "status";

    /**
     *  根据区域名称模糊查询
     */
    public static Specification<BasicFileInfoPO> areaNameLike(String value) {
        if(StringUtils.isBlank(value)){
            return null;
        }
        return like(FIELD_AREA_NAME, value);
    }

    /**
     *  根据文件名称模糊查询
     */
    public static Specification<BasicFileInfoPO> fileNameLike(String value) {
        if(StringUtils.isEmpty(value)){
            return null;
        }
        return like(FIELD_FILE_NAME, value);
    }

    /**
     *  该方法用于实现查询疫情文件状态（-1：删除，5：正常）
     */
    public static Specification<BasicFileInfoPO> statusEqual(String status) {
        if (StringUtils.isEmpty(status)) {
            return null;
        }
        return (root, query, builder) -> builder.equal(root.get(FIELD_FILE_STATUS), status);
    }

    /**
     * 该方法用于实现按属性名称字段模糊查询的功能
     *
     * @param fieldName 属性名称
     * @param value     属性值
     * @return
     * @author xiongjinhui
     * @since 2020年2月18日 16:02:45
     */
    public static Specification<BasicFileInfoPO> like(String fieldName, String value) {
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        return (root, query, cb) -> cb.like(root.get(fieldName), pattern(value.trim()), '\\');
    }

    public static String pattern(String string) {
        StringBuilder sb = new StringBuilder();
        sb.append("%");
        sb.append(StringUtils.replace(StringUtils.replace(string, "%", "\\%"), "_", "\\_"));
        sb.append("%");
        return sb.toString();
    }
}
